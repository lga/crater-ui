import { describe, expect, it } from 'vitest';

import { calculerNombrePersonnesNourriesSelonHectares } from './thematique-terres-agricoles.js';

describe('Tests thématique Terres Agricoles', () => {
    it('Test de la fonction calculerNombrePersonnesNourriesSelonHectares', () => {
        expect(calculerNombrePersonnesNourriesSelonHectares(null, null)).toEqual(null);
        expect(calculerNombrePersonnesNourriesSelonHectares(30, 4000)).toEqual(75);
        expect(calculerNombrePersonnesNourriesSelonHectares(-30, 4000)).toEqual(null);
    });
});
