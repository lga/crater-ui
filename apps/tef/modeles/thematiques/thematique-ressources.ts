import { formaterNombreSelonValeurString } from '@lga/base';
import { INDICATEURS, type ValeurIndicateur } from '@lga/indicateurs';

import { anneeFinaleConstatEau, anneeInitialeConstatEau } from '../constats/constats-ressources.js';
import type { DonneesApi } from '../fonctions-utils.js';
import { calculerTendanceEvolutionPrelevementEauPourcent } from '../fonctions-utils.js';
import type { ThematiqueModele } from './ThematiqueModele.js';

export interface ThematiqueRessourcesModele extends ThematiqueModele {
    quantitePesticides: ValeurIndicateur;
    tendanceEvolutionPrelevementsEau: ValeurIndicateur;
}

export function creerThematiqueRessources(donneesApi: DonneesApi): ThematiqueRessourcesModele {
    const anneePesticides = 2020;
    const quantiteSubstanceAvecDUKg =
        donneesApi.diagnosticApi.pesticides.indicateursParAnnees.find((i) => i.annee === anneePesticides)?.quantiteSubstanceAvecDUKg ?? null;
    const quantiteSubstanceSansDUKg =
        donneesApi.diagnosticApi.pesticides.indicateursParAnnees.find((i) => i.annee === anneePesticides)?.quantiteSubstanceSansDUKg ?? null;
    const quantiteSubstanceTonnes =
        quantiteSubstanceAvecDUKg && quantiteSubstanceSansDUKg ? (quantiteSubstanceAvecDUKg + quantiteSubstanceSansDUKg) / 1000 : null;

    return {
        idTheme: 'ressources',
        quantitePesticides: {
            chiffre: formaterNombreSelonValeurString(quantiteSubstanceTonnes),
            chiffreComplement: 'tonnes de pesticides achetés en ' + anneePesticides.toString(),
            unite: '',
            indicateur: INDICATEURS.qsa
        },
        tendanceEvolutionPrelevementsEau: {
            chiffre: formaterNombreSelonValeurString(
                calculerTendanceEvolutionPrelevementEauPourcent(
                    donneesApi.diagnosticApi.eau.irrigation.indicateursParAnnees,
                    anneeInitialeConstatEau,
                    anneeFinaleConstatEau
                ),
                true
            ),
            unite: '%',
            chiffreComplement: `de prélèvements d'eau en tendantiel entre ${anneeInitialeConstatEau} et ${anneeFinaleConstatEau}`,
            indicateur: INDICATEURS.prelevementsIrrigation
        }
    };
}
