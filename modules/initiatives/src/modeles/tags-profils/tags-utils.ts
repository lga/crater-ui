import { valeursCodeProfilAgriculture, valeursCodeProfilCollectivite } from '@lga/specification-api';

import type { CodeProfil } from './Codes';
import type { JokerProfil } from './Jokers';
import { DEFINITIONS_JOKERS_PROFILS } from './jokers-definition';
import type { TagProfil } from './Tags';

const VALEURS_CODES_PROFILS = [...valeursCodeProfilAgriculture, ...valeursCodeProfilCollectivite];

export function convertirTagsEnCodes(tags: TagProfil[]): CodeProfil[] {
    const listeCodeProfils: CodeProfil[] = [];
    tags.forEach((tag) => {
        if (tag in DEFINITIONS_JOKERS_PROFILS) {
            listeCodeProfils.push(...DEFINITIONS_JOKERS_PROFILS[tag as JokerProfil]);
        } else if (VALEURS_CODES_PROFILS.includes(tag as CodeProfil)) {
            listeCodeProfils.push(tag as CodeProfil);
        } else {
            throw new Error(`Le tag ${tag} ne correspond ni à un code de profil ni à un joker`);
        }
    });
    return listeCodeProfils;
}
