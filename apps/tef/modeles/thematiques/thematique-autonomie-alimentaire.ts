import { type DefinitionIndicateur, INDICATEURS } from '@lga/indicateurs';
import type { CodeGroupeCultureApi, CodeOtexMajoritaire5PostesApi } from '@lga/specification-api/build/specification/api/index.js';

import { classerBesoinsAssiette, type DonneesApi } from '../fonctions-utils.js';
import type { ThematiqueModele } from './ThematiqueModele.js';

export interface ThematiqueAutonomieAlimentaireModele extends ThematiqueModele {
    otex: {
        indicateur: DefinitionIndicateur;
        phrase: string;
    };
    cultureLaPlusDeficitaire: {
        codeGroupeCulture: CodeGroupeCultureApi;
        nomGroupeCulture: string;
        tauxAdequationBrutPourcent: number;
    };
}

const CODES_CULTURES_SUGGERES_POUR_RELOCALISATION: CodeGroupeCultureApi[] = ['CER', 'FLC', 'OLP'];
export function creerThematiqueAutonomieAlimentaire(donneesApi: DonneesApi): ThematiqueAutonomieAlimentaireModele {
    return {
        idTheme: 'autonomie-alimentaire',
        otex: {
            indicateur: INDICATEURS.otexMajoritaire5Postes,
            phrase: OTEX_5_POSTES.find((i) => i.code === donneesApi.diagnosticApi.codeOtexMajoritaire5Postes)?.libelle ?? ''
        },
        cultureLaPlusDeficitaire: classerBesoinsAssiette(
            donneesApi.diagnosticApi.productionsBesoins.besoinsAssietteActuelle.besoinsParGroupeCulture
        ).manques.filter((i) => CODES_CULTURES_SUGGERES_POUR_RELOCALISATION.includes(i.codeGroupeCulture))[0]
    };
}

interface OtexMajoritaire5Postes {
    code: CodeOtexMajoritaire5PostesApi;
    libelle: string;
}
const OTEX_5_POSTES: OtexMajoritaire5Postes[] = [
    {
        code: 'GRANDES_CULTURES',
        libelle: 'Grandes Cultures'
    },
    {
        code: 'VITICULTURE',
        libelle: 'Viticulture'
    },
    {
        code: 'FRUITS_ET_LEGUMES',
        libelle: 'Maraîchage Horticulture ou Fruits'
    },
    {
        code: 'ELEVAGE',
        libelle: 'Élevage'
    },
    {
        code: 'POLYCULTURE_POLYELEVAGE',
        libelle: 'Polyculture Polyélevage'
    },
    {
        code: 'NON_DEFINIE',
        libelle: 'Non définie'
    },
    {
        code: 'SANS_OTEX_MAJORITAIRE',
        libelle: 'Sans spécialisation majoritaire'
    }
];
