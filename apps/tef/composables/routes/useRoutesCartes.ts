import { type RouteLocationRaw, type RouteParams, useRoute } from 'vue-router/auto';

export function useRouteCartes(idIndicateur?: string, echelleTerritoriale?: string): RouteLocationRaw<'cartes-idIndicateur-echelleTerritoriale'> {
    const route: RouteLocationRaw<'cartes-idIndicateur-echelleTerritoriale'> = {
        name: 'cartes-idIndicateur-echelleTerritoriale',
        // eslint-disable-next-line @typescript-eslint/prefer-nullish-coalescing
        params: { idIndicateur: idIndicateur, echelleTerritoriale: echelleTerritoriale }
    };
    return route;
}

export function useRouteParamsCartes(): RouteParams<'cartes-idIndicateur-echelleTerritoriale'> {
    return useRoute().params as RouteParams<'cartes-idIndicateur-echelleTerritoriale'>;
}
