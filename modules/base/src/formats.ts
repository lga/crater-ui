import { arrondirANDecimales, calculerPuissance } from './calculs';

export function formaterNombreEnEntierString(nombre: number | null | undefined): string {
    return formaterNombreEnNDecimalesString(nombre, 0);
}

export function formaterNombreEnNDecimalesString(nombre: number | null | undefined, nbDecimales: number): string {
    if (nombre === null || nombre === undefined || isNaN(nombre)) return '-';

    return arrondirANDecimales(nombre, nbDecimales).toLocaleString('fr-FR');
}

export function formaterNombreSelonValeurString(nombre: number | null | undefined, prefixePlusSiPositif = false): string {
    if (nombre === null || nombre === undefined || isNaN(nombre)) {
        return '-';
    }
    let prefixe = '';
    if (prefixePlusSiPositif && nombre > 0) prefixe = '+';

    let suffixe = '';
    if (nombre >= 1e9) {
        nombre = nombre / 1e9;
        suffixe = ' milliards';
    } else if (nombre >= 1e6) {
        nombre = nombre / 1e6;
        suffixe = ' millions';
    }
    nombre = arrondirANDecimales(nombre, 1 - calculerPuissance(nombre));
    return prefixe + nombre.toLocaleString('fr-FR') + suffixe;
}

export function calculerEvolutionPourcent(nombreInitial: number | null | undefined, nombreFinal: number | null | undefined): number | null {
    if (
        nombreInitial === null ||
        nombreInitial === undefined ||
        isNaN(nombreInitial) ||
        nombreFinal === null ||
        nombreFinal === undefined ||
        isNaN(nombreFinal)
    ) {
        return null;
    } else return arrondirANDecimales(100 * (nombreFinal / nombreInitial - 1), 2);
}

export function formaterEvolutionPourcentString(evolutionPourcent: number | null | undefined): string {
    if (evolutionPourcent === null || evolutionPourcent === undefined || isNaN(evolutionPourcent)) {
        return '-%';
    } else if (evolutionPourcent < 100) {
        return (evolutionPourcent >= 0 ? '+' : '') + formaterNombreSelonValeurString(evolutionPourcent) + '%';
    } else {
        return 'x' + formaterNombreSelonValeurString(evolutionPourcent / 100 + 1);
    }
}

export function calculerEtFormaterEvolutionString(nombreInitial: number | null | undefined, nombreFinal: number | null | undefined): string {
    return formaterEvolutionPourcentString(calculerEvolutionPourcent(nombreInitial, nombreFinal));
}

export function calculerEtFormaterPourcentageString(numerateur: number | null, denominateur: number | null): string {
    if (numerateur === null || denominateur === null || denominateur === 0) {
        return '-';
    } else {
        return formaterNombreEnEntierString((numerateur / denominateur) * 100);
    }
}

export function supprimerEspacesRedondantsEtRetoursLignes(texte: string): string {
    //remplace espaces et retours chariots multiple en espaces simples
    return texte.replace(/(\s|\n)+/g, ' ').trim();
}

export function toJson(objet: object) {
    return JSON.stringify(
        objet,
        function (k, v) {
            // eslint-disable-next-line @typescript-eslint/no-unsafe-return
            return v === undefined ? 'undefined' : v;
        },
        2
    );
}

export function supprimerSlashFinal(url: string): string {
    return url.endsWith('/') ? url.slice(0, -1) : url;
}

export function rendreInitialeMajuscule(string: string): string {
    if (!string) return string;
    return string.charAt(0).toUpperCase() + string.slice(1);
}
