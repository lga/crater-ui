import './TemplatePage.js';

import { CSS_BREAKPOINT_MAX_WIDTH_DESKTOP_PETIT } from '@lga/design-system/build/styles/styles-breakpoints.js';
import { css, html, LitElement, unsafeCSS } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { unsafeSVG } from 'lit/directives/unsafe-svg.js';

import { STYLES_CRATER } from '../pages-styles';

declare global {
    interface HTMLElementTagNameMap {
        'c-template-page-accueil': TemplatePageAccueil;
    }
}

@customElement('c-template-page-accueil')
export class TemplatePageAccueil extends LitElement {
    static styles = [
        STYLES_CRATER,
        css`
            main {
                padding: calc(2 * var(--dsem));
                padding-top: calc(8 * var(--dsem));
                background-color: var(--couleur-fond);
                position: relative;
                z-index: 0;
            }

            #background {
                position: absolute;
                z-index: -1;
                top: 0;
                left: 0;
                width: 100%;
                height: 550px;
                fill: var(--couleur-fond-accueil, var(--couleur-primaire, #ffffff));
            }

            @media screen and (max-width: ${unsafeCSS(CSS_BREAKPOINT_MAX_WIDTH_DESKTOP_PETIT + 'px')}) {
                main {
                    padding-top: calc(6 * var(--dsem));
                }

                #background {
                    height: 600px;
                }
            }
        `
    ];

    private background = `<path d="M-0.000244141 0H1440V398.605C1440 503.026 1359.67 589.873 1255.56 597.998L-0.000244141 696V0Z"/>`;

    @property()
    idItemActifMenuPrincipal = '';

    @property()
    idElementCibleScroll = '';

    render() {
        return html`
            <c-template-page
                idItemActifMenuPrincipal=${this.idItemActifMenuPrincipal}
                idElementCibleScroll=${this.idElementCibleScroll}
                desactiverBoutonSommaire
            >
                <main slot="contenu">
                    <svg id="background" width="100%" height="100%" viewBox="0 0 1440 696" preserveAspectRatio="xMaxYMax slice">
                        ${unsafeSVG(this.background)}
                    </svg>
                    <slot name="contenu"></slot>
                </main>
            </c-template-page>
        `;
    }
}
