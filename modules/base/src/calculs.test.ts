import { describe, expect, it } from 'vitest';

import {
    arrondirANDecimales,
    calculerEvolutionParPasDeTemps,
    calculerIndiceClasse,
    calculerPartsArray,
    calculerPuissance,
    calculerQualificatifEvolution,
    calculerSigneNombre,
    calculerValeurAbsolue,
    calculerValeurEntiereNonNulle,
    clip,
    coordonneesPolairesVersCartesiennes,
    QualificatifEvolution,
    regressionLineaire,
    Signe,
    sommeArray
} from './calculs';

describe('Tests des fonctions de calculs.ts', () => {
    it('Fonction arrondirANDecimales', () => {
        expect(arrondirANDecimales(123.456, -1)).toEqual(120);
        expect(arrondirANDecimales(123.456, 0)).toEqual(123);
        expect(arrondirANDecimales(123.5, 0)).toEqual(124);
        expect(arrondirANDecimales(123.456, 1)).toEqual(123.5);
        expect(arrondirANDecimales(123.456, 2)).toEqual(123.46);
    });

    it('Fonction calculerValeurEntiereNonNulle', () => {
        expect(calculerValeurEntiereNonNulle(null)).toEqual(0);
        expect(calculerValeurEntiereNonNulle(10)).toEqual(10);
        expect(calculerValeurEntiereNonNulle(10.1)).toEqual(10);
        expect(calculerValeurEntiereNonNulle(9.9)).toEqual(10);
    });

    it('Fonction clip', () => {
        expect(clip(5, 0, 10)).toEqual(5);
        expect(clip(-1, 0, 10)).toEqual(0);
        expect(clip(11, 0, 10)).toEqual(10);
    });

    it('Fonction sommeArray', () => {
        expect(sommeArray([])).toEqual(null);
        expect(sommeArray([null])).toEqual(null);
        expect(sommeArray([null, 0])).toEqual(0);
        expect(sommeArray([null, 1, 2])).toEqual(3);
        expect(sommeArray([1, 2, 3])).toEqual(6);
    });

    it('Fonction calculerPartsArray', () => {
        expect(calculerPartsArray([])).toEqual([]);
        expect(calculerPartsArray([1, 9, 90])).toEqual([1, 9, 90]);
        expect(calculerPartsArray([10, 90, 900])).toEqual([1, 9, 90]);
        expect(calculerPartsArray([0, 4, 5, 1, 8.96])).toEqual([0, 21, 26, 5, 48]);
        expect(calculerPartsArray([0, 4, 5, 1, 8.96], 1)).toEqual([0, 21.1, 26.4, 5.3, 47.2]);
    });

    it('Fonction calculerValeurAbsolue', () => {
        expect(calculerValeurAbsolue(null)).toEqual(null);
        expect(calculerValeurAbsolue(NaN)).toEqual(NaN);
        expect(calculerValeurAbsolue(undefined)).toEqual(undefined);
        expect(calculerValeurAbsolue(100.1)).toEqual(100.1);
        expect(calculerValeurAbsolue(-100.1)).toEqual(100.1);
    });
    it('Fonction calculerPuissanceNombre', () => {
        expect(calculerPuissance(0)).toEqual(0);
        expect(calculerPuissance(0.01)).toEqual(-2);
        expect(calculerPuissance(0.09)).toEqual(-2);
        expect(calculerPuissance(-0.01)).toEqual(-2);
        expect(calculerPuissance(-0.09)).toEqual(-2);
        expect(calculerPuissance(0.1)).toEqual(-1);
        expect(calculerPuissance(0.9)).toEqual(-1);
        expect(calculerPuissance(-0.1)).toEqual(-1);
        expect(calculerPuissance(-0.9)).toEqual(-1);
        expect(calculerPuissance(1.1)).toEqual(0);
        expect(calculerPuissance(-1.1)).toEqual(0);
        expect(calculerPuissance(11.1)).toEqual(1);
        expect(calculerPuissance(-11.1)).toEqual(1);
        expect(calculerPuissance(111.1)).toEqual(2);
        expect(calculerPuissance(-111.1)).toEqual(2);
    });

    it('Fonction calculerSigneNombre', () => {
        expect(calculerSigneNombre(null)).toEqual(Signe.NULL);
        expect(calculerSigneNombre(0)).toEqual(Signe.ZERO);
        expect(calculerSigneNombre(1)).toEqual(Signe.POSITIF);
        expect(calculerSigneNombre(-1)).toEqual(Signe.NEGATIF);
    });

    it('Fonction calculerQualificatifEvolution', () => {
        expect(calculerQualificatifEvolution(Signe.NULL)).toEqual(QualificatifEvolution.neutre);
        expect(calculerQualificatifEvolution(Signe.ZERO)).toEqual(QualificatifEvolution.neutre);
        expect(calculerQualificatifEvolution(Signe.POSITIF)).toEqual(QualificatifEvolution.progres);
        expect(calculerQualificatifEvolution(Signe.NEGATIF)).toEqual(QualificatifEvolution.declin);
        expect(calculerQualificatifEvolution(Signe.NULL, 'negative')).toEqual(QualificatifEvolution.neutre);
        expect(calculerQualificatifEvolution(Signe.ZERO, 'negative')).toEqual(QualificatifEvolution.neutre);
        expect(calculerQualificatifEvolution(Signe.POSITIF, 'negative')).toEqual(QualificatifEvolution.declin);
        expect(calculerQualificatifEvolution(Signe.NEGATIF, 'negative')).toEqual(QualificatifEvolution.progres);
    });

    it('Fonction calculerEvolutionParPasDeTemps', () => {
        expect(calculerEvolutionParPasDeTemps(null)).toEqual({ nombre: null, periode: null });
        expect(calculerEvolutionParPasDeTemps(0)).toEqual({ nombre: 0, periode: 'tous les dix ans' });
        expect(calculerEvolutionParPasDeTemps(1)).toEqual({ nombre: 10, periode: 'tous les dix ans' });
        expect(calculerEvolutionParPasDeTemps(-1)).toEqual({ nombre: -10, periode: 'tous les dix ans' });
        expect(calculerEvolutionParPasDeTemps(1 * 2)).toEqual({ nombre: 10, periode: 'tous les cinq ans' });
        expect(calculerEvolutionParPasDeTemps(1 * 2 * 5)).toEqual({ nombre: 10, periode: 'par an' });
        expect(calculerEvolutionParPasDeTemps(1 * 2 * 5 * 12)).toEqual({ nombre: 10, periode: 'par mois' });
        expect(calculerEvolutionParPasDeTemps(1 * 2 * 5 * 52)).toEqual({ nombre: 10, periode: 'par semaine' });
        expect(calculerEvolutionParPasDeTemps(-1 * 2 * 5 * 52)).toEqual({ nombre: -10, periode: 'par semaine' });
        expect(calculerEvolutionParPasDeTemps(1 * 2 * 5 * 52 * 7)).toEqual({ nombre: 10, periode: 'par jour' });
        expect(calculerEvolutionParPasDeTemps(1 * 2 * 5 * 52 * 7 * 24)).toEqual({ nombre: 10, periode: 'par heure' });
        expect(calculerEvolutionParPasDeTemps(1 * 2 * 5 * 52 * 7 * 24 * 60)).toEqual({ nombre: 10, periode: 'par minute' });
        expect(calculerEvolutionParPasDeTemps(1 * 2 * 5 * 52 * 7 * 24 * 60 * 60)).toEqual({ nombre: 10, periode: 'par seconde' });
    });

    it('Fonction regressionLineaire', () => {
        const retourNull = { a: null, b: null, r2: null };
        expect(regressionLineaire([], [])).toMatchObject(retourNull);
        expect(regressionLineaire([1], [1])).toMatchObject(retourNull);
        expect(regressionLineaire([1], [2, 3])).toMatchObject(retourNull);
        expect(regressionLineaire([1, null], [2, 3])).toMatchObject(retourNull);
        expect(regressionLineaire([0, 1], [2, 4])).toMatchObject({ a: 2, b: 2, r2: 1 });
        expect(regressionLineaire([0, 1], [2, 4]).fn(0.5)).toEqual(3);
        expect(regressionLineaire([0, 1, null], [2, 4, 6])).toMatchObject({ a: 2, b: 2, r2: 1 });
    });

    it('Fonction coordonneesPolairesVersCartesiennes', () => {
        expect(coordonneesPolairesVersCartesiennes(10, 0)).toEqual({ x: 0, y: 10 });
        expect(coordonneesPolairesVersCartesiennes(10, Math.PI / 2)).toEqual({ x: 10, y: 0 });
        expect(coordonneesPolairesVersCartesiennes(20, Math.PI)).toEqual({ x: 0, y: -20 });
    });

    it('Test calculerIndiceClasse', () => {
        expect(calculerIndiceClasse(null, 30, 5)).toEqual(null);
        expect(calculerIndiceClasse(-100, 30, 5)).toEqual(0);
        expect(calculerIndiceClasse(-1, 30, 5)).toEqual(0);
        expect(calculerIndiceClasse(0, 30, 5)).toEqual(0);
        expect(calculerIndiceClasse(1, 30, 5)).toEqual(0);
        expect(calculerIndiceClasse(5.999, 30, 5)).toEqual(0);
        expect(calculerIndiceClasse(6, 30, 5)).toEqual(1);
        expect(calculerIndiceClasse(23.99, 30, 5)).toEqual(3);
        expect(calculerIndiceClasse(24, 30, 5)).toEqual(4);
        expect(calculerIndiceClasse(30, 30, 5)).toEqual(4);
        expect(calculerIndiceClasse(31, 30, 5)).toEqual(4);
        expect(calculerIndiceClasse(100, 30, 5)).toEqual(4);
        expect(calculerIndiceClasse(22, 88, 8)).toEqual(2);
    });
});
