import { formaterNombreSelonValeurString, htmlstring, type ImageModele } from '@lga/base';
import { INDICATEURS } from '@lga/indicateurs';
import { recupererListe3InitiativesParConstats } from '@lga/initiatives';
import { CategorieTerritoire } from '@lga/territoires';

import type { DonneesApi } from '../fonctions-utils.js';
import { calculerLibelleSourcesIndicateur, creerLienExterneHtmlBrut } from '../liens-utils.js';
import type { ConstatModele } from './ConstatModele';
import type {
    InfographieCartePrecariteAlimentaireModele,
    InfographieImageChiffreCleModele,
    InformationsMethodologieInfographieModele
} from './InfographieModele.js';

export const IMAGE_RAYON_FRUITS: ImageModele = {
    url: `/images/constats/titres/rayon-fruit-supermarche-800x861.jpg`,
    descriptionAlt: 'Rayon de fruits dans un supermarché',
    resolutionY: 861,
    resolutionX: 800
};

export const informationsMethodologieCartePrecariteAlimentaire: InformationsMethodologieInfographieModele = {
    titre: 'Indice de risque de précarité alimentaire',
    descriptif: htmlstring`Cet indicateur, créé dans le cadre du projet ${creerLienExterneHtmlBrut('https://obso-alim.org', 'Obsoalim34')} évalue les risques de précarité alimentaire au travers de quatre dimensions : 
    monétaire, socio-économique, mobilité, niveau d’information. 
    Il a été construit à partir d'une revue de la littérature scientifique sur les facteurs de précarité alimentaire. 
    Il combine des données INSEE en libre accès et actualisées régulièrement pour garantir un accès libre à tous-tes.`,
    urlComplementInformation: 'https://diagnostic.obso-alim.org',
    libelleLienComplementInformation: "Plus d'informations sur diagnostic.obso-alim.org",
    libelleBouton: 'Méthode de calcul'
};

export function creerConstatPrecariteAlimentaire(
    donneesApi: DonneesApi
): ConstatModele<InfographieCartePrecariteAlimentaireModele | InfographieImageChiffreCleModele> {
    const codeNiveauRisquePrecariteAlimentaire = donneesApi.diagnosticApi.consommation.codeNiveauRisquePrecariteAlimentaire;
    const tauxAdequationBrutPourcent = donneesApi.diagnosticApi.productionsBesoins.besoinsAssietteActuelle.tauxAdequationBrutPourcent;

    const categorieTerritoire = CategorieTerritoire.creer(
        donneesApi.territoireApi.categorie,
        donneesApi.territoireApi.sousCategorie
    ).libelleCategorieHtml;

    const estConstatGeneriqueSansCarte = donneesApi.territoireApi.categorie === 'PAYS';

    let messageTitre = 'De plus en plus de personnes ne mangent pas à leur faim en France';
    let messageTexte = 'C’est étonnant alors que la France, 7ᵉ puissance économique mondiale, produit suffisamment pour nourrir tous ses habitants.';
    const changerMessageTexteSiTerritoireExcedentaire = () => {
        if (tauxAdequationBrutPourcent !== null && tauxAdequationBrutPourcent >= 100)
            messageTexte = htmlstring`C’est étonnant alors que votre ${categorieTerritoire} pourrait produire suffisamment pour nourrir tous ses habitants.`;
    };
    if (donneesApi.territoireApi.categorie === 'PAYS' || codeNiveauRisquePrecariteAlimentaire === null) {
        //
    } else if (codeNiveauRisquePrecariteAlimentaire === '1_TRES_BAS' || codeNiveauRisquePrecariteAlimentaire === '2_BAS') {
        messageTitre = 'Le risque de précarité alimentaire est relativement faible sur votre territoire.';
        messageTexte = htmlstring`Une situation à surveiller cependant car la précarité alimentaire est en hausse globalement en France et que des disparités au sein de votre ${categorieTerritoire} sont possibles.`;
    } else if (codeNiveauRisquePrecariteAlimentaire === '3_MOYEN') {
        messageTitre = 'Le risque de précarité alimentaire est assez élevé sur votre territoire.';
        changerMessageTexteSiTerritoireExcedentaire();
    } else if (codeNiveauRisquePrecariteAlimentaire === '4_ELEVE') {
        messageTitre = 'Le risque de précarité alimentaire est élevé sur votre territoire.';
        changerMessageTexteSiTerritoireExcedentaire();
    } else {
        messageTitre = 'Le risque de précarité alimentaire est très élevé sur votre territoire.';
        changerMessageTexteSiTerritoireExcedentaire();
    }

    const infographie: InfographieCartePrecariteAlimentaireModele | InfographieImageChiffreCleModele = estConstatGeneriqueSansCarte
        ? {
              nomComposant: 'InfographieImageChiffreCle',
              informationsMethodologie: {
                  titre: "Recours à l'aide alimentaire des Français",
                  descriptif: htmlstring`Ce chiffre est issu du rapport de l’IGAS ${creerLienExterneHtmlBrut('https://igas.gouv.fr/La-lutte-contre-la-precarite-alimentaire-Evolution-du-soutien-public-a-une', 'La lutte contre la précarité alimentaire. Évolution du soutien public à une politique sociale, agricole et de santé publique')}.`,
                  estPerimetreNational: true
              },
              chiffreCle: {
                  chiffre: '5',
                  unite: 'millions',
                  phrase: "de Français ont recours à l'aide alimentaire"
              },
              accroche: 'La faim justifie les moyens ?',
              image: {
                  url: '/images/constats/infographies/distribution-aide-alimentaire-419x455.png',
                  descriptionAlt: 'Personnes participants à une distribution de l’aide alimentaire',
                  resolutionX: 419,
                  resolutionY: 455,
                  position: 'gauche'
              }
          }
        : {
              nomComposant: 'InfographieCartePrecariteAlimentaire',
              informationsMethodologie: informationsMethodologieCartePrecariteAlimentaire,
              accroche: 'La faim est proche ?',
              territoire: donneesApi.territoireApi
          };

    return {
        id: 'precarite-alimentaire',
        idTheme: 'accessibilite',
        titre: {
            libelle: "Tous égaux face à l'alimentation ?",
            image: {
                url: `/images/constats/titres/${estConstatGeneriqueSansCarte ? 'rayon-fruit-supermarche-800x861' : 'distribution-aide-alimentaire-800x861'}.jpg`,
                descriptionAlt: 'Rayon de fruits dans un supermarché',
                resolutionY: 861,
                resolutionX: 800
            }
        },
        message: {
            titre: messageTitre,
            texte: messageTexte,
            texteCourt: estConstatGeneriqueSansCarte
                ? null
                : htmlstring`En France, <strong>8 millions</strong> de personnes sont en situation de <strong>précarité alimentaire</strong>.`,
            question: "Et si on garantissait un droit à l'alimentation pour tous ?",
            initiatives: recupererListe3InitiativesParConstats(donneesApi.diagnosticApi.profils, ['precarite-alimentaire'])
        },
        infographie: infographie
    };
}

export function creerConstatCommercesAlimentaires(donneesApi: DonneesApi): ConstatModele<InfographieImageChiffreCleModele> {
    const partPopulationDependanteVoiturePourcent = formaterNombreSelonValeurString(
        donneesApi.diagnosticApi.proximiteCommerces.partPopulationDependanteVoiturePourcent
    );
    return {
        id: 'commerces-alimentaires',
        idTheme: 'accessibilite',
        titre: {
            libelle: 'Un accès facile à l’alimentation ?',
            image: {
                url: '/images/constats/titres/rayon-fruit-supermarche-800x861.jpg',
                descriptionAlt: 'Rayon de fruits dans un supermarché',
                resolutionX: 800,
                resolutionY: 861
            }
        },
        message: {
            titre: '90 % des achats alimentaires sont réalisés en voiture.',
            texte: htmlstring`Les foyers parcourent ainsi en moyenne 60 kilomètres par semaine dans ce but. En cause, la disparition des commerces alimentaires de proximité et l’étalement urbain.`,
            question: 'Et si on redynamisait les bourgs et les centres-villes ?',
            initiatives: recupererListe3InitiativesParConstats(donneesApi.diagnosticApi.profils, ['commerces-alimentaires'])
        },
        infographie: {
            nomComposant: 'InfographieImageChiffreCle',
            informationsMethodologie: {
                titre: 'Indicateur de proximité aux commerces',
                descriptif: htmlstring`
                <p>Cet indicateur correspond à la part de la population du territoire qui est située à plus de 2 km à vol d'oiseau (distance difficilement atteignable à vélo pour réaliser ses courses) d’un ensemble représentatif de commerces alimentaires.</p>
                <br>
                <p>Source : ${calculerLibelleSourcesIndicateur(INDICATEURS.partPopulationDependanteVoiture)}</p>`
            },
            chiffreCle: {
                chiffre: partPopulationDependanteVoiturePourcent,
                unite: '%',
                phrase: 'de la population du territoire est dépendante de la voiture pour ses achats alimentaires'
            },
            accroche: 'Faites le “plein” de courses',
            image: {
                url: '/images/constats/infographies/parking-voitures-416x448.png',
                descriptionAlt: 'Parking avec des voitures',
                resolutionX: 416,
                resolutionY: 448,
                position: 'gauche'
            }
        }
    };
}
