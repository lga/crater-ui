import { calculerEtFormaterEvolutionString } from '@lga/base';

export class MessageEvolutionPesticides {
    public static readonly messageHtmlAugmentationQsaAugmentationNodu =
        'les quantités totales de substances actives achetées et le nombre de doses unités ont <strong>augmenté</strong>';
    public static readonly messageHtmlBaisseQsaBaisseNodu =
        'les quantités totales de substances actives achetées et le nombre de doses unités ont <strong>diminué</strong>';
    public static readonly messageHtmlBaisseQsaAugmentationNodu =
        'les quantités totales de substances actives achetées ont <strong>diminué</strong> mais le nombre de doses unités a <strong>augmenté</strong>';
    public static readonly messageHtmlAutreCase =
        'les quantités totales de substances actives achetées et le nombre de doses unités ont évolués de la façon suivante ';

    static construireDebutMessage(QsaInitial: number, QsaFinal: number, NoduInitial: number, NoduFinal: number): string {
        if (QsaFinal > QsaInitial && NoduFinal > NoduInitial) {
            return MessageEvolutionPesticides.messageHtmlAugmentationQsaAugmentationNodu;
        } else if (QsaFinal < QsaInitial && NoduFinal < NoduInitial) {
            return MessageEvolutionPesticides.messageHtmlBaisseQsaBaisseNodu;
        } else if (QsaFinal < QsaInitial && NoduFinal > NoduInitial) {
            return MessageEvolutionPesticides.messageHtmlBaisseQsaAugmentationNodu;
        } else {
            return MessageEvolutionPesticides.messageHtmlAutreCase;
        }
    }

    static construireMessage(
        nomTerritoire: string,
        anneeInitiale: number,
        anneeFinale: number,
        quantiteSubstanceTotalKgInitale: number | null,
        quantiteSubstanceTotalKgFinale: number | null,
        NoduHaInital: number | null,
        NoduHaFinal: number | null
    ) {
        if (quantiteSubstanceTotalKgInitale === null || quantiteSubstanceTotalKgFinale === null || NoduHaInital === null || NoduHaFinal === null) {
            return `Pour le territoire <em>${nomTerritoire}</em>, les données ne sont pas disponibles.`;
        }
        let message =
            `Sur le territoire <em>${nomTerritoire}</em>, ` +
            MessageEvolutionPesticides.construireDebutMessage(
                quantiteSubstanceTotalKgInitale,
                quantiteSubstanceTotalKgFinale,
                NoduHaInital,
                NoduHaFinal
            );
        message += ` entre 
            ${anneeInitiale}
             et 
            ${anneeFinale} :
            <ul>
                <li><strong>${calculerEtFormaterEvolutionString(quantiteSubstanceTotalKgInitale, quantiteSubstanceTotalKgFinale)}</strong>
                pour les quantités de substances actives ;
                <li><strong>${calculerEtFormaterEvolutionString(NoduHaInital, NoduHaFinal)}</strong>
                pour le nombre de doses unités.`;

        return message;
    }
}
