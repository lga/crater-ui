import type { KebabVersCamelCase } from '@lga/base';

export type IdConstatAbsenceDonnees = 'absence-donnees';

export type IdConstatTerresAgricoles = 'terres-insuffisantes' | 'terres-partiellement-suffisantes' | 'terres-suffisantes';

export type IdConstatAutonomieAlimentaire = 'distances';

export type IdConstatConsommation = 'regime-alimentaire-trop-carne';

export type IdConstatAccessibilite = 'precarite-alimentaire' | 'commerces-alimentaires';

export type IdConstatAgriculteurs = 'evolution-population-agricole' | 'revenus';

export type IdConstatRessources = 'dependances-ressources' | 'alimentation-animale-importee' | 'prelevements-eau';

export type IdConstatBiodiversite = 'paysages-agricoles' | 'haies';

export type IdConstatClimat = 'analogue-climatique';

export type IdConstat =
    | IdConstatTerresAgricoles
    | IdConstatAutonomieAlimentaire
    | IdConstatAccessibilite
    | IdConstatConsommation
    | IdConstatAgriculteurs
    | IdConstatRessources
    | IdConstatBiodiversite
    | IdConstatClimat;

export type IdConstatAvecAbsenceDonnees = IdConstatAbsenceDonnees | IdConstat;

export const IDS_CONSTATS_TERRES_AGRICOLES: Record<KebabVersCamelCase<IdConstatTerresAgricoles>, IdConstatTerresAgricoles> = {
    terresInsuffisantes: 'terres-insuffisantes',
    terresPartiellementSuffisantes: 'terres-partiellement-suffisantes',
    terresSuffisantes: 'terres-suffisantes'
};
export const IDS_CONSTATS_AUTONOMIE_ALIMENTAIRE: Record<KebabVersCamelCase<IdConstatAutonomieAlimentaire>, IdConstatAutonomieAlimentaire> = {
    distances: 'distances'
};
export const IDS_CONSTATS_ACCESSIBILITE: Record<KebabVersCamelCase<IdConstatAccessibilite>, IdConstatAccessibilite> = {
    precariteAlimentaire: 'precarite-alimentaire',
    commercesAlimentaires: 'commerces-alimentaires'
};
export const IDS_CONSTATS_CONSOMMATION: Record<KebabVersCamelCase<IdConstatConsommation>, IdConstatConsommation> = {
    regimeAlimentaireTropCarne: 'regime-alimentaire-trop-carne'
};
export const IDS_CONSTATS_AGRICULTEURS: Record<KebabVersCamelCase<IdConstatAgriculteurs>, IdConstatAgriculteurs> = {
    evolutionPopulationAgricole: 'evolution-population-agricole',
    revenus: 'revenus'
};
export const IDS_CONSTATS_RESSOURCES: Record<KebabVersCamelCase<IdConstatRessources>, IdConstatRessources> = {
    dependancesRessources: 'dependances-ressources',
    alimentationAnimaleImportee: 'alimentation-animale-importee',
    prelevementsEau: 'prelevements-eau'
};
export const IDS_CONSTATS_BIODIVERSITE: Record<KebabVersCamelCase<IdConstatBiodiversite>, IdConstatBiodiversite> = {
    paysagesAgricoles: 'paysages-agricoles',
    haies: 'haies'
};
export const IDS_CONSTATS_CLIMAT: Record<KebabVersCamelCase<IdConstatClimat>, IdConstatClimat> = {
    analogueClimatique: 'analogue-climatique'
};

export const IDS_CONSTATS: Record<KebabVersCamelCase<IdConstat>, IdConstat> = {
    ...IDS_CONSTATS_TERRES_AGRICOLES,
    ...IDS_CONSTATS_AUTONOMIE_ALIMENTAIRE,
    ...IDS_CONSTATS_ACCESSIBILITE,
    ...IDS_CONSTATS_CONSOMMATION,
    ...IDS_CONSTATS_AGRICULTEURS,
    ...IDS_CONSTATS_RESSOURCES,
    ...IDS_CONSTATS_BIODIVERSITE,
    ...IDS_CONSTATS_CLIMAT
};
