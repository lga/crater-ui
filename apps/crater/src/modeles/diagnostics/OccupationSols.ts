export class OccupationSols {
    constructor(
        public readonly superficieTotaleHa: number,
        public readonly superficieArtificialiseeClcHa: number | null,
        public readonly superficieAgricoleClcHa: number | null,
        public readonly superficieNaturelleOuForestiereClcHa: number | null,
        public readonly superficieAgricoleRA2020Ha: number | null
    ) {}
    get partSuperficieAgricoleClcPourcent(): number | null {
        if (this.superficieAgricoleClcHa === null || this.superficieTotaleHa === null || this.superficieTotaleHa === 0) {
            return null;
        } else {
            return (this.superficieAgricoleClcHa / this.superficieTotaleHa) * 100;
        }
    }
}
