import { describe, expect, it } from 'vitest';

import { creerDonneesApi } from '../tests-utils';
import { creerConstatCommercesAlimentaires, creerConstatPrecariteAlimentaire } from './constats-accessibilite';

describe('Tests constats Accessibilite', () => {
    it('Test de creerConstatPrecariteAlimentaire pour France', () => {
        const donneesFranceApi = creerDonneesApi({
            tauxAdequationAssietteActuelleBrutPourcent: 40,
            codeNiveauRisquePrecariteAlimentaire: '5_TRES_ELEVE'
        });

        donneesFranceApi.territoireApi = {
            categorie: 'PAYS',
            id: 'france',
            idCode: 'P-FR',
            idNom: 'france',
            idTerritoireParcel: '1',
            nom: 'France',
            boundingBox: {
                latitudeMax: 0,
                latitudeMin: 0,
                longitudeMax: 0,
                longitudeMin: 0
            },
            genreNombre: 'FEMININ_SINGULIER',
            preposition: 'en'
        };

        const constat = creerConstatPrecariteAlimentaire(donneesFranceApi);
        expect(constat.message.titre).toContain('De plus en plus de personnes ne mangent pas à leur faim en France');
        expect(constat.message.texte).toContain('C’est étonnant alors que la France');
        expect(constat.infographie?.nomComposant).toEqual('InfographieImageChiffreCle');
    });

    it('Test de creerConstatPrecariteAlimentaire pour catégorie de territoire différente de PAYS, risque inconnu', () => {
        const donneesApi = creerDonneesApi({
            tauxAdequationAssietteActuelleBrutPourcent: 99
        });

        const constat = creerConstatPrecariteAlimentaire(donneesApi);
        expect(constat.message.titre).toContain('De plus en plus de personnes ne mangent pas à leur faim en France');
        expect(constat.message.texte).toContain('C’est étonnant alors que la France');
        expect(constat.infographie?.nomComposant).toEqual('InfographieCartePrecariteAlimentaire');
    });

    it('Test de creerConstatPrecariteAlimentaire pour catégorie de territoire différente de PAYS, risque très élevé, excédentaire', () => {
        const donneesApi = creerDonneesApi({
            tauxAdequationAssietteActuelleBrutPourcent: 101,
            codeNiveauRisquePrecariteAlimentaire: '5_TRES_ELEVE'
        });

        const constat = creerConstatPrecariteAlimentaire(donneesApi);
        expect(constat.message.titre).toContain('Le risque de précarité alimentaire est très élevé sur votre territoire');
        expect(constat.message.texte).toContain(
            'C’est étonnant alors que votre commune pourrait produire suffisamment pour nourrir tous ses habitants'
        );
        expect(constat.infographie?.nomComposant).toEqual('InfographieCartePrecariteAlimentaire');
    });

    it('Test de creerConstatPrecariteAlimentaire pour catégorie de territoire différente de PAYS, risque faible', () => {
        const donneesApi = creerDonneesApi({
            codeNiveauRisquePrecariteAlimentaire: '1_TRES_BAS'
        });

        const constat = creerConstatPrecariteAlimentaire(donneesApi);
        expect(constat.message.titre).toContain('Le risque de précarité alimentaire est relativement faible sur votre territoire');
        expect(constat.message.texte).toContain('Une situation à surveiller cependant');
        expect(constat.infographie?.nomComposant).toEqual('InfographieCartePrecariteAlimentaire');
    });

    it('Test de creerConstatCommercesAlimentaires', () => {
        const donneesApi = creerDonneesApi({
            partPopulationDependanteVoiturePourcent: 99.51
        });

        const constat = creerConstatCommercesAlimentaires(donneesApi);
        expect(constat.infographie?.chiffreCle?.chiffre).toEqual('100');
    });
});
