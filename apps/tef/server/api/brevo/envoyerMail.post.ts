import { createError, defineEventHandler, readValidatedBody } from 'h3';

import { type Mail, mailSchema } from '@/modeles/contacts/Mail';
import { useRuntimeConfig } from '#imports';

const ID_EXPEDITEUR_BREVO = 7; // Correspond à ne-pas-repondre@territoiresfertiles.fr, pour trouver l'id de l'expéditeur utiliser l'api GET /senders via cette page https://developers.brevo.com/reference/sendtransacemail
const MAIL_DESTINATAIRE = 'contact@territoiresfertiles.fr';

/**
 * Endpoint qui sert de proxy pour accéder à l'API Brevo
 * https://developers.brevo.com/reference/sendtransacemail
 * Permet d'éviter d'exposer la clé API Brevo dans les browsers, et d'avoir une config CORS restrictive (par défaut dans nuxt => CORS: false => Same-origin)
 */
export default defineEventHandler<{ body: Mail }>(async (event) => {
    const resulatValidation = await readValidatedBody(event, (body) => mailSchema.safeParse(body));
    if (!resulatValidation.success)
        throw createError({
            statusCode: 400,
            statusMessage: JSON.stringify(resulatValidation.error.issues)
        });

    const config = useRuntimeConfig();

    return $fetch('https://api.brevo.com/v3/smtp/email', {
        method: 'POST',
        headers: {
            accept: 'application/json',
            'content-type': 'application/json',
            'api-key': config.cleApiBrevo
        },
        body: {
            sender: { id: ID_EXPEDITEUR_BREVO },
            to: [{ email: MAIL_DESTINATAIRE }],
            replyTo: [{ email: resulatValidation.data.repondreA }],
            htmlContent: resulatValidation.data.contenuHtml,
            subject: resulatValidation.data.sujet
        }
    });
});
