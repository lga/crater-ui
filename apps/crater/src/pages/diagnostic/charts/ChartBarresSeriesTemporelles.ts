import { fusionner } from '@lga/base';
import ApexCharts, { type ApexOptions } from 'apexcharts';
import { css, html, LitElement } from 'lit';
import { customElement, property, query } from 'lit/decorators.js';

import { APEX_OPTIONS_GLOBALES } from './chart-config';
import { formaterLabelNombreEleve, type SerieMultiple } from './chart-outils';

export interface OptionsChartBarresSeriesTemporelles {
    series: SerieMultiple;
    nomAxeOrdonnees: string;
    minY?: number;
    maxY?: number;
    minX?: Date;
    maxX?: Date;
    texteNoData?: string;
}

declare global {
    interface HTMLElementTagNameMap {
        'c-chart-barres-series-temporelles': ChartBarresSeriesTemporelles;
    }
}

@customElement('c-chart-barres-series-temporelles')
export class ChartBarresSeriesTemporelles extends LitElement {
    private chart!: ApexCharts;

    @property({ attribute: false })
    options?: OptionsChartBarresSeriesTemporelles;

    @query('figure')
    private figure?: HTMLElement;

    static styles = css`
        figure {
            margin: auto;
        }
    `;

    render() {
        return html`
            <div>
                <figure></figure>
            </div>
        `;
    }

    async updated() {
        if (!this.options) return;
        if (this.chart) {
            this.chart.updateOptions(this.calculerApexOptions(this.options), true, true);
        } else {
            this.chart = new ApexCharts(this.figure, this.calculerApexOptions(this.options));
            await this.chart.render();
            // Le premier render d'apex ne fonctionne pas dans lit (diagramme vide). Contournement : on attend la fin du render, et on fait un update
            this.chart.updateOptions(this.calculerApexOptions(this.options), true, true);
        }
    }

    private calculerApexOptions(options: OptionsChartBarresSeriesTemporelles) {
        // Sans cette ligne on a une exception dans les logs sur certaines pages, uniquement sur l'app buildée (ne se produit pas avec npm run start:crater)
        if (!options) return;

        let apexOptionsLocales: ApexOptions = {
            series: options.series.positionnerNomSiVide(options.nomAxeOrdonnees).versSerieApexXY(),
            chart: {
                type: 'bar',
                height: 400,
                stacked: false
            },
            xaxis: {
                type: 'datetime',
                min: options.minX?.getTime(),
                max: options.maxX?.getTime(),
                labels: {
                    offsetX: 24
                }
            },
            yaxis: {
                tickAmount: 5,
                min: options.minY ?? undefined,
                max: options.maxY ?? undefined,
                title: {
                    text: options.nomAxeOrdonnees
                },
                labels: {
                    formatter: (labelInitial: number) => formaterLabelNombreEleve(labelInitial)
                }
            },
            legend: {
                show: true,
                showForSingleSeries: true
            }
        };

        if (options.texteNoData || options.texteNoData === '') apexOptionsLocales = { ...apexOptionsLocales, noData: { text: options.texteNoData } };

        return fusionner(APEX_OPTIONS_GLOBALES, apexOptionsLocales);
    }
}
