export class PolitiqueAmenagement {
    static readonly ARTIF_QUASI_NULLE_OU_NEGATIVE = new PolitiqueAmenagement('ARTIF_QUASI_NULLE_OU_NEGATIVE');
    static readonly ARTIF_POSITIVE_ET_NB_MENAGES_EMPLOIS_EN_HAUSSE = new PolitiqueAmenagement('ARTIF_POSITIVE_ET_NB_MENAGES_EMPLOIS_EN_HAUSSE');
    static readonly ARTIF_POSITIVE_ET_NB_MENAGES_EMPLOIS_EN_BAISSE = new PolitiqueAmenagement('ARTIF_POSITIVE_ET_NB_MENAGES_EMPLOIS_EN_BAISSE');
    static readonly DONNEES_NON_DISPONIBLES = new PolitiqueAmenagement('DONNEES_NON_DISPONIBLES');

    private constructor(public readonly code: string) {}

    static get listerToutes(): PolitiqueAmenagement[] {
        return Object.values(PolitiqueAmenagement)
            .filter((e) => e instanceof PolitiqueAmenagement)
            .sort((e1, e2) => e1.comparer(e2));
    }

    static fromString(code: string): PolitiqueAmenagement {
        const valeurEnum = this.listerToutes.find((e) => this.verifierEgalite(e, code));
        if (valeurEnum) return valeurEnum;

        throw new RangeError(`Valeur incorrecte : le code "${code}" ne correspondent à aucun des codes de l'énumération`);
    }

    private static verifierEgalite(e: PolitiqueAmenagement, code: string) {
        return e.code === code;
    }

    comparer(politiqueAmenagement: PolitiqueAmenagement) {
        return this.code.localeCompare(politiqueAmenagement.code);
    }
}
