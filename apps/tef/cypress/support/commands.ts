/* eslint-disable */
// @ts-nocheck

import Chainable = Cypress.Chainable;

Cypress.Commands.add('verifierAffichagePage', (contenuH1) => {
    cy.get('h1').contains(contenuH1).should('be.visible');
    cy.get('h1').should('have.length', 1);
    cy.window().scrollTo('top');
    cy.window().its('scrollY').should('be.equal', 0);
    cy.window().scrollTo('right');
    // Désactivé car pas stable
    // cy.window().its('scrollX').should('be.equal', 0);
});

Cypress.Commands.add('verifierAffichagePageAccueil', () => {
    cy.get('h1 img').should('have.attr', 'alt', 'Plateforme de projets de territoires');
    cy.window().scrollTo('top');
    cy.window().its('scrollY').should('be.equal', 0);
    cy.window().scrollTo('right');
    // Désactivé car pas stable
    // cy.window().its('scrollX').should('be.equal', 0);
});
