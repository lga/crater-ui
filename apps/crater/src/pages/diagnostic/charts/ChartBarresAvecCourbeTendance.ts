import { fusionner, regressionLineaire } from '@lga/base';
import ApexCharts from 'apexcharts';
import { css, html, LitElement } from 'lit';
import { customElement, property, query } from 'lit/decorators.js';

import { APEX_OPTIONS_GLOBALES } from './chart-config';
import { formaterLabelNombreEleve, type SerieMultiple } from './chart-outils';

export interface OptionsChartBarresAvecCourbeTendance {
    series: SerieMultiple;
    nomAxeOrdonnees: string;
    maxY?: number;
}

declare global {
    interface HTMLElementTagNameMap {
        'c-chart-barres-avec-courbe-tendance': ChartBarresAvecCourbeTendance;
    }
}

@customElement('c-chart-barres-avec-courbe-tendance')
export class ChartBarresAvecCourbeTendance extends LitElement {
    private chart!: ApexCharts;

    @property({ attribute: false })
    options?: OptionsChartBarresAvecCourbeTendance;

    @query('figure')
    private figure?: HTMLElement;

    static styles = css`
        figure {
            margin: auto;
            max-width: 500px;
        }
    `;

    render() {
        return html`
            <div>
                <figure></figure>
            </div>
        `;
    }

    async updated() {
        if (!this.options) return;
        if (this.chart) {
            this.chart.updateOptions(this.calculerApexOptions(this.options), true, true);
        } else {
            this.chart = new ApexCharts(this.figure, this.calculerApexOptions(this.options));
            await this.chart.render();
            // Le premier render d'apex ne fonctionne pas dans lit (diagramme vide). Contournement : on attend la fin du render, et on fait un update
            this.chart.updateOptions(this.calculerApexOptions(this.options), true, true);
        }
    }

    private calculerApexOptions(options: OptionsChartBarresAvecCourbeTendance) {
        // Sans cette ligne on a une exception dans les logs sur certaines pages, uniquement sur l'app buildée (ne se produit pas avec npm run start:crater)
        if (!options) return;

        const series = options.series.positionnerNomSiVide(options.nomAxeOrdonnees).formaterLabels(15).versSerieApexXY('bar');

        const x = options.series.labels.map((l) => {
            return parseFloat(l.toString());
        });
        const y = options.series.series[0].valeurs.map((v) => {
            return v!;
        });
        const regression = regressionLineaire(x, y);
        const valeursCourbes = x.map((i) => {
            return { x: i, y: regression.fn(i) };
        });
        series.push({ name: 'Tendance', data: valeursCourbes, type: 'line' });

        const apexOptionsLocales = {
            series: series,
            chart: {
                height: 400
            },
            stroke: {
                width: [0, 5],
                dashArray: [0, 5]
            },
            xaxis: {
                labels: {
                    rotate: -35
                }
            },
            yaxis: {
                tickAmount: 5,
                forceNiceScale: options.maxY ? false : true,
                min: 0,
                max: options.maxY ?? undefined,
                title: {
                    text: options.nomAxeOrdonnees
                },
                labels: {
                    formatter: (labelInitial: string) => formaterLabelNombreEleve(labelInitial)
                }
            },
            tooltip: {
                y: {
                    formatter: (labelInitial: string) => formaterLabelNombreEleve(labelInitial)
                }
            }
        };

        return fusionner(APEX_OPTIONS_GLOBALES, apexOptionsLocales);
    }
}
