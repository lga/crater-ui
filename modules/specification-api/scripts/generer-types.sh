#!/bin/bash

rm -rf build/api

openapi --input config/spec-openapi.yml --output src/specification/api --useUnionTypes --exportCore false --exportServices false --exportModels true --exportSchemas false --postfixModels Api

mkdir -p src/specification/schemas
openapi-zod-client config/spec-openapi.yml --output src/specification/schemas/schemas-zod.ts --template scripts/template-generation-schema-zod.hbs

