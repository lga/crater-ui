<template>
    <div :id="IDS_DOMAINES.irrigation">
        <TitreH4>Indicateurs sur les volumes d'eau prélevés pour l'irrigation des cultures</TitreH4>

        <TexteP
        >Ces indicateurs sont calculés à partir des
            <LienMethodologieSourceDonnee :sourceDonnee="SOURCES_DONNEES.prelevements_irrigation" />.</TexteP
        >

        <TitreH5>Calcul du volume d’eau utilisé pour l’irrigation par années en m³</TitreH5>
        <TexteP
        >Le calcul de cet indicateur est réalisé à partir des mesures de prélèvements annuels par ouvrage pour chaque année retenue ({{
            SOURCES_DONNEES.prelevements_irrigation.annees
        }}) :
            <ul>
                <li>
                    pour chaque commune, calcul du volume brut prélevé par année à destination de l’irrigation, tel que mesuré au niveau des ouvrages
                    :
                    <ul>
                        <li>obtenu par somme des volumes prélevés pour l’usage IRRIGATION sur tous les ouvrages de la commune ;</li>
                        <li>
                            pour les communes sans données, le volume prélevé est positionné à 0 (voir les limitations des données
                            <LienMethodologieSourceDonnee :sourceDonnee="SOURCES_DONNEES.prelevements_irrigation" />) ;
                        </li>
                    </ul>
                </li>
                <li>
                    calcul du volume prélevé pour l’irrigation pour les territoires supra-communaux en faisant la somme des valeurs de chaque commune,
                    pour chaque année ;
                </li>
                <li>
                    et enfin, calcul du volume estimé de prélèvement au niveau de chaque commune. Pour cela le volume de prélèvements calculé au
                    niveau de l’EPCI est reparti sur les communes, au prorata de la
                    <LienMethodologieSurfacesAgricoles libelle="SAU productive hors prairies" />.
                </li>
            </ul>
        </TexteP>

        <TexteP>
            Remarques :
            <ul>
                <li>
                    La dernière étape du calcul permet de modérer le biais sur les données de mesure qui ne permettent pas d’identifier finement la
                    commune sur laquelle est effectivement située la parcelle irriguée du fait de la répartition géographique hétérogène des ouvrages.
                </li>
                <li>
                    En conséquence, les valeurs obtenues pour les communes sont des estimations, et non des mesures. Elles peuvent différer des
                    valeurs présentes dans les
                    <LienMethodologieSourceDonnee :sourceDonnee="SOURCES_DONNEES.prelevements_irrigation"
                    >la source de données</LienMethodologieSourceDonnee
                    >.
                </li>
                <li>
                    Malgré cet ajustement, il est possible que le volume de prélèvements présenté au niveau de la commune - voire de l'EPCI - soit
                    surestimé ou sous-estimé de manière significative, en particulier pour les territoires comportant des réseaux de canaux
                    d'irrigation (ex: Pyrénées-Orientales, Bouches-du-Rhône, etc.). Voir les limites dans les
                    <LienMethodologieSourceDonnee :sourceDonnee="SOURCES_DONNEES.prelevements_irrigation" />.
                </li>
            </ul>
        </TexteP>

        <TitreH5>Calcul du volume d’eau utilisé pour l’irrigation par années en mm ou m³/ha</TitreH5>
        <TexteP
        >La valeur en m³ est ramenée à une valeur en m³/ha ou mm, ce qui permet de proposer un indicateur comparable entre territoires :
            <TexteFormule>
                irrigation_annee_n [mm] = (volume_irrigation_annee_n [m³] / sau_productive_hors_prairies [m²]) * 1000 [mm] irrigation_annee_n [m³/ha]
                = 10 * irrigation_annee_n [mm]
            </TexteFormule>
        </TexteP>

        <TitreH5>Calcul d’une valeur moyenne sur les cinq dernières années</TitreH5>
        <TexteP
        >Le calcul de la moyenne sur les 5 années les plus récentes permet de ramener les séries annuelles à une seule valeur et d’obtenir les
            indicateurs irrigation_m3 et irrigation_mm.
        </TexteP>

        <TitreH5>Calcul de la tendance de l’irrigation sur les années considérées</TitreH5>
        <TexteP
        >Une régression linéaire sur les mesures de l’indicateur irrigation_annee_n permet d’évaluer la tendance suivie par la série, que l’on
            peut ensuite exprimer en évolution moyenne sur base du coefficient de la droite de régression.
        </TexteP>

        <TitreH5>Calcul de la note irrigation</TitreH5>
        <TexteP
        >La note est calculée à partir de l'indicateur irrigation_mm (moyenne des 5 dernières années). La distribution des valeurs de cet
            indicateur pour les départements permet d'obtenir des seuils de référence : le minimum (min), le premier quartile (Q1), la médiane (Q2),
            et le troisième quartile (Q3). À partir de ces seuils les notes sont calculées comme suit :
            <TexteFormule>
                <div>si irrigation_mm = min alors note = 10</div>
                <div>si irrigation_mm = Q1 alors note = 7.5</div>
                <div>si irrigation_mm = Q2 alors note = 5</div>
                <div>si irrigation_mm = Q3 alors note = 2.5</div>
                <div>si irrigation_mm >= Q3 + 1.5 * (Q3 - Q1) alors note = 0</div>
                <div>si irrigation_mm est entre 2 seuils, la note est calculée par interpolation linéaire</div>
            </TexteFormule>
        </TexteP>
        <TexteP
        >A titre indicatif en 2020 (ie moyenne 2016-2020) les valeurs des seuils sont : min=0, Q1=1mm, Q2=9mm, Q3=30mm, et
            Q3+1,5*(Q3-Q1)=85mm.</TexteP
        >

        <TitreH5>Pratiques d'irrigation : répartition des surfaces irriguées en fonction des types de cultures</TitreH5>
        <TexteP
        >Des indicateurs sur la répartition des surfaces irriguées en fonction des types de cultures sont présentés en complément des données de
            prélèvement.</TexteP
        >
        <TexteP>Ces données sont calculées :</TexteP>
        <ul>
            <li>
                essentiellement à partir de la source de données <LienMethodologieSourceDonnee :sourceDonnee="SOURCES_DONNEES.pratiques_irrigation" />
            </li>
            <li>
                en intégrant également les cultures de riz grâce aux données de
                <LienMethodologieSourceDonnee :sourceDonnee="SOURCES_DONNEES.surfaces_riz" />. Cette distinction est intéressante car le riz est une
                culture fortement irriguée (hypothèse : 100% des surfaces de riz irriguées par irrigation gravitaire, ~ 20 000 m³/ha/an selon
                Climagri). Cette étape permet d’enrichir la classification Agreste du RA 2010 avec le type de culture “Riz”
            </li>
            <li>uniquement pour les échelles départementales et supérieures</li>
        </ul>

        <TitreH5>Évaluation des volumes d'eau mobilisés pour l'irrigation gravitaire et non gravitaire</TitreH5>
        <TexteP
        >À partir de l'évaluation des volumes d'eau prélevés, des indicateurs de pratiques d’irrigation, et des coefficients ClimAgri d’irrigation
            par type de culture (cas France, onglet A5a), il est possible d'évaluer les volumes d'eau consacrés à l'irrigation gravitaire et non
            gravitaire :
            <TexteFormule>
                <ol>
                    <li>
                        pour chaque département, à partir des surfaces irriguées (issues des indicateurs de pratiques d’irrigation) et des
                        coefficients d’irrigation Climagri, on calcule un volume d’eau estimé pour l’irrigation gravitaire et irrigation hors
                        gravitaire : irrigation_gravitaire_modelisee_m3 et irrigation_hors_gravitaire_modelisee_m3
                    </li>
                    <li>
                        Ces valeurs estimées permettent d’obtenir à l’échelle des départements, régions et pays, 2 coefficients :
                        part_irrigation_gravitaire et part_irrigation_hors_gravitaire
                    </li>
                    <li>
                        Ces coefficients sont finalement appliqués à l’indicateur irrigation_m3 pour obtenir irrigation_gravitaire_m3, et
                        irrigation_hors_gravitaire_m3
                    </li>
                    <li>
                        Pour les territoires infra-départementaux, ce sont les valeurs part_irrigation_gravitaire et part_irrigation_hors_gravitaire
                        du département qui sont utilisées
                    </li>
                </ol>
            </TexteFormule>
        </TexteP>

        <TexteP
        >Ces indicateurs ne sont pas présentés dans l'application, mais ils sont utilisés par ailleurs, dans les calculs sur la dépendance à
            l'énergie pour l'irrigation.</TexteP
        >
    </div>
</template>

<script setup lang="ts">
import { IDS_DOMAINES, SOURCES_DONNEES } from '@lga/indicateurs';

import TexteFormule from '~/components/base/TexteFormule.vue';
import TexteP from '~/components/base/TexteP.vue';
import TitreH4 from '~/components/base/TitreH4.vue';
import TitreH5 from '~/components/base/TitreH5.vue';

import LienMethodologieSourceDonnee from '../liens/LienMethodologieSourceDonnee.vue';
import LienMethodologieSurfacesAgricoles from '../liens/LienMethodologieSurfacesAgricoles.vue';
</script>
