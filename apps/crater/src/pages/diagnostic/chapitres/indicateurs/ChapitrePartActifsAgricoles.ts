import '../../charts/ChartBarres.js';
import '../composants/ChapitreDomaine.js';
import '../../../commun/PageSablierCrater.js';

import { IDS_DOMAINES } from '@lga/indicateurs';
import { SA } from '@lga/indicateurs';
import { html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { EtatRapport, type Rapport } from '../../../../modeles/diagnostics/Rapport.js';
import { Serie, SerieMultiple } from '../../charts/chart-outils.js';
import type { OptionsChartBarres } from '../../charts/ChartBarres';
import type { DonneesPageDiagnostic } from '../../PageDiagnostic.js';
import type { OptionsDomaineDetaille } from '../composants/ChapitreDomaine';

declare global {
    interface HTMLElementTagNameMap {
        'c-chapitre-part-actifs-agricoles': ChapitrePartActifsAgricoles;
    }
}

@customElement('c-chapitre-part-actifs-agricoles')
export class ChapitrePartActifsAgricoles extends LitElement {
    @property({ attribute: false })
    donneesPage?: DonneesPageDiagnostic;

    @property({ attribute: false })
    rapport?: Rapport;

    render() {
        if (this.rapport?.getEtat() === EtatRapport.PRET) {
            return html`<c-chapitre-domaine
                .options=${this.calculerOptionsChapitreDomainePartActifsAgricoles(this.rapport, this.donneesPage)}
                nbLignesMessageMinimum="7"
            >
                <c-chart-barres .options=${this.calculerOptionsChartPartActifsAgricoles(this.rapport)} slot="chart-chapitre-domaine"></c-chart-barres>
            </c-chapitre-domaine>`;
        } else {
            return html`<c-page-sablier-crater></c-page-sablier-crater>`;
        }
    }

    private calculerOptionsChapitreDomainePartActifsAgricoles(
        rapport: Rapport,
        donneesPage: DonneesPageDiagnostic | undefined
    ): OptionsDomaineDetaille {
        return {
            domaine: SA.getDomaine(IDS_DOMAINES.actifsAgricoles)!,
            message: `<p>
                En France métropolitaine, le nombre d'actifs agricoles permanents est passé de <strong>2 039 000</strong> en 1988 à
                <strong>966 000</strong> en 2010 (-53%) et <strong>759 000</strong> en 2020 (-21%) selon les résultats provisoires du
                <c-lien
                    href="https://agriculture.gouv.fr/recensement-agricole-2020-julien-denormandie-presente-les-premiers-resultats-du-recensement-decennal"
                    target="_blank"
                    >dernier recensement de 2020</c-lien
                >.
            </p>
            <br>
            ${rapport.getDiagnosticActif()?.agriculteursExploitations.messageActifsAgricoles}
            `,
            territoire: rapport.getTerritoireActif(),
            estPagePourTef: donneesPage?.estPagePourTef
        };
    }
    private calculerOptionsChartPartActifsAgricoles(rapport: Rapport): OptionsChartBarres {
        return {
            series: new SerieMultiple(
                [
                    new Serie(
                        rapport.getDiagnostics().map((d) => d!.agriculteursExploitations.partActifsAgricoles1988Pourcent),
                        '1988'
                    ),
                    new Serie(
                        rapport.getDiagnostics().map((d) => d!.agriculteursExploitations.partActifsAgricoles2010Pourcent),
                        '2010'
                    )
                ],
                rapport.getDiagnostics().map((d) => d!.territoire.nom)
            ),
            nomAxeOrdonnees: 'Actifs agricoles permanents / pop. totale (%)'
        };
    }
}
