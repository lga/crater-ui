import { configurerInterceptRequetesAPI } from '../config';

describe('Tests e2e/isolation, page accueil  ', () => {
    beforeEach(() => {
        configurerInterceptRequetesAPI();
    });

    it('afficher la liste des suggestions à partir du Occi', () => {
        cy.visit('/');
        cy.get('input[type="text"]').type('Occi');
        cy.get('c-champ-recherche-territoire').within(() => {
            cy.get('menu').children().should('have.length', 2);
        });
    });

    it('la récupération du détail du territoire Occitanie ', () => {
        cy.visit('/');
        cy.get('input[type="text"]').type('occitanie{enter}');
        cy.verifierAffichagePage('Diagnostic du système alimentaire');
    });

    it('afficher page accueil et tester le contenu de h1', () => {
        cy.viewport(1366, 768);
        cy.visit('/');
        cy.verifierAffichagePage('Mon territoire peut-il garantir une alimentation saine et durable à ses habitants ?');
        cy.window().scrollTo('right');
        cy.window().its('scrollX').should('be.equal', 0);
    });
});
