import { html, type TemplateResult } from 'lit';

export const templateResultToString = (tr: TemplateResult) => {
    const v: string[] = [...tr.values, ''].map((e) => (typeof e === 'object' ? templateResultToString(e as TemplateResult) : (e as string)));
    const s1: string = tr.strings.reduce((acc, s, i) => acc + s + v[i], '');

    const REGEX_ESPACE_TAB_RETOUR_LIGNE = /\s+/g;
    return s1.replace(REGEX_ESPACE_TAB_RETOUR_LIGNE, ' ').trim();
};

export function formaterNombreEnEntierHtml(nombre: number | null | undefined): TemplateResult {
    if (nombre === null || nombre === undefined || isNaN(nombre)) {
        return html`-`;
    }
    return html`<span class="chiffre">${Math.round(nombre).toLocaleString('fr-FR')}</span>`;
}
