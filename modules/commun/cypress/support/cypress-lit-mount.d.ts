import type { mount } from 'cypress-lit';

declare global {
    namespace Cypress {
        interface Chainable {
            mount: typeof mount;
        }
    }
}
