import { htmlstring } from '@lga/base';

import { ICONES_SVG } from '../../ressources/icones.js';
import { INDICATEURS } from '../indicateurs/indicateurs-definitions.js';
import { IDS_MAILLONS } from '../systeme-alimentaire/maillons.js';
import type { DonneesDomaine } from './Domaine.js';

const PREFIXE_ID_DOMAINE_MAILLON = 'evaluation-';

export const getIdDomaineDepuisIdMaillon = (id: string) => PREFIXE_ID_DOMAINE_MAILLON + id;

export const IDS_DOMAINES = {
    terresAgricoles: getIdDomaineDepuisIdMaillon(IDS_MAILLONS.terresAgricoles),
    sauParHabitant: 'surface-agricole-utile-par-habitant',
    politiqueAmenagement: 'politique-amenagement',
    rythmeArtificialisation: 'rythme-artificialisation',

    agriculteursExploitations: getIdDomaineDepuisIdMaillon(IDS_MAILLONS.agriculteursExploitations),
    actifsAgricoles: 'actifs-agricoles',
    revenuAgriculteurs: 'revenu-agriculteurs',
    agesChefsExploitation: 'ages-chefs-exploitations',
    superficiesExploitations: 'superficies-exploitations',
    logementsVacants: 'logements-vacants',

    production: getIdDomaineDepuisIdMaillon(IDS_MAILLONS.production),
    adequationProductionConsommation: 'adequation-production-consommation',
    importationExportationConsommationProduction: 'importation-exportation-consommation-production',
    bio: 'bio',
    hvn: 'haute-valeur-naturelle',

    intrants: getIdDomaineDepuisIdMaillon(IDS_MAILLONS.intrants),
    energie: 'energie',
    eau: 'eau',
    irrigation: 'irrigation',
    secheresses: 'secheresses',
    pesticides: 'dependance-pesticides',
    qsaNodu: 'pesticides-quantites-utilisees',
    noduNormalise: 'pesticides-intensite-usage',

    transformationDistribution: getIdDomaineDepuisIdMaillon(IDS_MAILLONS.transformationDistribution),
    dependancePopulationVoiture: 'dependance-population-voiture',
    dependanceTerritoireVoiture: 'dependance-territoire-voiture',
    distancesPlusProchesCommerces: 'distances-plus-proches-commerces',

    consommation: getIdDomaineDepuisIdMaillon(IDS_MAILLONS.consommation),
    consommationProduitsAnimaux: 'consommation-produits-animaux',
    pauvrete: 'pauvrete',
    aideAlimentaire: 'aide-alimentaire',
    obesite: 'obesite'
};

export const DONNEES_DOMAINES: DonneesDomaine[] = [
    {
        id: IDS_DOMAINES.terresAgricoles,
        idMaillon: IDS_MAILLONS.terresAgricoles,
        type: 'maillon',
        libelle: 'Maillon Terres agricoles',
        description: htmlstring`Ce maillon aborde la capacité du territoire à préserver ses terres agricoles, en particulier face à l'artificialisation.`,
        icone: ICONES_SVG.note,
        indicateurs: [INDICATEURS.scoreTerresAgricoles]
    },
    {
        id: IDS_DOMAINES.sauParHabitant,
        idMaillon: IDS_MAILLONS.terresAgricoles,
        type: 'indicateur',
        libelle: 'Surface agricole utile par habitant',
        description: htmlstring`Ce chapitre permet d'appréhender succintement la capacité de production du territoire.`,
        icone: ICONES_SVG.champs,
        indicateurs: [INDICATEURS.sauParHabitant]
    },
    {
        id: IDS_DOMAINES.rythmeArtificialisation,
        idMaillon: IDS_MAILLONS.terresAgricoles,
        type: 'indicateur',
        libelle: "Rythme d'artificialisation",
        description: htmlstring`Ce chapitre rend compte du rythme de disparition des espaces agricoles, naturels et forestiers.`,
        icone: ICONES_SVG.rythmeArtificialisation,
        indicateurs: [INDICATEURS.rythmeArtificialisation]
    },
    {
        id: IDS_DOMAINES.politiqueAmenagement,
        idMaillon: IDS_MAILLONS.terresAgricoles,
        type: 'indicateur',
        libelle: "Politique d'aménagement",
        description: htmlstring`Ce chapitre permet d'appréhender le respect de l'objectif national de <c-lien href="https://www.ecologie.gouv.fr/artificialisation-des-sols">Zéro Artificialisation Nette</c-lien>.`,
        icone: ICONES_SVG.politiqueAmenagement,
        indicateurs: [INDICATEURS.politiqueAmenagement]
    },
    {
        id: IDS_DOMAINES.logementsVacants,
        idMaillon: IDS_MAILLONS.terresAgricoles,
        type: 'indicateur',
        libelle: 'Part de logements vacants',
        description: htmlstring`Ce chapitre présente la part de logements vacants. Celle-ci peut notamment être mise en regard du rythme d'artificialisation car il arrive souvent que des terres soient
        artificialisées alors que de nombreux logements sont vacants.`,
        icone: ICONES_SVG.partLogementsVacants,
        indicateurs: [INDICATEURS.partLogementsVacants]
    },
    {
        id: IDS_DOMAINES.agriculteursExploitations,
        idMaillon: IDS_MAILLONS.agriculteursExploitations,
        type: 'maillon',
        libelle: 'Maillon Agriculteurs et exploitations',
        description: htmlstring`Ce maillon aborde l'effectif et la dynamique de renouvellement de la population agricole, laa condition de vie des agriculteurs et le profil des exploitations.`,
        icone: ICONES_SVG.note,
        indicateurs: [INDICATEURS.scoreAgriculteursExploitations]
    },
    {
        id: IDS_DOMAINES.actifsAgricoles,
        idMaillon: IDS_MAILLONS.agriculteursExploitations,
        type: 'indicateur',
        libelle: 'Part des actifs agricoles permanents dans la population totale',
        description: htmlstring`Ce chapitre présente la part des actifs agricoles dans la population, et son évolution.`,
        icone: ICONES_SVG.partActifsAgricoles,
        indicateurs: [INDICATEURS.partActifsAgricoles]
    },
    {
        id: IDS_DOMAINES.revenuAgriculteurs,
        idMaillon: IDS_MAILLONS.agriculteursExploitations,
        type: 'indicateur',
        libelle: `Revenu des agriculteurs`,
        description: htmlstring`Ce chapitre aborde le revenu des agriculteurs.`,
        icone: ICONES_SVG.revenu,
        indicateurs: [INDICATEURS.revenuAgriculteurs]
    },
    {
        id: IDS_DOMAINES.agesChefsExploitation,
        idMaillon: IDS_MAILLONS.agriculteursExploitations,
        type: 'indicateur',
        libelle: `Âge des chefs d'exploitation`,
        description: htmlstring`Ce chapitre aborde la pyramide des âges des agriculteurs.`,
        icone: ICONES_SVG.agesChefsExploitation,
        indicateurs: [INDICATEURS.agesChefsExploitation]
    },
    {
        id: IDS_DOMAINES.superficiesExploitations,
        idMaillon: IDS_MAILLONS.agriculteursExploitations,
        type: 'indicateur',
        libelle: 'Nombre et superficie des exploitations',
        description: htmlstring`Ce chapitre aborde le profil des exploitations.`,
        icone: ICONES_SVG.superficiesExploitations,
        indicateurs: [INDICATEURS.superficieMoyenneExploitations]
    },
    {
        id: IDS_DOMAINES.intrants,
        idMaillon: IDS_MAILLONS.intrants,
        type: 'maillon',
        libelle: 'Maillon Intrants',
        description: htmlstring`Ce maillon aborde la dépendance aux intrants de l'agriculture du territoire.`,
        icone: ICONES_SVG.note,
        indicateurs: [INDICATEURS.scoreIntrants]
    },
    {
        id: IDS_DOMAINES.energie,
        idMaillon: IDS_MAILLONS.intrants,
        type: 'indicateur',
        libelle: "Consommation d'énergie primaire de l'agriculture par hectare agricole",
        description: htmlstring`Ce chapitre aborde la consommation d'énergie des exploitations agricoles.`,
        icone: ICONES_SVG.energie,
        indicateurs: [INDICATEURS.consommationEnergetiqueParHectare, INDICATEURS.postePrincipalEnergie]
    },
    {
        id: IDS_DOMAINES.eau,
        idMaillon: IDS_MAILLONS.intrants,
        type: 'sans-page',
        libelle: 'Chapitre Dépendance à la ressource eau pour l’irrigation',
        description: htmlstring`Ce chapitre aborde la dépendance à l'eau pour l'irrigation des exploitations.`,
        icone: ICONES_SVG.eau,
        indicateurs: [INDICATEURS.scoreEau]
    },
    {
        id: IDS_DOMAINES.irrigation,
        idMaillon: IDS_MAILLONS.intrants,
        type: 'indicateur',
        libelle: `Prélèvements en eau pour l'irrigation`,
        description: htmlstring`Ce chapitre aborde l'usage de l'eau pour l'irrigation (Prélèvements et pratiques d'irrigation).`,
        icone: ICONES_SVG.irrigation,
        indicateurs: [INDICATEURS.prelevementsIrrigation]
    },
    {
        id: IDS_DOMAINES.secheresses,
        idMaillon: IDS_MAILLONS.intrants,
        type: 'indicateur',
        libelle: `Alertes sécheresse des eaux superficielles`,
        description: htmlstring`Ce chapitre aborde l'occurence et l'impact des sécheresses sur l'agriculture.`,
        icone: ICONES_SVG.alertesSecheresse,
        indicateurs: [INDICATEURS.alertesSecheresse]
    },
    {
        id: IDS_DOMAINES.qsaNodu,
        idMaillon: IDS_MAILLONS.intrants,
        type: 'indicateur',
        libelle: 'Quantités de substances actives achetées et Nombre de doses unités équivalent',
        description: htmlstring`Ce chapitre aborde la quantité de pesticides utilisés par le territoire.`,
        icone: ICONES_SVG.qsaNodu,
        indicateurs: [INDICATEURS.qsa, INDICATEURS.nodu]
    },
    {
        id: IDS_DOMAINES.noduNormalise,
        idMaillon: IDS_MAILLONS.intrants,
        type: 'indicateur',
        libelle: `Intensité d'usage de pesticides`,
        description: htmlstring`Ce chapitre aborde l'intensité d'usage de pesticides par le territoire.`,
        icone: ICONES_SVG.noduNormalise,
        indicateurs: [INDICATEURS.noduNormalise]
    },
    {
        id: IDS_DOMAINES.production,
        idMaillon: IDS_MAILLONS.production,
        type: 'maillon',
        libelle: 'Maillon Production',
        description: htmlstring`Ce chapitre aborde la capacité du territoire à produire une alimentation répondant à la consommation des habitants du
        territoire tout en utilisant des pratiques agricoles qui soutiennent la biodiversité.`,
        icone: ICONES_SVG.note,
        indicateurs: [INDICATEURS.scoreProduction]
    },
    {
        id: IDS_DOMAINES.adequationProductionConsommation,
        idMaillon: IDS_MAILLONS.production,
        type: 'indicateur',
        libelle: 'Adéquation théorique entre production et consommation',
        description: htmlstring`Ce chapitre aborde l'adéquation entre la consommation et la production du territoire.`,
        icone: ICONES_SVG.adequationProductionConsommation,
        indicateurs: [
            INDICATEURS.adequationTheoriqueBruteProductionConsommation,
            INDICATEURS.adequationTheoriqueMoyennePondereeProductionConsommation
        ]
    },
    {
        id: IDS_DOMAINES.importationExportationConsommationProduction,
        idMaillon: IDS_MAILLONS.production,
        type: 'indicateur',
        libelle: 'Importation et exportation',
        description: htmlstring`Ce chapitre aborde les questions d'importation/exportation de nourriture.`,
        icone: ICONES_SVG.partProductionExportee,
        indicateurs: [INDICATEURS.partProductionExportee]
    },
    {
        id: IDS_DOMAINES.bio,
        idMaillon: IDS_MAILLONS.production,
        type: 'indicateur',
        libelle: 'Agriculture Biologique',
        description: htmlstring`Ce chapitre aborde le poids de l'agriculture biologique dans le territoire.`,
        icone: ICONES_SVG.partSAUBio,
        indicateurs: [INDICATEURS.partSAUBio]
    },
    {
        id: IDS_DOMAINES.hvn,
        idMaillon: IDS_MAILLONS.production,
        type: 'indicateur',
        libelle: 'Indicateur Haute Valeur Naturelle',
        description: htmlstring`Ce chapitre aborde la durabilité des pratiques agricoles au travers de l'indicateur HVN.`,
        icone: ICONES_SVG.indiceHVN,
        indicateurs: [INDICATEURS.hvn]
    },
    {
        id: IDS_DOMAINES.transformationDistribution,
        idMaillon: IDS_MAILLONS.transformationDistribution,
        type: 'indicateur',
        libelle: `Maillon Transformation et Distribution`,
        description: htmlstring`Ce maillon aborde le niveau d'indépendance aux énergies fossiles des consommateurs pour leurs achats alimentaires.`,
        icone: ICONES_SVG.note,
        indicateurs: [INDICATEURS.scoreTransformationDistribution]
    },
    {
        id: IDS_DOMAINES.dependancePopulationVoiture,
        idMaillon: IDS_MAILLONS.transformationDistribution,
        type: 'indicateur',
        libelle: 'Dépendance de la population à la voiture pour ses achats alimentaires',
        description: htmlstring`Ce chapitre aborde la dépendance théorique de la population à la voiture pour accéder à un ensemble
        représentatif de commerces alimentaires.`,
        icone: ICONES_SVG.partPopulationDependanteVoiturePourcent,
        indicateurs: [INDICATEURS.partPopulationDependanteVoiture]
    },
    {
        id: IDS_DOMAINES.dependanceTerritoireVoiture,
        idMaillon: IDS_MAILLONS.transformationDistribution,
        type: 'indicateur',
        libelle: 'Dépendance du territoire à la voiture pour ses achats alimentaires',
        description: htmlstring`Ce chapitre aborde la dépendance théorique du territoire à la voiture pour accéder à un ensemble
        représentatif de commerces alimentaires.`,
        icone: ICONES_SVG.partTerritoireDependantVoiturePourcent,
        indicateurs: [INDICATEURS.partTerritoireDependantVoiture]
    },
    {
        id: IDS_DOMAINES.distancesPlusProchesCommerces,
        idMaillon: IDS_MAILLONS.transformationDistribution,
        type: 'indicateur',
        libelle: 'Distances moyennes aux plus proches commerces par type de commerce alimentaire',
        description: htmlstring`Ce chapitre aborde la question de la proximité des commerces aux habitants. `,
        icone: ICONES_SVG.distancesPlusProchesCommerces,
        indicateurs: [INDICATEURS.distancesPlusProchesCommerces]
    },
    {
        id: IDS_DOMAINES.consommation,
        idMaillon: IDS_MAILLONS.consommation,
        type: 'indicateur',
        libelle: 'Maillon Consommation',
        description: htmlstring`Ce chapitre aborde les questions de consommation alimentaire des habitants, et ses conséquences.`,
        icone: ICONES_SVG.note,
        indicateurs: [INDICATEURS.scoreConsommation]
    },
    {
        id: IDS_DOMAINES.consommationProduitsAnimaux,
        idMaillon: IDS_MAILLONS.consommation,
        type: 'indicateur',
        libelle: 'Consommation de produits animaux',
        description: htmlstring`Ce chapitre aborde la consommation de produits animaux, notamment son poids dans l'agriculture.`,
        icone: ICONES_SVG.partAlimentationAnimaleDansConsommation,
        indicateurs: [INDICATEURS.partAlimentationAnimaleDansConsommation]
    },
    {
        id: IDS_DOMAINES.pauvrete,
        idMaillon: IDS_MAILLONS.consommation,
        type: 'indicateur',
        libelle: 'Pauvreté',
        description: htmlstring`Ce chapitre aborde la question de la pauvreté dans la population du territoire.`,
        icone: ICONES_SVG.tauxPauvrete,
        indicateurs: [INDICATEURS.tauxPauvrete]
    },
    {
        id: IDS_DOMAINES.aideAlimentaire,
        idMaillon: IDS_MAILLONS.consommation,
        type: 'indicateur',
        libelle: "Recours à l'aide alimentaire",
        description: htmlstring`Ce chapitre aborde la question de l'aide alimentaire en France.`,
        icone: ICONES_SVG.aideAlimentaire,
        indicateurs: [INDICATEURS.aideAlimentaire]
    },
    {
        id: IDS_DOMAINES.obesite,
        idMaillon: IDS_MAILLONS.consommation,
        type: 'indicateur',
        libelle: 'Obésité',
        description: htmlstring`Ce chapitre aborde la prévalence de l'obésité en France.`,
        icone: ICONES_SVG.partPopulationObese,
        indicateurs: [INDICATEURS.partPopulationObese]
    }
];
