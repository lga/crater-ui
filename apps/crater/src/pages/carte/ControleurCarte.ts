import { getApiBaseUrl } from '@lga/commun/build/env/config-baseurl';

import type { ModeleCarte } from '../../modeles/carte/ModeleCarte';
import { chargerHierarchieTerritoires } from '../../requetes-api/requetes-api-territoires';

export class ControleurCarte {
    chargerTerritoirePrincipal(idTerritoirePrincipal: string, domaineCarte: ModeleCarte): Promise<ModeleCarte> {
        return chargerHierarchieTerritoires(getApiBaseUrl(), idTerritoirePrincipal).then((h) => {
            domaineCarte.positionnerTerritoirePrincipal(h.territoirePrincipal, h.departement?.id);
            return domaineCarte;
        });
    }
}
