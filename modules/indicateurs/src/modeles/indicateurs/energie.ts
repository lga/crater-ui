import type { CodePosteEnergieApi, CodeSourceEnergieApi } from '@lga/specification-api';

interface DefinitionPosteEnergie {
    code: CodePosteEnergieApi;
    libelle: string;
    libelleCourt: string;
    couleur: string;
}

interface DefinitionSourceEnergie {
    code: CodeSourceEnergieApi;
    libelle: string;
    couleur: string;
}

export const DEFINITIONS_POSTES_ENERGIE: DefinitionPosteEnergie[] = [
    {
        code: 'TRACTEURS',
        libelle: 'le carburant pour les tracteurs et machines',
        libelleCourt: 'carburant (tracteurs)',
        couleur: '#b98f17'
    },
    {
        code: 'BATIMENTS_ELEVAGE',
        libelle: "la consommation des bâtiments d'élevage",
        libelleCourt: 'bâtiments d’élevage',
        couleur: '#63190a'
    },
    {
        code: 'IRRIGATION',
        libelle: "le fonctionnement des systèmes d'irrigation",
        libelleCourt: 'irrigation',
        couleur: '#2480bb'
    },
    {
        code: 'SERRES',
        libelle: 'le chauffage des serres',
        libelleCourt: 'chauffage des serres',
        couleur: '#468e2b'
    },
    {
        code: 'ENGRAIS_AZOTES',
        libelle: "la production d'engrais azotés de synthèse",
        libelleCourt: 'engrais azotés',
        couleur: '#77229f'
    },
    {
        code: 'ALIMENTATION_ANIMALE',
        libelle: "la production de l'alimentation animale importée de l'étranger",
        libelleCourt: "alimentation animale importée de l'étranger",
        couleur: '#ce7676'
    },
    {
        code: 'MATERIEL',
        libelle: 'la production du matériel agricole',
        libelleCourt: 'matériel agricole',
        couleur: '#193e56'
    }
];

export const DEFINITIONS_SOURCES_ENERGIE: DefinitionSourceEnergie[] = [
    {
        code: 'CARBURANTS',
        libelle: 'carburants',
        couleur: '#203564'
    },
    {
        code: 'GAZ',
        libelle: 'gaz',
        couleur: '#7f4ba0'
    },
    {
        code: 'ELECTRICITE',
        libelle: 'électricité',
        couleur: '#f0e045'
    },
    {
        code: 'BIOMASSE',
        libelle: 'biomasse',
        couleur: '#4ba072'
    },
    {
        code: 'DIVERS',
        libelle: 'divers (énergie indirecte)',
        couleur: '#888888'
    }
];
