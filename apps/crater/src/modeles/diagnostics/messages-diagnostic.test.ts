import { supprimerEspacesRedondantsEtRetoursLignes } from '@lga/base';
import { CategorieTerritoire, Territoire } from '@lga/territoires';
import { describe, expect, it } from 'vitest';

import { calculerMessageMetaDescriptionDiagnostic, calculerMessageOtex, calculerMessageTerritoire } from './messages-diagnostic';
import { Note, NOTE_VALEUR_WARNING } from './Note';
import { LIBELLE_OTEX_NON_DEFINIE_OU_NON_MAJORITAIRE } from './Otex.js';

describe("Tests des fonctions de calcul des messages au niveau du diagnostic d'un territoire", () => {
    it('Calculer le message information territoire', () => {
        expect(calculerMessageTerritoire(CategorieTerritoire.Region, 99, null, LIBELLE_OTEX_NON_DEFINIE_OU_NON_MAJORITAIRE)).toEqual(``);
        expect(calculerMessageTerritoire(CategorieTerritoire.Region, 99, 5, LIBELLE_OTEX_NON_DEFINIE_OU_NON_MAJORITAIRE)).toEqual(
            `Région peu dense avec une faible part de surface agricole et aucune spécialisation agricole dominante.`
        );
        expect(calculerMessageTerritoire(CategorieTerritoire.Region, 100, 5, 'Otex majoritaire 5 postes')).toEqual(
            `Région de densité moyenne avec une faible part de surface agricole et une spécialisation agricole dominante 'Otex majoritaire 5 postes'.`
        );
        expect(calculerMessageTerritoire(CategorieTerritoire.Region, 200, 5, 'Otex majoritaire 5 postes')).toEqual(
            `Région densément peuplée avec une faible part de surface agricole et une spécialisation agricole dominante 'Otex majoritaire 5 postes'.`
        );
        expect(calculerMessageTerritoire(CategorieTerritoire.Region, 200, 15, 'Otex majoritaire 5 postes')).toEqual(
            `Région densément peuplée avec une part minoritaire de surface agricole et une spécialisation agricole dominante 'Otex majoritaire 5 postes'.`
        );
        expect(calculerMessageTerritoire(CategorieTerritoire.Region, 200, 50, 'Otex majoritaire 5 postes')).toEqual(
            `Région densément peuplée avec une part majoritaire de surface agricole et une spécialisation agricole dominante 'Otex majoritaire 5 postes'.`
        );
    });

    it("Calculer le message sur l'OTEX", () => {
        const territoire = new Territoire('C-1', 'NomCommune', CategorieTerritoire.Commune);
        const territoirePays = new Territoire('P-FR', 'NomPays', CategorieTerritoire.Pays);
        expect(supprimerEspacesRedondantsEtRetoursLignes(calculerMessageOtex(territoire, LIBELLE_OTEX_NON_DEFINIE_OU_NON_MAJORITAIRE))).toEqual(
            `Il n'y a pas de <c-lien href="/methodologie#otex" >spécialisation dominante de la production agricole</c-lien > dans la commune.`
        );
        expect(supprimerEspacesRedondantsEtRetoursLignes(calculerMessageOtex(territoire, 'Otex majoritaire 5 postes'))).toEqual(
            `La <c-lien href="/methodologie#otex" >spécialisation dominante de la production agricole</c-lien > dans la commune est Otex majoritaire 5 postes.`
        );
        expect(supprimerEspacesRedondantsEtRetoursLignes(calculerMessageOtex(territoirePays, 'Otex majoritaire 5 postes'))).toEqual(
            `La <c-lien href="/methodologie#otex" >spécialisation dominante de la production agricole</c-lien > dans le territoire est Otex majoritaire 5 postes. Celle-ci est déterminée selon la répartition des spécialisations des communes appartenant au territoire (OTEX 5 postes partagée par une majorité de communes) :`
        );
    });

    it('Calculer le message meta description du diagnostic', () => {
        const messageTousMaillonsAvecNote = calculerMessageMetaDescriptionDiagnostic(
            'NomTerritoire',
            new Note(1),
            new Note(2),
            new Note(3),
            new Note(4),
            new Note(5),
            new Note(6)
        );
        expect(messageTousMaillonsAvecNote).toEqual(
            'Évaluation pour NomTerritoire : Score Terres agricoles=1/10, Score Agriculteurs & Exploitations=2/10, Score Intrants=3/10, Score Production=4/10, Score Transformation & Distribution=5/10, Score Consommation=6/10'
        );

        const messageMaillonsSansNote = calculerMessageMetaDescriptionDiagnostic(
            'NomTerritoire',
            new Note(5),
            new Note(null),
            new Note(5),
            new Note(5),
            new Note(5),
            new Note(NOTE_VALEUR_WARNING)
        );
        expect(messageMaillonsSansNote).toEqual(
            `Évaluation pour NomTerritoire : Score Terres agricoles=5/10, Score Intrants=5/10, Score Production=5/10, Score Transformation & Distribution=5/10`
        );

        const messageTousMaillonNoteNullOuWarning = calculerMessageMetaDescriptionDiagnostic(
            'NomTerritoire',
            new Note(null),
            new Note(null),
            new Note(NOTE_VALEUR_WARNING),
            new Note(null),
            new Note(null),
            new Note(NOTE_VALEUR_WARNING)
        );
        expect(messageTousMaillonNoteNullOuWarning).toEqual(`Évaluation pour NomTerritoire : aucun score disponible`);
    });
});
