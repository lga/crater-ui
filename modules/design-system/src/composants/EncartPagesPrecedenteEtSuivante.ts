import './Bouton.js';

import { css, html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { ifDefined } from 'lit/directives/if-defined.js';

import { ICONES_DESIGN_SYSTEM } from '../styles/icones-design-system.js';
import { Bouton } from './Bouton.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-encart-pages-precedente-suivante': EncartPagesPrecedenteEtSuivante;
    }
}

@customElement('c-encart-pages-precedente-suivante')
export class EncartPagesPrecedenteEtSuivante extends LitElement {
    static styles? = [
        css`
            :host {
                display: flex;
                width: 100%;
                height: 100%;
                border-top: var(--couleur-neutre-20, gray) solid 2px;
                margin-top: var(--dsem4);
                padding-top: var(--dsem2);
                margin-bottom: var(--dsem2);
                justify-content: space-between;
            }

            .bouton-gauche {
                margin-right: auto;
            }

            .bouton-droit {
                margin-left: auto;
            }
        `
    ];

    @property()
    libellePagePrecedente?: string;

    @property()
    libellePageSuivante?: string;

    @property()
    hrefPagePrecedente?: string;

    @property()
    hrefPageSuivante?: string;

    render() {
        return html`
            ${this.hrefPagePrecedente
                ? html` <c-bouton
                      class="bouton-gauche"
                      libelle=${ifDefined(this.libellePagePrecedente)}
                      libelleCourt="Précédent"
                      href=${this.hrefPagePrecedente}
                      positionIcone=${Bouton.POSITION_ICONE.gauche}
                      type=${Bouton.TYPE.plat}
                      themeCouleur=${Bouton.THEME_COULEUR.accent}
                  >
                      ${ICONES_DESIGN_SYSTEM.flecheGauche}
                  </c-bouton>`
                : ''}
            ${this.hrefPageSuivante
                ? html`<c-bouton
                      class="bouton-droit"
                      libelle=${ifDefined(this.libellePageSuivante)}
                      libelleCourt="Suivant"
                      href=${this.hrefPageSuivante}
                      positionIcone=${Bouton.POSITION_ICONE.droite}
                      type=${Bouton.TYPE.plat}
                      themeCouleur=${Bouton.THEME_COULEUR.accent}
                  >
                      ${ICONES_DESIGN_SYSTEM.flecheDroite}
                  </c-bouton>`
                : ''}
        `;
    }
}
