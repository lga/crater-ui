import type { BoundingBoxApi } from '@lga/specification-api';
import { describe, expect, it } from 'vitest';

import { CategorieTerritoire } from './CategorieTerritoire.js';
import { TerritoireBase } from './TerritoireBase.js';

describe('Test creation territoires', () => {
    const boundingBox: BoundingBoxApi = {
        longitudeMin: 0,
        longitudeMax: 1,
        latitudeMin: 3,
        latitudeMax: 4
    };
    it('test Bretagne ', () => {
        const territoire = new TerritoireBase('bretagne', 'Bretagne', CategorieTerritoire.Region, 'FEMININ_SINGULIER', 'en', boundingBox);
        expect(territoire.nomTerritoireAvecDeterminant).toEqual('la Bretagne');
        expect(territoire.nomTerritoireAvecPrepositionA).toEqual('en Bretagne');
        expect(territoire.nomTerritoireAvecPrepositionDe).toEqual('de la Bretagne');
    });
    it('test Ain', () => {
        const territoire = new TerritoireBase('ain', 'Ain', CategorieTerritoire.Region, 'MASCULIN_SINGULIER', 'dans', boundingBox);
        expect(territoire.nomTerritoireAvecDeterminant).toEqual("l'Ain");
        expect(territoire.nomTerritoireAvecPrepositionA).toEqual("dans l'Ain");
        expect(territoire.nomTerritoireAvecPrepositionDe).toEqual("de l'Ain");
    });
    it('test Pays de la Loire', () => {
        const territoire = new TerritoireBase(
            'pays-de-la-loire',
            'Pays de la Loire',
            CategorieTerritoire.Region,
            'MASCULIN_PLURIEL',
            'dans',
            boundingBox
        );
        expect(territoire.nomTerritoireAvecDeterminant).toEqual('les Pays de la Loire');
        expect(territoire.nomTerritoireAvecPrepositionA).toEqual('dans les Pays de la Loire');
        expect(territoire.nomTerritoireAvecPrepositionDe).toEqual('des Pays de la Loire');
    });
});
