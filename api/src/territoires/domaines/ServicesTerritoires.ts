import type { CodeSousCategorieTerritoireApi } from '@lga/specification-api';
import { valeursCodeCategorieTerritoire, valeursCodeSousCategorieTerritoire } from '@lga/specification-api';
import type { CodeCategorieTerritoireApi } from '@lga/specification-api/build/specification/api/index.js';

import { StockageMemoireIndexParId } from '../../communs/servicesUtils';
import { Commune, RegroupementCommunes, type Territoire } from './entitesTerritoires';

type CritereFiltre = CodeCategorieTerritoireApi | CodeSousCategorieTerritoireApi;
type CritereTri = CodeSousCategorieTerritoireApi;

interface DefinitionMotReserve {
    motReserve: string;
    filtre: CritereFiltre;
    tri?: CritereTri;
}
// Ces mots clés particuliers peuvent etre présents au début du critère de recherche pour filtrer/trier les catégories/sous categories de territoires
// Rq : les mots clés ne doivent pas être strictement égaux à des noms de territoires existant (ex : "PAU") sinon on ne pourrait plus trouver ces derniers
// Rq : si des mots clés se recoupent (ex : "PAYS" et "PAYS PETR", il faut déclarer les plus long en 1er, pour matcher au mieux le critère
export const MOTS_RESERVES: DefinitionMotReserve[] = [
    { motReserve: 'COMMUNE', filtre: 'COMMUNE' },
    { motReserve: 'EPCI', filtre: 'EPCI' },
    { motReserve: 'RC', filtre: 'REGROUPEMENT_COMMUNES' },
    { motReserve: 'DEPARTEMENT', filtre: 'DEPARTEMENT' },
    { motReserve: 'REGION', filtre: 'REGION' },
    // Mots clés pour Sous catégories d'EPCIs
    { motReserve: 'CC', filtre: 'EPCI', tri: 'COMMUNAUTE_COMMUNES' },
    { motReserve: 'COMMUNAUTE COMMUNES', filtre: 'EPCI', tri: 'COMMUNAUTE_COMMUNES' },
    { motReserve: 'CA', filtre: 'EPCI', tri: 'COMMUNAUTE_AGGLOMERATION' },
    { motReserve: 'COMMUNAUTE AGGLOMERATION', filtre: 'EPCI', tri: 'COMMUNAUTE_AGGLOMERATION' },
    { motReserve: 'CU', filtre: 'EPCI', tri: 'COMMUNAUTE_URBAINE' },
    { motReserve: 'COMMUNAUTE URBAINE', filtre: 'EPCI', tri: 'COMMUNAUTE_URBAINE' },
    { motReserve: 'METROPOLE', filtre: 'EPCI', tri: 'METROPOLE' },
    // Mots clés pour Regroupements de communes - PAT
    { motReserve: 'PAT', filtre: 'PROJET_ALIMENTAIRE_TERRITORIAL' },
    { motReserve: 'PAIT', filtre: 'PROJET_ALIMENTAIRE_TERRITORIAL' },
    { motReserve: 'PAAT', filtre: 'PROJET_ALIMENTAIRE_TERRITORIAL' },
    { motReserve: 'PROJET ALIMENTAIRE TERRITOIRE', filtre: 'PROJET_ALIMENTAIRE_TERRITORIAL' },
    { motReserve: 'PROJET ALIMENTAIRE TERRITORIAL', filtre: 'PROJET_ALIMENTAIRE_TERRITORIAL' },
    // Mots clés pour Regroupements de communes - PN & PNR
    { motReserve: 'PN', filtre: 'PARC_NATIONAL' },
    { motReserve: 'PARC NATIONAL', filtre: 'PARC_NATIONAL' },
    { motReserve: 'PNR', filtre: 'PARC_NATUREL_REGIONAL' },
    { motReserve: 'PARC NATUREL REGIONAL', filtre: 'PARC_NATUREL_REGIONAL' },
    // Mots clés pour Regroupements de communes - PETR et PAYS
    { motReserve: 'PAYS PETR', filtre: 'PAYS_PETR' },
    { motReserve: 'PETR PAYS', filtre: 'PAYS_PETR' },
    { motReserve: 'PAYS', filtre: 'PAYS_PETR' },
    { motReserve: 'PETR', filtre: 'PAYS_PETR' },
    // Mots clés pour Regroupements de communes - Bassins de vie
    { motReserve: 'BV', filtre: 'BASSIN_DE_VIE_2022' },
    { motReserve: 'BASSIN VIE', filtre: 'BASSIN_DE_VIE_2022' },
    // Mots clés pour Regroupements de communes - SCOT
    { motReserve: 'SCOT', filtre: 'SCHEMA_COHERENCE_TERRITORIAL' },
    { motReserve: 'SCHEMA COHERENCE TERRITORIAL', filtre: 'SCHEMA_COHERENCE_TERRITORIAL' }
];

interface Critere {
    critereNormaliseSansMotsReserves: string;
    filtre?: CritereFiltre;
    tri?: CritereTri;
}

export class ServicesTerritoires {
    private stockageTerritoires: StockageMemoireIndexParId<Territoire> = new StockageMemoireIndexParId<Territoire>();

    ajouterTerritoire(territoire: Territoire): void {
        this.stockageTerritoires.save(territoire.id, territoire);
    }

    rechercherTous(): Territoire[] {
        return this.stockageTerritoires.findAll();
    }

    rechercher(idTerritoire: string): Territoire | undefined {
        return this.stockageTerritoires.findById(idTerritoire);
    }

    rechercherTousParCritere(critere: string, nbMaxResultat: number): Territoire[] {
        const critereNormalise: Critere = ServicesTerritoires.creerCritere(critere);

        const listePreselectionParCategorieOuSousCategorie = this.rechercherSelonFiltreCritere(critereNormalise);

        const REGEX_DE_2_A_5_CHIFFRES = /^\d{2,5}$/;
        if (critereNormalise.critereNormaliseSansMotsReserves.match(REGEX_DE_2_A_5_CHIFFRES)) {
            return this.rechercherParCodePostal(listePreselectionParCategorieOuSousCategorie, critereNormalise);
        } else if (critereNormalise.critereNormaliseSansMotsReserves.length < 3)
            return this.rechercherParCorrespondanceExacteDuNom(
                listePreselectionParCategorieOuSousCategorie,
                critereNormalise.critereNormaliseSansMotsReserves,
                nbMaxResultat
            );
        else {
            return this.rechercherParCorrespondancePartielleDuNom(listePreselectionParCategorieOuSousCategorie, critereNormalise, nbMaxResultat);
        }
    }

    private rechercherParCodePostal(listePreselectionParCategorieOuSousCategorie: Territoire[], critereNormalise: Critere) {
        const resultatCorrespondanceCodePostal = listePreselectionParCategorieOuSousCategorie.filter(
            this.contientUnCodePostalContenant(critereNormalise.critereNormaliseSansMotsReserves)
        );
        return resultatCorrespondanceCodePostal.sort(this.triLongeurNomPuisOrdreAlphabetique);
    }

    private rechercherSelonFiltreCritere(critere: Critere) {
        if (valeursCodeCategorieTerritoire.includes(critere.filtre as CodeCategorieTerritoireApi)) {
            return this.rechercherParCategories([critere.filtre as CodeCategorieTerritoireApi]);
        } else if (valeursCodeSousCategorieTerritoire.includes(critere.filtre as CodeSousCategorieTerritoireApi)) {
            return this.rechercherParSousCategories([critere.filtre as CodeSousCategorieTerritoireApi]);
        } else return this.stockageTerritoires.findAll();
    }

    private rechercherParCategories(filtreParCategoriesTerritoires: CodeCategorieTerritoireApi[]) {
        return this.stockageTerritoires.findAll().filter((t) => {
            return filtreParCategoriesTerritoires.includes(t.categorie);
        });
    }

    private rechercherParSousCategories(filtreParSousCategoriesTerritoires: CodeSousCategorieTerritoireApi[]) {
        return this.stockageTerritoires.findAll().filter((t) => {
            if (t instanceof RegroupementCommunes) {
                return filtreParSousCategoriesTerritoires.includes(t.sousCategorie);
            } else return false;
        });
    }

    private rechercherParCorrespondanceExacteDuNom(listeTerritoires: Territoire[], critereNormalise: string, nbMaxResultat: number) {
        return listeTerritoires.filter((t) => ServicesTerritoires.normaliserString(t.nom) === critereNormalise).slice(0, nbMaxResultat);
    }

    private rechercherParCorrespondancePartielleDuNom(listeTerritoires: Territoire[], critere: Critere, nbMaxResultat: number) {
        const resultat = new Set<Territoire>();

        const resultatCorrespondanceMotsEntiersSaufDernier = listeTerritoires.filter((t) => {
            const REGEX_MOTS_ENTIERS_SAUF_DERNIER = new RegExp(`\\b${critere.critereNormaliseSansMotsReserves.split(' ').join('\\b.*\\b')}`);
            return REGEX_MOTS_ENTIERS_SAUF_DERNIER.test(ServicesTerritoires.normaliserString(t.nom));
        });
        const resultatCorrespondanceTousMotsEntiers = resultatCorrespondanceMotsEntiersSaufDernier.filter((t) => {
            const REGEX_TOUS_MOTS_ENTIERS = new RegExp(`\\b${critere.critereNormaliseSansMotsReserves.split(' ').join('\\b.*\\b')}\\b`);
            return REGEX_TOUS_MOTS_ENTIERS.test(ServicesTerritoires.normaliserString(t.nom));
        });

        resultatCorrespondanceTousMotsEntiers.sort(this.creerFonctionTriSelonCritere(critere)).forEach((t) => resultat.add(t));
        resultatCorrespondanceMotsEntiersSaufDernier.sort(this.creerFonctionTriSelonCritere(critere)).forEach((t) => resultat.add(t));

        return Array.from(resultat).slice(0, nbMaxResultat);
    }

    private creerFonctionTriSelonCritere(critere: Critere) {
        return critere.tri
            ? (t1: Territoire, t2: Territoire) => {
                  if (t1 instanceof RegroupementCommunes && t1.sousCategorie === critere.tri) {
                      if (t2 instanceof RegroupementCommunes && t2.sousCategorie === critere.tri) {
                          return this.triLongeurNomPuisOrdreAlphabetique(t1, t2);
                      } else {
                          return -1;
                      }
                  } else if (t2 instanceof RegroupementCommunes && t2.sousCategorie === critere.tri) {
                      return 1;
                  } else {
                      return this.triLongeurNomPuisOrdreAlphabetique(t1, t2);
                  }
              }
            : this.triLongeurNomPuisOrdreAlphabetique;
    }

    private triLongeurNomPuisOrdreAlphabetique = (t1: Territoire, t2: Territoire) => {
        const compareLength = t1.nom.length - t2.nom.length;
        if (compareLength == 0) {
            return t1.nom.localeCompare(t2.nom);
        } else {
            return compareLength;
        }
    };

    private contientUnCodePostalContenant(codePostalPartiel: string) {
        return (territoire: Territoire) => {
            if (territoire instanceof Commune) {
                const commune = territoire;
                return commune.codesPostaux.startsWith(codePostalPartiel) || commune.codesPostaux.includes(codePostalPartiel);
            }
            return false;
        };
    }

    static normaliserString(chaine: string): string {
        return (
            chaine
                .toUpperCase()
                .replace(/\u0152/g, 'OE') // cas du Œ
                .replace(/\u00C6/g, 'AE') // cas du Æ
                .replace(/['|-]/g, ' ') // remplacer ' et - par des espaces
                .normalize('NFD') // remplacer les caractères accentués par leur équivalent sans accent
                .replace(/[\u0300-\u036f]/g, '') // supprimer certains caractère spéciaux
                // "D " et "L " permettent de traiter les mots "D'" et "L'" dans le cas ou l'apostrophe a été supprimée
                .replace(/\bDE\b(?!$)|\bDU\b(?!$)|\bDES\b(?!$)|\bD\b(?!$)/g, ' ')
                .replace(/\bLE\b(?!$)|\bLA\b(?!$)|\bLES\b(?!$)|\bL\b(?!$)/g, ' ')
                .replace(/[ ]+/g, ' ') // remplacer n espaces par un seul espace
                .trim()
        );
    }

    static creerCritere(critereInitial: string): Critere {
        const critereNormalise = ServicesTerritoires.normaliserString(critereInitial);

        const definitionMotReserve = MOTS_RESERVES.find((mr) => critereNormalise.startsWith(mr.motReserve + ' '));

        return definitionMotReserve
            ? {
                  critereNormaliseSansMotsReserves: critereNormalise.split(definitionMotReserve.motReserve)[1].trim(),
                  filtre: definitionMotReserve.filtre,
                  tri: definitionMotReserve.tri
              }
            : {
                  critereNormaliseSansMotsReserves: critereNormalise
              };
    }
}
