import '@lga/design-system/public/theme-defaut/theme-defaut.css';
import '../../src/pages/commun/EncartLienVersPage.js';

import { html } from 'lit';
import { unsafeSVG } from 'lit/directives/unsafe-svg.js';

import { POSITION_ILLUSTRATION } from '../../src/pages/commun/EncartLienVersPage';

describe('Test EncartLienPage', () => {
    it('Test affichage desktop', () => {
        // given
        cy.viewport(1100, 500);
        cy.mount<'c-encart-lien-vers-page'>(
            html` <c-encart-lien-vers-page
                numero="01"
                titre="Un modèle agro-industriel mondialisé qui atteint ses limites"
                positionIllustration=${POSITION_ILLUSTRATION.droite}
            >
                <span slot="texte"
                    >Produits alimentaires standardisés, concentration et spécialisation d'acteurs opérant sur un marché mondialisé.</span
                >
                <div slot="illustration">${unsafeSVG(illustrationExemple)}</div>
            </c-encart-lien-vers-page>`
        );
        // then
        cy.get('c-encart-lien-vers-page').should('exist');
        cy.get('c-encart-lien-vers-page').find('article').should('exist');
        cy.get('c-encart-lien-vers-page').find('header').should('exist');
        cy.get('c-encart-lien-vers-page').find('p').should('exist');
        cy.get('c-encart-lien-vers-page').find('figure').should('exist');
        cy.get('c-encart-lien-vers-page').find('c-bouton').should('exist');
        cy.get('c-encart-lien-vers-page').find('div[slot=illustration]').find('svg').should('be.visible');
    });
    it('Test affichage desktop avec image à gauche', () => {
        //given
        cy.viewport(1100, 500);
        cy.mount<'c-encart-lien-vers-page'>(
            html` <c-encart-lien-vers-page
                numero="02"
                titre="Un modèle agro-industriel mondialisé qui atteint ses limites"
                positionIllustration=${POSITION_ILLUSTRATION.gauche}
            >
                <span slot="texte"
                    >Produits alimentaires standardisés, concentration et spécialisation d'acteurs opérant sur un marché mondialisé.</span
                >
                <div slot="illustration">${unsafeSVG(illustrationExemple)}</div>
            </c-encart-lien-vers-page>`
        );
        //then
        cy.get('c-encart-lien-vers-page').find('article').should('have.class', 'gauche');
        cy.get('c-encart-lien-vers-page').find('article.gauche').should('have.css', 'flex-direction', 'row-reverse');
        cy.get('c-encart-lien-vers-page').find('div[slot=illustration]').find('svg').should('be.visible');
    });
    it('Test affichage desktop sans illustration', () => {
        //given
        cy.viewport(1100, 500);
        cy.mount<'c-encart-lien-vers-page'>(
            html` <c-encart-lien-vers-page numero="02" titre="Un modèle agro-industriel mondialisé qui atteint ses limites">
                <span slot="texte"
                    >Produits alimentaires standardisés, concentration et spécialisation d'acteurs opérant sur un marché mondialisé.</span
                >
            </c-encart-lien-vers-page>`
        );
        //then
        cy.get('c-encart-lien-vers-page').find('figure').should('not.be.visible');
    });
    it('Test affichage mobile', () => {
        //given
        cy.viewport(300, 500);
        cy.mount<'c-encart-lien-vers-page'>(
            html` <c-encart-lien-vers-page numero="01" titre="Un modèle agro-industriel mondialisé qui atteint ses limites">
                <span slot="texte"
                    >Produits alimentaires standardisés,
                    <strong>concentration et spécialisation d'acteurs opérant sur un marché mondialisé.</strong></span
                >
                <div slot="illustration">${unsafeSVG(illustrationExemple)}</div>
            </c-encart-lien-vers-page>`
        );
        //then
        cy.get('c-encart-lien-vers-page').should('exist');
        cy.get('c-encart-lien-vers-page').find('article').should('have.css', 'flex-direction', 'column-reverse');
        cy.get('c-encart-lien-vers-page').find('div[slot=illustration]').find('svg').should('be.visible');
    });
});

// Changer les dimensions en % pour rendre SVG responsive
const illustrationExemple = `
<svg width=100% height=100% viewBox="0 0 20 10" preserveAspectRatio="none">
    <polygon fill=red points="0,10 20,10 10,0" />
</svg>`;
