import { EvenementPersonnalisable } from '@lga/design-system/build/evenements/EvenementPersonnalisable';

export class EvenementNaviguer extends EvenementPersonnalisable<{ url: string; cible?: string }> {
    static ID = 'naviguer';
    constructor(url: string, cible = '_self') {
        super(EvenementNaviguer.ID, { url: url, cible: cible });
    }
}
