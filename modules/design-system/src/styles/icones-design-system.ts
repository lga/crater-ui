import { html } from 'lit';

export const ICONES_DESIGN_SYSTEM = {
    croix: html`<svg slot="icone" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
        <path d="M8 8L16 16" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
        <path d="M16 8L8 16" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
    </svg>`,
    check: html`<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M17.3152 9L10.7008 15.6145L6.72449 11.6508" stroke="#162A41" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
    </svg>`,
    flecheDroite: html`<svg slot="icone" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
        <path d="M19 12H5" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
        <path d="M14 17L19 12" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
        <path d="M14 7L19 12" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
    </svg>`,
    flecheGauche: html`<svg slot="icone" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M5 12H19" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
        <path d="M10 7L5 12" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
        <path d="M10 17L5 12" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
    </svg>`,
    flecheBasDroit: html`<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
        <g>
            <path d="M16.95 16.9498L7.05005 7.0498" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
            <path d="M16.9501 9.87695V16.95L9.87805 16.949" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
        </g>
    </svg>`,
    flecheHautDroit: html`<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M16.95 7.0502L7.05005 16.9502" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
        <path d="M9.87708 7.05007H16.9501L16.9491 14.1221" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
    </svg>`,
    lienExterne: html`<svg slot="icone" width="24" height="24" viewBox="0 0 24 24" fill="#FF" xmlns="http://www.w3.org/2000/svg">
        <path
            fill-rule="evenodd"
            clip-rule="evenodd"
            d="M17.3332 13C17.3332 12.4478 17.781 12 18.3332 12C18.8854 12 19.3332 12.4478 19.3332 13V19C19.3332 20.6569 17.9901 22 16.3332 22H5.33334C3.67644 22 2.33337 20.6569 2.33337 19V8C2.33337 6.34309 3.67644 5 5.33334 5H11.3333C11.8855 5 12.3333 5.44777 12.3333 6C12.3333 6.55223 11.8855 7 11.3333 7H5.33334C4.78111 7 4.33335 7.44777 4.33335 8V19C4.33335 19.5522 4.78111 20 5.33334 20H16.3332C16.8854 20 17.3332 19.5522 17.3332 19V13Z"
        />
        <path
            fill-rule="evenodd"
            clip-rule="evenodd"
            d="M20.3332 4H15.3332C14.781 4 14.3333 3.55223 14.3333 3C14.3333 2.44777 14.781 2 15.3332 2H21.3332C21.8854 2 22.3332 2.44777 22.3332 3V9C22.3332 9.55223 21.8854 10 21.3332 10C20.7809 10 20.3332 9.55223 20.3332 9V4Z"
        />
        <path
            fill-rule="evenodd"
            clip-rule="evenodd"
            d="M11.0407 14.7071C10.6501 15.0975 10.017 15.0975 9.62641 14.7071C9.23602 14.3165 9.23602 13.6834 9.62641 13.2928L20.6263 2.2928C21.0169 1.9024 21.6499 1.9024 22.0406 2.2928C22.431 2.68342 22.431 3.31646 22.0406 3.70709L11.0407 14.7071Z"
        />
    </svg>`,
    loupe: html`<svg slot="icone" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path
            fill-rule="evenodd"
            clip-rule="evenodd"
            d="M11.0586 18.1206C14.9588 18.1206 18.1206 14.9588 18.1206 11.0586C18.1206 7.15844 14.9588 3.9967 11.0586 3.9967C7.15844 3.9967 3.9967 7.15844 3.9967 11.0586C3.9967 14.9588 7.15844 18.1206 11.0586 18.1206Z"
            stroke="#000000"
            stroke-width="2"
            stroke-linecap="round"
            stroke-linejoin="round"
        />
        <path d="M20.0033 20.0033L16.0516 16.0516" stroke="#000000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
    </svg>`,
    question: html`
        <svg slot="icone" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
            <path
                fill-rule="evenodd"
                clip-rule="evenodd"
                d="M12 21V21C7.029 21 3 16.971 3 12V12C3 7.029 7.029 3 12 3V3C16.971 3 21 7.029 21 12V12C21 16.971 16.971 21 12 21Z"
                stroke-width="1"
                stroke-linecap="round"
                stroke-linejoin="round"
            />
            <path
                d="M12 13.25V13C12 12.183 12.505 11.74 13.011 11.4C13.505 11.067 14 10.633 14 9.83301C14 8.72801 13.105 7.83301 12 7.83301C10.895 7.83301 10 8.72801 10 9.83301"
                fill="none"
                stroke-width="1"
                stroke-linecap="round"
                stroke-linejoin="round"
            />
            <path
                d="M11.999 16C11.861 16 11.749 16.112 11.75 16.25C11.75 16.388 11.862 16.5 12 16.5C12.138 16.5 12.25 16.388 12.25 16.25C12.25 16.112 12.138 16 11.999 16"
                stroke-width="1"
                fill="none"
                stroke-linecap="round"
                stroke-linejoin="round"
            />
        </svg>
    `,
    telecharger: html`
        <svg slot="icone" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M3 16V19C3 20.1046 3.89543 21 5 21H12" stroke-width="2" stroke-linecap="round" />
            <path d="M12 21H19C20.1046 21 21 20.1046 21 19V16" stroke-width="2" stroke-linecap="round" />
            <path d="M12 17L12 3" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
            <path d="M7 12L12 17" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
            <path d="M17 12L12 17" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
        </svg>
    `
};
