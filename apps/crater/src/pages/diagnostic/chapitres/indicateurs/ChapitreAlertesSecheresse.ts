import '../composants/ChapitreDomaine.js';
import '../../../commun/PageSablierCrater.js';
import '../../charts/ChartBarresSeriesTemporelles.js';
import '../../charts/ChartBarresAvecCourbeTendance.js';

import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { IDS_DOMAINES, INDICATEURS } from '@lga/indicateurs';
import { SA } from '@lga/indicateurs';
import { html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import type { Secheresse } from '../../../../modeles/diagnostics/eau/Secheresse';
import { AOUT, JUILLET } from '../../../../modeles/diagnostics/IndicateursParAnneesMois';
import { EtatRapport, type Rapport } from '../../../../modeles/diagnostics/Rapport.js';
import { construireLienVersSourcesIndicateur } from '../../../commun/liens-utils.js';
import { Serie, SerieMultiple } from '../../charts/chart-outils';
import type { OptionsChartBarresAvecCourbeTendance } from '../../charts/ChartBarresAvecCourbeTendance';
import type { OptionsChartBarresSeriesTemporelles } from '../../charts/ChartBarresSeriesTemporelles';
import type { DonneesPageDiagnostic } from '../../PageDiagnostic.js';
import type { OptionsDomaineDetaille } from '../composants/ChapitreDomaine';

declare global {
    interface HTMLElementTagNameMap {
        'c-chapitre-alertes-secheresse': ChapitreAlertesSecheresse;
    }
}

// FIXME : constantes temporaires, a extraire qq part dans le demain (ds classe Indicateur ?)
const ANNEE_MIN_SECHERESSE = 2012;
const ANNEE_MAX_SECHERESSE = 2022;

@customElement('c-chapitre-alertes-secheresse')
export class ChapitreAlertesSecheresse extends LitElement {
    static styles = [STYLES_DESIGN_SYSTEM];

    @property({ attribute: false })
    donneesPage?: DonneesPageDiagnostic;

    @property({ attribute: false })
    rapport?: Rapport;

    render() {
        if (this.rapport?.getEtat() === EtatRapport.PRET) {
            const indicateursSecheresse = this.rapport.getDiagnosticActif()?.intrants.eau.secheresse;

            return html`<c-chapitre-domaine
                .options=${{
                    domaine: SA.getDomaine(IDS_DOMAINES.secheresses),
                    message: indicateursSecheresse?.calculerMessageImpactArretesSecheresse(ANNEE_MIN_SECHERESSE, ANNEE_MAX_SECHERESSE),
                    territoire: this.rapport.getTerritoireActif(),
                    estPagePourTef: this.donneesPage?.estPagePourTef
                } as OptionsDomaineDetaille}
                contenuAdhoc
            >
                <div slot="contenu">
                    <c-figure .titre="${construireLienVersSourcesIndicateur(INDICATEURS.alertesSecheresse)}">
                        <c-chart-barres-avec-courbe-tendance
                            slot="chart-figure"
                            .options=${this.calculerOptionsChartArretesSecheresseAnnees(indicateursSecheresse)}
                        ></c-chart-barres-avec-courbe-tendance>
                    </c-figure>

                    <p>Le détail par mois depuis ${ANNEE_MIN_SECHERESSE} est présenté ci-dessous :</p>

                    <c-figure .titre="${construireLienVersSourcesIndicateur(INDICATEURS.alertesSecheresse)}">
                        <c-chart-barres-series-temporelles
                            slot="chart-figure"
                            .options=${this.calculerOptionsChartArreteSecheresseMoisAnnees(indicateursSecheresse)}
                        ></c-chart-barres-series-temporelles>
                    </c-figure>
                </div>
            </c-chapitre-domaine>`;
        } else {
            return html`<c-page-sablier-crater></c-page-sablier-crater>`;
        }
    }

    private calculerOptionsChartArretesSecheresseAnnees(indicateursSecheresse: Secheresse | undefined): OptionsChartBarresAvecCourbeTendance {
        const valeursMoyennesParAnnees = indicateursSecheresse?.indicateursParAnneesMois.valeursMoyennesParAnnees(
            [JUILLET, AOUT],
            (i) => i.partTerritoireEnArretePourcent,
            ANNEE_MIN_SECHERESSE,
            ANNEE_MAX_SECHERESSE
        );
        const series = new SerieMultiple(
            [
                new Serie(
                    valeursMoyennesParAnnees?.valeurs((i) => i.valeurMoyenne),
                    'Part du territoire en arrêté sécheresse, moyenne juillet-août'
                )
            ],
            valeursMoyennesParAnnees?.annees
        );

        return {
            series: series,
            nomAxeOrdonnees: 'Part du territoire [%]',
            maxY: 100
        };
    }

    private calculerOptionsChartArreteSecheresseMoisAnnees(indicateursSecheresse: Secheresse | undefined): OptionsChartBarresSeriesTemporelles {
        const series = new SerieMultiple(
            [
                new Serie(
                    indicateursSecheresse?.indicateursParAnneesMois.valeurs((i) => i.partTerritoireEnArretePourcent),
                    'Part du territoire en arrêté sécheresse, moyenne par mois'
                )
            ],
            indicateursSecheresse?.indicateursParAnneesMois.anneesMois
        );

        return {
            series: series,
            minX: new Date(ANNEE_MIN_SECHERESSE, 0),
            maxX: new Date(ANNEE_MAX_SECHERESSE, 11),
            maxY: 100,
            nomAxeOrdonnees: 'Part du territoire [%]',
            texteNoData: ''
        };
    }
}
