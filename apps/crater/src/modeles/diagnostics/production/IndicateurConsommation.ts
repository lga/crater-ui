import { arrondirANDecimales } from '@lga/base';
import { GroupeCulture } from '@lga/indicateurs';
import type { CodeGroupeCultureApi } from '@lga/specification-api/build/specification/api/index.js';

function ajouter(a: number | null, b: number | null): number | null {
    if (a === null && b === null) {
        return null;
    } else {
        return (a ?? 0) + (b ?? 0);
    }
}

export class ConsommationParCulture {
    constructor(
        public readonly codeCulture: string,
        public readonly nomCulture: string,
        public readonly alimentationAnimale: boolean,
        public readonly consommationHa: number | null
    ) {}
}

export class ConsommationParGroupeCulture {
    constructor(
        public readonly codeGroupeCulture: CodeGroupeCultureApi,
        public readonly nomGroupeCulture: string,
        public readonly consommationHa: number | null,
        public readonly tauxAdequationBrutPourcent: number | null,
        // TODO : le champ besoinsTotauxTousGroupesCulturesHa est temporaire, a remplacer par partDansBesoinsTotaux quand ce dernier sera fourni par l'api
        public readonly consommationTotalTousGroupesCulturesHa: number | null,
        public readonly consommationParCulture: ConsommationParCulture[]
    ) {}

    get groupeCulture(): GroupeCulture {
        return GroupeCulture.fromString(this.codeGroupeCulture);
    }

    get partDansBesoinsTotaux(): number | null {
        if (
            this.consommationHa === null ||
            this.consommationTotalTousGroupesCulturesHa === null ||
            this.consommationTotalTousGroupesCulturesHa === 0
        ) {
            return null;
        } else {
            return (this.consommationHa / this.consommationTotalTousGroupesCulturesHa) * 100;
        }
    }

    get besoinsHumainsHa() {
        return this.consommationParCulture
            .filter((bc) => !bc.alimentationAnimale)
            .map((bc) => bc.consommationHa)
            .reduce(ajouter, 0);
    }

    get besoinsAnimauxHa() {
        return this.consommationParCulture
            .filter((bc) => bc.alimentationAnimale)
            .map((bc) => bc.consommationHa)
            .reduce(ajouter, 0);
    }

    get partBesoinsHumainsHa(): number | null {
        if (this.consommationHa === null || this.besoinsHumainsHa === null || this.consommationHa === 0) {
            return null;
        } else {
            return arrondirANDecimales((this.besoinsHumainsHa / this.consommationHa) * 100);
        }
    }

    get partBesoinsAnimauxHa(): number | null {
        if (this.consommationHa === null || this.besoinsAnimauxHa === null || this.consommationHa === 0) {
            return null;
        } else {
            return arrondirANDecimales((this.besoinsAnimauxHa / this.consommationHa) * 100);
        }
    }
}

export class IndicateurConsommation {
    constructor(
        public readonly consommationHa: number | null,
        private _consommationsParGroupeCultures: ConsommationParGroupeCulture[]
    ) {}

    public get consommationsParGroupeCultures(): ConsommationParGroupeCulture[] {
        return this._consommationsParGroupeCultures.sort((b1, b2) =>
            GroupeCulture.fromString(b1.codeGroupeCulture).comparer(GroupeCulture.fromString(b2.codeGroupeCulture))
        );
    }

    public get consommationHumainsParGroupeCulture() {
        return this._consommationsParGroupeCultures
            .map((bgc) => {
                const besoinsHumainsParCulture = bgc.consommationParCulture.filter((bc) => !bc.alimentationAnimale);
                const besoinsHumainsHa = besoinsHumainsParCulture.map((bc) => bc.consommationHa).reduce(ajouter, 0);
                return new ConsommationParGroupeCulture(
                    bgc.codeGroupeCulture,
                    bgc.nomGroupeCulture,
                    besoinsHumainsHa,
                    bgc.tauxAdequationBrutPourcent,
                    bgc.consommationTotalTousGroupesCulturesHa,
                    besoinsHumainsParCulture
                );
            })
            .filter((b) => b.besoinsHumainsHa != 0);
    }

    public get consommationAnimauxParGroupeCulture() {
        return this._consommationsParGroupeCultures
            .map((bgc) => {
                const besoinsAnimauxParCulture = bgc.consommationParCulture.filter((bc) => bc.alimentationAnimale);
                const besoinsAnimauxHa = besoinsAnimauxParCulture.map((bc) => bc.consommationHa).reduce(ajouter, 0);
                return new ConsommationParGroupeCulture(
                    bgc.codeGroupeCulture,
                    bgc.nomGroupeCulture,
                    besoinsAnimauxHa,
                    bgc.tauxAdequationBrutPourcent,
                    bgc.consommationTotalTousGroupesCulturesHa,
                    besoinsAnimauxParCulture
                );
            })
            .filter((b) => b.besoinsAnimauxHa != 0);
    }

    public get consommationAnimauxHa() {
        return this.consommationAnimauxParGroupeCulture.map((b) => b.besoinsAnimauxHa).reduce(ajouter, 0);
    }
    public get consommationHumainsHa() {
        return this.consommationHumainsParGroupeCulture.map((b) => b.besoinsHumainsHa).reduce(ajouter, 0);
    }
    public get pourcentageConsommationAnimauxSurConsommationTotale(): number | null {
        if (this.consommationAnimauxHa === null || this.consommationHa === null || this.consommationHa === 0) {
            return null;
        } else {
            return arrondirANDecimales((this.consommationAnimauxHa / this.consommationHa) * 100);
        }
    }
}
