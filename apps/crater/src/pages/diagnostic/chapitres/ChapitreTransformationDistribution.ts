import '../../commun/TimeLine.js';
import '../charts/ChartBarres.js';
import '../charts/ChartDistancesAuxCommerces.js';
import './composants/CommentAgir.js';
import './composants/ChapitreDomaine.js';
import './composants/EncartResumeDomaine.js';
import './composants/PourquoiCestImportant.js';
import './composants/VoirEgalement.js';
import './SyntheseMaillon.js';
import '@lga/design-system/build/composants/EncartPagesPrecedenteEtSuivante.js';

import { arrondirANDecimales, formaterNombreSelonValeurString } from '@lga/base';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { INDICATEURS } from '@lga/indicateurs';
import { IDS_MAILLONS } from '@lga/indicateurs';
import { IDS_DOMAINES } from '@lga/indicateurs';
import { SA } from '@lga/indicateurs';
import { html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { ifDefined } from 'lit/directives/if-defined.js';

import { construireUrlPageDiagnosticMaillon } from '../../../configuration/pages/pages-utils.js';
import type { Rapport, TransformationDistribution } from '../../../modeles/diagnostics';
import { STYLES_CRATER } from '../../commun/pages-styles';
import { STYLES_CHAPITRES_DIAGNOSTIC } from '../diagnostic-utils.js';
import type { DonneesPageDiagnostic } from '../PageDiagnostic.js';
import { creerOptionsSyntheseMaillon } from './chapitres-outils';
import type { OptionsEncartResumeDomaine } from './composants/EncartResumeDomaine';

declare global {
    interface HTMLElementTagNameMap {
        'c-chapitre-transformation-distribution': ChapitreTransformationDistribution;
    }
}

@customElement('c-chapitre-transformation-distribution')
export class ChapitreTransformationDistribution extends LitElement {
    static styles = [STYLES_DESIGN_SYSTEM, STYLES_CRATER, STYLES_CHAPITRES_DIAGNOSTIC];

    @property({ attribute: false })
    donneesPage?: DonneesPageDiagnostic;

    @property({ attribute: false })
    rapport?: Rapport;

    @property({ type: Boolean })
    affichagePDF? = false;

    private readonly maillon = SA.getMaillon(IDS_MAILLONS.transformationDistribution);

    render() {
        if (!this.rapport) return;

        const diagTD = this.rapport.getDiagnosticActif()!.transformationDistribution;

        return html`
            <section class="${this.affichagePDF ? 'PDF' : ''}">
                <h1>${this.maillon?.nom}</h1>

                <c-synthese-maillon
                    .options=${creerOptionsSyntheseMaillon(
                        this.rapport,
                        this.rapport.getDiagnosticActif()!.transformationDistribution,
                        IDS_MAILLONS.transformationDistribution,
                        INDICATEURS.scoreTransformationDistribution.id
                    )}
                ></c-synthese-maillon>

                <h2 id="pourquoi-cest-important">Pourquoi c'est important ?</h2>
                <c-pourquoi-cest-important .maillon=${this.maillon}></c-pourquoi-cest-important>

                <h2 id="etat-lieux">Quel est l'état des lieux ?</h2>
                <div class="domaines-resumes">
                    <p>
                        <span>Quelle dépendance au pétrole de la population pour ses achats alimentaires ?</span>
                    </p>
                    <c-timeline></c-timeline>
                    <div class="bloc-domaines-resumes">
                        <c-encart-resume-domaine
                            .options=${this.calculerOptionsEncartResumeDomainePartPopulationDependanteVoiture(diagTD, this.donneesPage)}
                        ></c-encart-resume-domaine>
                        <c-encart-resume-domaine
                            .options=${this.calculerOptionsEncartResumeDomainePartTerritoireDependanteVoiture(diagTD, this.donneesPage)}
                        ></c-encart-resume-domaine>
                        <c-encart-resume-domaine
                            .options=${this.calculerOptionsEncartResumeDomaineDistancesAuxCommerces(diagTD, this.donneesPage)}
                        ></c-encart-resume-domaine>
                    </div>
                </div>

                <h2 id="leviers-action">Comment ma collectivité peut-elle s&#39;améliorer ?</h2>
                <c-comment-agir
                    .maillon=${this.maillon}
                    idTerritoireParcel=${ifDefined(this.rapport.getDiagnosticActif()?.idTerritoireParcel ?? undefined)}
                ></c-comment-agir>

                <c-encart-pages-precedente-suivante
                    libellePagePrecedente=${ifDefined(SA.getMaillon(IDS_MAILLONS.production)?.nom)}
                    hrefPagePrecedente=${construireUrlPageDiagnosticMaillon(
                        IDS_MAILLONS.production,
                        this.donneesPage?.idTerritoirePrincipal ?? '',
                        this.donneesPage?.idEchelleTerritoriale
                    )}
                    libellePageSuivante=${ifDefined(SA.getMaillon(IDS_MAILLONS.consommation)?.nom)}
                    hrefPageSuivante=${construireUrlPageDiagnosticMaillon(
                        IDS_MAILLONS.consommation,
                        this.donneesPage?.idTerritoirePrincipal ?? '',
                        this.donneesPage?.idEchelleTerritoriale
                    )}
                ></c-encart-pages-precedente-suivante>
            </section>
        `;
    }

    private calculerOptionsEncartResumeDomainePartPopulationDependanteVoiture(
        diagTD: TransformationDistribution,
        donneesPage: DonneesPageDiagnostic | undefined
    ): OptionsEncartResumeDomaine {
        return {
            domaine: SA.getDomaine(IDS_DOMAINES.dependancePopulationVoiture)!,
            chiffresCles: [
                {
                    libelleAvantChiffre: '',
                    chiffre: `${diagTD.partPopulationDependanteVoiturePourcent!} %`,
                    libelleApresChiffre: 'de la population est théoriquement dépendante de la voiture pour ses achats alimentaires'
                }
            ],
            donneesPage: donneesPage
        };
    }

    private calculerOptionsEncartResumeDomainePartTerritoireDependanteVoiture(
        diagTD: TransformationDistribution,
        donneesPage: DonneesPageDiagnostic | undefined
    ): OptionsEncartResumeDomaine {
        return {
            domaine: SA.getDomaine(IDS_DOMAINES.dependanceTerritoireVoiture)!,
            chiffresCles: [
                {
                    libelleAvantChiffre: 'sur',
                    chiffre: `${diagTD.partTerritoireDependantVoiturePourcent!} %`,
                    libelleApresChiffre:
                        'du territoire, plus de la moitié de la population est théoriquement dépendante de la voiture pour ses achats alimentaires'
                }
            ],
            donneesPage: donneesPage
        };
    }

    private calculerOptionsEncartResumeDomaineDistancesAuxCommerces(
        diagTD: TransformationDistribution,
        donneesPage: DonneesPageDiagnostic | undefined
    ): OptionsEncartResumeDomaine {
        const valeurDistancePlusProcheCommerce =
            diagTD.indicateursProximiteCommerceGeneraliste.distancePlusProcheCommerceMetres !== null
                ? arrondirANDecimales(diagTD.indicateursProximiteCommerceGeneraliste.distancePlusProcheCommerceMetres / 1000, 1)
                : null;
        return {
            domaine: SA.getDomaine(IDS_DOMAINES.distancesPlusProchesCommerces)!,
            chiffresCles: [
                {
                    libelleAvantChiffre: '',
                    chiffre: `${formaterNombreSelonValeurString(valeurDistancePlusProcheCommerce)} km`,
                    libelleApresChiffre: `distance moyenne à vol d'oiseau entre le domicile et le plus proche commerce <strong>généraliste</strong>`
                }
            ],
            donneesPage: donneesPage
        };
    }
}
