import '@lga/design-system/public/theme-defaut/theme-defaut.css';
import '../../../src/pages/diagnostic/charts/ChartTreemapDoubleCultures';

import { html } from 'lit';

import { metropoleDuGrandParis, occitanie, toulouseMetropole } from '../../fixtures/treemapProductionBesoins';

const LARGEUR_PAR_DEFAUT_PIXELS = 250;
const HAUTEUR_MAX_PIXELS = 350;
const TAILLE_MIN_CARRE_PIXELS = 15;

describe('Test ChartTreemapDoubleProductionBesoins', () => {
    context('Adequation theorique production consommation', () => {
        beforeEach(() => {
            cy.viewport(1024, 768);
        });

        it("quand la différence n'est pas trop importante, l'affichage est conforme et proportionel", () => {
            cy.mount<'c-chart-treemap-double-cultures'>(html`
                <c-chart-treemap-double-cultures .options=${occitanie}></c-chart-treemap-double-cultures>
            `);

            const hauteurTreemapDroiteAttendu = Math.round(
                (occitanie.sauTotaleCulturesDroiteHa! / occitanie.sauTotaleCulturesGaucheHa!) * HAUTEUR_MAX_PIXELS
            );

            cy.get('#chart-treemap-cultures-gauche')
                .find('figure')
                .find('.apexcharts-canvas')
                .invoke('height')
                .should('be.equal', HAUTEUR_MAX_PIXELS);
            cy.get('#chart-treemap-cultures-gauche')
                .find('figure')
                .find('.apexcharts-canvas')
                .invoke('width')
                .should('be.equal', LARGEUR_PAR_DEFAUT_PIXELS);
            cy.get('#chart-treemap-cultures-droite')
                .find('figure')
                .find('.apexcharts-canvas')
                .invoke('height')
                .should('be.equal', hauteurTreemapDroiteAttendu);
            cy.get('#chart-treemap-cultures-droite')
                .find('figure')
                .find('.apexcharts-canvas')
                .invoke('width')
                .should('be.equal', LARGEUR_PAR_DEFAUT_PIXELS);

            cy.get('#selecteur').select('etirer');
            cy.wait(500);
            cy.get('#chart-treemap-cultures-droite')
                .find('figure')
                .find('.apexcharts-canvas')
                .invoke('height')
                .should('be.equal', HAUTEUR_MAX_PIXELS);
            cy.get('#chart-treemap-cultures-droite')
                .find('figure')
                .find('.apexcharts-canvas')
                .invoke('width')
                .should('be.equal', LARGEUR_PAR_DEFAUT_PIXELS);
        });

        it("quand la différence est trop importante, l'affichage est ajusté, toujours proportionnel, mais en carré", () => {
            cy.mount<'c-chart-treemap-double-cultures'>(html`
                <c-chart-treemap-double-cultures .options=${toulouseMetropole}></c-chart-treemap-double-cultures>
            `);

            const largeurCarreTreemapGaucheAttendu = Math.round(
                Math.sqrt(
                    (toulouseMetropole.sauTotaleCulturesGaucheHa! / toulouseMetropole.sauTotaleCulturesDroiteHa!) *
                        HAUTEUR_MAX_PIXELS *
                        LARGEUR_PAR_DEFAUT_PIXELS
                )
            );

            cy.get('#chart-treemap-cultures-gauche')
                .find('figure')
                .find('.apexcharts-canvas')
                .invoke('height')
                .should('be.equal', largeurCarreTreemapGaucheAttendu);
            cy.get('#chart-treemap-cultures-gauche')
                .find('figure')
                .find('.apexcharts-canvas')
                .invoke('width')
                .should('be.equal', largeurCarreTreemapGaucheAttendu);
            cy.get('#chart-treemap-cultures-droite')
                .find('figure')
                .find('.apexcharts-canvas')
                .invoke('height')
                .should('be.equal', HAUTEUR_MAX_PIXELS);
            cy.get('#chart-treemap-cultures-droite')
                .find('figure')
                .find('.apexcharts-canvas')
                .invoke('width')
                .should('be.equal', LARGEUR_PAR_DEFAUT_PIXELS);
        });

        it('quand la différence est vraiment trop importante, on ne respecte plus la proportionnalité et on utilise une taille mininum', () => {
            cy.mount<'c-chart-treemap-double-cultures'>(html`
                <c-chart-treemap-double-cultures .options=${metropoleDuGrandParis}></c-chart-treemap-double-cultures>
            `);

            cy.get('#chart-treemap-cultures-gauche')
                .find('figure')
                .find('.apexcharts-canvas')
                .invoke('height')
                .should('be.equal', TAILLE_MIN_CARRE_PIXELS);
            cy.get('#chart-treemap-cultures-gauche')
                .find('figure')
                .find('.apexcharts-canvas')
                .invoke('width')
                .should('be.equal', TAILLE_MIN_CARRE_PIXELS);
            cy.get('#chart-treemap-cultures-droite')
                .find('figure')
                .find('.apexcharts-canvas')
                .invoke('height')
                .should('be.equal', HAUTEUR_MAX_PIXELS);
            cy.get('#chart-treemap-cultures-droite')
                .find('figure')
                .find('.apexcharts-canvas')
                .invoke('width')
                .should('be.equal', LARGEUR_PAR_DEFAUT_PIXELS);
        });
    });
});
