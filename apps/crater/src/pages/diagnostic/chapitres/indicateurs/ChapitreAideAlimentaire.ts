import '../composants/ChapitreDomaine.js';
import '../../../commun/PageSablierCrater.js';

import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { IDS_DOMAINES } from '@lga/indicateurs';
import { SA } from '@lga/indicateurs';
import { html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { EtatRapport, type Rapport } from '../../../../modeles/diagnostics/Rapport.js';
import type { DonneesPageDiagnostic } from '../../PageDiagnostic.js';
import type { OptionsDomaineDetaille } from '../composants/ChapitreDomaine';

declare global {
    interface HTMLElementTagNameMap {
        'c-chapitre-aide-alimentaire': ChapitreAideAlimentaire;
    }
}

@customElement('c-chapitre-aide-alimentaire')
export class ChapitreAideAlimentaire extends LitElement {
    static styles = [STYLES_DESIGN_SYSTEM];

    @property({ attribute: false })
    donneesPage?: DonneesPageDiagnostic;

    @property({ attribute: false })
    rapport?: Rapport;

    render() {
        if (this.rapport?.getEtat() === EtatRapport.PRET) {
            return html`<c-chapitre-domaine
                .options=${{
                    domaine: SA.getDomaine(IDS_DOMAINES.aideAlimentaire),
                    territoire: this.rapport.getTerritoireActif(),
                    estPagePourTef: this.donneesPage?.estPagePourTef,
                    masquerLienMethodologie: true
                } as OptionsDomaineDetaille}
                contenuAdhoc
            >
                <div slot="contenu">
                    <p>
                        En France, le recours à l'aide alimentaire, un dispositif censé pallier les situations d'urgence, a quasiment doublé en dix
                        ans et concerne aujourd'hui plus de cinq millions de personnes.
                    </p>
                    <p>
                        Pour en savoir plus, voir le rapport
                        <c-lien
                            href="https://publications.resiliencealimentaire.org/qui-veille-au-grain/partie-I_un-systeme-defaillant/#_une_malnutrition_omnipr%C3%A9sente_au_nord_comme_au_sud"
                            >Qui veille grain&nbsp;?</c-lien
                        >.
                    </p>
                </div>
            </c-chapitre-domaine>`;
        } else {
            return html`<c-page-sablier-crater></c-page-sablier-crater>`;
        }
    }
}
