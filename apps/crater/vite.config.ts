import minifyLitTemplates from 'rollup-plugin-minify-html-literals';
import { defineConfig } from 'vite';
import * as path from 'node:path';

export default defineConfig({
    root: 'src',
    envDir: '../config',
    assetsInclude: ['**/*.geojson'],
    resolve: {
        alias: {
            '@lga/indicateurs': path.resolve(__dirname, '../../modules/indicateurs')
        }
    },
    build: {
        outDir: '../build',
        target: 'es2019',
        rollupOptions: {
            output: {
                manualChunks: {
                    apexcharts: ['apexcharts'],
                    leaflet: ['leaflet'],
                    swaggerui: ['swagger-ui'],
                    // geojson: ['geojson'], chunkvide qui crée une erreur si activé
                    lit: ['lit'],
                    page: ['page'],
                    queryselectorshadowdom: ['query-selector-shadow-dom']
                    // TODO: pour activer ces lignes, il faut déclarer des exports dans les packages.json de design-system et commun
                    // lgadesignsystem: ['@lga/design-system'],
                    // lgacommun: ['@lga/commun'],
                }
            }
        }
    },
    plugins: [
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        {
            ...minifyLitTemplates({
                // set to `true` to abort bundling on a minification error
                failOnError: true,
                options: {
                    minifyOptions: {
                        // Voir https://www.npmjs.com/package/html-minifier#options-quick-reference
                        // rollup-plugin-minify-html-literals positionne collapseWhitespace à true par défaut => on utilise conservativeCollapse pour éviter de supprimer les espaces entre les balises (ex <c-lien>)
                        collapseWhitespace: true,
                        conservativeCollapse: true
                    }
                }
            }),
            enforce: 'post',
            apply: 'build'
        }
    ],
    server: {
        fs: {
            // Autorise le server de dev de vite à faire l'import dynamique des modules dans ../../modules/* (nécessaire pour cypress) et à accéder à design-system/public
            strict: false
        }
    },
    test: {}
});
