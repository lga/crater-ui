import '../../charts/ChartBarres.js';
import '../composants/ChapitreDomaine.js';
import '../../../commun/PageSablierCrater.js';

import { IDS_DOMAINES } from '@lga/indicateurs';
import { SA } from '@lga/indicateurs';
import { css, html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { EtatRapport, type Rapport } from '../../../../modeles/diagnostics/Rapport.js';
import { Serie, SerieMultiple } from '../../charts/chart-outils.js';
import type { OptionsChartBarres } from '../../charts/ChartBarres';
import type { DonneesPageDiagnostic } from '../../PageDiagnostic.js';
import type { OptionsDomaineDetaille } from '../composants/ChapitreDomaine';

declare global {
    interface HTMLElementTagNameMap {
        'c-chapitre-sau-par-habitant': ChapitreSauParHabitant;
    }
}

@customElement('c-chapitre-sau-par-habitant')
export class ChapitreSauParHabitant extends LitElement {
    static styles = css`
        c-chart-barres {
            max-width: 500px;
            display: block;
        }
    `;

    @property({ attribute: false })
    donneesPage?: DonneesPageDiagnostic;

    @property({ attribute: false })
    rapport?: Rapport;

    render() {
        if (this.rapport?.getEtat() === EtatRapport.PRET) {
            return html`<c-chapitre-domaine
                .options=${this.calculerOptionsChapitreDomaineSauParHabitant(this.rapport, this.donneesPage)}
                nbLignesMessageMinimum="5"
            >
                <c-chart-barres .options=${this.calculerOptionsChartSauParHabitant(this.rapport)} slot="chart-chapitre-domaine"></c-chart-barres>
            </c-chapitre-domaine>`;
        } else {
            return html`<c-page-sablier-crater></c-page-sablier-crater>`;
        }
    }

    private calculerOptionsChapitreDomaineSauParHabitant(rapport: Rapport, donneesPage: DonneesPageDiagnostic | undefined): OptionsDomaineDetaille {
        return {
            domaine: SA.getDomaine(IDS_DOMAINES.sauParHabitant)!,
            message: rapport.getDiagnosticActif()!.terresAgricoles.messageSauParHabitant,
            territoire: rapport.getTerritoireActif(),
            estPagePourTef: donneesPage?.estPagePourTef
        };
    }

    private calculerOptionsChartSauParHabitant(rapport: Rapport): OptionsChartBarres {
        const labels = [''];
        return {
            series: new SerieMultiple([new Serie([rapport.getDiagnosticActif()!.terresAgricoles.sauParHabitantM2])], labels),
            nomAxeOrdonnees: 'Surface Agricole par habitant (m²)',
            maxY: Math.max(5000, ...rapport.getDiagnostics()!.map((d) => d!.terresAgricoles.sauParHabitantM2 ?? 0)),
            minY: 0,
            annotations: {
                yaxis: [
                    {
                        y: rapport.getDiagnosticActif()!.terresAgricoles.besoinsParHabitantAssietteDemitarienneM2,
                        borderColor: 'var(--couleur-neutre-80)',
                        strokeDashArray: 5,
                        label: {
                            style: {
                                color: 'var(--couleur-blanc)',
                                background: 'var(--couleur-neutre-40)'
                            },
                            text: 'Surface nécessaire pour un régime moins carné'
                        }
                    },
                    {
                        y: rapport.getDiagnosticActif()!.terresAgricoles.besoinsParHabitantAssietteActuelleM2,
                        borderColor: 'var(--couleur-neutre-80)',
                        strokeDashArray: 5,
                        label: {
                            style: {
                                color: 'var(--couleur-blanc)',
                                background: 'var(--couleur-neutre-40)'
                            },
                            text: 'Surface nécessaire pour le régime alimentaire actuel'
                        }
                    }
                ]
            }
        };
    }
}
