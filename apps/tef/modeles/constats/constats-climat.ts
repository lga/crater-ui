import { type HtmlString, htmlstring, type ImageModele } from '@lga/base';
import { recupererListe3InitiativesParConstats } from '@lga/initiatives';
import type { CodeProfilDiagnosticApi, CommuneAnalogueClimatique2000_2050Api, CommuneReferenceApi } from '@lga/specification-api';
import { point as turfPoint } from '@turf/helpers';
import { toMercator } from '@turf/projection';

import type { DonneesApi } from '../fonctions-utils.js';
import { creerLienExterneHtmlBrut } from '../liens-utils.js';
import type { ConstatModele } from './ConstatModele';
import type {
    InfographieAnalogueClimatiqueModele,
    InfographieImageMessageModele,
    InformationsMethodologieInfographieModele
} from './InfographieModele.js';

export const IMAGE_NUAGES: ImageModele = {
    url: '/images/constats/titres/paysage-nuages-800x861.jpg',
    descriptionAlt: 'Paysage avec des nuages',
    resolutionX: 800,
    resolutionY: 861
};

const INFORMATIONS_METHODOLOGIE_ANALOGUES_CLIMATIQUES: InformationsMethodologieInfographieModele = {
    titre: 'Analogues climatiques',
    descriptif: htmlstring`Cet indicateur utilise les données du projet ${creerLienExterneHtmlBrut('https://ccexplorer.eu/', 'Climate Change Explorer')}, qui calcule les analogues climatiques pour plusieurs villes de France. La notion d’analogue climatique est utilisée pour exprimer en termes simples, compréhensibles par tous, quels seront les effets du changement climatique en s’appuyant sur des exemples précis. Elle permet de prendre des décisions d'aménagement en anticipant leurs effets potentiels.`,
    urlComplementInformation: 'https://ccexplorer.eu/',
    libelleLienComplementInformation: "Voir l'application CC Explorer"
};

export interface PointCarteMercator {
    x: number;
    y: number;
}

export interface PointCarteLatLong {
    latitude: number;
    longitude: number;
}

export interface CoinsBBoxMercator {
    nordEst: PointCarteMercator;
    sudOuest: PointCarteMercator;
}

export interface PointsAnaloguesClimatiquesMercator {
    communeReference: PointCarteMercator;
    communeAnalogueClimatique2000_2050: PointCarteMercator;
}

type Pays = 'France' | 'Italy' | 'Morocco' | 'Algeria' | 'Portugal' | 'Spain';
export interface ConfigFondCarteAnalogueClimatiqueModele {
    communesAnaloguesClimatiques2000_2050Pays: Pays[];
    url: string;
    longitudeMin: number;
    longitudeMax: number;
    latitudeMin: number;
    latitudeMax: number;
    hauteur: number;
    largeur: number;
}

export function creerConstatAnalogueClimatique(
    donneesApi: DonneesApi
): ConstatModele<InfographieAnalogueClimatiqueModele | InfographieImageMessageModele> {
    return {
        id: 'analogue-climatique',
        idTheme: 'climat',
        titre: {
            libelle: 'Quel temps demain ?',
            image: IMAGE_NUAGES
        },
        message: {
            titre: 'Le dérèglement climatique va fortement impacter l’agriculture.',
            texte: calculerMessageTexte(donneesApi.diagnosticApi.profils.codeProfilDiagnostic),
            texteCourt: htmlstring`Les sécheresses exceptionnelles d’aujourd’hui vont devenir la norme et les événements extrêmes (canicules, tempêtes, inondations…) vont se multiplier.`,
            question: 'Et si on allait vers des pratiques agricoles plus résilientes face à cette menace ?',
            initiatives: recupererListe3InitiativesParConstats(donneesApi.diagnosticApi.profils, ['analogue-climatique'])
        },
        infographie: creerInfographieAnalogueClimatique(donneesApi)
    };
}

export function calculerMessageTexte(codeProfilDiagnostic: CodeProfilDiagnosticApi): HtmlString {
    if (codeProfilDiagnostic === 'VITICULTURE') {
        return htmlstring`Les sécheresses exceptionnelles d’aujourd’hui vont devenir la norme d’ici une trentaine d’années, et les événements extrêmes (canicules, tempêtes, inondations…) vont se multiplier. Les rendements agricoles vont être impactés et la teneur en alcool du vin va devenir difficile à maîtriser.`;
    } else {
        return htmlstring`Les sécheresses exceptionnelles d’aujourd’hui vont devenir la norme d’ici une trentaine d’années, et les événements extrêmes (canicules, tempêtes, inondations…) vont se multiplier. Les rendements agricoles vont être impactés et certains choix de cultures remis en question.`;
    }
}

export function creerInfographieAnalogueClimatique(donneesApi: DonneesApi): InfographieAnalogueClimatiqueModele | InfographieImageMessageModele {
    const configFondCarte: ConfigFondCarteAnalogueClimatiqueModele | undefined = CONFIG_FONDS_CARTES_ANALOGUES_CLIMATIQUES.find((carte) =>
        carte.communesAnaloguesClimatiques2000_2050Pays.includes(
            donneesApi.diagnosticApi.climat.analoguesClimatiques.communeAnalogueClimatique2000_2050?.pays as Pays
        )
    );

    if (
        donneesApi.diagnosticApi.climat.analoguesClimatiques.communeAnalogueClimatique2000_2050 === undefined ||
        donneesApi.diagnosticApi.climat.analoguesClimatiques.communeReference === undefined ||
        configFondCarte === undefined
    ) {
        return creerInfographieGenerique();
    } else {
        return creerInfographieAvecCarte(
            donneesApi.diagnosticApi.climat.analoguesClimatiques.communeReference,
            donneesApi.diagnosticApi.climat.analoguesClimatiques.communeAnalogueClimatique2000_2050,
            configFondCarte
        );
    }
}

function creerInfographieGenerique(): InfographieImageMessageModele {
    return {
        nomComposant: 'InfographieImageMessage',
        informationsMethodologie: INFORMATIONS_METHODOLOGIE_ANALOGUES_CLIMATIQUES,
        message: htmlstring`En 2050, le climat que vous connaissez va largement changer.`,
        image: {
            url: '/images/constats/infographies/climat-656x450.png',
            descriptionAlt: 'Carte des analogues climatiques',
            resolutionX: 656,
            resolutionY: 450
        },
        attributionHtml: `En savoir plus sur ${creerLienExterneHtmlBrut('https://ccexplorer.eu/', 'CC Explorer')}`,
        accroche: 'Sale temps pour les rendements'
    };
}

function creerInfographieAvecCarte(
    communeReference: CommuneReferenceApi,
    communeAnalogueClimatique2000_2050: CommuneAnalogueClimatique2000_2050Api,
    configFondCarte: ConfigFondCarteAnalogueClimatiqueModele
): InfographieAnalogueClimatiqueModele {
    const coinsBBox: CoinsBBoxMercator = {
        nordEst: convertirPointLatLongEnMercator({ latitude: configFondCarte.latitudeMax, longitude: configFondCarte.longitudeMax }),
        sudOuest: convertirPointLatLongEnMercator({ latitude: configFondCarte.latitudeMin, longitude: configFondCarte.longitudeMin })
    };

    const pointsAnaloguesClimatiques: PointsAnaloguesClimatiquesMercator = {
        communeReference: convertirPointLatLongEnMercator(communeReference),
        communeAnalogueClimatique2000_2050: convertirPointLatLongEnMercator(communeAnalogueClimatique2000_2050)
    };

    return {
        nomComposant: 'InfographieAnalogueClimatique',
        pointsAnaloguesClimatiques: pointsAnaloguesClimatiques,
        coinsBBox: coinsBBox,
        configFondCarte: configFondCarte,
        informationsMethodologie: INFORMATIONS_METHODOLOGIE_ANALOGUES_CLIMATIQUES,
        message: creerMessageCommuneAnalogueClimatique(communeReference, communeAnalogueClimatique2000_2050),
        image: {
            resolutionX: 520,
            resolutionY: 505
        },
        accroche: 'Sale temps pour les rendements'
    };
}

function convertirPointLatLongEnMercator(point: PointCarteLatLong): PointCarteMercator {
    const pointTurf = turfPoint([point.longitude, point.latitude]);
    return { x: toMercator(pointTurf).geometry.coordinates[0], y: toMercator(pointTurf).geometry.coordinates[1] };
}

export const CONFIG_FONDS_CARTES_ANALOGUES_CLIMATIQUES: ConfigFondCarteAnalogueClimatiqueModele[] = [
    {
        communesAnaloguesClimatiques2000_2050Pays: ['Italy'],
        url: '/analogues-climatiques-fonds-cartes/italie.svg',
        longitudeMin: -9.31,
        longitudeMax: 22.28,
        latitudeMin: 35.2,
        latitudeMax: 51.49,
        largeur: 351.6582714159513,
        hauteur: 251.7337386227269
    },
    {
        communesAnaloguesClimatiques2000_2050Pays: ['Spain', 'Portugal'],
        url: '/analogues-climatiques-fonds-cartes/espagne.svg',
        longitudeMin: -15.81,
        longitudeMax: 15.9,
        latitudeMin: 35.2,
        latitudeMax: 51.49,
        largeur: 352.9941053054705,
        hauteur: 251.7337386227269
    },
    {
        communesAnaloguesClimatiques2000_2050Pays: ['Algeria', 'Morocco'],
        url: '/analogues-climatiques-fonds-cartes/maghreb.svg',
        longitudeMin: -14.55,
        longitudeMax: 24.51,
        latitudeMin: 30.69,
        latitudeMax: 51.5,
        largeur: 434.81393103852656,
        hauteur: 308.9152645479186
    },
    {
        communesAnaloguesClimatiques2000_2050Pays: ['France'],
        url: '/analogues-climatiques-fonds-cartes/france.svg',
        longitudeMin: -8.36,
        longitudeMax: 12.8,
        latitudeMin: 41.14,
        latitudeMax: 51.54,
        largeur: 235.5520425185669,
        hauteur: 168.43615014860615
    }
];

function creerMessageCommuneAnalogueClimatique(
    communeReference: CommuneReferenceApi,
    communeAnalogueClimatique2000_2050: CommuneAnalogueClimatique2000_2050Api
) {
    return htmlstring`Il fera le même temps à ${communeReference.nom} en 2050 qu'à ${communeAnalogueClimatique2000_2050.nom} aujourd’hui.`;
}
