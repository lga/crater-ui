import type { ClasseNote, Note } from '../Note';

class ComposanteMessageIntrant {
    static qualificatif = ['NULL', 'très marquée', 'marquée', 'relativement faible'].map((q) => `<strong>${q}</strong>`);

    constructor(
        public nom: string,
        public classe: ClasseNote
    ) {}

    comparerAvec(b: ComposanteMessageIntrant): number {
        const aNumber: number = this.classe ?? 0;
        const bNumber: number = b.classe ?? 0;
        return aNumber - bNumber;
    }

    estIndisponible(): boolean {
        return this.classe === null;
    }

    get qualificatif() {
        return this.classe ? ComposanteMessageIntrant.qualificatif[this.classe] : null;
    }
}

export function calculerMessageSyntheseIntrants(energieNote: Note, eauNote: Note, pesticidesNote: Note): string {
    let composantesMessage: ComposanteMessageIntrant[] = [
        new ComposanteMessageIntrant("à l'énergie", energieNote.classe),
        new ComposanteMessageIntrant("à l'eau d'irrigation", eauNote.classe),
        new ComposanteMessageIntrant('aux pesticides', pesticidesNote.classe)
    ];

    composantesMessage = composantesMessage.sort((a, b) => {
        return a.comparerAvec(b);
    });

    if (composantesMessage.every((p) => p.estIndisponible())) {
        return `Données non disponibles.`;
    }

    composantesMessage = composantesMessage.filter((p) => !p.estIndisponible());
    let message = `Dépendance ${composantesMessage[0].qualificatif} ${composantesMessage[0].nom}`;
    for (let i = 1; i < composantesMessage.length; i++) {
        if (estDernierElement(i, composantesMessage)) {
            message += ' et ';
        } else if (estDernierElementGroupeDeClasseEquivalente(composantesMessage, i)) {
            message += ' et ';
        } else if (estPremierElementDuDernierGroupeDeClasseEquivalente(composantesMessage, i)) {
            message += ' et ';
        } else {
            message += ', ';
        }
        if (estGroupeDeClasseEquivalente(composantesMessage, i)) {
            message += `${composantesMessage[i].nom}`;
        } else {
            message += `${composantesMessage[i].qualificatif} ${composantesMessage[i].nom}`;
        }
    }
    return message + '.';
}

function estGroupeDeClasseEquivalente(postes: ComposanteMessageIntrant[], i: number) {
    return postes[i].classe === postes[i - 1].classe;
}

function estPremierElementDuDernierGroupeDeClasseEquivalente(postes: ComposanteMessageIntrant[], i: number) {
    return postes[i].classe !== postes[i - 1].classe && postes[i].classe === postes[postes.length - 1].classe;
}

function estDernierElementGroupeDeClasseEquivalente(postes: ComposanteMessageIntrant[], i: number) {
    return postes[i].classe === postes[i - 1].classe && postes[i].classe !== postes[i + 1].classe;
}

function estDernierElement(i: number, postes: ComposanteMessageIntrant[]) {
    return i === postes.length - 1;
}
