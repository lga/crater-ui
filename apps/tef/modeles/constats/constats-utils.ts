import type { ImageModele } from '@lga/base';
import { htmlstring } from '@lga/base';
import type { IdTheme } from '@lga/indicateurs';

import type { ConstatModele } from './ConstatModele';
import type { InfographieImageChiffreCleModele } from './InfographieModele';

export function creerConstatAbsenceDonnees(
    idTheme: IdTheme,
    libelleTitreTheme: string,
    imageTitreTheme: ImageModele
): ConstatModele<InfographieImageChiffreCleModele> {
    return {
        id: 'absence-donnees',
        idTheme: idTheme,
        titre: {
            libelle: libelleTitreTheme,
            image: imageTitreTheme
        },
        message: {
            titre: 'Désolé !',
            texte: htmlstring`Des données sont malheureusement absentes pour calculer ce constat. Nous vous invitons à consulter une échelle supérieure (collectivité ou département) et nous contacter pour nous faire part du territoire concerné !`,
            question: '',
            initiatives: []
        }
    };
}
