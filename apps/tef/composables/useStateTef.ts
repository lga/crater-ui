import type { TerritoireApi } from '@lga/specification-api';
import { computed, type Ref } from 'vue';

import { creerTerritoire } from '@/modeles/DiagnosticModele';
import { useState } from '#imports';

interface Territoire {
    id: string;
    nom: string;
    nomAvecDeterminant: string;
    nomAvecPrepostionDe: string;
    nomAvecPrepositionA: string;
}

export interface StateTef {
    territoireActif: Territoire | undefined;
}

const creerStateTerritoire = (territoireApi: TerritoireApi): Territoire => {
    const territoire = creerTerritoire(territoireApi);
    const territoireState: Territoire = {
        id: territoire.id,
        nom: territoire.nom,
        nomAvecDeterminant: territoire.nomTerritoireAvecDeterminant,
        nomAvecPrepostionDe: territoire.nomTerritoireAvecPrepositionDe,
        nomAvecPrepositionA: territoire.nomTerritoireAvecPrepositionA
    };
    return territoireState;
};

export const useSetStateTef = (territoireApi: TerritoireApi): void => {
    const stateTef = useState<StateTef>('state-tef');
    stateTef.value = {
        territoireActif: creerStateTerritoire(territoireApi)
    };
};

export const useGetStateTef = (): Ref<StateTef> => {
    const stateTefRef = useState<StateTef>('state-tef', () => ({
        territoireActif: undefined
    }));
    return stateTefRef;
};

export const useGetStateTerritoire = (): Territoire | undefined => {
    return useGetStateTef().value.territoireActif;
};

export const useGetStateIdTerritoire = (): Ref<string | undefined> => {
    return computed(() => useGetStateTerritoire()?.id ?? undefined);
};
