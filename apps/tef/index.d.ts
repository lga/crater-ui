declare module 'nuxt/schema' {
    interface RuntimeConfig {
        cleApiBrevo: string;
    }
    interface PublicRuntimeConfig {
        apiBaseUrl: string;
        craterUrl: string;
        siteEnv: 'developpement' | 'production'; // nuxt-seo impose la valeur production
        brevo: {
            idListeSouscription: number;
            idTemplateMail: number;
            urlPageConfirmation: string;
        };
        matomo: {
            siteId: string;
        };
    }
}

declare global {
    interface Window {
        _paq?: unknown[];
    }
}

export {};
