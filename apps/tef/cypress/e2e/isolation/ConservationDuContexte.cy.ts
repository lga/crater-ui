describe('Tests e2e/isolation, conservation du contexte entre pages', () => {
    it('Conserver le territoire dans le contexte lors de la navigation', () => {
        cy.viewport(1400, 800);
        cy.visit('/occitanie');
        cy.verifierAffichagePage('Occitanie');

        cy.get('nav').find('a').contains('Qui sommes-nous').click({ force: true });
        cy.verifierAffichagePage('À propos');

        cy.get('nav').find('a').contains('Mon territoire').click({ force: true });
        cy.verifierAffichagePage('Occitanie');

        cy.get('nav').find('a').contains('Initiatives inspirantes').click({ force: true });
        cy.get('body').contains('Occitanie').should('be.visible');
    });
});
