import { calculerValeurAbsolue, formaterNombreSelonValeurString } from '@lga/base';
import { GLOSSAIRE } from '@lga/indicateurs';

import { type Note, NOTE_VALEUR_WARNING } from '../Note';
import { PolitiqueAmenagement } from './PolitiqueAmenagement';
import { SEUIL_ARTIFICIALISATION_HA_QUASI_NULLE_OU_NEGATIVE } from './TerresAgricoles';

export function calculerMessageSyntheseTerresAgricoles(
    note: Note,
    sauParHabitantM2: number | null,
    besoinsParHabitantAssietteActuelleM2: number | null,
    besoinsParHabitantAssietteDemitarienneM2: number | null,
    artificialisation5ansHa: number | null
): string {
    if (
        note.valeur === NOTE_VALEUR_WARNING ||
        note.valeur === null ||
        sauParHabitantM2 === null ||
        besoinsParHabitantAssietteActuelleM2 === null ||
        besoinsParHabitantAssietteDemitarienneM2 === null ||
        artificialisation5ansHa === null
    ) {
        return `Données non disponibles (voir les échelles supérieures telles que l'EPCI).`;
    }
    let message1 = '';
    let message1EstPositif = false;
    let message2 = '';
    let message2EstPositif = false;
    let adverbe = '';

    if (sauParHabitantM2 < besoinsParHabitantAssietteDemitarienneM2) {
        message1 = 'est trop faible';
        message1EstPositif = false;
    }
    if (sauParHabitantM2 >= besoinsParHabitantAssietteDemitarienneM2) {
        message1 = 'peut convenir pour un régime alimentaire moins carné';
        message1EstPositif = true;
    }
    if (sauParHabitantM2 >= besoinsParHabitantAssietteActuelleM2) {
        message1 = 'est suffisante pour le régime alimentaire actuel';
        message1EstPositif = true;
    }

    if (artificialisation5ansHa <= SEUIL_ARTIFICIALISATION_HA_QUASI_NULLE_OU_NEGATIVE) {
        message2 = 'a été atteint entre 2013 et 2018';
        message2EstPositif = true;
    } else if (artificialisation5ansHa > SEUIL_ARTIFICIALISATION_HA_QUASI_NULLE_OU_NEGATIVE) {
        message2 = 'n’a pas été atteint entre 2013 et 2018';
        message2EstPositif = false;
    }

    if (message1EstPositif === message2EstPositif) {
        adverbe = 'et';
    } else {
        adverbe = 'mais';
    }

    return `La surface agricole par habitant <strong>${message1}</strong> ${adverbe} l’objectif <abbr title="${GLOSSAIRE.zeroArtificialisation.definition}">Zéro Artificialisation</abbr> <strong>${message2}</strong>.`;
}

export class MessageSauParHabitant {
    static readonly messageReserve =
        " Il convient quoi qu'il en soit de vérifier que cette surface agricole utile productive est suffisamment diversifiée pour être nourricière à l'échelle du territoire ou d'un bassin de vie plus étendu.";
    static readonly messageInferieur100 =
        ", la surface agricole utile productive par habitant est nulle ou quasiment inexistante, ce qui représente un risque important par manque de ressources pour produire de la nourriture localement. Il est important et urgent de mettre en place les politiques adéquates pour préserver des terres agricoles et de créer des liens avec les territoires voisins. Il convient également de s'assurer que les terres agricoles sont suffisantes pour répondre aux besoins des populations au niveau départemental ou régional.";
    static readonly messageEntre100etDemitarien =
        ', la surface agricole utile productive par habitant est trop faible, même pour un régime alimentaire moins carné.' +
        MessageSauParHabitant.messageReserve;
    static readonly messageEntreDemitarienEtActuel =
        ', la surface agricole utile productive par habitant est trop faible pour le régime actuel mais suffisante pour un régime alimentaire moins carné.' +
        MessageSauParHabitant.messageReserve;
    static readonly messageSuperieurActuel =
        ', la surface agricole utile productive par habitant est suffisante pour le régime alimentaire actuel.' +
        MessageSauParHabitant.messageReserve;

    static construireMessage(
        nomTerritoire: string,
        sauParHabitantM2: number | null,
        besoinsParHabitantAssietteActuelleM2: number | null,
        besoinsParHabitantAssietteDemitarienneM2: number | null
    ): string {
        const message = `Sur le territoire <em>${nomTerritoire}</em>`;
        if (sauParHabitantM2 === null || besoinsParHabitantAssietteActuelleM2 === null || besoinsParHabitantAssietteDemitarienneM2 === null) {
            return message + ", l'indicateur est indisponible.";
        } else if (sauParHabitantM2 < 100) {
            return message + this.messageInferieur100;
        } else if (sauParHabitantM2 < besoinsParHabitantAssietteDemitarienneM2) {
            return message + MessageSauParHabitant.messageEntre100etDemitarien;
        } else if (sauParHabitantM2 < besoinsParHabitantAssietteActuelleM2) {
            return message + MessageSauParHabitant.messageEntreDemitarienEtActuel;
        } else {
            return message + MessageSauParHabitant.messageSuperieurActuel;
        }
    }
}

export class MessageRythmeArtificialisation {
    static readonly messageRythmeArtificialisation0 =
        ", aucun espace agricole, forestier ou naturel n'a été artificialisé entre 2013 et 2018, ce qui permet notamment de préserver les terres agricoles existantes. Il convient néanmoins de s'assurer que les terres agricoles sont suffisantes pour nourrir la population et qu’elles ne sont pas menacées d’urbanisation";
    static readonly messageRythmeArtificialisation0_80 =
        ", le rythme d'artificialisation entre 2013 et 2018 est inférieur à la moyenne française. Ce rythme doit néanmoins être examiné au regard des terres agricoles disponibles et des besoins en terres agricoles au niveau local, départemental ou régional.";
    static readonly messageRythmeArtificialisation80_120 =
        ", le rythme d'artificialisation entre 2013 et 2018 correspond à la moyenne française. La politique de préservation des espaces agricoles, forestiers et naturels doit être renforcée, et ce d’autant plus si la surface agricole utile productive par habitant est insuffisante localement ou aux échelles de territoires supérieures.";
    static readonly messageRythmeArtificialisation120_1 =
        ", le rythme d'artificialisation est supérieur à la moyenne française. La politique de préservation des espaces agricoles, forestiers et naturels doit être très renforcée, et ce d’autant plus si la surface agricole utile productive par habitant est insuffisante localement ou aux échelles de territoires supérieures.";
    static readonly messageRythmeArtificialisationSup1 =
        ", l'équivalent de plus de 1% des surfaces agricoles a été artificialisé entre 2013 et 2018, ce qui va à l’encontre du principe de préservation des espaces agricoles, forestiers et naturels et représente un risque de perte de ressources pour le territoire ou pour d’autres territoires dépendants.";

    static construireMessage(
        nomTerritoire: string,
        rythmeArtificialisationSauPourcentTerritoireActif: number | null,
        rythmeArtificialisationSauPourcentPays: number | null
    ): string {
        const message = `Sur le territoire <em>${nomTerritoire}</em>`;
        if (rythmeArtificialisationSauPourcentTerritoireActif === null || rythmeArtificialisationSauPourcentPays === null) {
            return message + ", l'indicateur est indisponible.";
        } else if (rythmeArtificialisationSauPourcentTerritoireActif <= 0) {
            return message + MessageRythmeArtificialisation.messageRythmeArtificialisation0;
        } else if (
            rythmeArtificialisationSauPourcentTerritoireActif > 0 &&
            rythmeArtificialisationSauPourcentTerritoireActif <= 0.8 * rythmeArtificialisationSauPourcentPays
        ) {
            return message + this.messageRythmeArtificialisation0_80;
        } else if (
            rythmeArtificialisationSauPourcentTerritoireActif > 0.8 * rythmeArtificialisationSauPourcentPays &&
            rythmeArtificialisationSauPourcentTerritoireActif <= 1.2 * rythmeArtificialisationSauPourcentPays
        ) {
            return message + this.messageRythmeArtificialisation80_120;
        } else if (
            rythmeArtificialisationSauPourcentTerritoireActif > 1.2 * rythmeArtificialisationSauPourcentPays &&
            rythmeArtificialisationSauPourcentTerritoireActif <= 1
        ) {
            return message + this.messageRythmeArtificialisation120_1;
        } else {
            // cas restant => if (rythmeArtificialisationSauPourcentTerritoireActif > 1) {
            return message + this.messageRythmeArtificialisationSup1;
        }
    }
}

export function calculerMessageDetaillePolitiqueAmenagement(
    nomTerritoire: string,
    politiqueAmenagement: PolitiqueAmenagement,
    artificialisation5ansHa: number | null,
    evolutionMenagesEmplois5ans: number | null,
    ratioArtificialisation5ansSurSuperficieTerritoirePourcent: number | null
) {
    if (politiqueAmenagement === PolitiqueAmenagement.ARTIF_QUASI_NULLE_OU_NEGATIVE) {
        return `Sur le territoire <em>${nomTerritoire}</em>, l'objectif Zéro Artificialisation Nette a été atteint entre 2013 et 2018.`;
    } else if (politiqueAmenagement === PolitiqueAmenagement.ARTIF_POSITIVE_ET_NB_MENAGES_EMPLOIS_EN_HAUSSE) {
        return `Sur le territoire <em>${nomTerritoire}</em>, l'objectif Zéro Artificialisation Nette n'a pas été atteint entre 2013 et 2018 puisque ${formaterNombreSelonValeurString(
            artificialisation5ansHa
        )} ha ont été artificialisés soit ${formaterNombreSelonValeurString(
            ratioArtificialisation5ansSurSuperficieTerritoirePourcent
        )} % de la superficie totale du territoire.`;
    } else if (politiqueAmenagement === PolitiqueAmenagement.ARTIF_POSITIVE_ET_NB_MENAGES_EMPLOIS_EN_BAISSE) {
        return `Sur le territoire <em>${nomTerritoire}</em>, l'objectif Zéro Artificialisation Nette n'a pas été atteint entre 2013 et 2018 puisque ${formaterNombreSelonValeurString(
            artificialisation5ansHa
        )} ha ont été artificialisés soit ${formaterNombreSelonValeurString(
            ratioArtificialisation5ansSurSuperficieTerritoirePourcent
        )} % de la superficie totale du territoire, alors que le territoire a perdu ${formaterNombreSelonValeurString(
            calculerValeurAbsolue(evolutionMenagesEmplois5ans)
        )} ménages et emplois.`;
    } else {
        // Cas PolitiqueAmenagement.DONNEES_NON_DISPONIBLES
        return `Sur le territoire <em>${nomTerritoire}</em>, l'indicateur est indisponible.`;
    }
}

export function calculerMessagePartLogementsVacants(
    nomTerritoire: string,
    partLogementsVacants2013Pourcent: number | null,
    partLogementsVacants2018Pourcent: number | null
) {
    if (partLogementsVacants2013Pourcent === null || partLogementsVacants2018Pourcent === null) {
        return `Sur le territoire <em>${nomTerritoire}</em>, l'indicateur est indisponible.`;
    } else {
        return `Sur le territoire <em>${nomTerritoire}</em>, la part de logements vacants était de ${formaterNombreSelonValeurString(
            partLogementsVacants2018Pourcent
        )} % en 2018 (${formaterNombreSelonValeurString(partLogementsVacants2013Pourcent)} % en 2013).`;
    }
}
