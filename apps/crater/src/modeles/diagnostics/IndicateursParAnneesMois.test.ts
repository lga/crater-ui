import { describe, expect, it } from 'vitest';

import { IndicateursParAnneesMois } from './IndicateursParAnneesMois';

describe('Tests de la classe IndicateursParAnneesMois', () => {
    it('Creer IndicateursParAnneesMois et verifier annees et valeurs', () => {
        interface Indicateur {
            anneeMois: Date;
            valeur: number | null;
        }
        const indicateursParAnneesMois = new IndicateursParAnneesMois<Indicateur>([
            { anneeMois: new Date(2019, 11), valeur: 2 },
            { anneeMois: new Date(2018, 7), valeur: 1 }
        ]);
        expect(indicateursParAnneesMois.anneesMois).toEqual([new Date(2018, 7), new Date(2019, 11)]);
        expect(indicateursParAnneesMois.valeurs((i) => i.valeur)).toEqual([1, 2]);
    });

    it('Calculer moyenne par annee', () => {
        interface Indicateur {
            anneeMois: Date;
            valeur: number | null;
        }
        const indicateursParAnneesMois = new IndicateursParAnneesMois<Indicateur>([
            { anneeMois: new Date(2019, 6), valeur: 2 },
            { anneeMois: new Date(2019, 7), valeur: 3 },
            { anneeMois: new Date(2019, 8), valeur: 99 },
            { anneeMois: new Date(2018, 6), valeur: 1 },
            { anneeMois: new Date(2018, 7), valeur: 2 },
            { anneeMois: new Date(2017, 6), valeur: null },
            { anneeMois: new Date(2017, 7), valeur: 2 }
        ]);
        const indicateursParAnnees = indicateursParAnneesMois.valeursMoyennesParAnnees([6, 7], (i) => i.valeur, 2016, 2020);
        expect(indicateursParAnnees.annees).toEqual([2016, 2017, 2018, 2019, 2020]);
        expect(indicateursParAnnees.valeurs((i) => i.valeurMoyenne)).toEqual([0, null, 1.5, 2.5, 0]);
    });
});
