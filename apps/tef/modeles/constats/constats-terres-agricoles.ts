import { formaterNombreEnEntierString, formaterNombreSelonValeurString, type HtmlString, htmlstring, type ImageModele } from '@lga/base';
import { GroupeCulture, INDICATEURS } from '@lga/indicateurs';
import { recupererListe3InitiativesParConstats } from '@lga/initiatives';
import type { ProfilsApi } from '@lga/specification-api';
import type {
    BesoinsParGroupeCultureApi,
    CodeProfilDiagnosticApi,
    SauParGroupeCultureApi
} from '@lga/specification-api/build/specification/api/index.js';

import type { DonneesTreemapCultures } from '~/components/charts/ChartTreemapCultures.vue';

import type { DonneesApi } from '../fonctions-utils.js';
import { calculerLibellesPrincipauxGroupesCulturesExcedentaires, calculerLibellesPrincipauxGroupesCulturesManquants } from '../fonctions-utils.js';
import { calculerComplementUrlCRATer, calculerLibelleSourcesIndicateur } from '../liens-utils.js';
import { calculerNombrePersonnesNourriesSelonHectares } from '../thematiques/thematique-terres-agricoles.js';
import type { ConstatModele } from './ConstatModele';
import { creerConstatAbsenceDonnees } from './constats-utils';
import type {
    ImageInfographieModele,
    InfographieImageChiffreCleModele,
    InfographieSurfacesAgricolesProductionConsommationModele,
    InformationsMethodologieInfographieModele
} from './InfographieModele.js';

const LIBELLE_TITRE_THEME = 'Un territoire nourricier ?';

export const IMAGE_PAYSAGE_AGRICOLE: ImageModele = {
    url: '/images/constats/titres/paysage-agricole-grandes-cultures-800x861.jpg',
    descriptionAlt: 'Paysage agricole de grandes cultures',
    resolutionX: 800,
    resolutionY: 861
};

const IMAGE_CHAMP_DE_BLE_TITRE: ImageModele = {
    url: '/images/constats/titres/champ-ble-800x861.jpg',
    descriptionAlt: 'Champ de blé',
    resolutionX: 800,
    resolutionY: 861
};

const IMAGE_CHAMP_DE_BLE_CONSTAT: ImageInfographieModele = {
    url: '/images/constats/infographies/champ-ble-416x448.png',
    descriptionAlt: 'Champ de blé',
    resolutionX: 416,
    resolutionY: 448,
    position: 'gauche'
};

const INFORMATIONS_METHODOLOGIE_TAUX_ADEQUATION: InformationsMethodologieInfographieModele = {
    titre: "Taux d'adéquation théorique entre production et consommation",
    descriptif: htmlstring`<p>Ce chiffre est obtenu en comparant les surfaces agricoles disponibles sur le territoire (toutes productions confondues) et celles théoriquement nécessaires pour nourrir ses habitants.</p>
            <br>
            <p>Source : ${calculerLibelleSourcesIndicateur(INDICATEURS.adequationTheoriqueBruteProductionConsommation)}</p>
            `,
    urlComplementInformation: calculerComplementUrlCRATer(INDICATEURS.adequationTheoriqueBruteProductionConsommation)
};

export type DonneesApiTerresAgricoles = DonneesApi & {
    diagnosticApi: {
        productionsBesoins: {
            besoinsAssietteActuelle: {
                tauxAdequationBrutPourcent: number;
            };
        };
    };
};

export function creerConstatAbsenceDonneesTerresAgricoles(): ConstatModele<InfographieImageChiffreCleModele> {
    return creerConstatAbsenceDonnees('terres-agricoles', LIBELLE_TITRE_THEME, IMAGE_PAYSAGE_AGRICOLE);
}

export function creerConstatTerresInsuffisantes(donneesApi: DonneesApiTerresAgricoles): ConstatModele<InfographieImageChiffreCleModele> {
    const tauxAdequationBrutPourcent = donneesApi.diagnosticApi.productionsBesoins.besoinsAssietteActuelle.tauxAdequationBrutPourcent;
    const besoinsParHabitantAssietteActuelleM2 =
        donneesApi.diagnosticApi.productionsBesoins.besoinsAssietteActuelle.besoinsHa && donneesApi.diagnosticApi.population
            ? (donneesApi.diagnosticApi.productionsBesoins.besoinsAssietteActuelle.besoinsHa * 10000) / donneesApi.diagnosticApi.population
            : null;
    const messageTitre = calculerMessageTitreConstatTerresInsuffisantes(donneesApi.diagnosticApi.profils.codeProfilDiagnostic);
    const messageTextesEtQuestion = calculerMessageTextesEtQuestionConstatTerresInsuffisantes(
        donneesApi.diagnosticApi.profils.codeProfilDiagnostic,
        donneesApi.diagnosticApi.politiqueFonciere.rythmeArtificialisationSauPourcent,
        donneesApi.diagnosticApi.politiqueFonciere.artificialisation5ansHa,
        besoinsParHabitantAssietteActuelleM2
    );

    return {
        id: 'terres-insuffisantes',
        idTheme: 'terres-agricoles',
        titre: {
            libelle: LIBELLE_TITRE_THEME,
            image: IMAGE_CHAMP_DE_BLE_TITRE
        },
        message: {
            titre: messageTitre,
            texte: messageTextesEtQuestion.texte,
            texteCourt: messageTextesEtQuestion.texteCourt,
            question: messageTextesEtQuestion.question,
            initiatives: recupererListe3InitiativesParConstats(donneesApi.diagnosticApi.profils, ['terres-insuffisantes'])
        },
        infographie: {
            nomComposant: 'InfographieImageChiffreCle',
            informationsMethodologie: INFORMATIONS_METHODOLOGIE_TAUX_ADEQUATION,
            chiffreCle: {
                chiffre: tauxAdequationBrutPourcent >= 1 ? formaterNombreEnEntierString(tauxAdequationBrutPourcent) : 'Moins de 1',
                unite: '%',
                phrase: 'de la population pourrait être nourrie par les terres agricoles du territoire'
            },
            image: {
                url: '/images/constats/infographies/file-personnes-416x448.jpg',
                descriptionAlt: "Personnes dans une file d'attente",
                resolutionX: 416,
                resolutionY: 448,
                position: 'gauche'
            },
            accroche: 'Premiers arrivés, premiers servis ?'
        }
    };
}

export function calculerMessageTitreConstatTerresInsuffisantes(codeProfilDiagnostic: CodeProfilDiagnosticApi) {
    return codeProfilDiagnostic === 'URBAIN'
        ? `Votre territoire est densément peuplé et dépend d’autres territoires pour se nourrir.`
        : `Les terres agricoles de votre territoire sont insuffisantes pour nourrir toute la population.`;
}

export function calculerMessageTextesEtQuestionConstatTerresInsuffisantes(
    codeProfilDiagnostic: CodeProfilDiagnosticApi,
    rythmeArtificialisationSauPourcent: number | null,
    artificialisation5ansHa: number | null,
    besoinsParHabitantAssietteActuelleM2: number | null
) {
    let texte;
    let question;
    if (
        rythmeArtificialisationSauPourcent != null &&
        rythmeArtificialisationSauPourcent > 2.5 &&
        artificialisation5ansHa != null &&
        besoinsParHabitantAssietteActuelleM2 != null
    ) {
        const nombrePersonnes = calculerNombrePersonnesNourriesSelonHectares(artificialisation5ansHa, besoinsParHabitantAssietteActuelleM2);
        texte = htmlstring`Pour ne rien arranger, les constructions humaines détruisent des terres agricoles de façon irréversible chaque année, ${formaterNombreEnEntierString(artificialisation5ansHa)} hectares ont ainsi été artificialisés en 5 ans, soit de quoi nourrir ${formaterNombreEnEntierString(nombrePersonnes)} personnes.`;
        question = 'Et si on arrêtait de tout bétonner ?';
    } else if (codeProfilDiagnostic === 'URBAIN') {
        texte = htmlstring`Même si l'agriculture urbaine a du potentiel, en particulier pour les fruits et légumes, les villes dépendront toujours d'autres territoires pour leur alimentation.`;
        question = 'Et si on protégeait notre patrimoine agricole commun ?';
    } else {
        texte = htmlstring`L’essentiel de l’alimentation est donc à faire venir d’ailleurs : mieux vaut entretenir de bonnes relations avec les territoires qui vous nourrissent et préserver les terres disponibles localement.`;
        question = 'Et si on protégeait notre patrimoine agricole commun ?';
    }
    return {
        texte: texte,
        texteCourt: calculerMessageTexteCourt(artificialisation5ansHa, besoinsParHabitantAssietteActuelleM2, 'Et'),
        question: question
    };
}

export function creerConstatTerresPartiellementSuffisantes(donneesApi: DonneesApiTerresAgricoles): ConstatModele<InfographieImageChiffreCleModele> {
    const besoinsParHabitantAssietteActuelleM2 =
        donneesApi.diagnosticApi.productionsBesoins.besoinsAssietteActuelle.besoinsHa && donneesApi.diagnosticApi.population
            ? (donneesApi.diagnosticApi.productionsBesoins.besoinsAssietteActuelle.besoinsHa * 10000) / donneesApi.diagnosticApi.population
            : null;

    return {
        id: 'terres-partiellement-suffisantes',
        idTheme: 'terres-agricoles',
        titre: {
            libelle: LIBELLE_TITRE_THEME,
            image: IMAGE_PAYSAGE_AGRICOLE
        },
        message: {
            titre: 'Les terres agricoles de votre territoire pourraient nourrir une partie significative de la population.',
            texte: calculerMessageTexteConstatTerresPartiellementSuffisantes(
                donneesApi.diagnosticApi.productionsBesoins.besoinsAssietteActuelle.tauxAdequationBrutPourcent,
                donneesApi.diagnosticApi.productionsBesoins.besoinsAssietteActuelle.tauxAdequationMoyenPonderePourcent ?? 0,
                donneesApi.diagnosticApi.productionsBesoins.besoinsAssietteDemitarienne.besoinsParGroupeCulture
            ),
            texteCourt: calculerMessageTexteCourt(
                donneesApi.diagnosticApi.politiqueFonciere.artificialisation5ansHa,
                besoinsParHabitantAssietteActuelleM2,
                'Mais'
            ),
            question: 'Et si on protégeait ce patrimoine agricole ?',
            initiatives: recupererListe3InitiativesParConstats(donneesApi.diagnosticApi.profils, ['terres-partiellement-suffisantes'])
        },
        infographie: {
            nomComposant: 'InfographieImageChiffreCle',
            informationsMethodologie: INFORMATIONS_METHODOLOGIE_TAUX_ADEQUATION,
            chiffreCle: {
                chiffre: formaterNombreEnEntierString(donneesApi.diagnosticApi.productionsBesoins.besoinsAssietteActuelle.tauxAdequationBrutPourcent),
                unite: '%',
                phrase: 'de la population pourrait être nourrie par les terres agricoles du territoire'
            },
            image: IMAGE_CHAMP_DE_BLE_CONSTAT,
            accroche: 'Vous avez du potentiel !'
        }
    };
}

export function calculerMessageTexteConstatTerresPartiellementSuffisantes(
    tauxAdequationBrutRegimeActuel: number,
    tauxAdequationMoyenPondereRegimeActuel: number,
    besoinsParGroupeCultureAssietteDemitarienne: BesoinsParGroupeCultureApi[]
): HtmlString {
    const ratioTauxAdequationMoyenPondereSurBrut = tauxAdequationMoyenPondereRegimeActuel / tauxAdequationBrutRegimeActuel;
    const libellesPrincipauxGroupesCulturesManquants = calculerLibellesPrincipauxGroupesCulturesManquants(
        besoinsParGroupeCultureAssietteDemitarienne
    );
    if (ratioTauxAdequationMoyenPondereSurBrut < 0.67 && libellesPrincipauxGroupesCulturesManquants !== '') {
        return htmlstring`Mais les fermes de votre territoire ne produisent pas assez <strong>${libellesPrincipauxGroupesCulturesManquants}</strong>. Vous avez du potentiel pour développer l’alimentation locale sous réserve de diversifier les productions.`;
    } else {
        return htmlstring`L'agriculture du territoire est par ailleurs relativement diversifiée. Vous avez du potentiel pour développer l’alimentation locale !`;
    }
}

export function creerConstatTerresSuffisantes(donneesApi: DonneesApiTerresAgricoles): ConstatModele<InfographieImageChiffreCleModele> {
    const besoinsParHabitantAssietteActuelleM2 =
        donneesApi.diagnosticApi.productionsBesoins.besoinsAssietteActuelle.besoinsHa && donneesApi.diagnosticApi.population
            ? (donneesApi.diagnosticApi.productionsBesoins.besoinsAssietteActuelle.besoinsHa * 10000) / donneesApi.diagnosticApi.population
            : null;

    return {
        id: 'terres-suffisantes',
        idTheme: 'terres-agricoles',
        titre: {
            libelle: LIBELLE_TITRE_THEME,
            image: IMAGE_PAYSAGE_AGRICOLE
        },
        message: calculerMessageConstatTerresSuffisantes(
            donneesApi.diagnosticApi.productionsBesoins.besoinsAssietteActuelle.tauxAdequationBrutPourcent,
            donneesApi.diagnosticApi.productionsBesoins.besoinsAssietteActuelle.besoinsParGroupeCulture,
            donneesApi.diagnosticApi.profils,
            donneesApi.diagnosticApi.politiqueFonciere.artificialisation5ansHa,
            besoinsParHabitantAssietteActuelleM2
        ),
        infographie: {
            nomComposant: 'InfographieImageChiffreCle',
            informationsMethodologie: INFORMATIONS_METHODOLOGIE_TAUX_ADEQUATION,
            chiffreCle: {
                chiffre: formaterNombreEnEntierString(donneesApi.diagnosticApi.productionsBesoins.besoinsAssietteActuelle.tauxAdequationBrutPourcent),
                unite: '%',
                phrase: 'de la population pourrait être nourrie par les terres agricoles du territoire'
            },
            theme: 'jaune',
            image: IMAGE_CHAMP_DE_BLE_CONSTAT,
            accroche: 'Vous avez du potentiel !'
        }
    };
}

export function calculerMessageConstatTerresSuffisantes(
    tauxAdequationBrutRegimeActuel: number,
    besoinsParGroupeCultureAssietteActuelle: BesoinsParGroupeCultureApi[],
    profils: ProfilsApi,
    artificialisation5ansHa: number | null,
    besoinsParHabitantAssietteActuelleM2: number | null
) {
    const libellesPrincipauxGroupesCulturesManquants = calculerLibellesPrincipauxGroupesCulturesManquants(besoinsParGroupeCultureAssietteActuelle);
    const libellesPrincipauxGroupesCulturesExcedentaires = calculerLibellesPrincipauxGroupesCulturesExcedentaires(
        besoinsParGroupeCultureAssietteActuelle
    );
    const question = 'Et si on protégeait ce patrimoine agricole ?';
    const texteCourt = calculerMessageTexteCourt(artificialisation5ansHa, besoinsParHabitantAssietteActuelleM2);
    const initiatives = recupererListe3InitiativesParConstats(profils, ['terres-suffisantes']);
    const diversificationAgriculture: 'CARENCEE' | 'SPECIALISEE' | 'DIVERSIFIEE' =
        libellesPrincipauxGroupesCulturesManquants !== ''
            ? 'CARENCEE'
            : libellesPrincipauxGroupesCulturesExcedentaires !== ''
              ? 'SPECIALISEE'
              : 'DIVERSIFIEE';

    switch (diversificationAgriculture) {
        case 'CARENCEE':
            return {
                titre:
                    tauxAdequationBrutRegimeActuel < 100
                        ? 'Votre territoire pourrait produire suffisamment pour nourrir la grande majorité des habitants, mais…'
                        : 'Votre territoire a suffisamment de terres agricoles pour nourrir ses habitants, mais…',
                texte: htmlstring`Les fermes de votre territoire ne produisent pas assez <strong>${libellesPrincipauxGroupesCulturesManquants}</strong>. Vous avez donc un bon potentiel pour développer l’alimentation locale sous réserve de diversifier les productions.`,
                texteCourt: texteCourt,
                question: question,
                initiatives: initiatives
            };
        case 'SPECIALISEE':
            return {
                titre:
                    tauxAdequationBrutRegimeActuel < 100
                        ? 'Votre territoire pourrait produire suffisamment pour nourrir la grande majorité des habitants.'
                        : 'Bonne nouvelle : votre territoire pourrait produire suffisamment pour nourrir tous ses habitants.',
                texte: htmlstring`Bien que spécialisée dans la production <strong>${libellesPrincipauxGroupesCulturesExcedentaires}</strong>, l’agriculture du territoire couvre les principaux besoins en grands groupes d’aliments. Vous avez un bon potentiel pour développer l’alimentation locale !`,
                texteCourt: 'Mais ' + texteCourt,
                question: question,
                initiatives: initiatives
            };
        case 'DIVERSIFIEE':
            return {
                titre:
                    tauxAdequationBrutRegimeActuel < 100
                        ? 'Votre territoire pourrait produire suffisamment pour nourrir la grande majorité des habitants.'
                        : 'Bonne nouvelle : votre territoire pourrait produire suffisamment pour nourrir tous ses habitants.',
                texte: htmlstring`L’agriculture du territoire couvre les principaux besoins en grands groupes d’aliments. Vous avez un bon potentiel pour développer l’alimentation locale !`,
                texteCourt: 'Mais ' + texteCourt,
                question: question,
                initiatives: initiatives
            };
    }
}

export function calculerMessageTexteCourt(
    artificialisation5ansHa: number | null,
    besoinsParHabitantAssietteActuelleM2: number | null,
    motDebut?: string
) {
    const nombrePersonnes = calculerNombrePersonnesNourriesSelonHectares(artificialisation5ansHa, besoinsParHabitantAssietteActuelleM2);
    return nombrePersonnes !== null && artificialisation5ansHa !== null
        ? htmlstring`${motDebut ? motDebut + ' ' : ''}<strong>${formaterNombreEnEntierString(artificialisation5ansHa)} ha</strong> ont été <strong>artificialisés en 5 ans</strong>, qui auraient pu nourrir ${formaterNombreSelonValeurString(nombrePersonnes)} personnes.`
        : null;
}

export function creerInfographieAvecChartSurfacesConsommationProduction(
    donneesApi: DonneesApiTerresAgricoles
): InfographieSurfacesAgricolesProductionConsommationModele {
    return {
        nomComposant: 'InfographieSurfacesAgricolesProductionConsommation',
        informationsMethodologie: {
            titre: 'Adéquation théorique entre production et consommation',
            descriptif: htmlstring`<p>Ces diagrammes comparent les surfaces agricoles disponibles sur le territoire et celles théoriquement nécessaires pour nourrir ses habitants.</p>
            <br>
            <p>Source : ${calculerLibelleSourcesIndicateur(INDICATEURS.adequationTheoriqueBruteProductionConsommation)}</p>`,
            libelleBouton: 'Chiffres'
        },
        donneesTreemapCulturesProduction: creerDonneesTreemapProduction(donneesApi.diagnosticApi.surfaceAgricoleUtile.sauParGroupeCulture),
        donneesTreemapCulturesConsommation: creerDonneesTreemapConsommation(
            donneesApi.diagnosticApi.productionsBesoins.besoinsAssietteActuelle.besoinsParGroupeCulture
        ),
        tauxAdequationAssietteActuelleBrutPourcent: donneesApi.diagnosticApi.productionsBesoins.besoinsAssietteActuelle.tauxAdequationBrutPourcent
    };
}

function creerDonneesTreemapConsommation(consommationParGroupeCultures: BesoinsParGroupeCultureApi[]): DonneesTreemapCultures[] {
    return consommationParGroupeCultures.map((b: BesoinsParGroupeCultureApi) => ({
        code: b.codeGroupeCulture,
        labelCourt: b.codeGroupeCulture,
        labelLong: GroupeCulture.fromString(b.codeGroupeCulture).nom,
        couleur: GroupeCulture.fromString(b.codeGroupeCulture).couleur,
        sauHa: b.besoinsHa ?? 0,
        detailCultures: b.besoinsParCulture.map((s) => ({
            codeCulture: s.codeCulture,
            nomCulture: s.nomCulture,
            sauHa: s.besoinsHa ?? 0
        }))
    }));
}

function creerDonneesTreemapProduction(sauParGroupeCultures: SauParGroupeCultureApi[]): DonneesTreemapCultures[] {
    return sauParGroupeCultures
        .filter((i) => GroupeCulture.groupesHorsSNC.map((i) => i.code).includes(i.codeGroupeCulture))
        .map((s) => ({
            code: s.codeGroupeCulture,
            labelCourt: s.codeGroupeCulture,
            labelLong: GroupeCulture.fromString(s.codeGroupeCulture).nom,
            couleur: GroupeCulture.fromString(s.codeGroupeCulture).couleur,
            sauHa: s.sauHa,
            detailCultures: s.sauParCulture.map((s) => ({
                codeCulture: s.codeCulture,
                nomCulture: s.nomCulture,
                sauHa: s.sauHa
            }))
        }));
}
