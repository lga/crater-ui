import type { KebabVersCamelCase } from '@lga/base';

export type IdTheme =
    | 'risques-securite-alimentaire'
    | 'accessibilite'
    | 'agriculteurs'
    | 'autonomie-alimentaire'
    | 'biodiversite'
    | 'climat'
    | 'consommation'
    | 'ressources'
    | 'terres-agricoles'
    | 'transformation-distribution';

export const IDS_THEMES: Record<KebabVersCamelCase<IdTheme>, IdTheme> = {
    risquesSecuriteAlimentaire: 'risques-securite-alimentaire',
    accessibilite: 'accessibilite',
    agriculteurs: 'agriculteurs',
    autonomieAlimentaire: 'autonomie-alimentaire',
    biodiversite: 'biodiversite',
    climat: 'climat',
    consommation: 'consommation',
    ressources: 'ressources',
    terresAgricoles: 'terres-agricoles',
    transformationDistribution: 'transformation-distribution'
};
