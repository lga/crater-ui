import '@lga/design-system/build/composants/Bouton.js';
import '@lga/design-system/build/composants/Lien.js';

import { Bouton } from '@lga/design-system/build/composants/Bouton.js';
import { ICONES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/icones-design-system';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system';
import { type Domaine, ICONES_SVG } from '@lga/indicateurs';
import { css, html, LitElement } from 'lit';
import { customElement, property, state } from 'lit/decorators.js';
import { unsafeHTML } from 'lit/directives/unsafe-html.js';
import { unsafeSVG } from 'lit/directives/unsafe-svg.js';

import { construireUrlPageMethodologie } from '../../../../configuration/pages/pages-utils';
import { construireLienVersMethodologieSources } from '../../../commun/liens-utils.js';
import { STYLES_CRATER } from '../../../commun/pages-styles';

declare global {
    interface HTMLElementTagNameMap {
        'c-titre-chapitre-domaine': TitreChapitreDomaine;
    }
}

@customElement('c-titre-chapitre-domaine')
export class TitreChapitreDomaine extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        STYLES_CRATER,
        css`
            .titre {
                display: flex;
                margin-top: calc(2 * var(--dsem));
                margin-bottom: calc(2 * var(--dsem));
                align-items: center;
            }

            h1 {
                margin: 0;
            }

            p {
                margin-top: 0;
                margin-bottom: 0;
            }

            .texte {
                color: var(--couleur-primaire);
                padding-right: var(--dsem);
            }

            .details {
                display: block;
                position: absolute;
                background-color: var(--couleur-primaire);
                border-radius: calc(1 * var(--dsem));
                padding: calc(1 * var(--dsem));
                z-index: 100;
                margin-right: var(--dsem);
                max-width: var(--largeur-maximum-contenu);
            }

            .details * {
                color: white;
            }

            .details div {
                padding-bottom: 1rem;
            }

            .details a:hover {
                text-decoration: underline;
                cursor: pointer;
            }

            .details ul {
                margin: 0;
            }

            #lien-methodologie {
                margin-top: calc(2 * var(--dsem));
                margin-bottom: 0;
            }

            c-lien {
                --couleur: var(--couleur-blanc);
            }
        `
    ];

    @property({ attribute: false })
    domaine?: Domaine;

    @property()
    lienCarte?: string;

    @state()
    visible = false;

    @property({ type: Boolean })
    masquerLienMethodologie = false;

    @property({ type: Boolean })
    estPagePourTef = false;

    render() {
        const titreHTML = html` <div class="titre">
            <h1 class="texte titre-moyen">${this.domaine?.libelle}</h1>
            <c-bouton @click="${this.ouvrir}" libelleTooltip="Plus d'infos sur cet indicateur" type="${Bouton.TYPE.plat}"
                >${ICONES_DESIGN_SYSTEM.question}</c-bouton
            >
            ${this.lienCarte
                ? html`<c-bouton target="_top" href=${this.lienCarte} libelleTooltip="Voir la carte" type="${Bouton.TYPE.plat}">
                      ${unsafeSVG(ICONES_SVG.carte)}
                  </c-bouton>`
                : ''}
        </div>`;

        const detailHTML = this.visible
            ? html` <div class="details">
                  <p>${unsafeHTML(this.domaine?.description)}</p>
                  ${this.domaine?.indicateursDetailles
                      .filter((i) => !i.estScore)
                      .map(
                          (i) =>
                              html`<br />
                                  <p>${unsafeHTML(i.description.replace('Cet indicateur', `L'indicateur <strong>${i.libelle}</strong>`))}</p>
                                  <p>${construireLienVersMethodologieSources(i.sources)}</p>`
                      )}
                  ${!this.masquerLienMethodologie
                      ? html`<p id="lien-methodologie">
                            ${this.domaine?.id
                                ? html`<c-lien href="${construireUrlPageMethodologie(this.domaine?.idMaillon, this.domaine?.id)}"
                                      >→ voir la méthodologie${this.estPagePourTef ? ' dans CRATer' : ''}</c-lien
                                  >`
                                : ``}
                        </p>`
                      : ''}
              </div>`
            : ``;
        return html`<div>${titreHTML} ${detailHTML}</div>`;
    }

    private ouvrir() {
        this.visible = true;
        window.addEventListener('click', this.gererClickFermeture, { capture: true });
    }

    private fermer() {
        this.visible = false;
        window.removeEventListener('click', this.gererClickFermeture, { capture: true });
    }

    private gererClickFermeture = (event: Event) => {
        if (!this.contains(event.target as Node)) {
            this.fermer();
        }
    };
}
