#!/bin/sh

echo "BUILD API : copie crater-data-resultat, environnement local"
echo "Copie des données crater-data-resultats : de $(pwd)/../../crater-data-resultat vers $(pwd)/prebuild/crater-data-resultat"

rm -rf ./build/crater-data-resultats
mkdir -p ./build/crater-data-resultats
cp -r ../../crater-data-resultats/data ./build/crater-data-resultats/data