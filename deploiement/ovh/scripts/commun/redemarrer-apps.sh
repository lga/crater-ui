#!/bin/bash

# Reset de la config pm2, et redemarrage de toutes les apps
pm2 reload /home/ubuntu/lga/ovh/config/pm2/ecosystem.config.js
# Puis mise à jour du dump pm2 pour restauration en cas de reboot serveur ou de crahs du process
pm2 save