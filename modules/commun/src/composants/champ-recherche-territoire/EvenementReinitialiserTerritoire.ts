import { EvenementPersonnalisable } from '@lga/design-system/build/evenements/EvenementPersonnalisable.js';

export class EvenementReinitialiserTerritoire extends EvenementPersonnalisable<void> {
    static ID = 'reinitialiserTerritoire';
    constructor() {
        super(EvenementReinitialiserTerritoire.ID);
    }
}
