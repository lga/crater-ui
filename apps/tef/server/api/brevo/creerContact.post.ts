import { createError, defineEventHandler, readValidatedBody } from 'h3';

import { type Contact, contactSchema } from '@/modeles/contacts/Contact';
import { useRuntimeConfig } from '#imports';

/**
 * Endpoint qui sert de proxy pour accéder à l'API Brevo
 * https://developers.brevo.com/reference/createdoicontact
 * Permet d'éviter d'exposer la clé API Brevo dans les browsers, et d'avoir une config CORS restrictive (par défaut dans nuxt => CORS: false => Same-origin)
 */
export default defineEventHandler<{ body: Contact }>(async (event) => {
    const bodyValide = await readValidatedBody(event, (body) => contactSchema.safeParse(body));
    if (!bodyValide.success)
        throw createError({
            statusCode: 400,
            statusMessage: JSON.stringify(bodyValide.error.issues)
        });

    const url = 'https://api.brevo.com/v3/contacts/doubleOptinConfirmation';
    const config = useRuntimeConfig();
    return $fetch(url, {
        method: 'POST',
        headers: {
            accept: 'application/json',
            'content-type': 'application/json',
            'api-key': config.cleApiBrevo
        },
        body: {
            email: bodyValide.data.email,
            includeListIds: [config.public.brevo.idListeSouscription],
            templateId: config.public.brevo.idTemplateMail,
            redirectionUrl: config.public.brevo.urlPageConfirmation,
            attributes: {
                TERRITOIRE: bodyValide.data.territoire,
                PROFIL: bodyValide.data.profil
            }
        }
    });
});
