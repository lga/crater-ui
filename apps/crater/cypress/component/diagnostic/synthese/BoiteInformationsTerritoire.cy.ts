import '@lga/design-system/public/theme-defaut/theme-defaut.css';
import '../../../../src/pages/diagnostic/synthese/BoiteInformationsTerritoire.js';

import { html } from 'lit';

import type { OptionsBoiteInformationsTerritoire } from '../../../../src/pages/diagnostic/synthese/BoiteInformationsTerritoire';

describe('Test composant BoiteInformationTerritoire', () => {
    const options: OptionsBoiteInformationsTerritoire = {
        nomTerritoire: 'Nom territoire',
        message: html`Ceci est un message clé un peu long pour <strong>tester</strong> l'affichage sur plusieurs lignes.`,
        population: 1000,
        superficieHa: 1000,
        sauProductiveHa: 1000,
        lienPageTerritoire: 'https://www.google.fr'
    };

    it.skip('Affichage BoiteInformationTerritoire', () => {
        cy.viewport(1000, 600);
        cy.mount<'c-boite-informations-territoire'>(html`<c-boite-informations-territoire .options=${options}></c-boite-informations-territoire>`);
    });
});
