import { supprimerSlashFinal } from '@lga/base';

import { useRuntimeConfig } from '#imports';

export function useGetApiBaseUrl() {
    const config = useRuntimeConfig();
    const apiBaseUrl = config.public.apiBaseUrl;
    return supprimerSlashFinal(apiBaseUrl);
}
