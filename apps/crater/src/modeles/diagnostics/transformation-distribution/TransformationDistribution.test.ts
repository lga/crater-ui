import { CategorieTerritoire, Territoire } from '@lga/territoires';
import { describe, expect, it } from 'vitest';

import { Note } from '../Note';
import { CategorieCommerce } from './CategorieCommerce';
import { IndicateursProximiteCommerces, TransformationDistribution } from './TransformationDistribution';

describe('Tests de la classe Transformation & Distribution', () => {
    it('Calculer les données', () => {
        const territoire = new Territoire('C-1', 'NomCommune', CategorieTerritoire.Commune);

        const indicateursProximiteCommerces1 = new IndicateursProximiteCommerces(CategorieCommerce.BoulangeriePatisserie, 400, 5, 50);
        const indicateursProximiteCommerces2 = new IndicateursProximiteCommerces(CategorieCommerce.CommerceGeneraliste, 500, 10, 90);
        const indicateursProximiteCommerces3 = new IndicateursProximiteCommerces(CategorieCommerce.TroisTypesCommerces, 600, 31, 70);

        const transformationDistribution = new TransformationDistribution(territoire, new Note(3), 40, 60, [
            indicateursProximiteCommerces1,
            indicateursProximiteCommerces2,
            indicateursProximiteCommerces3
        ]);
        transformationDistribution.setTransformationDistributionPays(transformationDistribution);

        expect(transformationDistribution.indicateursProximiteCommercesParTypesCommerces.length).toEqual(2);
        expect(transformationDistribution.indicateursProximiteCommercesParTypesCommerces[0].categorieCommerce).toEqual(
            CategorieCommerce.CommerceGeneraliste
        );
        expect(transformationDistribution.indicateursProximiteCommercesParTypesCommerces[0].distancePlusProcheCommerceMetres).toEqual(500);
        expect(transformationDistribution.partPopulationDependanteVoiturePourcent).toEqual(40);
        expect(transformationDistribution.partTerritoireDependantVoiturePourcent).toEqual(60);
        expect(transformationDistribution.note.valeur).toEqual(3);
    });
});
