import type { RouteLocationRaw } from 'vue-router/auto';

export function useRouteQuiSommesNous(): RouteLocationRaw<'qui-sommes-nous'> {
    const route: RouteLocationRaw<'qui-sommes-nous'> = {
        name: 'qui-sommes-nous'
    };
    return route;
}
export function useRouteCredits(): RouteLocationRaw<'credits'> {
    const route: RouteLocationRaw<'credits'> = {
        name: 'credits'
    };
    return route;
}
export function useRouteMentionsLegales(): RouteLocationRaw<'mentions-legales'> {
    const route: RouteLocationRaw<'mentions-legales'> = {
        name: 'mentions-legales'
    };
    return route;
}
export function useRouteLicencesUtilisation(): RouteLocationRaw<'licences-utilisation'> {
    const route: RouteLocationRaw<'licences-utilisation'> = {
        name: 'licences-utilisation'
    };
    return route;
}
