import { arrondirANDecimales, sommeArray } from '@lga/base';
import type { CodeCheptelApi } from '@lga/specification-api';

interface DefinitionCheptel {
    code: CodeCheptelApi;
    libelle: string;
    couleur: string;
}

export const DEFINITIONS_CHEPTELS: DefinitionCheptel[] = [
    {
        code: 'BOVINS',
        libelle: 'Bovins',
        couleur: '#26ac56'
    },
    {
        code: 'PORCINS',
        libelle: 'Porcins',
        couleur: '#e168d1'
    },
    {
        code: 'OVINS',
        libelle: 'Ovins',
        couleur: '#c6bcc5'
    },
    {
        code: 'CAPRINS',
        libelle: 'Caprins',
        couleur: '#817580'
    },
    {
        code: 'VOLAILLES',
        libelle: 'Volailles',
        couleur: '#d3bb50'
    }
];

export class Cheptel {
    public readonly libelle: string;
    public readonly couleur: string;

    constructor(
        public readonly code: CodeCheptelApi,
        public readonly tetes: number,
        public readonly estEstime: boolean,
        public readonly ugb: number
    ) {
        if (!DEFINITIONS_CHEPTELS.find((i) => i.code === this.code))
            throw new RangeError(`Valeur incorrecte : Le code cheptel ${this.code} ne correspond à aucun des codes de l'énumération`);
        this.libelle = DEFINITIONS_CHEPTELS.find((i) => i.code === this.code)?.libelle ?? 'CODE_CHEPTEL_INCONNU';
        this.couleur = DEFINITIONS_CHEPTELS.find((i) => i.code === this.code)?.couleur ?? 'CODE_CHEPTEL_INCONNU';
        this.ugb = arrondirANDecimales(this.ugb, 0);
    }
}

export class Cheptels {
    constructor(public readonly listeCheptels: Cheptel[]) {}

    get ugbTotal() {
        return sommeArray(this.listeCheptels.map((i) => i.ugb));
    }

    get tetesTotal() {
        return sommeArray(this.listeCheptels.map((i) => i.tetes));
    }
}
