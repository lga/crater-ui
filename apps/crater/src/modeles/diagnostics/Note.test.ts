import { describe, expect, it } from 'vitest';

import { CLASSE_NOTE_BASSE, CLASSE_NOTE_HAUTE, CLASSE_NOTE_MOYENNE, Note, NOTE_VALEUR_WARNING } from './Note';

describe('Tests de la classe Note', () => {
    it('Creer une Note avec une valeur non valide doit lever une erreur', () => {
        expect(() => new Note(-0.5)).toThrowError();
        expect(() => new Note(10.1)).toThrowError();
    });

    it('Creer une Note et récupérer sa valeur, qui doit etre arrondie', () => {
        expect(new Note(null).valeur).toEqual(null);
        expect(new Note(1.49).valeur).toEqual(1);
        expect(new Note(NOTE_VALEUR_WARNING).valeur).toEqual(NOTE_VALEUR_WARNING);
    });

    it('Creer une Note et recupérer la classe', () => {
        expect(new Note(null).classe).toEqual(null);
        expect(new Note(1.49).classe).toEqual(CLASSE_NOTE_BASSE);
        expect(new Note(5.5).classe).toEqual(CLASSE_NOTE_MOYENNE);
        expect(new Note(NOTE_VALEUR_WARNING).classe).toEqual(CLASSE_NOTE_MOYENNE);
        expect(new Note(9.9).classe).toEqual(CLASSE_NOTE_HAUTE);
    });

    it('Creer une Note et calculer le libelle', () => {
        expect(new Note(null).calculerLibelle(['faible', 'moyen', 'fort'])).toEqual('inconnue');
        expect(new Note(2).calculerLibelle(['faible', 'moyen', 'fort'])).toEqual('faible');
        expect(new Note(NOTE_VALEUR_WARNING).calculerLibelle(['faible', 'moyen', 'fort'])).toEqual('moyen');
    });
});
