import { css, html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { STYLES_DESIGN_SYSTEM } from '../styles/styles-design-system.js';

export interface ItemLegende {
    couleur: string;
    libelle: string;
    motif?: string;
}

export const MOTIF_RAYURE = 'repeating-linear-gradient(90deg, rgba(255, 255, 255, 1) 0 2px, rgba(0, 0, 0, 0) 1px 5px)';

declare global {
    interface HTMLElementTagNameMap {
        'c-legende': Legende;
    }
}

enum TYPE_LEGENDE {
    discrete = 'discrete',
    continue = 'continue'
}

@customElement('c-legende')
export class Legende extends LitElement {
    static TYPE_LEGENDE = TYPE_LEGENDE;

    static styles = [
        STYLES_DESIGN_SYSTEM,
        css`
            :host {
                --largeur-barre: 200px;
                --direction-flex-legende-discrete: row;
                margin: auto;
                display: block;
            }

            #legende-discrete ul {
                display: flex;
                flex-direction: var(--direction-flex-legende-discrete);
                padding: 0.4rem;
                margin: 0;
                flex-wrap: wrap;
                justify-content: space-evenly;
                width: fit-content;
            }
            #legende-discrete li {
                list-style: none;
                display: flex;
                padding: 0.1rem 0.4rem;
            }

            #legende-discrete .dot {
                position: relative;
                top: 2px;
                height: 1rem;
                width: 1rem;
                background-color: #bbb;
                flex-shrink: 0;
                border-radius: 50%;
                display: inline-block;
                margin-right: 0.3rem;
                border: solid 1px var(--couleur-fond);
            }

            #legende-continue {
                display: flex;
                justify-content: left;
                align-items: end;
                height: 50px;
                padding-bottom: 10px;
                width: fit-content;
            }

            #legende-continue .rond {
                box-sizing: content-box;
                width: 5px;
                height: 5px;
                background-color: #2c3e50;
                border-radius: 50%;
                border: solid 3px var(--couleur-blanc);
            }

            #legende-continue .rond.debut {
                transform: translate(5px, 3.5px);
            }

            #legende-continue .rond.fin {
                transform: translate(-5px, 3.5px);
            }

            #legende-continue .delimitation {
                box-sizing: content-box;
                width: 1px;
                height: 5px;
                background-color: #2c3e50;
            }

            #legende-continue .trait {
                width: 50px;
                height: 5px;
            }

            #legende-continue span {
                position: absolute;
                transform: translate(-50%, -25px);
                color: var(--couleur-neutre);
            }
        `
    ];

    @property({ attribute: false })
    items?: ItemLegende[] = [];

    @property()
    type: `${TYPE_LEGENDE}` = TYPE_LEGENDE.discrete;

    render() {
        if (this.type === TYPE_LEGENDE.discrete) {
            return this.renderLegendeDiscrete();
        } else {
            return this.renderLegendeContinue();
        }
    }

    renderLegendeDiscrete() {
        return html` <div id="legende-discrete">
            <ul>
                ${this.items!.map(
                    (item) =>
                        html`<li class="texte-petit">
                            <span class="dot" style="background-color: ${item.couleur}; background-image: ${item.motif}"></span>
                            <span>${item.libelle}</span>
                        </li>`
                )}
            </ul>
        </div>`;
    }

    renderLegendeContinue() {
        if (!this.items) return ``;
        if (this.items.length == 0) return '';
        return html` <div id="legende-continue" class="texte-petit">
            <div class="rond debut" style="background-color: ${this.items[0].couleur}"></div>
            ${this.items.slice(0, -1).map(
                (i, index) => html`
                    <div class="${index !== 100 ? 'delimitation' : ''}">
                        <span>${i.libelle}</span>
                    </div>
                    <div
                        class="trait"
                        style="
                            background-image: linear-gradient(0.25turn, ${i.couleur}, ${this.items![index + 1].couleur});
                            width: calc(var(--largeur-barre) / ${this.items!.length - 1});"
                    ></div>
                `
            )}
            <div class="rond fin" style="background-color: ${this.items.slice(-1)[0].couleur}">
                <span>${this.items.slice(-1)[0].libelle}</span>
            </div>
        </div>`;
    }
}
