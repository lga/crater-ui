import { formaterNombreSelonValeurString } from '@lga/base';
import { INDICATEURS, type ValeurIndicateur } from '@lga/indicateurs';

import type { DonneesApi } from '../fonctions-utils.js';
import type { ThematiqueModele } from './ThematiqueModele.js';

export interface ThematiqueTerresAgricolesModele extends ThematiqueModele {
    indicateurTauxAdequationBrutRegimeActuel: ValeurIndicateur;
    indicateurTauxAdequationMoyenPondereRegimeActuel: ValeurIndicateur;
    indicateurArtificialisation5ans: ValeurIndicateur;
}

export function creerThematiqueTerresAgricoles(donneesApi: DonneesApi): ThematiqueTerresAgricolesModele {
    const besoinsParHabitantAssietteActuelleM2 =
        donneesApi.diagnosticApi.productionsBesoins.besoinsAssietteActuelle.besoinsHa && donneesApi.diagnosticApi.population
            ? (donneesApi.diagnosticApi.productionsBesoins.besoinsAssietteActuelle.besoinsHa * 10000) / donneesApi.diagnosticApi.population
            : null;
    const nombrePersonnesNourriesSelonHectares = calculerNombrePersonnesNourriesSelonHectares(
        donneesApi.diagnosticApi.politiqueFonciere.artificialisation5ansHa,
        besoinsParHabitantAssietteActuelleM2
    );
    return {
        idTheme: 'terres-agricoles',
        indicateurTauxAdequationBrutRegimeActuel: {
            indicateur: INDICATEURS.adequationTheoriqueBruteProductionConsommation,
            chiffre: formaterNombreSelonValeurString(donneesApi.diagnosticApi.productionsBesoins.besoinsAssietteActuelle.tauxAdequationBrutPourcent),
            unite: '%'
        },
        indicateurTauxAdequationMoyenPondereRegimeActuel: {
            indicateur: INDICATEURS.adequationTheoriqueMoyennePondereeProductionConsommation,
            chiffre: formaterNombreSelonValeurString(
                donneesApi.diagnosticApi.productionsBesoins.besoinsAssietteActuelle.tauxAdequationMoyenPonderePourcent
            ),
            unite: '%'
        },
        indicateurArtificialisation5ans: {
            indicateur: INDICATEURS.politiqueAmenagement,
            chiffre: formaterNombreSelonValeurString(donneesApi.diagnosticApi.politiqueFonciere.artificialisation5ansHa),
            unite: 'ha',
            chiffreComplement:
                nombrePersonnesNourriesSelonHectares && nombrePersonnesNourriesSelonHectares > 0
                    ? `Ces terres auraient pu nourrir ${formaterNombreSelonValeurString(nombrePersonnesNourriesSelonHectares)} personnes.`
                    : ''
        }
    };
}

export function calculerNombrePersonnesNourriesSelonHectares(hectares: number | null, besoinsParHabitantM2: number | null) {
    return hectares !== null && hectares > 0 && besoinsParHabitantM2 ? (hectares * 10000) / besoinsParHabitantM2 : null;
}
