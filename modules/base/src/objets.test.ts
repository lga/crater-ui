import { describe, expect, it } from 'vitest';

import { fusionner, stringifyAvecTri } from './objets';

describe('Tests des fonctions de objets.ts', () => {
    it('Fonction fusionner', () => {
        expect(fusionner({ a: 1 }, { b: 2 })).toEqual({ a: 1, b: 2 });
        expect(fusionner({ a: 1 }, { a: 2 })).toEqual({ a: 2 });
        expect(fusionner({ a: { a1: 1, a2: 2 }, b: [1, 2] }, { a: { a3: 3 }, b: [3] })).toEqual({ a: { a1: 1, a2: 2, a3: 3 }, b: [3] });
    });
});

describe('Test conversion stringify avec tri des propriétés', () => {
    it('Trier les propriétés avec stringify', () => {
        const json = {
            C: {
                CB: 'B',
                CA: {
                    CAB: 'B',
                    CAA: 'A'
                }
            },
            B: [{ BB: 'B', ba: 'a' }],
            d: 'D',
            a: 'A'
        };
        const resultatAttendu = `{
  "a": "A",
  "d": "D",
  "B": [
    {
      "ba": "a",
      "BB": "B"
    }
  ],
  "C": {
    "CB": "B",
    "CA": {
      "CAA": "A",
      "CAB": "B"
    }
  }
}`;
        expect(stringifyAvecTri({})).toEqual('{\n}');
        expect(stringifyAvecTri(json)).toEqual(resultatAttendu);
    });
});
