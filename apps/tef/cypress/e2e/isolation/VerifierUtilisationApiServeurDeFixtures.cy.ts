describe("Tests e2e/isolation, vérifier l'utilisation de l'api du serveur de fixtures", () => {
    it('Vérifier que l`api utilisée est celle du serveur de fixture', () => {
        cy.viewport(1400, 800);
        cy.visit('/occitanie');
        cy.get('h1').contains('[Fixture]').should('be.visible');
    });
});
