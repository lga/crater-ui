import { CSS_BREAKPOINT_MAX_WIDTH_MOBILE_PAYSAGE } from '@lga/design-system/build/styles/styles-breakpoints';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system';
import { css, html, LitElement, unsafeCSS } from 'lit';
import { customElement } from 'lit/decorators.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-aller-plus-loin': AllerPlusLoin;
    }
}
@customElement('c-aller-plus-loin')
export class AllerPlusLoin extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        css`
            :host {
                width: 100%;
                display: block;
                background: var(--couleur-fond-sombre);
                border-radius: calc(2 * var(--dsem)) calc(2 * var(--dsem)) 200px;
            }
            main {
                padding: calc(2 * var(--dsem));
            }
            h3 {
                margin-top: 0;
                margin-bottom: var(--dsem4);
            }

            @media screen and (max-width: ${unsafeCSS(CSS_BREAKPOINT_MAX_WIDTH_MOBILE_PAYSAGE + 'px')}) {
                main {
                    padding-bottom: calc(5 * var(--dsem));
                }
            }
        `
    ];
    render() {
        return html`
            <main>
                <h3 class="titre-moyen">Pour aller plus loin</h3>
                <slot></slot>
            </main>
        `;
    }
}
