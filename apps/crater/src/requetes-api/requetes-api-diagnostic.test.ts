import { toJson } from '@lga/base';
import type { Territoire } from '@lga/territoires';
import * as fs from 'fs';
import * as path from 'path';
import { beforeEach, describe, expect, it } from 'vitest';

import type { Diagnostic } from '../modeles/diagnostics';
import { traduireReponseApiEnDiagnostic } from './requetes-api-diagnostic';
import { traduireJsonEnTerritoire } from './requetes-api-territoires';

const CARACTERE_SAUT_LIGNE = /\r/g;
const BASEPATH = __dirname;

describe('Test interface API Diagnostic...', () => {
    let territoire: Territoire;
    let diagnostic: Diagnostic;

    beforeEach(() => {
        // TODO : doublon avec le fichier dans crater-api...voir comment utiliser le même, ou bien baser ce test sur un vrai appel a l'api
        const territoireJson = JSON.parse(fs.readFileSync(`${BASEPATH}/__test__/input/territoire_P-FR.json`).toString());
        territoire = traduireJsonEnTerritoire(territoireJson);
        const diagnosticJson = JSON.parse(fs.readFileSync(`${BASEPATH}/__test__/input/diagnostic_P-FR.json`).toString());
        diagnostic = traduireReponseApiEnDiagnostic(territoire, diagnosticJson);
    });

    it("S'assurer que l'on vérifie bien tous les sous objets du diagnostic", () => {
        // Si ce test échoue, c'est que la liste des attributs du diagnostic a changé
        // => il faut la mettre a jour dans ce test, en s'assurant que des tests correspondants existent pour ces nouveaux attributs dans la suite de ce fichier
        const attributsInfoTerritoire = [
            'territoire',
            'population',
            'densitePopulationHabParKM2',
            'idTerritoireParcel',
            'nbCommunes',
            'otex',
            'occupationSols',
            'surfaceAgricoleUtile',
            'cheptels',
            'ugbParHa'
        ];
        const attributsMaillons = [
            'terresAgricoles',
            'agriculteursExploitations',
            'intrants',
            'production',
            'transformationDistribution',
            'consommation'
        ];
        expect(Object.keys(diagnostic)).toEqual(attributsInfoTerritoire.concat(attributsMaillons));
    });

    it("Mapper le retour JSON dans l'objet du domaine Diagnostic.territoire", () => {
        expect(diagnostic.territoire.id).toEqual('P-FR');
        expect(diagnostic.territoire.nom).toEqual('France');
        expect(diagnostic.idTerritoireParcel).toEqual('1');
    });

    it("Mapper le retour JSON dans les champs simples de l'objet du domaine Diagnostic", () => {
        expect(diagnostic.population).toEqual(64616201);
        expect(diagnostic.ugbParHa).toEqual(2.3);
    });

    it("Mapper le retour JSON dans l'objet du domaine Diagnostic.otex", () => {
        verifierEgaliteAvecFichierTexte(toJson(diagnostic.otex), `${BASEPATH}/__test__/`, 'diagnostic.otex.json');
    });

    it("Mapper le retour JSON dans l'objet du domaine Diagnostic.occupationSols", () => {
        verifierEgaliteAvecFichierTexte(toJson(diagnostic.occupationSols), `${BASEPATH}/__test__/`, 'diagnostic.occupationSols.json');
    });

    it("Mapper le retour JSON dans l'objet du domaine Diagnostic.surfaceAgricoleUtile", () => {
        verifierEgaliteAvecFichierTexte(toJson(diagnostic.surfaceAgricoleUtile), `${BASEPATH}/__test__/`, 'diagnostic.surfaceAgricoleUtile.json');
    });

    it("Mapper le retour JSON dans l'objet du domaine Diagnostic.cheptels", () => {
        verifierEgaliteAvecFichierTexte(toJson(diagnostic.cheptels), `${BASEPATH}/__test__/`, 'diagnostic.cheptels.json');
    });

    // TODO : renommer dans l'api politiqueFonciere en terresAgricoles
    it("Mapper le retour JSON dans l'objet du domaine Diagnostic.terresAgricoles", () => {
        verifierEgaliteAvecFichierTexte(toJson(diagnostic.terresAgricoles), `${BASEPATH}/__test__/`, 'diagnostic.terresAgricoles.json');
    });

    it("Mapper le retour JSON dans l'objet du domaine Diagnostic.populationAgricole", () => {
        verifierEgaliteAvecFichierTexte(
            toJson(diagnostic.agriculteursExploitations),
            `${BASEPATH}/__test__/`,
            'diagnostic.agriculteursExploitations.json'
        );
    });

    it("Mapper le retour JSON dans l'objet du domaine Diagnostic.intrants", () => {
        verifierEgaliteAvecFichierTexte(toJson(diagnostic.intrants), `${BASEPATH}/__test__/`, 'diagnostic.intrants.json');
    });

    it("Mapper le retour JSON dans l'objet du domaine Diagnostic.production", () => {
        verifierEgaliteAvecFichierTexte(toJson(diagnostic.production), `${BASEPATH}/__test__/`, 'diagnostic.production.json');
    });

    // TODO : renommer dans l'api proximiteCommerces en transformationDistribution, ou materialiser un maillon transformationDistribution
    it("Mapper le retour JSON dans l'objet du domaine Diagnostic.transformationDistribution", () => {
        verifierEgaliteAvecFichierTexte(
            toJson(diagnostic.transformationDistribution),
            `${BASEPATH}/__test__/`,
            'diagnostic.transformationDistribution.json'
        );
    });

    it("Mapper le retour JSON dans l'objet du domaine Diagnostic.consommation", () => {
        verifierEgaliteAvecFichierTexte(toJson(diagnostic.consommation), `${BASEPATH}/__test__/`, 'diagnostic.consommation.json');
    });
});

// TODO : duplication avec fonction dans apps/crater. A factoriser dans un module de /modules
function verifierEgaliteAvecFichierTexte(contenuFichierAValider: string, cheminDossierDonneesTest: string, nomFichierResultatAttendu: string) {
    const dossierOutput = path.join(cheminDossierDonneesTest, 'output');
    if (!fs.existsSync(dossierOutput)) {
        fs.mkdirSync(dossierOutput);
    }
    // écriture d'un fichier avec le contenu pour faciliter les diff avec le resultat attendu en cas d'erreur
    const cheminFichierResultat = path.join(dossierOutput, nomFichierResultatAttendu);
    fs.writeFileSync(cheminFichierResultat, contenuFichierAValider);

    const cheminFichierAttendu = path.join(cheminDossierDonneesTest, 'expected', nomFichierResultatAttendu);
    const contenuFichierAttendu = fs.readFileSync(cheminFichierAttendu).toString().replace(CARACTERE_SAUT_LIGNE, '');

    if (contenuFichierAValider !== contenuFichierAttendu) {
        throw new Error(
            `Erreur : les données ne sont pas identiques entre le fichier resultat (${cheminFichierResultat}) et le fichier attendu ${cheminFichierAttendu}`
        );
    }
}
