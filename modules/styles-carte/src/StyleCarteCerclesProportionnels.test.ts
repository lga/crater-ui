import { describe, expect, it } from 'vitest';

import { StyleCarteCerclesProportionnels } from './StyleCarteCerclesProportionnels';

describe('Test de la classe StyleCarteCerclesProportionnels', () => {
    it('Tester les différentes fonctions ', () => {
        const style = new StyleCarteCerclesProportionnels({
            couleurValeursNegatives: 'red',
            couleurValeursPositives: 'green',
            facteurMultiplicatifTailleCercles: 1,
            couleurContours: 'black'
        });

        expect(style.calculerCouleurFondFeature()).toEqual('transparent');
        expect(style.calculerCouleurFondFeature(0)).toEqual('transparent');
        expect(style.couleurContours).toEqual('black');
        expect(style.opacite).toEqual(0.7);
        expect(style.calculerRayon(2)).toBeCloseTo(1.41, 2);
        expect(style.calculerRayon(-2)).toBeCloseTo(1.41, 2);
        expect(style.calculerCouleurCercle(2)).toEqual('green');
        expect(style.calculerCouleurCercle(-2)).toEqual('red');
        expect(style.calculerLibelleTooltip(2, 'm')).toEqual('Valeur : 2 m');
        expect(style.seuilsLegende).toBeUndefined();
        expect(style.estIndicateurContinu).toBeTruthy();
    });
});
