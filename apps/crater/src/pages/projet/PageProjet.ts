import '../commun/template/TemplatePageAccueil.js';
import '@lga/design-system/build/composants/Lien.js';

import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { type FournisseurDonnee, fournisseursDonnees } from '@lga/indicateurs';
import { css, html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { ifDefined } from 'lit/directives/if-defined.js';

import { HASH_PROJET_LICENCE, LIENS_EXTERNES, PAGES_AIDE, PAGES_PRINCIPALES } from '../../configuration/pages/declaration-pages.js';
import imageAdeme from '../../ressources/images/logo-ademe.svg';
import imageAnrPia from '../../ressources/images/logo-anr-pia.png';
import imageEul from '../../ressources/images/logo-eul.svg';
import imageFondationCarasso from '../../ressources/images/logo-fondation-carasso.png';
import imageMaa from '../../ressources/images/logo-maa.png';
import imageMetropoleNCA from '../../ressources/images/logo-nice-cote-azur.png';
import { STYLES_CRATER } from '../commun/pages-styles';

declare global {
    interface HTMLElementTagNameMap {
        'c-page-projet': PageProjet;
    }
}

@customElement('c-page-projet')
export class PageProjet extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        STYLES_CRATER,
        css`
            c-template-page-accueil {
                --couleur-fond-accueil: var(--couleur-fond-sombre);
            }

            main {
                padding-right: max((100vw - var(--largeur-maximum-contenu)) / 2, var(--dsem));
                padding-left: max((100vw - var(--largeur-maximum-contenu)) / 2, var(--dsem));
            }

            h1 {
                display: flex;
                flex-direction: column;
                align-items: center;
                gap: calc(var(--dsem) * 2);
                padding-bottom: calc(4 * var(--dsem));
            }

            h1 > svg {
                fill: var(--couleur-primaire);
            }

            .logos {
                display: flex;
                flex-wrap: wrap;
                justify-content: center;
                align-items: center;
            }

            .logo {
                margin: 0.5em 0.5em 0.5em 0.5em;
            }

            .historique {
                padding-left: 5px;
                display: grid;
                grid-template-columns: 30% 70%;
            }

            .historique p {
                grid-column: 1;
                text-align: right;
                padding-right: 1em;
                margin-top: auto;
                margin-bottom: auto;
            }

            .historique p::after {
                content: '';
                position: absolute;
                display: inline-block;
                background-color: var(--couleur-primaire);
                width: 20px;
                height: 20px;
                margin-left: 8px;
                margin-top: 2px;
                border-radius: 50%;
            }

            .historique ul {
                grid-column: 2;
                border-left: 5px solid var(--couleur-primaire);
                padding-left: 2em;
                padding-top: 1em;
                padding-bottom: 1em;
                list-style: none;
                margin: 0;
            }

            .historique ul.sous-liste {
                border: none;
                padding-left: 1em;
                text-align: center;
            }

            .historique li {
                padding-bottom: 0.3em;
            }
        `
    ];

    @property()
    idElementCibleScroll = '';

    render() {
        return html`
            <c-template-page-accueil idItemActifMenuPrincipal=${PAGES_PRINCIPALES.projet.getId()} idElementCibleScroll=${this.idElementCibleScroll}>
                <main slot="contenu">
                    <h1>Le projet CRATer</h1>

                    <h2>Pourquoi CRATer ?</h2>

                    <p>
                        Il est urgent et capital de repenser l’<c-lien href=${PAGES_AIDE.comprendreLeSystemeAlimentaire.getUrl()}
                            >organisation de nos systèmes alimentaires</c-lien
                        >
                        et construire de nouveaux modèles plus durables et plus résilients, en commençant par évaluer les systèmes actuels.
                    </p>
                    <p>
                        <strong>
                            C’est l’objectif de CRATer : proposer un outil numérique de sensibilisation et d'aide au diagnostic de la résilience
                            alimentaire des territoires </strong
                        >.
                    </p>

                    <p>Son objectif est double :</p>
                    <ul>
                        <li>
                            d'une part participer à la prise de conscience d’un large public (citoyens, élus, etc...) sur les enjeux de résilience
                            alimentaire ;
                        </li>
                        <li>
                            et d'autre part faciliter le travail lors de la construction de diagnostics terrain approfondis, par exemple lors de la
                            réalisation de Projets Alimentaires Territoriaux (PAT) dont le déploiement se généralise dans les collectivités.
                        </li>
                    </ul>

                    <p>
                        Le contenu du diagnostic proposé se veut donc accessible à tous, et ne nécessite pas de connaissances préalables. Il doit
                        aider à l’identification des enjeux essentiels, des vulnérabilités et des leviers d’action prioritaires sur chaque territoire.
                    </p>
                    <p>
                        L’application s’adresse également aux acteurs terrains, membres de collectivités et spécialistes du domaine afin de leur
                        faciliter le travail de consolidation et d’interprétation des données. Les résultats sont fournis sous forme d’un
                        pré-diagnostic, qui ne se substitue pas à une étude terrain, mais permet en amont de rassembler une partie des indicateurs
                        utiles.
                    </p>

                    <p>Le contenu de l’application, les données et le code sont disponibles en licence ouverte.</p>

                    <h2>Remerciements</h2>

                    <p>Merci à toutes celles et ceux qui font avancer le projet au quotidien :</p>
                    <ul>
                        <li>l'équipe de l'association Les Greniers d'Abondance et au délà tous les bénévoles qui contribuent au projet ;</li>
                        <li>les représentants des collectivités qui participent à la définition de la feuille de route ;</li>
                        <li>les experts et acteurs de la recherche qui nous aident dans la construction des indicateurs.</li>
                    </ul>

                    <p>Nous tenons également à remercier pour leur confiance et leur soutien financier :</p>
                    <ul>
                        <li>
                            l'<c-lien href="https://www.ademe.fr/">ADEME</c-lien> et le
                            <c-lien href="https://agriculture.gouv.fr/">Ministère de l'Agriculture et de l'Alimentation</c-lien>
                        </li>
                        <li>
                            l’<c-lien href="https://anr.fr/">Agence Nationale de la Recherche</c-lien> au titre du Programme d’Investissements
                            d’Avenir portant la référence <c-lien href="https://anr.fr/ProjetIA-17-CONV-0004">ANR-17-CONV-0004</c-lien>
                        </li>
                        <li>la <c-lien href="https://www.fondationcarasso.org/">fondation Daniel et Nina CARASSO</c-lien></li>
                        <li>
                            et la <c-lien href="https://www.nicecotedazur.org/">métropole de Nice Côte d'Azur</c-lien>
                            pour son soutien au
                            <c-lien href="/pdf/Rapport_d_etude_pour_la_metropole_de_Nice_Cote_d_Azur-v1.1.pdf"
                                >projet d'amélioration de l'utilisation des données du
                                <abbr title="Recensement Parcellaire Graphique">RPG</abbr></c-lien
                            >.
                        </li>
                    </ul>

                    <p class="logos">
                    <img class="logo" src="${imageAdeme}" width="84" height="100" alt="ADEME" loading="lazy" />
                        <img
                            class="logo"
                            src="${imageMaa}"
                            width="120"
                            alt="Ministère de l'Agriculture et de l'Alimentation"
                            loading="lazy"
                        />
                        <img
                            class="logo"
                            src="${imageAnrPia}"
                            alt="Agence Nationale de la Recherche, Programme d’Investissements d’Avenir"
                            width="100"
                            loading="lazy"
                        />
                        <img class="logo" src="${imageEul}" alt="École Urbaine de Lyon" width="190" height="66" loading="lazy" />
                        <img
                            class="logo"
                            src="${imageFondationCarasso}"
                            alt="Fondation Daniel et Nina Carasso"
                            width="150"
                            loading="lazy"
                        />
                        <img class="logo" src="${imageMetropoleNCA}" alt="Métropole Nice Côte d'Azur" width="160" height="56" loading="lazy" />
                    </p>

                    <p>Enfin, merci à tous nos partenaires :</p>
                    <p></p>
                    <ul>
                        <li>
                            l'équipe <c-lien href="https://solagro.org/">Solagro</c-lien> pour la mise à disposition et l'aide à l'analyse des données
                            HVN, ainsi que l'aide à la création des indicateurs sur l'énergie ;
                        </li>
                        <li>
                            <c-lien href="https://lebasic.com/">Le BASIC</c-lien> et les partenaires de
                            <c-lien href="https://parcel-app.org">l'outil PARCEL</c-lien> pour leur collaboration ;
                        </li>
                        <li>ainsi que l'ensemble des fournisseurs de données ou méthodologies :</li>
                    </ul>

                    ${this.renderLogos()}

                    <h2>Comment contribuer ?</h2>

                    <p>CRATer est développé par une équipe de l'association Les Greniers d'Abondance, avec l'aide de nombreux bénévoles.</p>

                    <p>Pour nous aider vous pouvez :</p>
                    <ul>
                        <li><c-lien href=${PAGES_PRINCIPALES.contact.getUrl()}>nous donner votre avis</c-lien> sur l'application;</li>
                        <li><c-lien href=${LIENS_EXTERNES.lienFaireUnDon}> faire un don</c-lien> ;</li>
                        <li>
                            <c-lien href="https://framagit.org/groups/lga/-/issues">contribuer directement</c-lien> en proposant une merge request.
                        </li>
                    </ul>

                    <h2 id="${HASH_PROJET_LICENCE}">Licences et droits d'utilisation</h2>
                    <h3>Contenus</h3>
                    <p>
                        Sauf mention contraire, le contenu de ce site est publié par l'association Les Greniers d'Abondance sous la
                        <c-lien href="https://creativecommons.org/licenses/by-nc-sa/4.0/">licence CC BY-NC-SA 4.0</c-lien>
                        (Creative Commons, Attribution, Pas d’utilisation commerciale, Partage dans les mêmes conditions).
                    </p>
                    <h3>Données</h3>
                    <p>
                        Les données accessibles dans l’application ou via l'<c-lien href=${PAGES_PRINCIPALES.api.getUrl()}>API</c-lien> sont mises à
                        disposition sous la licence
                        <c-lien href="http://opendatacommons.org/licenses/odbl/1.0/">"Open Database Licence" (ODbL)</c-lien>. Tous les droits sur le
                        contenu individuel de la base de données sont concédés sous la
                        <c-lien href="http://opendatacommons.org/licenses/dbcl/1.0/">licence de contenu de la base de données</c-lien>. Vous êtes
                        libre de copier, distribuer, transmettre et adapter ces données, à condition que vous créditiez le projet CRATer et Les
                        Greniers d'Abondance. Si vous modifiez ou utilisez nos données dans d’autres œuvres dérivées, vous ne pouvez distribuer
                        celles-ci que sous la même licence (voir
                        <c-lien href="https://opendatacommons.org/licenses/odbl/1-0/"
                        >le texte juridique complet</c-lien> détaillant vos droits et responsabilités).
                    </p>
                    <p>Aussi :</p>
                    <ul>
                        <li>
                            les données concernant les besoins en surfaces agricoles par cultures proviennent de l'application PARCEL et ne peuvent
                            être utilisées sans l’accord des porteurs du projet (demande à adresser par courriel à 
                            <c-lien href="mailto:contact@parcel-app.org">contact@parcel-app.org</c-lien>).
                            Toute réutilisation doit mentionner la paternité de l’information en faisant référence au site web de
                            <c-lien href="https://parcel-app.org">PARCEL</c-lien>;
                        </li>
                        <li>
                            les données concernant les indicateurs HVN sont fournies par SOLAGRO, et nécessitent un accord de Solagro avant
                            rediffusion ou ré-utilisation. La demande est à adresser par courriel à
                            <c-lien href="mailto:solagro@solagro.asso.fr">solagro@solagro.asso.fr</c-lien>. Toute réutilisation doit mentionner la
                            paternité de l’information en faisant référence au site web de <c-lien href="https://www.solagro.org">Solagro</c-lien>,
                            ainsi qu’aux rapports décrivant la méthodologie de calcul des indicateurs HVN précisés dans cet
                            <c-lien href="https://solagro.org/nos-domaines-d-intervention/agroecologie/haute-valeur-naturelle">article</c-lien>.
                        </li>
                    </ul>
                    <h3>Code source</h3>
                    <p>
                        Le code de l'application est publié sous la licence
                        <c-lien href="https://www.gnu.org/licenses/agpl-3.0.html#license-text">GNU Affero</c-lien>, il est accessible
                        <c-lien href="https://framagit.org/lga">ici</c-lien>.
                    </p>
                    <h3>Comment créditer CRATer ?</h3>
                    <p>Citer l'application comme suit :</br>"Les Greniers d'Abondance, Calculateur pour la Résilience Alimentaire des Territoires (CRATer). [Application disponible en
                        ligne :
                        <c-lien href="https://crater.resiliencealimentaire.org">https://crater.resiliencealimentaire.org</c-lien>
                        (consultée le XX/XX/20XX)]"
                    </p>  

                    <h2>Historique des versions</h2>

                    <div class="historique">
                        <p>Octobre 2020</p>
                        <ul>
                            <li>Première diffusion de l'application</li>
                            <li>
                                Le diagnostic est réalisé à partir d'une commune, et il est décliné sur son EPCI, son département, sa région et la
                                France
                            </li>
                            <li>
                                4 composantes du système alimentaire sont analysées (adéquation théorique production/besoins, pratiques agricoles,
                                population agricole et politique foncière)
                            </li>
                            <li>Périmètre France Métropolitaine</li>
                        </ul>
                        <p>Février 2021</p>
                        <ul>
                            <li>
                                Le diagnostic est consultable directement pour un EPCI, un département, une région ou le pays (sans passer
                                obligatoirement par la commune)
                            </li>
                            <li>
                                Possibilité d'utiliser une url pour cibler un diagnostic sur n'importe quel territoire (par exemple pour le
                                département de l'Allier :
                                <c-lien href="https://crater.resiliencealimentaire.org/diagnostic/allier"
                                    >https://crater.resiliencealimentaire.org/diagnostic/allier</c-lien
                                >)
                            </li>
                            <li>Amélioration de la navigation et de l'affichage sur écran mobile</li>
                        </ul>
                        <p>Avril 2021</p>
                        <ul>
                            <li>
                                Une carte de France permet de visualiser globalement le niveau de résilience des territoires pour différentes
                                thématiques du système alimentaire
                            </li>
                            <li>La recherche de territoire est accessible directement depuis la page diagnostic</li>
                            <li>Refonte de la charte graphique et de la page d'accueil</li>
                        </ul>
                        <p>Septembre 2021</p>
                        <ul>
                            <li>
                                Ajout de la notion de surface agricole productive et surface agricole peu productive pour une meilleure exploitation
                                des données du <abbr title="Recensement Parcellaire Graphique">RPG</abbr>
                            </li>
                            <li>
                                Mise à jour des indicateurs pour exclure systématiquement les surfaces peu productives et ne prendre en compte que les
                                surfaces productives dans les calculs
                            </li>
                            <li>
                                Ajout du chapitre "Présentation du territoire" dans la page de résultat du diagnostic et clarification du chapitre
                                "Production / Besoins"
                            </li>
                        </ul>
                        <p>Décembre 2021</p>
                        <ul>
                            <li>Ajout des diagnostics pour de nouveaux types de territoires comme les PNR ou les SCOT</li>
                            <li>
                                Enrichissement du chapitre "Population Agricole" avec des indicateurs sur l'âge des exploitants, le nombre
                                d'exploitations, et leur surface
                            </li>
                            <li>
                                Enrichissement du chapitre "Politique Foncière" avec des indicateurs sur la part des logements vacants et leur
                                évolution
                            </li>
                            <li>Mise à jour du référentiel des territoires (géographie au 1e janvier 2021 de l'INSEE)</li>
                        </ul>
                        <p>Février 2022</p>
                        <ul>
                            <li>
                                Enrichissement du chapitre "Pratiques agricoles" avec des indicateurs sur l'usage des produits phytosanitaires
                                (pesticides)
                            </li>
                            <li>Cartographie de ces résultats dans la page Carte</li>
                            <li>Ajout de passerelles entre les pages Diagnostic et Carte</li>
                        </ul>
                        <p>Juin 2022</p>
                        <ul>
                            <li>
                                Ajout du chapitre "Proximité aux commerces" avec des indicateurs permettant d'approcher la dépendance théorique des
                                habitants à la voiture pour réaliser leurs achats alimentaires
                            </li>
                        </ul>
                        <p>Septembre 2022</p>
                        <ul>
                            <li>Réorganisation du contenu avec un nouveau découpage des thématiques en maillons du système alimentaire :</li>
                            <ul class="sous-liste">
                                <li>Adéquation Besoins Production / Pratiques agricoles / Population Agricole / Politique Foncière</li>
                                <li>→</li>
                                <li>
                                    Terres Agricoles / Agriculteurs & Exploitations / Intrants / Production / Transformation & Distribution /
                                    Consommation
                                </li>
                            </ul>
                            <li>Mise à jour du référentiel des territoires (géographie au 1e janvier 2022 de l'INSEE)</li>
                            <li>Ajout de l'échelle communale dans le module cartographique</li>
                            <li>Ajout d'une barre de recherche des territoires dans le module cartographique</li>
                        </ul>
                        <p>Décembre 2022</p>
                        <ul>
                            <li>Refonte de la navigation sur mobile</li>
                            <li>Amélioration des performances de l'API pour éviter des erreurs fréquentes de timeout</li>
                            <li>Ajout de contenus pédagogiques sur les enjeux du système alimentaire</li>
                            <li>Ajout d'une FAQ et d'un glossaire</li>
                        </ul>
                        <p>Février 2023</p>
                        <ul>
                            <li>Refonte finale du design global de l'application</li>
                        </ul>
                        <p>Avril 2023</p>
                        <ul>
                            <li>Ajout de l'indicateur Taux de pauvreté dans le chapitre Consommation</li>
                            <li>Ajout de données sur la spécialisation agricole de la production</li>
                        </ul>
                        <p>Juillet 2023</p>
                        <ul>
                            <li>Ajout de nouveaux indicateurs sur la dépendance à l'eau d'irrigation dans le chapitre Intrants</li>
                        </ul>
                        <p>Janvier 2024</p>
                        <ul>
                            <li>Ajout du détail des surfaces agricoles et des cheptels dans le chapitre Présentation du territoire</li>
                            <li>Mise à jour du référentiel des territoires (géographie au 1e janvier 2023 de l'INSEE)</li>
                        </ul>
                        </ul>
                        <p>Avril 2024</p>
                        <ul>
                            <li>Ajout de nouveaux indicateurs sur la consommation énergétique du secteur agricole dans le chapitre Intrants</li>
                        </ul>
                        </ul>
                    </div>


                    <h2>Crédits</h2>
                    <ul>
                        <li>Les illustrations sont de <c-lien href="https://fr.freepik.com/auteur/stories">storyset (Freepik)</c-lien> et <c-lien href="https://dribbble.com/dyeos">Sarah Lepreux</c-lien>.</li>
                        <li>
                            Les icones utilisées dans la synthèse sont de :
                            <ul>
                                <li>pour le nombre d'habitants : People by Shashank Singh from the Noun Project</li>
                                <li>pour la superficie du territoire : Landscape by Surya Cannavale from the Noun Project</li>
                                <li>pour le nombre d'hectares agricoles : Agriculture by Made from the Noun Project</li>
                            </ul>
                        </li>
                    </ul>                    
                </main>
            </c-template-page-accueil>
        `;
    }

    private renderLogos() {
        return html`
            <p class="logos">
                ${Object.values(fournisseursDonnees)
                    .filter((i) => i.lienLogo !== '')
                    .map(
                        (i: FournisseurDonnee) =>
                            html`<img class="logo" src="${i.lienLogo}" alt="${i.nom}" width="${ifDefined(i.largeurLogo)}" loading="lazy" />`
                    )}
            </p>
        `;
    }
}
