import './composants/MenuMobileSommaireDiagnostic.js';
import '../commun/PageSablierCrater.js';
import '../commun/template/TemplatePageAvecMenuTerritoire.js';
import '@lga/design-system/build/composants/FilAriane.js';
import '@lga/design-system/build/composants/Bouton';

import { rendreInitialeMajuscule } from '@lga/base';
import type { DonneesPageSimple } from '@lga/commun/build/modeles/pages/donnees-pages';
import { importDynamique } from '@lga/commun/build/outils/importDynamiqueDirective.js';
import { Bouton } from '@lga/design-system/build/composants/Bouton';
import type { LienFilAriane } from '@lga/design-system/build/composants/FilAriane';
import { CSS_BREAKPOINT_MAX_WIDTH_DESKTOP_PETIT } from '@lga/design-system/build/styles/styles-breakpoints.js';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { ICONES_DESIGN_SYSTEM } from '@lga/design-system/src/styles/icones-design-system.js';
import { IDS_DOMAINES, IDS_MAILLONS } from '@lga/indicateurs';
import { SA } from '@lga/indicateurs';
import { css, html, unsafeCSS } from 'lit';
import { customElement } from 'lit/decorators.js';
import { choose } from 'lit/directives/choose.js';
import { styleMap } from 'lit/directives/style-map.js';

import { PAGES_PRINCIPALES } from '../../configuration/pages/declaration-pages.js';
import {
    construireUrlPageDiagnosticDomaine,
    construireUrlPageDiagnosticMaillon,
    construireUrlPageDiagnosticSynthese,
    construireUrlPageDiagnosticTerritoire
} from '../../configuration/pages/pages-utils.js';
import { EtatRapport, type Rapport } from '../../modeles/diagnostics';
import type { OptionsBarreMenuTerritoires } from '../commun/BarreMenuTerritoires';
import { STYLES_CRATER } from '../commun/pages-styles';
import { ControleurDiagnostic } from './ControleurDiagnostic';

declare global {
    interface HTMLElementTagNameMap {
        'c-page-diagnostic': PageDiagnostic;
    }
}

export type DonneesPageDiagnostic = DonneesPageSimple & {
    idTerritoirePrincipal: string;
    chapitre?: string;
    nomTerritoirePrincipal?: string;
    idTerritoireActif?: string;
    nomTerritoireActif?: string;
    idEchelleTerritoriale?: string;
    metaDescription?: string;
    estPagePourTef?: boolean;
};

export type DonneesPageDiagnosticMaillon = DonneesPageDiagnostic & {
    idMaillon: string;
    nomMaillon?: string;
};
export type DonneesPageDiagnosticDomaine = DonneesPageDiagnostic & {
    idDomaine: string;
    nomDomaine?: string;
};

@customElement('c-page-diagnostic')
export class PageDiagnostic extends ControleurDiagnostic {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        STYLES_CRATER,
        css`
            c-template-page-avec-menu-territoire {
                --largeur-menu-chapitres: 300px;
                --largeur-maximum-contenu: 1100px;
            }

            main {
                background-color: var(--couleur-fond);
            }

            c-menu-navigation-chapitres {
                float: left;
                position: sticky;
                top: var(--hauteur-totale-entete);
                width: var(--largeur-menu-chapitres);
                height: calc(100vh - var(--hauteur-totale-entete));
            }

            #coeur-page {
                padding: calc(2 * var(--dsem));
                padding-right: max(
                    (100vw - var(--largeur-maximum-contenu) - var(--largeur-menu-chapitres)) / 2 + calc(2 * var(--dsem)),
                    calc(2 * var(--dsem))
                );
                padding-left: max(
                    (100vw - var(--largeur-maximum-contenu) - var(--largeur-menu-chapitres)) / 2 + var(--largeur-menu-chapitres) +
                        calc(2 * var(--dsem)),
                    calc(2 * var(--dsem))
                );
                width: 100%;
            }

            #coeur-page-synthese {
                padding: calc(2 * var(--dsem));
                width: 100%;
            }

            c-chapitre-synthese {
                max-width: 1400px;
                margin: auto;
                display: block;
                padding: 0 calc(4 * var(--dsem));
            }

            c-bouton {
                padding: calc(2 * var(--dsem)) 0 0 0;
            }

            @media screen and (max-width: ${unsafeCSS(CSS_BREAKPOINT_MAX_WIDTH_DESKTOP_PETIT + 'px')}) {
                c-chapitre-synthese {
                    padding: 0 calc(0.5 * var(--dsem));
                }

                c-menu-navigation-chapitres {
                    display: none;
                }

                #coeur-page {
                    padding: var(--dsem2);
                }
            }
        `
    ];

    render() {
        return this.donneesPage.estPagePourTef
            ? this.renderTef()
            : html`
                  <c-template-page-avec-menu-territoire
                      idItemActifMenuPrincipal=${PAGES_PRINCIPALES.diagnostic.getId()}
                      idElementCibleScroll=${this.donneesPage.idElementCibleScroll ?? ''}
                      ?desactiverBoutonSommaire=${this.donneesPage.idTerritoirePrincipal === ''}
                      .optionsBarreMenuTerritoires=${this.calculerOptionsBarreMenuTerritoires(this.rapport)}
                  >
                      <main slot="contenu" aria-live="off">${this.renderCoeurDePage()}</main>
                      <c-menu-mobile-sommaire-diagnostic slot="contenu-sommaire" .donneesPage=${this.donneesPage} .rapport=${this.rapport}>
                      </c-menu-mobile-sommaire-diagnostic>
                  </c-template-page-avec-menu-territoire>
              `;
    }

    private renderTef() {
        if (this.rapport && this.rapport.getEtat() === EtatRapport.PRET) {
            const chapitreCourant = this.donneesPage.chapitre;
            return html`<div style=${styleMap({ backgroundColor: `transparent` })}>${this.renderCoeurDePageChapitre(chapitreCourant)}</div>`;
        } else {
            return html`<c-page-sablier-crater></c-page-sablier-crater>`;
        }
    }

    private calculerOptionsBarreMenuTerritoires(rapport: Rapport | undefined) {
        if (!rapport || rapport.getEtat() !== EtatRapport.PRET) {
            return {} as OptionsBarreMenuTerritoires;
        }

        return {
            onglets: rapport
                .getHierarchieTerritoires()!
                .getListeTerritoires()
                .map((t) => {
                    return {
                        id: t.categorie.codeCategorie,
                        libelle: rendreInitialeMajuscule(t.categorie.libelleCategorieHtml),
                        sousLibelle: t.nom
                    };
                }),
            idOngletSelectionne: rapport.getTerritoireActif()!.categorie.codeCategorie
        } as OptionsBarreMenuTerritoires;
    }

    private renderCoeurDePage() {
        if (this.rapport && this.rapport.getEtat() === EtatRapport.PRET) {
            const chapitreCourant = this.donneesPage.chapitre;
            const domaine = SA.getDomaine(this.donneesPage.chapitre ?? '');

            return html`
                ${chapitreCourant === 'synthese'
                    ? ''
                    : html`<c-menu-navigation-chapitres
                          .diagnostic="${this.rapport.getDiagnosticActif()}"
                          .donneesPage="${this.donneesPage}"
                      ></c-menu-navigation-chapitres>`}
                <div id="coeur-page${chapitreCourant === 'synthese' ? '-synthese' : ''}">
                    ${chapitreCourant === 'synthese'
                        ? ''
                        : html`<c-fil-ariane .liens=${this.construireLiensFilAriane(chapitreCourant!, this.donneesPage)}></c-fil-ariane>`}
                    <div id="chapitre">
                        ${domaine
                            ? html` <c-bouton
                                  id="bouton-retour"
                                  href=${construireUrlPageDiagnosticMaillon(
                                      domaine.idMaillon,
                                      this.donneesPage?.idTerritoirePrincipal ?? '',
                                      this.donneesPage?.idEchelleTerritoriale,
                                      domaine.id
                                  )}
                                  libelle="Retour"
                                  libelleTooltip="Retour"
                                  type="${Bouton.TYPE.plat}"
                                  positionIcone="${Bouton.POSITION_ICONE.gauche}"
                                  themeCouleur="${Bouton.THEME_COULEUR.accent}"
                                  tailleReduite
                                  >${ICONES_DESIGN_SYSTEM.flecheGauche}</c-bouton
                              >`
                            : ``}
                        ${this.renderCoeurDePageChapitre(chapitreCourant)}
                    </div>
                </div>
            `;
        } else {
            return html`<c-page-sablier-crater></c-page-sablier-crater>`;
        }
    }

    private construireLiensFilAriane(chapitre: string, donneesPage: DonneesPageDiagnostic): LienFilAriane[] {
        const filAriane = [
            {
                libelle: 'Synthèse',
                href: construireUrlPageDiagnosticSynthese(donneesPage.idTerritoirePrincipal, donneesPage.idEchelleTerritoriale)
            } as LienFilAriane
        ];
        if (chapitre === 'territoire')
            filAriane.push({
                libelle: 'Présentation du territoire',
                href: construireUrlPageDiagnosticTerritoire(donneesPage.idTerritoirePrincipal ?? '', donneesPage.idEchelleTerritoriale)
            } as LienFilAriane);
        const maillon = SA.getMaillon(chapitre) ?? SA.getMaillon(SA.getDomaine(chapitre)?.idMaillon ?? '');
        if (maillon)
            filAriane.push({
                libelle: maillon.nom,
                href: construireUrlPageDiagnosticMaillon(maillon.id, donneesPage.idTerritoirePrincipal ?? '', donneesPage.idEchelleTerritoriale)
            } as LienFilAriane);
        const domaine = SA.getDomaine(chapitre);
        if (domaine)
            filAriane.push({
                libelle: domaine.libelle,
                href: construireUrlPageDiagnosticDomaine(domaine.id, donneesPage.idTerritoirePrincipal ?? '', donneesPage.idEchelleTerritoriale)
            } as LienFilAriane);
        return filAriane;
    }

    private renderCoeurDePageChapitre = (chapitre: string | undefined) => {
        return choose(
            chapitre,
            [
                ['synthese', this.renderCoeurDePageSynthese],
                ['territoire', this.renderCoeurDePageTerritoire],
                [IDS_MAILLONS.terresAgricoles, this.renderCoeurDePageTerresAgricoles],
                [IDS_DOMAINES.sauParHabitant, this.renderCoeurDePageSauParHabitant],
                [IDS_DOMAINES.rythmeArtificialisation, this.renderCoeurDePageRythmeArtificialisation],
                [IDS_DOMAINES.politiqueAmenagement, this.renderCoeurDePagePolitiqueAmenagement],
                [IDS_DOMAINES.logementsVacants, this.renderCoeurDePagePartLogementsVacants],
                [IDS_MAILLONS.agriculteursExploitations, this.renderCoeurDePageAgriculteursExploitations],
                [IDS_DOMAINES.actifsAgricoles, this.renderCoeurDePagePartActifsAgricoles],
                [IDS_DOMAINES.revenuAgriculteurs, this.renderCoeurDePageRevenuAgriculteurs],
                [IDS_DOMAINES.agesChefsExploitation, this.renderCoeurDePageAgesChefsExploitation],
                [IDS_DOMAINES.superficiesExploitations, this.renderCoeurDePageSuperficiesExploitations],
                [IDS_MAILLONS.intrants, this.renderCoeurDePageIntrants],
                [IDS_DOMAINES.energie, this.renderCoeurDePageEnergie],
                [IDS_DOMAINES.irrigation, this.renderCoeurDePageIrrigation],
                [IDS_DOMAINES.secheresses, this.renderCoeurDePageAlertesSecheresse],
                [IDS_DOMAINES.qsaNodu, this.renderCoeurDePageQsaNodu],
                [IDS_DOMAINES.noduNormalise, this.renderCoeurDePageNoduNormalise],
                [IDS_MAILLONS.production, this.renderCoeurDePageProduction],
                [IDS_DOMAINES.adequationProductionConsommation, this.renderCoeurDePageAdequationTheoriqueProductionConsommation],
                [IDS_DOMAINES.importationExportationConsommationProduction, this.renderCoeurDePagePartProductionExportee],
                [IDS_DOMAINES.bio, this.renderCoeurDePagePartSAUBio],
                [IDS_DOMAINES.hvn, this.renderCoeurDePageIndiceHVN],
                [IDS_MAILLONS.transformationDistribution, this.renderCoeurDePageTransformationDistribution],
                [IDS_DOMAINES.dependancePopulationVoiture, this.renderCoeurDePagePartPopulationDependanteVoiture],
                [IDS_DOMAINES.dependanceTerritoireVoiture, this.renderCoeurDePagePartTerritoireDependantVoiture],
                [IDS_DOMAINES.distancesPlusProchesCommerces, this.renderCoeurDePageDistancesPlusProchesCommerces],
                [IDS_MAILLONS.consommation, this.renderCoeurDePageConsommation],
                [IDS_DOMAINES.consommationProduitsAnimaux, this.renderCoeurDePagePartAlimentationAnimaleDansConsommation],
                [IDS_DOMAINES.pauvrete, this.renderCoeurDePageTauxPauvrete],
                [IDS_DOMAINES.aideAlimentaire, this.renderCoeurDePageAideAlimentaire],
                [IDS_DOMAINES.obesite, this.renderCoeurDePagePartPopulationObese]
            ],
            () => ''
        );
    };

    private renderCoeurDePageSynthese = () =>
        importDynamique(
            import('./synthese/ChapitreSynthese.js'),
            html` <c-chapitre-synthese .rapport="${this.rapport}" .donneesPage="${this.donneesPage}"></c-chapitre-synthese>`,
            html` <c-page-sablier-crater></c-page-sablier-crater>`
        );

    private renderCoeurDePageTerritoire = () =>
        importDynamique(
            import('./chapitres/ChapitrePresentationTerritoire.js'),
            html` <c-chapitre-presentation-territoire
                .rapport="${this.rapport}"
                .donneesPage="${this.donneesPage}"
            ></c-chapitre-presentation-territoire>`,
            html` <c-page-sablier-crater></c-page-sablier-crater>`
        );

    private renderCoeurDePageTerresAgricoles = () =>
        importDynamique(
            import('./chapitres/ChapitreTerresAgricoles.js'),
            html` <c-chapitre-terres-agricoles .rapport="${this.rapport}" .donneesPage="${this.donneesPage}"></c-chapitre-terres-agricoles>`,
            html` <c-page-sablier-crater></c-page-sablier-crater>`
        );

    private renderCoeurDePageSauParHabitant = () =>
        importDynamique(
            import('./chapitres/indicateurs/ChapitreSauParHabitant.js'),
            html` <c-chapitre-sau-par-habitant .rapport="${this.rapport}" .donneesPage="${this.donneesPage}"></c-chapitre-sau-par-habitant>`,
            html` <c-page-sablier-crater></c-page-sablier-crater>`
        );

    private renderCoeurDePageRythmeArtificialisation = () =>
        importDynamique(
            import('./chapitres/indicateurs/ChapitreRythmeArtificialisation.js'),
            html` <c-chapitre-rythme-artificialisation
                .rapport="${this.rapport}"
                .donneesPage="${this.donneesPage}"
            ></c-chapitre-rythme-artificialisation>`,
            html` <c-page-sablier-crater></c-page-sablier-crater>`
        );

    private renderCoeurDePagePolitiqueAmenagement = () =>
        importDynamique(
            import('./chapitres/indicateurs/ChapitrePolitiqueAmenagement.js'),
            html` <c-chapitre-politique-amenagement
                .rapport="${this.rapport}"
                .donneesPage="${this.donneesPage}"
            ></c-chapitre-politique-amenagement>`,
            html` <c-page-sablier-crater></c-page-sablier-crater>`
        );

    private renderCoeurDePagePartLogementsVacants = () =>
        importDynamique(
            import('./chapitres/indicateurs/ChapitrePartLogementsVacants.js'),
            html` <c-chapitre-part-logements-vacants
                .rapport="${this.rapport}"
                .donneesPage="${this.donneesPage}"
            ></c-chapitre-part-logements-vacants>`,
            html` <c-page-sablier-crater></c-page-sablier-crater>`
        );

    private renderCoeurDePageAgriculteursExploitations = () =>
        importDynamique(
            import('./chapitres/ChapitreAgriculteursExploitations.js'),
            html` <c-chapitre-agriculteurs-exploitations
                .rapport="${this.rapport}"
                .donneesPage="${this.donneesPage}"
            ></c-chapitre-agriculteurs-exploitations>`,
            html` <c-page-sablier-crater></c-page-sablier-crater>`
        );

    private renderCoeurDePagePartActifsAgricoles = () =>
        importDynamique(
            import('./chapitres/indicateurs/ChapitrePartActifsAgricoles.js'),
            html` <c-chapitre-part-actifs-agricoles
                .rapport="${this.rapport}"
                .donneesPage="${this.donneesPage}"
            ></c-chapitre-part-actifs-agricoles>`,
            html` <c-page-sablier-crater></c-page-sablier-crater>`
        );

    private renderCoeurDePageRevenuAgriculteurs = () =>
        importDynamique(
            import('./chapitres/indicateurs/ChapitreRevenuAgriculteurs.js'),
            html` <c-chapitre-revenu-agriculteurs .rapport="${this.rapport}" .donneesPage="${this.donneesPage}"></c-chapitre-revenu-agriculteurs>`,
            html` <c-page-sablier-crater></c-page-sablier-crater>`
        );

    private renderCoeurDePageAgesChefsExploitation = () =>
        importDynamique(
            import('./chapitres/indicateurs/ChapitreAgesChefsExploitation.js'),
            html` <c-chapitre-ages-chefs-exploitations
                .rapport="${this.rapport}"
                .donneesPage="${this.donneesPage}"
            ></c-chapitre-ages-chefs-exploitations>`,
            html` <c-page-sablier-crater></c-page-sablier-crater>`
        );

    private renderCoeurDePageSuperficiesExploitations = () =>
        importDynamique(
            import('./chapitres/indicateurs/ChapitreSuperficiesExploitations.js'),
            html` <c-chapitre-superficies-exploitations
                .rapport="${this.rapport}"
                .donneesPage="${this.donneesPage}"
            ></c-chapitre-superficies-exploitations>`,
            html` <c-page-sablier-crater></c-page-sablier-crater>`
        );

    private renderCoeurDePageIntrants = () =>
        importDynamique(
            import('./chapitres/ChapitreIntrants.js'),
            html` <c-chapitre-intrants .rapport="${this.rapport}" .donneesPage="${this.donneesPage}"></c-chapitre-intrants>`,
            html` <c-page-sablier-crater></c-page-sablier-crater>`
        );

    private renderCoeurDePageEnergie = () =>
        importDynamique(
            import('./chapitres/indicateurs/ChapitreEnergie.js'),
            html` <c-chapitre-energie .rapport="${this.rapport}" .donneesPage="${this.donneesPage}"></c-chapitre-energie>`,
            html` <c-page-sablier-crater></c-page-sablier-crater>`
        );

    private renderCoeurDePageIrrigation = () =>
        importDynamique(
            import('./chapitres/indicateurs/ChapitreIrrigation.js'),
            html` <c-chapitre-irrigation .rapport="${this.rapport}" .donneesPage="${this.donneesPage}"></c-chapitre-irrigation>`,
            html` <c-page-sablier-crater></c-page-sablier-crater>`
        );

    private renderCoeurDePageAlertesSecheresse = () =>
        importDynamique(
            import('./chapitres/indicateurs/ChapitreAlertesSecheresse.js'),
            html` <c-chapitre-alertes-secheresse .rapport="${this.rapport}" .donneesPage="${this.donneesPage}"></c-chapitre-alertes-secheresse>`,
            html` <c-page-sablier-crater></c-page-sablier-crater>`
        );

    private renderCoeurDePageQsaNodu = () =>
        importDynamique(
            import('./chapitres/indicateurs/ChapitreQsaNodu.js'),
            html` <c-chapitre-qsa-nodu .rapport="${this.rapport}" .donneesPage="${this.donneesPage}"></c-chapitre-qsa-nodu>`,
            html` <c-page-sablier-crater></c-page-sablier-crater>`
        );

    private renderCoeurDePageNoduNormalise = () =>
        importDynamique(
            import('./chapitres/indicateurs/ChapitreNoduNormalise.js'),
            html` <c-chapitre-nodu-normalise .rapport="${this.rapport}" .donneesPage="${this.donneesPage}"></c-chapitre-nodu-normalise>`,
            html` <c-page-sablier-crater></c-page-sablier-crater>`
        );

    private renderCoeurDePageProduction = () =>
        importDynamique(
            import('./chapitres/ChapitreProduction.js'),
            html` <c-chapitre-production .rapport=${this.rapport} .donneesPage=${this.donneesPage}></c-chapitre-production>`,
            html` <c-page-sablier-crater></c-page-sablier-crater>`
        );

    private renderCoeurDePageAdequationTheoriqueProductionConsommation = () =>
        importDynamique(
            import('./chapitres/indicateurs/ChapitreAdequationTheoriqueProductionConsommation.js'),
            html` <c-chapitre-adequation-theorique-production-consommation
                .rapport=${this.rapport}
                .donneesPage=${this.donneesPage}
            ></c-chapitre-adequation-theorique-production-consommation>`,
            html` <c-page-sablier-crater></c-page-sablier-crater>`
        );

    private renderCoeurDePagePartProductionExportee = () =>
        importDynamique(
            import('./chapitres/indicateurs/ChapitrePartProductionExportee.js'),
            html` <c-chapitre-part-production-exportee
                .rapport=${this.rapport}
                .donneesPage=${this.donneesPage}
            ></c-chapitre-part-production-exportee>`,
            html` <c-page-sablier-crater></c-page-sablier-crater>`
        );

    private renderCoeurDePagePartSAUBio = () =>
        importDynamique(
            import('./chapitres/indicateurs/ChapitrePartSAUBio.js'),
            html` <c-chapitre-part-sau-bio .rapport=${this.rapport} .donneesPage=${this.donneesPage}></c-chapitre-part-sau-bio>`,
            html` <c-page-sablier-crater></c-page-sablier-crater>`
        );

    private renderCoeurDePageIndiceHVN = () =>
        importDynamique(
            import('./chapitres/indicateurs/ChapitreIndiceHVN.js'),
            html` <c-chapitre-indice-hvn .rapport=${this.rapport} .donneesPage=${this.donneesPage}></c-chapitre-indice-hvn>`,
            html` <c-page-sablier-crater></c-page-sablier-crater>`
        );

    private renderCoeurDePageTransformationDistribution = () =>
        importDynamique(
            import('./chapitres/ChapitreTransformationDistribution.js'),
            html` <c-chapitre-transformation-distribution
                .rapport="${this.rapport}"
                .donneesPage="${this.donneesPage}"
            ></c-chapitre-transformation-distribution>`,
            html` <c-page-sablier-crater></c-page-sablier-crater>`
        );

    private renderCoeurDePagePartPopulationDependanteVoiture = () =>
        importDynamique(
            import('./chapitres/indicateurs/ChapitrePartPopulationDependanteVoiture.js'),
            html` <c-chapitre-part-population-dependante-voiture
                .rapport="${this.rapport}"
                .donneesPage="${this.donneesPage}"
            ></c-chapitre-part-population-dependante-voiture>`,
            html` <c-page-sablier-crater></c-page-sablier-crater>`
        );

    private renderCoeurDePagePartTerritoireDependantVoiture = () =>
        importDynamique(
            import('./chapitres/indicateurs/ChapitrePartTerritoireDependantVoiture.js'),
            html` <c-chapitre-part-territoire-dependant-voiture
                .rapport="${this.rapport}"
                .donneesPage="${this.donneesPage}"
            ></c-chapitre-part-territoire-dependant-voiture>`,
            html` <c-page-sablier-crater></c-page-sablier-crater>`
        );

    private renderCoeurDePageDistancesPlusProchesCommerces = () =>
        importDynamique(
            import('./chapitres/indicateurs/ChapitreDistancesPlusProchesCommerces.js'),
            html` <c-chapitre-distances-plus-proches-commerces
                .rapport="${this.rapport}"
                .donneesPage="${this.donneesPage}"
            ></c-chapitre-distances-plus-proches-commerces>`,
            html` <c-page-sablier-crater></c-page-sablier-crater>`
        );

    private renderCoeurDePageConsommation = () =>
        importDynamique(
            import('./chapitres/ChapitreConsommation.js'),
            html` <c-chapitre-consommation .rapport=${this.rapport} .donneesPage=${this.donneesPage}></c-chapitre-consommation>`,
            html` <c-page-sablier-crater></c-page-sablier-crater>`
        );

    private renderCoeurDePagePartAlimentationAnimaleDansConsommation = () =>
        importDynamique(
            import('./chapitres/indicateurs/ChapitrePartAlimentationAnimaleDansConsommation.js'),
            html` <c-chapitre-part-alimentation-animale-dans-consommation
                .rapport="${this.rapport}"
                .donneesPage="${this.donneesPage}"
            ></c-chapitre-part-alimentation-animale-dans-consommation>`,
            html` <c-page-sablier-crater></c-page-sablier-crater>`
        );

    private renderCoeurDePageTauxPauvrete = () =>
        importDynamique(
            import('./chapitres/indicateurs/ChapitreTauxPauvrete.js'),
            html` <c-chapitre-taux-pauvrete .rapport="${this.rapport}" .donneesPage="${this.donneesPage}"></c-chapitre-taux-pauvrete>`,
            html` <c-page-sablier-crater></c-page-sablier-crater>`
        );

    private renderCoeurDePageAideAlimentaire = () =>
        importDynamique(
            import('./chapitres/indicateurs/ChapitreAideAlimentaire.js'),
            html` <c-chapitre-aide-alimentaire .rapport="${this.rapport}" .donneesPage=${this.donneesPage}></c-chapitre-aide-alimentaire>`,
            html` <c-page-sablier-crater></c-page-sablier-crater>`
        );

    private renderCoeurDePagePartPopulationObese = () =>
        importDynamique(
            import('./chapitres/indicateurs/ChapitrePartPopulationObese.js'),
            html` <c-chapitre-part-population-obese
                .rapport="${this.rapport}"
                .donneesPage="${this.donneesPage}"
            ></c-chapitre-part-population-obese>`,
            html` <c-page-sablier-crater></c-page-sablier-crater>`
        );
}
