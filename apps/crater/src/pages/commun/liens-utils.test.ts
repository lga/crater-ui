import { htmlstring } from '@lga/base';
import { templateResultToString } from '@lga/commun/build/composants/outils-composants.js';
import type { DefinitionIndicateur, DefinitionRapportEtude, DefinitionSourceDonnee, DefinitionSourceDonneeRetraitee } from '@lga/indicateurs';
import { describe, expect, it } from 'vitest';

import { construireLienVersMethodologieSources } from './liens-utils.js';

describe('Tests de liens-utils', () => {
    const s: DefinitionSourceDonnee = {
        id: 'id',
        nom: 'Nom source',
        definition: htmlstring`source`,
        annees: '2008 à 2012',
        perimetreGeographique: 'perimetre geographique',
        url: '',
        fournisseur: {
            nom: 'Nom fournisseur',
            lienLogo: '',
            largeurLogo: 100
        }
    };
    const sr: DefinitionSourceDonneeRetraitee = {
        id: 'id',
        nom: 'Nom source',
        annees: '2002',
        source: s
    };
    const r: DefinitionRapportEtude = {
        id: 'id',
        nom: 'Nom rapport',
        annee: 2012,
        auteur: 'auteur',
        url: 'url'
    };
    const i: DefinitionIndicateur = {
        id: 'id',
        libelle: 'Nom indicateur',
        description: htmlstring`source`,
        unite: 'unite',
        sources: [
            { source: s, anneesMobilisees: '2000' },
            { source: r, anneesMobilisees: '2020' }
        ]
    };
    it('Tester construireLienVersMethodologieSources avec une SourceDonnee', () => {
        expect(templateResultToString(construireLienVersMethodologieSources([{ source: s, anneesMobilisees: '2010' }]))).toEqual(
            `Source : <c-lien href="https://resiliencealimentaire.org">Les Greniers d'Abondance</c-lien>, à partir des données <c-lien href="/methodologie/sources-donnees#id">Nom source</c-lien> (Nom fournisseur, 2010)`
        );
    });

    it('Tester construireLienVersMethodologieSources avec une SourceDonneeRetraitee', () => {
        expect(templateResultToString(construireLienVersMethodologieSources([{ source: sr }]))).toEqual(
            `Source : <c-lien href="https://resiliencealimentaire.org">Les Greniers d'Abondance</c-lien>, à partir des données <c-lien href="/methodologie#id">Nom source</c-lien> (Nom fournisseur, 2002, retraité)`
        );
    });

    it('Tester construireLienVersMethodologieSources avec un RapportEtude', () => {
        expect(templateResultToString(construireLienVersMethodologieSources([{ source: r }]))).toEqual(
            `Source : <c-lien href="https://resiliencealimentaire.org">Les Greniers d'Abondance</c-lien>, à partir des données <c-lien href="url">Nom rapport</c-lien>, auteur (2012)`
        );
    });

    it('Tester construireLienVersMethodologieSources avec un Indicateur', () => {
        expect(templateResultToString(construireLienVersMethodologieSources([{ source: i }]))).toEqual(
            `Source : <c-lien href="https://resiliencealimentaire.org">Les Greniers d'Abondance</c-lien>, à partir des données <c-lien href="/methodologie">Nom indicateur</c-lien>`
        );
    });

    it('Tester construireLienVersMethodologieSources avec les trois types', () => {
        expect(
            templateResultToString(construireLienVersMethodologieSources([{ source: s, anneesMobilisees: '2022' }, { source: r }, { source: i }]))
        ).toEqual(
            `Source : <c-lien href="https://resiliencealimentaire.org">Les Greniers d'Abondance</c-lien>, à partir des données <c-lien href="/methodologie/sources-donnees#id">Nom source</c-lien> (Nom fournisseur, 2022) ; <c-lien href="url">Nom rapport</c-lien>, auteur (2012) ; <c-lien href="/methodologie">Nom indicateur</c-lien>`
        );
    });
});
