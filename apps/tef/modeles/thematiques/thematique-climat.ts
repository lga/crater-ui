import { formaterNombreSelonValeurString } from '@lga/base';
import { INDICATEURS, type ValeurIndicateur } from '@lga/indicateurs';

import { creerInfographieAnalogueClimatique } from '../constats/constats-climat';
import type { InfographieAnalogueClimatiqueModele, InfographieImageMessageModele } from '../constats/InfographieModele';
import type { DonneesApi } from '../fonctions-utils.js';
import type { ThematiqueModele } from './ThematiqueModele.js';

export interface ThematiqueClimatModele extends ThematiqueModele {
    indicateurArretesSecheresses: ValeurIndicateur;
    infographieAnalogueClimatique?: InfographieAnalogueClimatiqueModele | InfographieImageMessageModele;
}

export function creerThematiqueClimat(donneesApi: DonneesApi): ThematiqueClimatModele {
    const infographieAnalogueClimatique = creerInfographieAnalogueClimatique(donneesApi);
    const partSousArreteSecheresseEte2022 =
        ((donneesApi.diagnosticApi.eau.arretesSecheresse.indicateursParAnneesMois.find((i) => i.anneeMois === '2022-07-01')
            ?.partTerritoireEnArretePourcent ?? 0) +
            (donneesApi.diagnosticApi.eau.arretesSecheresse.indicateursParAnneesMois.find((i) => i.anneeMois === '2022-08-01')
                ?.partTerritoireEnArretePourcent ?? 0)) /
        2;

    infographieAnalogueClimatique.accroche = '';
    return {
        idTheme: 'climat',
        indicateurArretesSecheresses: {
            indicateur: INDICATEURS.alertesSecheresse,
            chiffre: formaterNombreSelonValeurString(partSousArreteSecheresseEte2022),
            unite: '%',
            chiffreComplement: `(${formaterNombreSelonValeurString(donneesApi.diagnosticApi.eau.arretesSecheresse.tauxImpactArretesSecheressePourcent)}% en moyenne entre 2016 et 2020)`
        },
        infographieAnalogueClimatique: infographieAnalogueClimatique
    };
}
