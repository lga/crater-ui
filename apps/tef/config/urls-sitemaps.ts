import type { CodeCategorieTerritoireApi, TerritoireSyntheseApi } from '@lga/specification-api';

const ID_NOM_FRANCE = 'france';
const URLS_AJOUT_EXPLICITE = [
    // En l'état nuxt-sitemap n'inclus pas ces routes qui ont un param optionnel
    '/diagnostics-flash',
    '/thematiques/accessibilite',
    '/thematiques/agriculteurs',
    '/thematiques/autonomie-alimentaire',
    '/thematiques/biodiversite',
    '/thematiques/climat',
    '/thematiques/consommation',
    '/thematiques/ressources',
    '/thematiques/terres-agricoles',
    '/thematiques/transformation-distribution',
    '/indicateurs',
    '/simulateur',
    '/initiatives'
];

export function calculerUrlsSitemap(apiBaseUrl: string): string[] {
    console.log(`#### Construction du sitemap`);
    return [...calculerUrlsTerritorialisees(apiBaseUrl), ...URLS_AJOUT_EXPLICITE];
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
function calculerUrlsTerritorialisees(apiBaseUrl: string) {
    const urlPays = [construireUrlPageAccueilTerritoire(ID_NOM_FRANCE)];
    // Désactivé pour le moment, car on ne veut pas indexer les pages régions et départements
    // const urlsRegions = await calculerUrlsSitemapPourCategorieTerritoire(apiBaseUrl, 'REGION');
    // const urlsDepartements = await calculerUrlsSitemapPourCategorieTerritoire(apiBaseUrl, 'DEPARTEMENT');
    // const urls = urlPays.concat(urlsRegions).concat(urlsDepartements);
    const urls = urlPays;
    console.log(`  => Ajout de ${urls.length} url(s) territorialisée(s)`);
    return urls;
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
async function calculerUrlsSitemapPourCategorieTerritoire(
    apiBaseUrl: string,
    codeCategorieTerritoire: CodeCategorieTerritoireApi
): Promise<string[]> {
    const urlRequete = `${await lazySupprimerSlashFinal(apiBaseUrl)}/crater/api/territoires?critere=${codeCategorieTerritoire}&nbMaxResultats=150`;
    console.log(`  => Construction pour la catégorie ${codeCategorieTerritoire}`);
    console.log(`    - récupération à partir de ${urlRequete}`);
    const response = await lazyFetchRetry(urlRequete).then((r) => {
        console.log(`    - réponse : ${r.statusText}`);
        return r.json() as Promise<TerritoireSyntheseApi[]>;
    });
    console.log(`    - nombre de territoires récupérés :  ${response.length}`);

    return response.map((t) => construireUrlPageAccueilTerritoire(t.idNom));
}

function construireUrlPageAccueilTerritoire(idNomTerritoire: string): string {
    return `/${idNomTerritoire}`;
}

async function lazyFetchRetry(url: string, delaiGlobalMs?: number): Promise<Response> {
    // Lazy loading pour que nuxt.config.ts puisse être parsé sans erreur quand @lga/base n'est pas buildé - par ex lors d'un prepare
    const module = await import('@lga/base');
    return module.fetchRetry(url, delaiGlobalMs);
}
async function lazySupprimerSlashFinal(url: string): Promise<string> {
    // Lazy loading pour que nuxt.config.ts puisse être parsé sans erreur quand @lga/base n'est pas buildé - par ex lors d'un prepare
    const module = await import('@lga/base');
    return module.supprimerSlashFinal(url);
}
