import '../../commun/template/TemplatePageAvecSommaire.js';
import '@lga/design-system/build/composants/Lien.js';

import { lien } from '@lga/design-system/build/composants/Lien.js';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { SOURCES_DONNEES, SOURCES_DONNEES_RETRAITEES } from '@lga/indicateurs';
import { IDS_DOMAINES } from '@lga/indicateurs';
import { html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import {
    PAGE_METHODOLOGIE_HASH_EAU,
    PAGE_METHODOLOGIE_HASH_ENERGIE,
    PAGE_METHODOLOGIE_HASH_FLUX_AZOTE,
    PAGE_METHODOLOGIE_HASH_PESTICIDES,
    PAGES_METHODOLOGIE,
    PAGES_PRINCIPALES
} from '../../../configuration/pages/declaration-pages.js';
import { construireUrlPageMethodologieSourceDonneeRetraitee } from '../../../configuration/pages/pages-utils.js';
import { construireLienVersMethodologieSourceDonnee } from '../../commun/liens-utils.js';
import { STYLES_CRATER } from '../../commun/pages-styles';
import { construireLiensFilArianePagesMethodologie, ITEMS_MENU_METHODOLOGIE, STYLES_METHODOLOGIE } from './methodologie-utils.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-page-methodologie-intrants': PageMethodologieIntrants;
    }
}

@customElement('c-page-methodologie-intrants')
export class PageMethodologieIntrants extends LitElement {
    static styles = [STYLES_DESIGN_SYSTEM, STYLES_CRATER, STYLES_METHODOLOGIE];
    render() {
        return html`
            <c-template-page-avec-sommaire
                idItemActifMenuPrincipal="${PAGES_PRINCIPALES.aide.getId()}"
                idItemActifMenuSommaire="${PAGES_METHODOLOGIE.intrants.getId()}"
                .itemsMenuSommaire="${ITEMS_MENU_METHODOLOGIE}"
                .liensFilAriane="${construireLiensFilArianePagesMethodologie(
                    PAGES_METHODOLOGIE.intrants.getTitreCourt(),
                    PAGES_METHODOLOGIE.intrants.getUrl()
                )}"
                idElementCibleScroll=${this.idElementCibleScroll}
            >
                <div slot="contenu">${this.renderCoeurPage()}</div>
            </c-template-page-avec-sommaire>
        `;
    }

    @property()
    idElementCibleScroll = '';

    private renderCoeurPage() {
        return html`
          <section class="texte-moyen">
                <h1>Règles de gestion pour le maillon Intrants</h1>

                <h2 id="${PAGE_METHODOLOGIE_HASH_ENERGIE}">Les indicateurs de dépendance à l'énergie</h2>

                <h3>Pourquoi des indicateurs sur la consommation d'énergie du secteur agricole ?</h3>
                <p>Un système alimentaire, considéré du point de vue de l’énergie, est un système qui fournit aux humains de l’énergie contenue dans les produits alimentaires grâce aux transformations de matières permises par l’énergie solaire (photosynthèse) et l’énergie utilisée à chaque étape du processus de production, transformation et distribution des produits alimentaires.</p>
                <p>Cette dernière peut avoir des sources différentes : le carburant pour les machines agricoles et les camions de transport, l’électricité pour le chauffage, l’éclairage des bâtiments d’élevage, etc.</p>
                <p>Aujourd’hui, ces sources d’énergie sont essentiellement fossiles. Cette situation est la conséquence de la révolution agricole des pays du Nord au tournant des années 60. Elles sont passées d’une origine animale (traction animale, force humaine) à fossile, grâce à la disponibilité en pétrole et en gaz et le développement conjoint du machinisme agricole et des engrais de synthèse.</p>
                <p>Constatant depuis des années la volatilité des prix des énergies fossiles, et n’ayant pour seule perspective leur raréfaction générale, le développement d’un indicateur de dépendance pour le système alimentaire français paraît nécessaire tant pour évaluer la menace que pour sensibiliser à cet enjeu majeur. Les conséquences lourdes de la guerre en Ukraine sur les approvisionnements en engrais et ses prolongements dans les prix des denrées agricoles montrent que la dépendance énergétique du secteur agricole est bel et bien un enjeu de sécurité alimentaire. D’ici 2050, la production de pétrole des principaux fournisseurs de l’UE aura baissé de moitié (source : rapport The Shift Project 2022). Il faut donc s’attendre à une diminution sérieuse des approvisionnements.</p>

                <h3>Périmètres des indicateurs</h3>
                <p>Les postes d’énergie suivant sont estimés :</p>
                <ul>
                    <li>Énergie directe : carburants pour les machines agricoles, chauffage des bâtiments d’élevage, chauffage des serres, irrigation.</li>
                    <li>Énergies indirectes : production du matériel agricole, des engrais azotés, de l’alimentation importée de l'étranger pour le bétail.</li>
                </ul>
                <p>L'énergie est dite indirecte quand elle concerne un produit qui n'est pas directement utilisé comme source d'énergie – un carburant par exemple – mais en a nécéssité pour le produire. Les engrais azotés de synthèse sont ainsi une énergie indirecte et désignent l'énergie qu'il a fallu pour les produire.</p>
                <p>Ces postes couvrent 90% de la consommation totale à l'échelle de la France. Les 10% restants rassemblent par exemple le séchage et la conservation des produits agricoles ou la production des produits phytosanitaires.</p>
                <p>Les résultats sont exprimés en énergie primaire (EP) car cela permet de comparer et sommer les énergies directe et indirecte (qui est forcément exprimée sous forme d’énergie primaire).</p>

                <h3>Principe de calcul des indicateurs</h3>
                <p>Le calcul des différents postes de consommation d'énergie est largement adaptée de la
                        <c-lien
                            href="https://librairie.ademe.fr/produire-autrement/3486-climagri-un-diagnostic-energie-gaz-a-effet-de-serre-au-service-d-une-demarche-de-territoire.html"
                            >méthodologie Climagri</c-lien
                        >
                        développée par l'<c-lien href="https://www.ademe.fr">ADEME</c-lien>.
                        Cette adaptation a été réalisée avec l'aide précieuse de <c-lien href="https://solagro.org/">SOLAGRO</c-lien>.</p>

                <h3>Carburants pour les tracteurs</h3>

                <h4>Données d'entrée</h4>
                <ul>
                  <li>Surfaces agricoles utiles issues du ${construireLienVersMethodologieSourceDonnee(SOURCES_DONNEES.rpg)} [ha]</li>
                  <li>Coefficients de consommation énergétique par catégories de cultures issus de ${construireLienVersMethodologieSourceDonnee(SOURCES_DONNEES.climagri)} [L/ha]</li>
                  <li>Coefficient de conversion d’un litre de carburant en énergie primaire issus de ${construireLienVersMethodologieSourceDonnee(SOURCES_DONNEES.climagri)} [GJ/L]</li>
                </ul>

                <h4>Méthode de calcul</h4>
                <blockquote>
                <ol>
                    <li>
                        <p>Calcul de la consommation de carburant pour chaque surface de culture :</p>
                        <p>consommation [L] = SAU de la culture [ha] * coefficient de consommation [L/ha]</p>
                    </li>
                    <li>Agrégation par communes</li>
                    <li>
                        <p>Conversion en énergie primaire en utilisant le coefficient de conversion :</p>
                        <p>énergie [GJ] = consommation [L] * coefficient de conversion [GJ/L]</p>
                    </li>
                    <li>Agrégation aux échelons supérieurs</li>
                </ol>
                </blockquote>

                <h4>Limites</h4>
                <p>Les pratiques culturales (labour, semis direct...) ne sont pas prises en compte. Les coefficients utilisés reflètent donc des pratiques moyennes en France.</p>

                <h3>Matériel agricole</h3>
                
                <p>Le principe de calcul est exactement le même que pour les carburants des tracteurs en utilisant d'autres coefficients de consommation énergétique.</p>

                <h3>Irrigation</h3>

                <h4>Données d'entrée</h4>
                <ul>
                  <li>${lien('#' + IDS_DOMAINES.irrigation, 'Prélèvements annuels d’eau utilisés en irrigation non gravitaire par territoires')} [m³]</li>
                  <li>Coefficient de consommation énergétique moyen de l’irrigation issu de ${construireLienVersMethodologieSourceDonnee(SOURCES_DONNEES.climagri)} [kWh/m³]</li>
                  <li>Coefficients de répartition par sources d’énergie (électricité vs fioul) issus de ${construireLienVersMethodologieSourceDonnee(SOURCES_DONNEES.climagri)} [%]</li>
                  <li>Coefficients de conversion des sources d’énergie entre énergies finale et primaire issus de ${construireLienVersMethodologieSourceDonnee(SOURCES_DONNEES.climagri)} [GJ/GJ]</li>
                </ul>

                <h4>Méthode de calcul</h4>
                <blockquote>
                <ol>
                    <li>
                        <p>Calcul de la consommation énergétique finale :</p>
                        <p>consommation [GJ] = volume_irrigation_hors_gravitaire [m³] * coefficient_consommation_energetique_irrigation_hors_gravitaire [kWh/m³] * coefficient_conversion_energie [GJ/kWh]</p>
                    </li>
                    <li>Ventilation par sources d’énergie</li>
                    <li>Conversion en énergie primaire</li>
                </ol>
                </blockquote>

                <h4>Limites</h4>
                <p>Il n’y a pas de données de répartition des systèmes d’irrigation à l’échelle de la France ou plus fin, et pas de corrélation évidente avec les cultures. En conséquence on utilise un coefficient qui correspond à la moyenne France, calculé sur base des volumes d’irrigation totaux par types d’irrigation hors gravitaire estimés dans le diagnostic Climagri France.</p>

                <h3>Chauffage des serres</h3>
                <p>En France, les cultures sous serres chauffées concernent surtout une partie des surfaces de tomates, de concombres, de fraises, de melons et de l’horticulture.
                On distingue essentiellement deux types de serres chauffées : les serres en maraîchage (utilisées surtout pour les tomates et concombres) qui consomment entre 200 et 400 kWh/m² et les tunnels hors gel (utilisés surtout pour les fraises, melons) qui consomment plutôt autour de 25 kWh/m².
                CRATer évalue uniquement l’énergie utilisée pour le chauffage des serres en maraîchage pour les tomates et concombres, faute de données disponibles pour les fraises, melons et l’horticulture. 
                On estime que la consommation énergétique de ces derniers représente une part inférieure à 20% du total (source : diagnostic Climagri France).</p>

                <h4>Données d'entrée</h4>
                <ul>
                  <li>Surfaces de serres chauffées pour les tomates et concombres par départements issues de l'${construireLienVersMethodologieSourceDonnee(SOURCES_DONNEES.etude_serres)} [m²]</li>
                  <li>${lien(
                      construireUrlPageMethodologieSourceDonneeRetraitee(SOURCES_DONNEES_RETRAITEES.surfaces_agricoles_crater),
                      'Surfaces agricoles productives'
                  )} [ha]</li>
                  <li>Consommations moyennes des serres (chauffage + électricité) par zones géographiques issus de l'${construireLienVersMethodologieSourceDonnee(SOURCES_DONNEES.etude_serres)} [kWh/m²]</li>
                  <li>Coefficients de conversion des sources d’énergie entre énergies finale et primaire issus de ${construireLienVersMethodologieSourceDonnee(SOURCES_DONNEES.climagri)} [GJ/GJ]</li>
                </ul>

                <h4>Méthode de calcul</h4>
                <blockquote>
                <ol>
                    <li>
                        <p>Calcul de la consommation énergétique pour chaque source d’énergie au niveau départemental :</p>
                        <p>consommation [GJ] = surface_serre [ha] * 10 000 [m²/ha] * coefficient_consommation_energetique_finale [kWh/m²] * coefficient_conversion_energie [GJ/kWh]</p>
                    </li>
                    <li>Conversion en énergie primaire</li>
                    <li>Calcul aux échelons supérieurs (pas de répartition infra-départementale)</li>
                </ol>
                </blockquote>
                <h4>Limites</h4>
                <p>L’estimation ne concerne que les concombres et tomates et n’est pas disponible à l'échelle infra-départementale.</p>

                <h3>Bâtiments d’élevage : chauffage & climatisation, électricité et fioul hors chauffage</h3>

                <h4>Données d'entrée</h4>
                <ul>
                  <li>${lien(
                      construireUrlPageMethodologieSourceDonneeRetraitee(SOURCES_DONNEES_RETRAITEES.cheptels_crater),
                      'Nombre de têtes par cheptels'
                  )} [têtes]</li>
                  <li>Coefficients de consommation énergétique pour le chauffage des bâtiments par catégories d’animaux issus de ${construireLienVersMethodologieSourceDonnee(SOURCES_DONNEES.climagri)} [kWh/tête]</li>
                  <li>Coefficients définissant le mix énergétique pour le chauffage des bâtiments par catégories d’animaux issus de ${construireLienVersMethodologieSourceDonnee(SOURCES_DONNEES.climagri)} [%]</li>
                  <li>Coefficients de consommation énergétique d'électricité des bâtiments d’élevage par catégories d’animaux issus de ${construireLienVersMethodologieSourceDonnee(SOURCES_DONNEES.climagri)} [kWh/tête]</li>
                  <li>Coefficients définissant le mix électrique France issus de ${construireLienVersMethodologieSourceDonnee(SOURCES_DONNEES.climagri)} [%]</li>
                  <li>Coefficients de consommation énergétique de fioul des bâtiments d’élevage pour chaque catégorie d’animaux issus de ${construireLienVersMethodologieSourceDonnee(SOURCES_DONNEES.climagri)} [L/tête/jour]</li>
                  <li>Temps de présence moyen en bâtiment d’élevage pour chaque catégorie d’animaux issus de ${construireLienVersMethodologieSourceDonnee(SOURCES_DONNEES.climagri)} [jour]</li>
                  <li>Coefficients de conversion des énergies finale vers primaire issus de ${construireLienVersMethodologieSourceDonnee(SOURCES_DONNEES.climagri)}</li>
                </ul>

                <h4>Méthode de calcul de la consommation énergétique du chauffage</h4>
                <blockquote>
                <ol>
                    <li>Conversion des données de cheptel Agreste en données de cheptel Climagri. Pour cela, on utilise la répartition fine des différentes catégories d'animaux du RA 2020 (total France métropolitaine) pour reconstituer chaque catégorie Climagri à partir d’une ou plusieurs catégories Agreste.</li>
                    <li>Calcul de la consommation énergétique totale pour chaque catégorie d’animaux Climagri par multiplication des cheptels avec les coefficients d’énergie</li>
                    <li>Ventilation par source d’énergie en utilisant les coefficients de mix énergétique</li>
                    <li>Conversion en énergie primaire en utilisant les coefficients de conversion</li>
                </ol>
                </blockquote>

                <h4>Méthode de calcul de la consommation d'électricité hors chauffage (machines de traite et autres machines, éclairage...)</h4>
                <blockquote>
                Même principe que précédemment en utilisant les coefficients de consommation énergétique pour l’électricité en bâtiments d’élevage et le mix électrique de la France.
                </blockquote>

                <h4>Méthode de calcul de la consommation de fioul des bâtiments d’élevage (carburant consommé par les véhicules, notamment pour le transport de la nourriture)</h4>
                <blockquote>
                Même principe que précédemment en utilisant les coefficients d’énergie pour la consommation de fioul des bâtiments d’élevage et les temps de présence moyens des animaux.
                </blockquote>

                <h3>Engrais azotés</h3>

                <h4>Données d'entrée</h4>
                <ul>
                  <li>${lien(
                      PAGES_METHODOLOGIE.presentationGenerale.getUrl({
                          idElementCibleScroll: PAGE_METHODOLOGIE_HASH_FLUX_AZOTE
                      }),
                      'Besoin en azote minéral de synthèse pour chaque commune obtenu par le modèle MacDyn-Fs'
                  )} [kgN/an]</li>
                  <li>${construireLienVersMethodologieSourceDonnee(SOURCES_DONNEES.ventes_engrais_azotes_synthese)} par types d'engrais [tN/an]</li>
                  <li>Coefficients de consommation énergétique par types d’engrais issus de ${construireLienVersMethodologieSourceDonnee(SOURCES_DONNEES.climagri)} [GJ/tN]</li>
                </ul>

                <h4>Méthode de calcul</h4>
                <blockquote>
                <ol>
                    <li>
                        <p>Calcul d’un coefficient de coût énergétique moyen de fabrication des fertilisants azotés (tous types confondus) [GJ_EP/tN] :</p>
                        <ul>
                            <li>Calcul de la part de chaque type d’engrais dans le total des ventes en utilisant les statistiques globales au niveau France (pour s’affranchir des biais liés à la différence entre lieux d’achat et lieux d’utilisation) et en lissant les résultats sur 3 ans (pour réduire l’effet de stock) [tN]</li>
                            <li>Calcul de la moyenne des coûts énergétiques pondérée par les tonnages par types d’engrais [GJ/tN]</li>
                        </ul>
                    </li>
                    <li>Calcul du coût énergétique total par communes par multiplication des besoins en azote minéral de la commune avec le coût énergétique moyen [GJ]</li>
                    <li>Calcul aux échelons supérieurs</li>
                </ol>
                </blockquote>

                <h3>Alimentation animale importée de l’étranger</h3>

                <h4>Données d'entrée</h4>
                <p>
                <ul>
                  <li>${lien(
                      PAGES_METHODOLOGIE.presentationGenerale.getUrl({
                          idElementCibleScroll: PAGE_METHODOLOGIE_HASH_FLUX_AZOTE
                      }),
                      'Importations nettes de nourriture par communes et par groupes d’aliments provenant du modèle MacDyn-FS'
                  )} [kgN]</li>
                  <li>Importations nationales de nourriture de l’étranger par types d’aliments (FAOSTAT - échelle France - valeur de 2020) [tonnes de matière brute, tMB]</li>
                  <li>Coefficients de consommation énergétique liée à la production de nourriture animale à l’étranger par types d’aliments issus de ${construireLienVersMethodologieSourceDonnee(SOURCES_DONNEES.climagri)} [GJ/tN]</li>
                  <li>Importations nationales de nourriture de l’étranger par types d’aliments (FAOSTAT - échelle France - valeur de 2020) [tonnes de matière brute, tMB]</li>
                  <li>Correspondances entre les différentes typologies d’aliments : classifications EFESE (groupes d’aliments: fourrages, concentrés énergétiques, concentrés protéiques), Climagri et FAOSTAT (types d’aliments: colza grain, tourteaux de soja…)</li>
                </ul>
                </p>

                <h4>Méthode de calcul</h4>
                <blockquote>
                <ol>
                    <li>
                        <p>Estimation de l’importation nette de nourriture animale depuis l’étranger en azote au niveau des communes par groupes d’aliments [tN] :</p>
                        <ul>
                            <li>Pour cela, on calcule le total France des importations nettes de nourriture des communes par groupes d’aliments = solde d’importation</li>
                            <li>Pour chacun des groupes d’aliments, si le solde est positif (ie que l’on a besoin d’importer de l’étranger), on calcule le même total que précédemment mais pour les communes dont les importations sont positives = total d’importation</li>
                            <li>On en déduit la part des importations nettes des communes importatrices qui provient de l’étranger : part = solde d’importation / total d’importation</li>
                            <li>Cette part est appliquée aux importations nettes pour obtenir les importations nettes depuis l’étranger</li>
                        </ul>
                    </li>
                    <li>
                        <p>Estimation des coefficients de consommation énergétique liée à la production de nourriture animale importée de l’étranger par groupes d’aliments [GJ/tN]:</p>
                        <ul>
                            <li>À partir des teneurs en azote par quantité de matière brute : on convertit les coefficients de consommation énergétique en GJ/tN et les importations nationales de nourriture de l’étranger par types d’aliments en tN</li>
                            <li>On calcule finalement les coefficients énergétiques par groupes d’aliments en faisant la moyenne des coefficients par types d’aliments, pondérée par la part du type d’aliment dans le groupe d’aliment</li>
                        </ul>
                    </li>
                    <li>Estimation de la consommation énergétique liée à l’import de nourriture animale au niveau des communes par multiplication des importations nettes de nourriture animale depuis l’étranger par les coefficients énergétiques [GJ]</li>
                    <li>Calcul aux échelons supérieurs</li>
                </ol>
                </blockquote>
                
                <h3 id="${IDS_DOMAINES.energie}">Calcul de l'indicateur Consommation d'énergie primaire de l'agriculture par hectare agricole</h3>
                <p>
                  Cette indicateur est calculé sur base de l'ensemble des consommations calculées comme décrit ci-dessus :
                </p>
                <blockquote>
                  consommation_energetique_primaire_par_hectare [GJ/ha] =  ∑ consommations_energetiques_par_postes [GJ] / surface_agricole_utile [ha]
                </blockquote>

                <h3>Calcul de la note énergie</h3>
                <p>La note est calculée à partir de l'indicateur consommation_energetique_primaire_par_hectare. 
                  La distribution des valeurs de cet indicateur pour les départements permet d'obtenir des seuils de référence : le minimum (min), le premier quartile (Q1), la médiane (Q2), et le troisième quartile (Q3). 
                  À partir de ces seuils les notes sont calculées comme suit :</p>
                <blockquote>
                si consommation_energetique_primaire_par_hectare = min alors note = 10</br>
                si consommation_energetique_primaire_par_hectare = Q1 alors note = 7.5</br>
                si consommation_energetique_primaire_par_hectare = Q2 alors note = 5</br>
                si consommation_energetique_primaire_par_hectare = Q3 alors note = 2.5</br>
                si consommation_energetique_primaire_par_hectare >= Q3 + 1.5 * (Q3 - Q1) alors note = 0</br>
                si consommation_energetique_primaire_par_hectare est entre 2 seuils, la note est calculée par interpolation linéaire  
                </blockquote>

                <h2 id=${PAGE_METHODOLOGIE_HASH_EAU}>Les indicateurs de dépendance à l'eau</h2>

                <h3 id="${IDS_DOMAINES.irrigation}">Indicateurs sur les volumes d'eau prélevés pour l'irrigation des cultures</h3>

                <p>Ces indicateurs sont calculés à partir des ${construireLienVersMethodologieSourceDonnee(SOURCES_DONNEES.prelevements_irrigation)}.</p>

                <h4>Calcul du volume d’eau utilisé pour l’irrigation par années en m³</h4>
                <p>Le calcul de cet indicateur est réalisé à partir des mesures de prélèvements annuels par ouvrage pour chaque année retenue
                        (${SOURCES_DONNEES.prelevements_irrigation.annees}) :</p>
                <ul>
                    <li>pour chaque commune, calcul du volume brut prélevé par année à destination de l’irrigation, tel que mesuré au niveau des
                        ouvrages :
                        <ul>
                            <li>obtenu par somme des volumes prélevés pour l’usage IRRIGATION sur tous les ouvrages de la commune ;</li>
                            <li>pour les communes sans données, le volume prélevé est positionné à 0 (voir les limitations des données 
                                ${construireLienVersMethodologieSourceDonnee(SOURCES_DONNEES.prelevements_irrigation)}) ;
                            </li>
                        </ul>
                    </li>
                    <li>calcul du volume prélevé pour l’irrigation pour les territoires supra-communaux en faisant la somme des valeurs de chaque
                        commune, pour chaque année ;
                    </li>
                    <li>et enfin, calcul du volume estimé de prélèvement au niveau de chaque commune. Pour cela le volume de prélèvements calculé au niveau de l’EPCI est reparti sur les communes, au prorata de la SAU productive hors prairies.
                    </li>
                </ul>
                <p>Remarques :</p>
                        <ul>
                            <li>
                                La dernière étape du calcul permet de modérer le biais sur les données de mesure qui ne permettent pas d’identifier finement
                                la commune sur laquelle est effectivement située la parcelle irriguée du fait de la répartition géographique
                                hétérogène des ouvrages.
                            </li>
                            <li>
                                En conséquence, les valeurs obtenues pour les communes sont des estimations, et non des mesures. Elles peuvent différer des valeurs présentes dans ${construireLienVersMethodologieSourceDonnee(SOURCES_DONNEES.prelevements_irrigation)}">la source de données</c-lien>.
                            </li>
                            <li>
                                Malgré cet ajustement, il est possible que le volume de prélèvements présenté au niveau de la commune - voire de l'EPCI - soit
                                surestimé ou sous-estimé de manière significative, en particulier pour les territoires comportant des réseaux de canaux d'irrigation
                                (ex: Pyrénées-Orientales, Bouches-du-Rhône, etc.). Voir les limites dans ${construireLienVersMethodologieSourceDonnee(
                                    SOURCES_DONNEES.prelevements_irrigation
                                )}.
                            </li>
                        </ul>
                    </li>
                </ul>

                <h4>Calcul du volume d’eau utilisé pour l’irrigation par années en mm ou m³/ha</h4>
                <p>La valeur en m³ est ramenée à une valeur en m³/ha ou mm, ce qui permet de proposer un indicateur comparable entre territoires :
                </p>
                <blockquote>
                  irrigation_annee_n [mm] = (volume_irrigation_annee_n [m³] / sau_productive_hors_prairies [m²]) * 1000 [mm]
                  irrigation_annee_n [m³/ha] = 10 * irrigation_annee_n [mm]
                </blockquote>
              
                <h4>Calcul d’une valeur moyenne sur les cinq dernières années</h4>
                <p>Le calcul de la moyenne sur les 5 années les plus récentes permet de ramener les séries annuelles à une seule valeur et d’obtenir les indicateurs irrigation_m3 et irrigation_mm.
                </p>

                <h4>Calcul de la tendance de l’irrigation sur les années considérées</h4>
                <p>Une régression linéaire sur les mesures de l’indicateur irrigation_annee_n permet d’évaluer la tendance suivie par la série, que l’on peut ensuite exprimer en évolution moyenne sur base du coefficient de la droite de régression.
                </p>

                <h4>Calcul de la note irrigation</h4>
                <p>La note est calculée à partir de l'indicateur irrigation_mm (moyenne des 5 dernières années). 
                  La distribution des valeurs de cet indicateur pour les départements permet d'obtenir des seuils de référence : le minimum (min), le premier quartile (Q1), la médiane (Q2), et le troisième quartile (Q3). 
                  À partir de ces seuils les notes sont calculées comme suit :</p>
                <blockquote>
                si irrigation_mm = min alors note = 10</br>
                si irrigation_mm = Q1 alors note = 7.5</br>
                si irrigation_mm = Q2 alors note = 5</br>
                si irrigation_mm = Q3 alors note = 2.5</br>
                si irrigation_mm >= Q3 + 1.5 * (Q3 - Q1) alors note = 0</br>
                si irrigation_mm est entre 2 seuils, la note est calculée par interpolation linéaire  
                </blockquote>
                <p>A titre indicatif en 2020 (ie moyenne 2016-2020) les valeurs des seuils sont : min=0, Q1=1mm, Q2=9mm, Q3=30mm, et Q3+1,5*(Q3-Q1)=85mm.</p>
              
              
              <h4>Pratiques d'irrigation : répartition des surfaces irriguées en fonction des types de cultures</h4>
              <p>Des indicateurs sur la répartition des surfaces irriguées en fonction des types de cultures sont présentés en complément des données de prélèvement.</p>
              <p>Ces données sont calculées :</p>
              <ul>
                  <li>essentiellement à partir de la source de données ${construireLienVersMethodologieSourceDonnee(SOURCES_DONNEES.pratiques_irrigation)}</li>
                  <li>en intégrant également les cultures de riz grâce aux données de ${construireLienVersMethodologieSourceDonnee(SOURCES_DONNEES.surfaces_riz)}. Cette distinction est intéressante car le riz est une culture fortement irriguée (hypothèse : 100% des surfaces de riz irriguées par irrigation gravitaire, ~ 20 000 m³/ha/an selon Climagri). Cette étape permet d’enrichir la classification Agreste du RA 2010 avec le type de culture “Riz”</li>
                  <li>uniquement pour les échelles départementales et supérieures</li>
              </ul>
                                
              <h4>Évaluation des volumes d'eau mobilisés pour l'irrigation gravitaire et non gravitaire</h4>
              <p>À partir de l'évaluation des volumes d'eau prélevés, des indicateurs de pratiques d’irrigation, et des coefficients ClimAgri d’irrigation par type de culture (cas France, onglet A5a), il est possible d'évaluer les volumes d'eau consacrés à l'irrigation gravitaire et non gravitaire : </p>
              <blockquote>
                <ol>
                  <li>pour chaque département, à partir des surfaces irriguées (issues des indicateurs de pratiques d’irrigation) et des coefficients d’irrigation Climagri, on calcule un volume d’eau estimé pour l’irrigation gravitaire et irrigation hors gravitaire : irrigation_gravitaire_modelisee_m3 et irrigation_hors_gravitaire_modelisee_m3</li>
                  <li>Ces valeurs estimées permettent d’obtenir à l’échelle des départements, régions et pays, 2 coefficients : part_irrigation_gravitaire et part_irrigation_hors_gravitaire</li>
                  <li>Ces coefficients sont finalement appliqués à l’indicateur irrigation_m3 pour obtenir irrigation_gravitaire_m3, et irrigation_hors_gravitaire_m3</li>
                  <li>Pour les territoires infra-départementaux, ce sont les valeurs part_irrigation_gravitaire et part_irrigation_hors_gravitaire du département qui sont utilisées</li>
              </ol>
            </blockquote>
              <p>Ces indicateurs ne sont pas présentés dans l'application, mais ils sont utilisés par ailleurs, dans les calculs sur la dépendance à l'énergie pour l'irrigation.</p>
                  
              
              <h3 id="${IDS_DOMAINES.secheresses}">Indicateurs de disponibilité de l’eau : les arrêtés sécheresse</h3>
              
              <p>Les arrêtés sécheresse sont pris au cours du temps sur l’ensemble du territoire français lorsque la disponibilité de la ressource en eau devient limitée.
                  Ils permettent d’approcher l’état quantitatif de la ressource à travers le temps et l’espace.</p>
              <p>L'indicateur principal utilisé pour représenter la disponibilité de l'eau est donc la part du territoire concernée par des mesures de restriction d’usage de l’eau. Cette approche est inspiré des pages 20-21 du rapport <c-lien href="https://www.ofb.gouv.fr/sites/default/files/Fichiers/Plaquettes%20et%20rapports%20instit/datalab_80_chiffres_cles_eau_edition_2020_decembre2020_1.pdf">Eau et milieux aquatiques, Les chiffres clés Édition 2020</c-lien> édité par le Service des Données et Études Statistiques (SDES) en partenariat avec l’Office français de la biodiversité (OFB).
                </p>
              <p>Il est calculé à partir des ${construireLienVersMethodologieSourceDonnee(SOURCES_DONNEES.donnees_secheresses)} disponibles pour les années ${
                  SOURCES_DONNEES.donnees_secheresses.annees
              }.
                  
              </p>
              
                <h4>Calcul de la part de chaque territoire concernée par un arrêté sécheresse</h4>
              <p>Cet indicateur part_territoire_en_arrete_par_annee_mois donne pour chaque mois et chaque territoire la part du territoire (en superficie) qui a été concernée par un arrêté sécheresse de niveau “alerte” minimum impactant l’usage d’eaux superficielles.</p>
              <p>Il est calculé via les étapes suivantes :</p>
              <blockquote>
              <ol>
                <li>Prétraitement des bases des arrêtés sécheresse et des géométries des zones d’alerte. 
                    Au vu de l'absence d'identifiant commun entre les 2 bases, un ID est construit sur base du code de la zone, son département, le type de zone (partie de l’id appelé catégorie de zone) et la superficie de la zone en hectares ;</li>
                <li>Calcul de l’intersection entre les géométries des communes et les géométries des zones d’alerte pour connaître la part de chaque commune contenue dans chaque zone d’alerte;</li>
                <li>Pour chaque jour et chaque commune, somme sur toutes les zones d’alerte concernées par un arrêté de niveau supérieur à “alerte” et de type “superficielle”, de la part de commune dans la zone d’alerte. Remarque : Un même jour, il peut arriver que plusieurs arrêtés, dont les zones d’alerte se recoupent, soient actifs. Un même morceau de territoire peut
                    alors être comptabilisé plusieurs fois ce qui se traduit par une “part de la commune en zone d’alerte” supérieure à 1. Dans ce cas la valeur est ramenée à 1 ;</li>
                <li>Agrégation par mois en faisant la moyenne sur les différents jours composant le mois ;</li>  
                <li>Agrégation aux niveaux supra-communaux par moyenne des valeurs des communes appartenant aux territoires, pondérée par la superficie des communes.</li>
              </ol>                   
            </blockquote>

              <h4>Calcul du taux d’impact des arrêtés sécheresse estivaux pour chaque année</h4>
              <p>Cet indicateur taux_impact_arretes_secheresse_pourcent permet d’évaluer l’impact estival annuel des arrếtés sécheresse, il est calculé en faisant la moyenne sur les mois de juillet et août sur chaque année.</p>

              <h4>Calcul d’une valeur moyenne sur les cinq dernières années</h4>
              <p>Le calcul de la moyenne sur les 5 années les plus récentes du taux d’impact estival permet de ramener les séries annuelles à une seule valeur et d’obtenir un indicateur taux_impact_arretes_secheresse_pourcent.
                  Par souci de cohérence, la moyenne est calculée sur les mêmes années que l’indicateur irrigation_mm.
              </p>

              <h4>Calcul de la tendance du taux d’impact estival des arrếtes sécheresse sur les années considérées</h4>
              <p>Une régression linéaire sur les mesures de l'indicateur taux_impact_arretes_secheresse_par_annees_pourcent permet d’évaluer la tendance suivie par la série, que l’on peut ensuite exprimer en évolution moyenne sur base du coefficient de la droite de régression.</p>

              <h4>Calcul de la note sur l’impact des arrêtés sècheresse</h4>
              <p>La note est obtenue à partir des valeur de l’indicateur taux_impact_arrete_secheresse_pourcent de la manière suivante :</p>
              <blockquote>
                  si taux_impact_arrete_secheresse_pourcent = 0% alors note = 10</br>
                  si taux_impact_arrete_secheresse_pourcent = 100% alors note = 0</br>
                  si taux_impact_arrete_secheresse_pourcent entre ces 2 seuils, la note est obtenue par interpolation linéaire</br>
              </blockquote>
              
              
              <h3 id="${IDS_DOMAINES.eau}">Calcul de la note globale pour la dépendance à l'eau</h3>
              <p>
                  La note finale note_eau, exprimant la dépendance à la ressource eau pour l’irrigation est obtenue en faisant la moyenne de la note irrigation et de la note impact des arrêtés sècheresse :</p>
              <blockquote>
                  note_eau = (note_irrigation + note_arretes_secheresse) / 2
              </blockquote>
              
                <h2 id=${PAGE_METHODOLOGIE_HASH_PESTICIDES}>
                    Les indicateurs de dépendance aux pesticides : <abbr title="Quantité de Substances Actives">QSA</abbr>,
                    <abbr title="NOmbre de Doses Unités">NODU</abbr> et <abbr title="NOmbre de Doses Unités">NODU</abbr>
                    normalisé
                </h2>

                <p>
                    Note : pour de plus amples détails sur la méthodologie utilisée, vous pouvez consulter
                    <c-lien href="/pdf/Methodologie_indicateurs_produits_phytosanitaires_v2.1.pdf">
                        le document d'élaboration de cet méthodologie
                    </c-lien
                    >
                    .
                </p>

                <h3>Pourquoi des indicateurs sur l’utilisation des pesticides ?</h3>
                <p>
                    Symboles de l’industrialisation des pratiques, les pesticides sont devenus indispensables à l’agriculture en France aujourd’hui.
                    D’un point de vue agronomique, la grande homogénéité des systèmes agricoles et les faibles niveaux de biodiversité sauvage
                    favorisent les « bioagresseurs » (insectes, champignons pathogènes, etc.) et limitent leur régulation naturelle. D’un point de vue
                    économique, la concurrence internationale, le faible prix des marchandises agricoles et la non prise en compte des coûts
                    sanitaires et environnementaux incitent mécaniquement les exploitations à maximiser les volumes de production à l’aide des
                    pesticides. Si les pesticides, en tant qu’outils à disposition des agriculteurs pour faire face à la propagation brutale d’un
                    bioagresseur, pourraient être perçus comme un facteur de résilience, la dépendance structurelle de notre modèle agricole à leur
                    utilisation est au contraire source de vulnérabilité. Leur usage massif et systématique entretient un cercle vicieux qui les rend
                    toujours plus indispensables : les bioagresseurs résistants sont naturellement sélectionnés et la toxicité des substances dégrade
                    les fonctions de régulation remplies par les espèces sauvages. De plus, leur fabrication dépend de ressources fossiles en voie
                    d’épuisement et est contrôlée par une poignée de multinationales en situation d’oligopole, plongeant les agriculteurs dans une
                    situation de dépendance risquée au vu de l’augmentation des tensions économiques et politiques à venir.
                </p>

                <h3 id="${IDS_DOMAINES.qsaNodu}">Quantité de Substances Actives (QSA)</h3>
                <p>La Quantité de Substances Actives correspond à la quantité totale, en kg, de substances actives.</p>

                <p>
                    Les substances actives contenues dans les pesticides vendus sont classées selon leur niveau de toxicité. Avant 2015 la
                    classification était la suivante :
                </p>
                <ul>
                    <li>T, T+, CMR : substance toxique, très toxique, cancérogène mutagène reprotoxique</li>
                    <li>N minéral : substance minérale dangereuse pour l’environnement</li>
                    <li>N organique : substance organique dangereuse pour l’environnement</li>
                    <li>Autre : autre substance</li>
                </ul>
                <p>
                    À partir de 2015 : les substances classées CMR 2 (supposés) sont intégrées dans la catégorie « T, T+, CMR » alors qu’elles étaient
                    auparavant catégorisées «N organique »
                </p>
                <p>À partir de 2019 la nomenclature de la classification est modifiée :</p>
                <ul>
                    <li>CMR: substance toxique, très toxique, cancérogène mutagène reprotoxique</li>
                    <li>
                        Santé A: toxicité aiguë de catégorie 1, 2 ou 3 ou toxicité spécifique pour certains organes cibles, de catégorie 1, à la suite
                        d’une exposition unique ou après une exposition répétée, soit en raison de leurs effets sur ou via l’allaitement
                    </li>
                    <li>
                        Env A : toxicité aiguë pour le milieu aquatique de catégorie 1 ou toxicité chronique pour le milieu aquatique de catégorie 1
                        ou 2
                    </li>
                    <li>Env B : toxicité chronique pour le milieu aquatique de catégorie 3 ou 4</li>
                    <li>Autre : autre substance</li>
                </ul>

                <h4>Dose Unité (DU)</h4>
                <p>
                    La <abbr title="Dose Unité">DU</abbr>, en kg/ha, correspond à la dose maximale applicable sur un hectare. Elle reflète en ce sens
                    la puissance des substances actives.
                </p>
                <p>Remarque : certaines substances n'ont pas de <abbr title="Dose Unité">DU</abbr>.</p>

                <h4>Nombre de Doses Unités (NODU)</h4>
                <p>
                    Le <abbr title="NOmbre de Doses Unités">NODU</abbr>, en hectares, est calculé pour chaque subtsance active en faisant le ratio de
                    la <abbr title="Quantité de Substances Actives">QSA</abbr> avec la <abbr title="Dose Unité">DU</abbr> de la substance active. Le
                    calcul du <abbr title="NOmbre de Doses Unités">NODU</abbr> permet donc de comparer et d'additionner des substances actives qui
                    n'ont pas le même impact à quantité égale utilisée.
                </p>
                <p>
                    <strong
                    >N. B. : les substances sans <abbr title="Dose Unité">DU</abbr> n'ont de facto pas de
                        <abbr title="NOmbre de Doses Unités">NODU</abbr>.</strong
                    >
                </p>

                <h3 id="${IDS_DOMAINES.noduNormalise}">Nombre de Doses Unités normalisé (NODU normalisé)</h3>
                <p>
                    Le <abbr title="NOmbre de Doses Unités">NODU</abbr> normalisé, grandeur adimensionnelle, est calculé en faisant le ratio entre le
                    <abbr title="NOmbre de Doses Unités">NODU</abbr> et la surface agricole utile (SAU) totale du territoire. Il peut s'interpréter
                    comme le nombre moyen de traitements par pesticides que reçoivent les terres agricoles du territoire, en tenant compte de la
                    toxicité des produits employés. Il permet de faire des comparaisons entre territoires et/ou périodes temporelles différent(e)s.
                </p>
                <p>
                    <strong
                    >N. B. : cet indicateur étant calculé à partir du <abbr title="NOmbre de Doses Unités">NODU</abbr> présenté plus haut, il ne
                        tient compte que de certaines substances actives. De plus, étant calculé sur base de la SAU totale, il peut cacher des
                        sur-dosages locaux.</strong
                    >
                </p>

                <h4>Données d'entrée</h4>

                <p>
                    Les indicateurs utilisent la base de données
                    <abbr title="Banque Nationale des Ventes de produits phytopharmaceutiques par les Distributeurs agréés">BNVD</abbr>
                    donnant les achats de produits phytosanitaires avec les ${construireLienVersMethodologieSourceDonnee(
                        SOURCES_DONNEES.quantites_substances_actives
                    )} qu'ils
                    contiennent par code postal de l'acheteur ; les ${construireLienVersMethodologieSourceDonnee(
                        SOURCES_DONNEES.doses_unites_substances_actives
                    )} ; et les
                    ${construireLienVersMethodologieSourceDonnee(SOURCES_DONNEES.sau_ra)}.
                </p>

                <h4>Méthode de calcul</h4>
                <p>Les étapes du calcul sont les suivantes :</p>
                <blockquote>
                    <ol>
                        <li>
                            import des doses unitaires : utilisation des <abbr title="Dose Unité">DU</abbr> décrites dans l’arrêté de 2019, complétées
                            par celles décrites dans l’arrêté de 2017 et non présentes dans celui de 2019.
                        </li>
                        <li>
                            import des <abbr title="Quantité de Substances Actives">QSA</abbr> par code postal acheteur et département sur l’ensemble
                            des années considérées (2015 à 2019)
                            <strong>en incluant toutes les substances sauf celles appartenant à la classification <i>Autre</i>.</strong>
                        </li>
                        <li>
                            calcul des <abbr title="NOmbre de Doses Unités">NODU</abbr> par substance via la formule
                            <abbr title="NOmbre de Doses Unités">NODU</abbr> = <abbr title="Quantité de Substances Actives">QSA</abbr> /
                            <abbr title="Dose Unité">DU</abbr>.
                            <strong>Il n’est donc disponible que pour les substances disposant d’une <abbr title="Dose Unité">DU</abbr>.</strong>
                        </li>
                        <li>
                            calcul du QSA_avec_DU, QSA_sans_DU et <abbr title="NOmbre de Doses Unités">NODU</abbr> total (toutes substances confondues
                            sauf classées <i>Autre</i>) :
                            <ol>
                                <li>
                                    au niveau des communes : la répartition depuis le code postal vers les communes est faite au prorata de la SAU de
                                    chaque commune (cas des codes postaux couvrants plusieurs communes) ;
                                </li>
                                <li>
                                    au niveau des EPCI et des regroupements de communes (ex: PAT) : le calcul est fait par somme des résultats obtenus
                                    au niveau des communes ;
                                </li>
                                <li>au niveau des départements : les résultats sont directement importés des données départementales ;</li>
                                <li>
                                    au niveau des régions et de la France métropolitaine : le calcul est fait par somme des résultats obtenus au
                                    niveau des départements.
                                </li>
                            </ol>
                        </li>
                        <li>
                            calcul du <abbr title="NOmbre de Doses Unités normalisé">NODU_normalisé</abbr> pour chaque année et chaque territoire
                            (communes, EPCI, regroupements de communes, département, région, pays), via la formule :
                            <abbr title="NOmbre de Doses Unités normalisé">NODU_normalisé</abbr> = <abbr title="NOmbre de Doses Unités">NODU</abbr> /
                            SAU.
                        </li>
                        <li>
                            calcul du QSA_avec_DU, QSA_sans_DU, du <abbr title="NOmbre de Doses Unités">NODU</abbr> et du
                            <abbr title="NOmbre de Doses Unités normalisé">NODU_normalisé</abbr> de l’année n par moyenne sur les années n, n-1 et
                            n-2. On obtient donc des valeurs pour les années 2017, 2018, 2019 et 2020.
                        </li>
                        <li>suppression des données QSA_avec_DU, QSA_sans_DU et <abbr title="NOmbre de Doses Unités">NODU</abbr> à l’échelle de la
                        commune et attribution du <abbr title="NOmbre de Doses Unités normalisé">NODU_normalisé</abbr> de leur EPCI (voir les limites
                        données plus bas).</li>
                    </ol>
                </blockquote>

                <p>Les résulats sont donc donnés :</p>
                <ul>
                    <li>pour les EPCI, autres regroupements de communes (PAT, PNR..), départements, régions, France entière ; le tout hors DROM.</li>
                    <li>
                        en moyenne triennale pour les années 2017, 2018, 2019 et 2020 (valeur année n = moyenne des années n, n-1 et n-2; e.g. 2019
                        correspond à la moyenne 2017, 2018 et 2019) ;
                    </li>
                    <li>toutes substances non classées <i>Autre</i> confondues.</li>
                </ul>

                <h4>Limites</h4>
                <p>
                    La <abbr title="Banque Nationale des Ventes de produits phytopharmaceutiques par les Distributeurs agréés">BNVD</abbr>
                    renseigne sur les achats de produits phytosanitaires. Les produits peuvent donc être utilisés ailleurs (que le code postal d’achat
                    renseigné, qui est lié à la domiciliation de l’acheteur). Les valeurs calculées à l’échelle de l’EPCI sont utilisées pour les 
                    communes pour limiter ce problème (notamment celui des exploitations qui sont sur plusieurs communes). Cela ne corrige
                    cependant pas les cas suivants :
                </p>

                <ul>
                    <li>exploitation sur plusieurs EPCI (dont les achats sont attribués au siège de l’exploitation) ;</li>
                    <li>achats de produits de traitement des récoltes avant entreposage (produits non épandus dans les champs) ;</li>
                    <li>
                        achats de produits à usage non agricole (services de voiries des mairies ou de la SNCF). L’usage des produits donné dans la
                        base ANSES n’a pas pu être utilisé pour isoler ces cas pour plusieurs raisons : 1. l’absence de données d’usage pour de
                        nombreux produits ; 2. l’usage mixte de nombreux produits (eg TOUCHDOWN FORET contenant du glyphosate utilisé par la SNCF :
                        sur 22 usages, seuls 2 ont pour cible les voies ferrées) avec très peu de produits utilisé.
                    </li>
                </ul>
                <p>Les produits peuvent également être utilisés après l'année d'achat (stockage). La moyenne triennale permet de lisser cet effet</p>
                <p>Limitation sur le périmètre des substances actives prises en compte :</p>
                <ul>
                    <li>
                        les substances actives provenant de produits importés (semences, fourrages, aliments...) et qui finissent in fine dans
                        l’environnement sont absents.
                    </li>
                    <li>
                        Les arrêtés ne sont pas exhaustifs : les <abbr title="Dose Unité">DU</abbr> de certaines substances actives sont inconnues.
                        Aussi, ils référencent les substances via leur nom sans leur numéro CAS . Si l’orthographe est différente de celle utilisée
                        dans la <abbr title="Banque Nationale des Ventes de produits phytopharmaceutiques par les Distributeurs agréés">BNVD</abbr>,
                        la correspondance ne peut pas être réalisée hormis via un travail manuel chronophage. L’utilisation de la base de données des
                        substances actives de l’ANSES (e-phy) qui contient pour chaque substance, un numéro CAS et des variants de noms ne peut être
                        utilisée car pour certaines substances, les variants proposés ne correspondent parfois pas à la même molécule mais à une
                        molécule de la même famille (e.g. glyphosate associé aux variants glyphosate sel monosodium | glyphosate sel de diméthylamine
                        | glyphosate sel d'ammonium [...]) qui n’a peut être pas forcément la même <abbr title="Dose Unité">DU</abbr>.
                    </li>
                    <li>
                        pour pallier à ces limites, le choix a été fait de se limiter aux substances non classées dans Autre, ce dernier étant le
                        groupe pour lequel il manque le plus de <abbr title="Dose Unité">DU</abbr> . Une partie des achats n’est donc pas prise en
                        compte, ce qui a pour conséquence de sous-estimer les valeurs de <abbr title="NOmbre de Doses Unités">NODU</abbr>.
                    </li>
                </ul>
                <p>
                    Limitation sur la mesure de la SAU : La SAU utilisée inclut les prairies permanentes qui sont pourtant les surfaces sur lesquelles
                    sont peu épandus de pesticides. Cependant, aucune donnée fiable et complète n’est disponible : le recensement de 2010 ne donne pas
                    les valeurs pour toutes les communes du fait du secret statistique ; le RPG sous-estime les surfaces agricoles en général,
                    notamment les zones viticoles fortes consommatrices de pesticides.
                </p>
                <p>
                    Le choix a été fait d’utiliser la SAU totale issue du recensement agricole 2020 qui est exhaustive pour les communes. Quand les
                    données plus détaillées du RA 2020 seront publiée, il sera possible de retirer la SAU toujours en herbe pour les communes non
                    soumises au secret statistique.
                </p>

                <h3 id="${IDS_DOMAINES.pesticides}">Calcul de la note pour les pesticides</h3>
                <p>
                    La note est basée sur l'indicateur NODU_normalisé. 
                    La distributions des valeurs de cet indicateur pour les départements permet d'obtenir le seuil du troisième quartile (Q3), pour calculer la note comme suit :</p>
                <blockquote>
                    si NODU_normalisé = 0 alors note = 10<br>
                    si NODU_normalisé = 1 alors note = 5<br>
                    si NODU_normalisé > max(Q3, 2) alors note = 0<br>
                    si NODU_normalisé entre ces seuils, la note est obtenue par interpolation linéaire
                </blockquote>

              <h2 id=${IDS_DOMAINES.intrants}>Évaluation globale du maillon intrants</h2>
              <p>
                  La note finale du maillon Intrants est obtenue en faisant la moyenne des notes énergie, eau et pesticides :
              </p>
              <blockquote>
                  note_intrants = (note_energie + note_eau + note_pesticides) / 3
              </blockquote>


          </section>
        `;
    }
}
