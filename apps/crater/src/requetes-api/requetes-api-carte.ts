import { fetchRetry } from '@lga/base';
import { EchelleTerritoriale } from '@lga/territoires';
import type { GeoJsonObject } from 'geojson';

import type { ValeurIndicateurTerritoire } from '../pages/carte/composants/widget-carte/TerritoiresVisiblesCarte';
import geojsonHotelsDeDepartementsUrl from '../ressources/geojson/hotels-de-departements.geojson';
import geojsonHotelsDeRegionsUrl from '../ressources/geojson/hotels-de-regions.geojson';

export function chargerIndicateurs(
    apiBaseUrl: string,
    nomIndicateurRequeteApi: string,
    categorieTerritoire: string,
    idsDepartements?: string
): Promise<ValeurIndicateurTerritoire[]> {
    let urlApi = '';
    if (categorieTerritoire === EchelleTerritoriale.Communes.id) {
        urlApi = `${apiBaseUrl}/crater/api/indicateurs/${nomIndicateurRequeteApi}${
            nomIndicateurRequeteApi.includes('?') ? '&' : '?'
        }categorieTerritoire=${categorieTerritoire}&idsDepartements=${idsDepartements}`;
    } else {
        urlApi = `${apiBaseUrl}/crater/api/indicateurs/${nomIndicateurRequeteApi}${
            nomIndicateurRequeteApi.includes('?') ? '&' : '?'
        }categorieTerritoire=${categorieTerritoire}`;
    }

    return fetchRetry(urlApi).then((response) => response.json());
}

export function chargerContoursTerritoires(apiBaseUrl: string, nomGeojson: string, idDepartement?: string) {
    if (nomGeojson === EchelleTerritoriale.Communes.nomGeojsonEtUrl) {
        return fetchRetry(apiBaseUrl + `/crater/api/cartes/geojson/${nomGeojson}-crater/communes_du_departement_${idDepartement}.geojson`).then(
            (response) => response.json()
        );
    }
    return fetchRetry(apiBaseUrl + `/crater/api/cartes/geojson/${nomGeojson}-crater.geojson`).then((response) => response.json());
}

export function chargerCoucheHotelsDepartements(): Promise<GeoJsonObject> {
    return fetchRetry(geojsonHotelsDeDepartementsUrl).then((response) => response.json());
}

export function chargerCoucheHotelsRegions(): Promise<GeoJsonObject> {
    return fetchRetry(geojsonHotelsDeRegionsUrl).then((response) => response.json());
}
