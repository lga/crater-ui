import type { BoundingBoxApi, CodeCategorieTerritoireApi, DiagnosticApi } from '@lga/specification-api/build/specification/api';
import type express from 'express';

import {
    ajouterBodyJson,
    convertirDateEnString,
    Etat,
    extraireValeurDepuisSousObjet,
    jsonToCsv,
    SAUT_LIGNE,
    trierObjet,
    verifierEtatApi
} from '../communs/apiUtils';
import { ErreurRequeteIncorrecte, ErreurRessourceNonTrouvee } from '../communs/erreurs';
import { CATEGORIES_TERRITOIRES } from '../territoires/domaines/entitesTerritoires';
import type { ServicesDiagnostics } from './ServicesDiagnostics';

interface PathParamApiIndicateurs {
    nomIndicateur: string;
}
interface QueryParamApiIndicateurs {
    annee?: string;
    idsDepartements?: string;
    categorieTerritoire?: string;
    longitudeMin?: string;
    longitudeMax?: string;
    latitudeMin?: string;
    latitudeMax?: string;
}
type RequeteApiIndicateurs = express.Request<PathParamApiIndicateurs, unknown, unknown, QueryParamApiIndicateurs>;

const CATEGORIE_TOUTES_COMMUNES = 'TOUTES_COMMUNES';

type ParametreApiIndicateurCodeCategorieTerritoire = CodeCategorieTerritoireApi | 'TOUTES_COMMUNES';

export class ApiDiagnostics {
    private servicesDiagnostics: ServicesDiagnostics;
    private expressApp: express.Application;
    public etat = Etat.INIT_EN_COURS;

    constructor(expressApp: express.Application, servicesDiagnostics: ServicesDiagnostics) {
        this.expressApp = expressApp;
        this.servicesDiagnostics = servicesDiagnostics;
    }

    configurerRoutes() {
        this.expressApp.get('/crater/api/diagnostics/:idTerritoire', this.construireReponseDiagnosticsJson);
        this.expressApp.get('/crater/api/diagnostics/csv/:idTerritoire', this.construireReponseDiagnosticsCsv);
        this.expressApp.get('/crater/api/indicateurs/:nomIndicateur', this.construireReponseIndicateursJson);
    }

    private construireReponseDiagnosticsJson = (req: express.Request, res: express.Response) => {
        verifierEtatApi(this.etat);
        const diagnostic = this.servicesDiagnostics.rechercher(req.params.idTerritoire);
        if (!diagnostic) {
            throw new ErreurRessourceNonTrouvee('Diagnostic non trouvé (idTerritoire=' + req.params.idTerritoire + ')');
        }
        ajouterBodyJson(res, diagnostic);
    };

    private construireReponseDiagnosticsCsv = (req: express.Request, res: express.Response) => {
        verifierEtatApi(this.etat);
        const diagnostic = this.servicesDiagnostics.rechercher(req.params.idTerritoire);
        if (!diagnostic) {
            throw new ErreurRessourceNonTrouvee('Diagnostic non trouvé (idTerritoire=' + req.params.idTerritoire + ')');
        }

        res.setHeader('Content-Type', 'text/csv; charset=utf-8');
        res.setHeader('Content-disposition', `attachment; filename= ${diagnostic.idTerritoire}_${convertirDateEnString(new Date())}.csv`);
        const baseUrl = req.protocol + '://' + req.hostname;
        let csvBody = `Résultat du diagnostic CRATer pour le territoire ${diagnostic.nomTerritoire}, exporté le ${convertirDateEnString(
            new Date()
        )}${SAUT_LIGNE}`;
        csvBody += `Adresse du rapport CRATer;${baseUrl}/diagnostic/${diagnostic.idTerritoire}${SAUT_LIGNE}`;
        csvBody += `Sources de données utilisées pour le calcul;${baseUrl}/aide/methodologie/sources-donnees${SAUT_LIGNE}`;
        csvBody += `${SAUT_LIGNE}${SAUT_LIGNE}`;
        csvBody += jsonToCsv(trierObjet(diagnostic)).join(SAUT_LIGNE);
        res.send(csvBody);
    };

    private construireReponseIndicateursJson = (req: RequeteApiIndicateurs, res: express.Response) => {
        verifierEtatApi(this.etat);
        const parametreCategorieTerritoire = this.extraireParametreCategorieTerritoire(req);
        const filtreAnnee = req.query.annee ? req.query.annee.toString() : '';
        const filtreIdsDepartements = req.query.idsDepartements ? req.query.idsDepartements.toString().split('|') : '';
        const filtreBbox: BoundingBoxApi | undefined = this.extraireParametresBoundingBox(req);

        let diagnostics: DiagnosticApi[] = [];
        if (filtreBbox) {
            diagnostics = this.rechercherIndicateursParBoundingBox(parametreCategorieTerritoire, filtreBbox);
        } else {
            diagnostics = this.rechercherIndicateursParCategorieTerritoireOuIdsDepartements(parametreCategorieTerritoire, filtreIdsDepartements);
        }

        // TODO : faire une version "compacte" de la réponse, qui permettrait d'avoir un objet json plus court
        // par ex : "i" plutot que idTerritoire, "v" plutot que valeur, et idCodeTerritoire plutot qu'idTerritoire qui contient le nom
        const listeIndicateurs = diagnostics.map((d) => {
            return {
                idTerritoire: d.idTerritoire,
                valeur: extraireValeurDepuisSousObjet(d, req.params.nomIndicateur, filtreAnnee)
            };
        });
        // Pas d'utilisation de ajouterBodyJson ici pour éviter un tri inutile
        res.setHeader('Content-Type', 'application/json;charset=utf-8');
        res.json(listeIndicateurs);
    };

    private extraireParametreCategorieTerritoire(req: RequeteApiIndicateurs): ParametreApiIndicateurCodeCategorieTerritoire {
        const categorieTerritoireString = req.query.categorieTerritoire ? req.query.categorieTerritoire.toString() : '';
        if (CATEGORIES_TERRITOIRES.includes(categorieTerritoireString as CodeCategorieTerritoireApi)) {
            return categorieTerritoireString as CodeCategorieTerritoireApi;
            // TODO : Temporaire pour permettre de récupérer les indicateurs pour toutes les communes. En attente d'une solution plus propre
        } else if (categorieTerritoireString === CATEGORIE_TOUTES_COMMUNES) {
            return CATEGORIE_TOUTES_COMMUNES;
        } else {
            throw new ErreurRequeteIncorrecte(
                "Le paramètre categorieTerritoire est obligatoire. Il doit être renseigné avec une valeur de code categorie territoire valide ou 'TOUTES_COMMUNES'"
            );
        }
    }

    private extraireParametresBoundingBox(req: RequeteApiIndicateurs): BoundingBoxApi | undefined {
        const longitudeMinParam = req.query.longitudeMin;
        const longitudeMaxParam = req.query.longitudeMax;
        const latitudeMinParam = req.query.latitudeMin;
        const latitudeMaxParam = req.query.latitudeMax;
        if (!longitudeMinParam && !longitudeMaxParam && !latitudeMinParam && !latitudeMaxParam) {
            return undefined;
        } else {
            if (
                longitudeMinParam &&
                longitudeMaxParam &&
                latitudeMinParam &&
                latitudeMaxParam &&
                !Number.isNaN(parseFloat(longitudeMinParam)) &&
                !Number.isNaN(parseFloat(longitudeMaxParam)) &&
                !Number.isNaN(parseFloat(latitudeMinParam)) &&
                !Number.isNaN(parseFloat(latitudeMaxParam))
            ) {
                return {
                    longitudeMin: parseFloat(longitudeMinParam),
                    longitudeMax: parseFloat(longitudeMaxParam),
                    latitudeMin: parseFloat(latitudeMinParam),
                    latitudeMax: parseFloat(latitudeMaxParam)
                };
            } else {
                throw new ErreurRequeteIncorrecte(
                    "Les paramètres longitudeMin, longitudeMax, latitudeMin et latitudeMax sont partiellement ou mal renseignés : il faut soit n'en donner aucun, soit les donner tous avec un format de nombre correct"
                );
            }
        }
    }

    private rechercherIndicateursParBoundingBox(
        parametreCategorieTerritoire: ParametreApiIndicateurCodeCategorieTerritoire,
        filtreBbox: BoundingBoxApi
    ) {
        if ((CATEGORIES_TERRITOIRES as string[]).includes(parametreCategorieTerritoire)) {
            return this.servicesDiagnostics.rechercherTousParCategorieEtBoundingBox(
                parametreCategorieTerritoire as CodeCategorieTerritoireApi,
                filtreBbox
            );
        } else {
            throw new ErreurRequeteIncorrecte(
                'La recherche par bounding box nécessite que le paramètre categorieTerritoire soit renseigné avec un valeur de code categorie territoire valide'
            );
        }
    }

    private rechercherIndicateursParCategorieTerritoireOuIdsDepartements(
        parametreCategorieTerritoire: CodeCategorieTerritoireApi | 'TOUTES_COMMUNES',
        filtreIdsDepartements: string[] | string
    ) {
        if (parametreCategorieTerritoire === 'COMMUNE' && filtreIdsDepartements === '')
            throw new ErreurRequeteIncorrecte('Le paramètre idsDepartements est obligatoire si categorieTerritoire=COMMUNE');

        // TODO : Temporaire pour permettre de récupérer les indicateurs pour toutes les communes. En attente d'une solution plus propre
        const categorieTerritoire: CodeCategorieTerritoireApi =
            parametreCategorieTerritoire === CATEGORIE_TOUTES_COMMUNES ? 'COMMUNE' : parametreCategorieTerritoire;

        let diagnostics: DiagnosticApi[] = [];
        if (categorieTerritoire === 'COMMUNE' && filtreIdsDepartements) {
            diagnostics = this.servicesDiagnostics.rechercherCommunesParDepartements(filtreIdsDepartements);
        } else {
            diagnostics = this.servicesDiagnostics.rechercherTousParCategorie(categorieTerritoire);
        }
        return diagnostics;
    }
}
