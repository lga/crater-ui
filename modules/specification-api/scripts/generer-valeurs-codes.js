// Script d'extraction des types union string dans des tableaux de string contenant toutes les valeurs possibles pour chaque enum
// Utile pour vérifier au runtime qu'une valeur injectée dans un code est correcte (voir les injecteurs de /api )
// Par convention, on cherche tous les fichiers de type commencant par "Code" (tous les types énumerés sont nommés ainsi dans la spec de l'api)

import * as fs from 'fs';
import * as path from 'path';

const DOSSIER_SOURCES_TYPES = './src/specification/api/models';
const DOSSIER_RESULTAT = './src/specification/valeurs-codes';

genererModuleValeursCodes(DOSSIER_SOURCES_TYPES, DOSSIER_RESULTAT);

function genererModuleValeursCodes(dossierSourcesTypes, dossierResultat) {
    reinitialiserDossierResultats(dossierResultat);

    const lignesImports = [];
    const lignesDeclarations = [];

    fs.readdirSync(dossierSourcesTypes).forEach((fichierSource) => {
        const estFichierTypeUnionString = fichierSource.startsWith('Code') && path.extname(fichierSource) === '.ts';
        if (estFichierTypeUnionString) {
            const contenuFichier = fs.readFileSync(path.join(dossierSourcesTypes, fichierSource), 'utf-8');
            const lignesDeclarationValeursCodes = genererLignesDeclarationValeursCodes(fichierSource, contenuFichier);
            lignesImports.push(lignesDeclarationValeursCodes.import);
            lignesDeclarations.push(lignesDeclarationValeursCodes.declaration);
        }
    });
    genererFichierIndex([...lignesImports, ...lignesDeclarations], dossierResultat);
}

function reinitialiserDossierResultats(nomDossier) {
    if (fs.existsSync(nomDossier)) {
        fs.rmSync(nomDossier, { recursive: true });
    }
    fs.mkdirSync(nomDossier);
}

function genererLignesDeclarationValeursCodes(fichierSource, contenuFichier) {
    let ligneImport = '';
    let ligneDeclarationTableau = '';
    const valeursUnionString = contenuFichier.match(/'.*?'|\bnull\b/g);
    if (valeursUnionString) {
        const valeursElementsTableau = valeursUnionString.join(', ');
        const nomTypeCode = `${path.basename(fichierSource, '.ts')}`;
        const nomVariable = `valeurs${nomTypeCode}`;
        ligneImport = `import type { ${nomTypeCode} } from '../api/models/${nomTypeCode}';`; //import type { CodeProfilAgriculture } from '../api/models/CodeProfilAgriculture';
        ligneDeclarationTableau = `export const ${nomVariable}:${nomTypeCode}[] = [${valeursElementsTableau}];`;
    }
    return { import: ligneImport, declaration: ligneDeclarationTableau };
}

function genererFichierIndex(lignesDeclarationsValeursCodes, dossierResultat) {
    const contenuFichierIndex = lignesDeclarationsValeursCodes.join('\n');
    const cheminFichierIndex = path.join(dossierResultat, 'index.ts');
    console.log(`Génération des valeurs pour le type union string dans le fichier ${cheminFichierIndex}`);
    fs.writeFileSync(cheminFichierIndex, contenuFichierIndex);
}
