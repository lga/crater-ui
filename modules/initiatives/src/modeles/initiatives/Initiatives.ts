import type { HtmlString, ImageModele } from '@lga/base';
import type { IdConstatOuTheme } from '@lga/indicateurs';

import type { TagProfil } from '../tags-profils';

export type TagSujet =
    | 'Sobriété foncière'
    | 'Agroécologie'
    | 'Protection des terres'
    | 'Facilitation logistique'
    | 'Filières de proximité'
    | 'Restauration collective'
    | 'Approvisionnement en produits locaux'
    | 'Installation agricole'
    | 'Lutte contre la précarité alimentaire'
    | 'Commerces alimentaires'
    | 'Gestion eau'
    | 'Autonomie';

export interface Initiative {
    id: string;
    titre: string;
    description: HtmlString;
    idTerritoire: string;
    localisation: string;
    idsConstatsThemes: IdConstatOuTheme[];
    tagsSujets: TagSujet[];
    tagsProfilsScoreEleve: TagProfil[];
    tagsProfilsScoreMoyen: TagProfil[];
    tagsProfilsScoreFaible: TagProfil[];
    bonusScore: number;
    informationSource: string;
    urlLienExterne: string;
    intituleLienExterne: string;
    image: ImageModele;
}

export interface InitiativeAvecScore extends Initiative {
    score: number;
}
