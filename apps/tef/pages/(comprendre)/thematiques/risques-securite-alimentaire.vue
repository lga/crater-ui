<template>
    <TemplatePage>
        <TemplateArticle titre="Menaces et enjeux pour la sécurité alimentaire en France" :sectionsMenu="SECTIONS">
            <template #bandeauTitre>
                <TexteP>
                    En France, comme dans la plupart des pays industrialisés, la
                    <GloseAvecPopoverDefinition cleGlose="securiteAlimentaire"></GloseAvecPopoverDefinition> est souvent considérée comme acquise de
                    longue date. Plusieurs faits majeurs mettent toutefois ce récit en défaut, si bien que celle-ci est en réalité loin d’être
                    atteinte et pourrait même se voir définitivement compromise sans changement rapide de trajectoire.
                </TexteP>
            </template>
            <template #[ID_SECTION_1]>
                <TitreH3>Manger c’est toute une affaire !</TitreH3>

                <TexteP>
                    L’industrialisation a bouleversé la façon dont nous produisons notre nourriture. Du champ à l’assiette, une multitude de machines,
                    d’usines, de procédés industriels, de camions, de travailleurs et d’institutions interviennent et font tourner le système
                    alimentaire. Derrière les produits les plus banals des grandes surfaces, se cache une organisation extrêmement complexe se
                    déployant à l’échelle mondiale. C’est ce modèle agro-industriel qui aujourd’hui produit l’essentiel de la nourriture d’un pays
                    comme la France.
                </TexteP>

                <Image
                    src="/images/introduction-enjeux-securite-alimentaire/systeme-alimentaire_852x583.png"
                    alt="Schéma du système alimentaire"
                    :width="852"
                    :height="583"
                    source="Les Greniers d’Abondance, CC BY-NC-SA"
                >
                    Représentation simplifiée du système alimentaire. Certains éléments comme l’environnement et les ressources, les marchés, la
                    recherche et la formation sont absents.
                </Image>

                <TitreH3>Qui se paie au prix fort</TitreH3>

                <TexteP>
                    La généralisation de ce modèle a permis en quelques décennies de réaliser des progrès indéniables en matière de productivité
                    agricole. Jamais l’humanité n’a ainsi produit autant de nourriture. Pourtant, près de la moitié d’entre elle souffre de
                    malnutrition. En cherchant à produire toujours plus à des coûts toujours plus faibles, le modèle agro-industriel se paye en
                    réalité au prix fort. Les prix bas en supermarché ont pour revers une dégradation de la santé publique, la précarité de millions
                    de travailleurs, et des dommages environnementaux d’ampleur planétaire.
                </TexteP>

                <Image
                    src="/images/introduction-enjeux-securite-alimentaire/almeria_600x400.jpg"
                    alt="Almeria"
                    :width="600"
                    :height="400"
                    credit="© Yann Arthus Bertrand"
                >
                    Dans le Sud de l’Espagne, la péninsule d’Alméria est recouverte de serres destinées à la culture intensive de fruits et légumes
                    sur une superficie équivalente à trois fois Paris (31&nbsp;000 hectares). Dans cette « mer de plastique », la main-d’œuvre se
                    compose principalement de travailleurs saisonniers immigrés d’Afrique du Nord, logés dans des bidonvilles. Ils endurent jusqu’à
                    douze heures par jour des conditions de travail accablantes (températures atteignant 50°C, pauses quasi-inexistantes, exposition
                    aux pesticides et aux particules cancérigènes) et ne bénéficient pas de protection sociale ni du salaire minimum légal.
                </Image>
            </template>

            <template #[ID_SECTION_2]>
                <TitreH3>La fin d’une époque</TitreH3>

                <TexteP>
                    D'ici une trentaine d'années, le niveau moyen d'humidité des sols correspondra aux records de sécheresse actuels dans plusieurs
                    régions françaises. L’épuisement des énergies fossiles va directement réduire la disponibilité en engrais, en pesticides et en
                    carburants tout en déstabilisant le système de transport. Il va nous falloir gérer à la fois une diminution tendancielle des
                    rendements et la multiplication des situations de crise (événement climatique extrême, choc énergétique, crise économique...).
                </TexteP>

                <Image
                    src="/images/introduction-enjeux-securite-alimentaire/vigne_brulee_400x400.jpg"
                    alt="Vigne brulée"
                    :width="400"
                    :height="400"
                    credit="© Chai d’Emilien"
                >
                    Au cours de l’été 2019, des milliers d’hectares de vignes ont été brûlés par la chaleur dans l’Hérault et dans le Gard. Ces vagues
                    de chaleur extrêmes vont augmenter en intensité et en durée.
                </Image>

                <TitreH3>Pas de plan B !</TitreH3>

                <TexteP>
                    Face à la mise sous tension croissante du modèle agro-industriel, nos vulnérabilités sont criantes. La population agricole ne
                    cesse de décliner, la qualité des sols se dégrade, des dizaines de milliers d’hectares fertiles disparaissent sous le béton chaque
                    année, notre système alimentaire est de plus en plus dépendant de transports longue distance, de chaînes de production
                    mondialisées et de technologies complexes. Du côté des pouvoirs publics, la sécurité alimentaire n’est plus un sujet depuis
                    longtemps et rien n’est prévu en cas de dysfonctionnement de la machine agro-industrielle.
                </TexteP>

                <TexteP> Sans changement en profondeur, c’est bien notre sécurité alimentaire qui est en danger. </TexteP>

                <Image
                    src="/images/introduction-enjeux-securite-alimentaire/camions_800x533.jpg"
                    alt="Camions"
                    :width="800"
                    :height="533"
                    credit="Pixabay"
                >
                    Le système alimentaire des pays industrialisés est entièrement dépendant du transport routier, et donc, du pétrole. La
                    quasi-totalité des marchandises agricoles produites dans un département français est exportée tandis que la quasi-totalité des
                    aliments qui y sont consommés est importée.
                </Image>
            </template>

            <template #[ID_SECTION_3]>
                <TexteP>
                    Les études scientifiques convergent : le système agricole et alimentaire doit profondément se transformer pour assurer durablement
                    notre sécurité alimentaire, et gagner en résilience face aux chocs à venir. Il faut pour cela faire évoluer conjointement notre
                    agriculture, notre consommation alimentaire, et nos circuits de transformation et distribution.
                </TexteP>

                <TitreH3>1/ Généraliser l'agroécologie</TitreH3>

                <TexteP>
                    L'agroécologie repose sur trois grands principes. Réduire nos dépendances aux ressources dont la disponibilité est compromise
                    (énergies fossiles, engrais minéraux, eau). Préserver les milieux (sols, eau, biodiversité) pour favoriser le renouvellement de la
                    fertilité et la régulation des espèces indésirables. Diversifier les systèmes agricoles à tous les niveaux (génétique,
                    productions, paysages) pour les rendre plus résilients face à des perturbations multiples et imprévisibles.
                </TexteP>

                <TitreH3>2/ Manger plus végétal</TitreH3>

                <TexteP>
                    En France, la moitié des terres arables sert à produire des plantes qui vont nourrir des animaux. Le rendement en termes de
                    calories est bien plus faible que si ces terres étaient cultivées directement pour l’alimentation humaine. Réduire la consommation
                    d’aliments d’origine animale et réserver les terres arables pour des cultures directement comestibles est notre principal levier
                    pour maintenir la disponibilité alimentaire malgré les baisses de rendements à venir.
                </TexteP>

                <TitreH3>3/ Réduire les distances</TitreH3>

                <TexteP>
                    Aujourd’hui, la quasi-totalité de la production agricole d’un département français est exportée tandis que la quasi-totalité de la
                    nourriture consommée par ses habitants est importée. Ce système repose sur un flux continu de camions transportant les matières
                    agricoles brutes et les produits transformés.
                </TexteP>
                <TexteP>
                    Sans pétrole abondant, cette organisation ne tient plus. Il est donc nécessaire de développer l'autonomie alimentaire des
                    territoires en reconstruisant des filières en circuits courts et de proximité pour les produits alimentaires de base.
                </TexteP>
            </template>
            <template #[ID_SECTION_4]>
                <TexteP>
                    La transformation de notre système alimentaire implique a fortiori des changements à l'échelle européenne et nationale.
                </TexteP>
                <TexteP>
                    Mais elle doit également se déployer dans les territoires sans attendre, en particulier via les Projets Alimentaires Territoriaux
                    (PAT) qui permettent de porter des politiques alimentaires locales et de décliner les projets et plan d'action associés.
                </TexteP>
                <Encadre
                    type="focus-territoire"
                    titre="Et chez vous : Votre territoire peut-il nourrir sainement et durablement ses habitants ?"
                    sous-titre="Accédez au diagnostic agricole et alimentaire de votre territoire, et découvrez les actions prioritaires."
                >
                    <BlocRechercheTerritoire></BlocRechercheTerritoire>
                </Encadre>
            </template>
        </TemplateArticle>
    </TemplatePage>
</template>

<script setup lang="ts">
import { type ChampsHead, usePositionnerInfosHead } from '#imports';
import TexteP from '~/components/base/TexteP.vue';
import TitreH3 from '~/components/base/TitreH3.vue';
import BlocRechercheTerritoire from '~/components/diagnostics/commun/BlocRechercheTerritoire.vue';
import GloseAvecPopoverDefinition from '~/components/diagnostics/thematiques/commun/GloseAvecPopoverDefinition.vue';
import Image from '~/components/diagnostics/thematiques/commun/Image.vue';
import TemplateArticle from '~/components/templates/TemplateArticle.vue';
import TemplatePage from '~/components/templates/TemplatePage.vue';
import type { ItemLienHash } from '~/modeles/navigation/ItemMenuModele.js';

import Encadre from '../../../components/diagnostics/thematiques/commun/Encadre.vue';

const champsHead: ChampsHead = {
    titre: 'Menaces et enjeux pour la sécurité alimentaire en France',
    description:
        'Découvrez les risques qui pèsent sur notre sécurité alimentaire et les actions pour aller vers des systèmes alimentaires plus durables et résilients',
    titreRS: `Menaces et enjeux pour la sécurité alimentaire en France`
};
usePositionnerInfosHead(champsHead);

const ID_SECTION_1 = 'un-systeme-fait-pour-produire-pas-pour-nourrir';
const ID_SECTION_2 = 'nuages-a-l-horizon';
const ID_SECTION_3 = 'changement-de-cap';
const ID_SECTION_4 = 'passer-a-l-action';

const SECTIONS: ItemLienHash[] = [
    {
        type: 'LIEN_HASH',
        hash: ID_SECTION_1,
        libelle: 'Un système fait pour produire, pas pour nourrir'
    },
    {
        type: 'LIEN_HASH',
        hash: ID_SECTION_2,
        libelle: 'Nuages à l’horizon'
    },
    {
        type: 'LIEN_HASH',
        hash: ID_SECTION_3,
        libelle: 'Changement de cap !'
    },
    {
        type: 'LIEN_HASH',
        hash: ID_SECTION_4,
        libelle: 'Agir maintenant, et à toutes les échelles territoriales'
    }
];
</script>
