import type { ThematiqueModele } from './ThematiqueModele.js';

export function creerThematiqueTransformationDistribution(): ThematiqueModele {
    return {
        idTheme: 'transformation-distribution'
    };
}
