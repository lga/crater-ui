import type { Territoire } from '@lga/territoires';

import type { IndicateursParAnnees } from '../IndicateursParAnnees';
import type { Note } from '../Note';
import { calculerMessagePrelevementsIrrigation } from './messages-eau';
import type { PratiquesIrrigation } from './PratiquesIrrigation';

export interface IrrigationIndicateursAnneeN {
    annee: number;
    irrigationM3: number | null;
    irrigationMM: number | null;
}

export class Irrigation {
    private readonly LIBELLES_CLASSES_NOTES = ['forte', 'moyenne', 'faible'];
    public irrigationM3ParHa: number | null = null;
    public irrigationPays?: Irrigation;

    constructor(
        private territoire: Territoire,
        public readonly note: Note,
        public readonly irrigationM3: number | null,
        public readonly irrigationMM: number | null,
        public readonly irrigationIndicateursParAnnees: IndicateursParAnnees<IrrigationIndicateursAnneeN>,
        public readonly pratiquesIrrigation: PratiquesIrrigation
    ) {
        this.irrigationM3ParHa = irrigationMM === null ? null : irrigationMM * 10;
    }

    setIrrigationPays(irrigationPays: Irrigation) {
        this.irrigationPays = irrigationPays;
    }

    get messagePrelevementsIrrigation() {
        return calculerMessagePrelevementsIrrigation(
            this.territoire.nom,
            this.note,
            this.irrigationM3,
            this.irrigationMM,
            this.irrigationPays?.irrigationMM ?? null,
            this.irrigationM3ParHa,
            this.irrigationIndicateursParAnnees
        );
    }
}
