import { describe, expect, it } from 'vitest';

import { MessageEvolutionPesticides } from './MessageEvolutionPesticides';

describe('Tests de la classe MessageEvolutionPesticides', () => {
    it('Calculer la première partie du message détaillé des indicateurs QSA et NODU', () => {
        expect(MessageEvolutionPesticides.construireDebutMessage(1, 2, 10, 20)).toEqual(
            MessageEvolutionPesticides.messageHtmlAugmentationQsaAugmentationNodu
        );
        expect(MessageEvolutionPesticides.construireDebutMessage(2, 1, 20, 10)).toEqual(MessageEvolutionPesticides.messageHtmlBaisseQsaBaisseNodu);
        expect(MessageEvolutionPesticides.construireDebutMessage(2, 1, 10, 20)).toEqual(
            MessageEvolutionPesticides.messageHtmlBaisseQsaAugmentationNodu
        );
        expect(MessageEvolutionPesticides.construireDebutMessage(1, 2, 20, 10)).toEqual(MessageEvolutionPesticides.messageHtmlAutreCase);
        expect(MessageEvolutionPesticides.construireDebutMessage(1, 1, 10, 10)).toEqual(MessageEvolutionPesticides.messageHtmlAutreCase);
    });

    it('Calculer la première partie du message détaillé des indicateurs QSA et NODU', () => {
        expect(MessageEvolutionPesticides.construireMessage('NomCommune', 2015, 2019, 1.5, 1, 20, 30)).toEqual(
            `Sur le territoire <em>NomCommune</em>, les quantités totales de substances actives achetées ont <strong>diminué</strong> mais le nombre de doses unités a <strong>augmenté</strong> entre 
            2015
             et 
            2019 :
            <ul>
                <li><strong>-33%</strong>
                pour les quantités de substances actives ;
                <li><strong>+50%</strong>
                pour le nombre de doses unités.`
        );
    });

    it('Calculer le message si une des données est manquante', () => {
        expect(MessageEvolutionPesticides.construireMessage('NomCommune', 2015, 2019, null, 1, 20, 30)).toEqual(
            `Pour le territoire <em>NomCommune</em>, les données ne sont pas disponibles.`
        );
        expect(MessageEvolutionPesticides.construireMessage('NomCommune', 2015, 2019, null, null, null, null)).toEqual(
            `Pour le territoire <em>NomCommune</em>, les données ne sont pas disponibles.`
        );
    });
});
