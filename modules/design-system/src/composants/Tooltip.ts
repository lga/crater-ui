import { css, html, LitElement, type TemplateResult, unsafeCSS } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { CSS_BREAKPOINT_MAX_WIDTH_MOBILE_PAYSAGE } from '../styles/styles-breakpoints.js';
import { STYLES_DESIGN_SYSTEM } from '../styles/styles-design-system.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-tooltip': Tooltip;
    }
}

@customElement('c-tooltip')
export class Tooltip extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        css`
            :host {
                display: block;
                position: absolute;
                z-index: 1;
                visibility: hidden;
            }

            main {
                background-color: var(--couleur-neutre);
                color: var(--couleur-blanc);
                border-radius: 5px;
                padding: 2px 5px;
            }

            header {
                text-align: center;
            }

            @media screen and (max-width: ${unsafeCSS(CSS_BREAKPOINT_MAX_WIDTH_MOBILE_PAYSAGE + 'px')}) {
                :host {
                    visibility: hidden !important;
                }
            }
        `
    ];

    @property()
    titre = '';

    @property({ attribute: false })
    contenu?: TemplateResult<1>;

    render() {
        return html`
            <main>
                <header class="texte-titre">${this.titre}</header>
                <article class="texte-moyen">${this.contenu}</article>
            </main>
        `;
    }
}
