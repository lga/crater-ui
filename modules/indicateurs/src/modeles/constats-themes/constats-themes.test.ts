import { describe, expect, it } from 'vitest';

import { getIdsConstatsDepuisIdsTheme } from './constats-themes';

describe('Tests de constats-themes', () => {
    it('Test getConstatsIdsForTheme', () => {
        expect(getIdsConstatsDepuisIdsTheme('terres-agricoles')).toEqual([
            'terres-insuffisantes',
            'terres-partiellement-suffisantes',
            'terres-suffisantes'
        ]);
    });
});
