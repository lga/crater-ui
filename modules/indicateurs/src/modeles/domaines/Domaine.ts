import type { HtmlString } from '@lga/base';

import type { DefinitionIndicateur, IndicateurDetaille } from '../indicateurs/indicateurs.js';

type typeDomaine = 'maillon' | 'indicateur' | 'sans-page';

export interface DonneesDomaine {
    id: string;
    idMaillon: string;
    type: typeDomaine;
    libelle: string;
    icone: string;
    description: HtmlString;
    indicateurs: DefinitionIndicateur[];
}

export class Domaine {
    public readonly id: string;
    public readonly idMaillon: string;
    public readonly type: typeDomaine;
    public readonly libelle: string;
    public readonly description: HtmlString;
    public readonly icone: string;
    public readonly indicateursDetailles: IndicateurDetaille[];

    constructor(parametresDomaine: DonneesDomaine) {
        this.id = parametresDomaine.id;
        this.idMaillon = parametresDomaine.idMaillon;
        this.type = parametresDomaine.type;
        this.libelle = parametresDomaine.libelle;
        this.description = parametresDomaine.description;
        this.icone = parametresDomaine.icone;
        let listeIndicateurs;
        if (Array.isArray(parametresDomaine.indicateurs)) {
            listeIndicateurs = parametresDomaine.indicateurs;
        } else {
            listeIndicateurs = [parametresDomaine.indicateurs];
        }
        this.indicateursDetailles = listeIndicateurs.map((i) => ({
            ...i,
            idMaillon: this.idMaillon,
            idDomaine: this.id,
            icone: this.icone,
            estScore: i.id.includes('score'),
            styleCarte: i.styleCarte
        }));
    }
}
