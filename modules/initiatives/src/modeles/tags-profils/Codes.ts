import type { CodeProfilAgricultureApi, CodeProfilCollectiviteApi } from '@lga/specification-api';

export type CodeProfil = CodeProfilAgricultureApi | CodeProfilCollectiviteApi;
