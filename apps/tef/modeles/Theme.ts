import type { ImageModele } from '@lga/base';
import type { IdTheme } from '@lga/indicateurs';

import { IMAGE_RAYON_FRUITS } from './constats/constats-accessibilite.js';
import { IMAGE_AGRICULTEUR } from './constats/constats-agriculteurs.js';
import { IMAGE_MARCHE, IMAGE_POTIMARRON } from './constats/constats-autonomie-alimentaire.js';
import { IMAGE_PAYSAGE_AGRICOLE_AVEC_DES_HAIES } from './constats/constats-biodiversite.js';
import { IMAGE_NUAGES } from './constats/constats-climat.js';
import { IMAGE_SELF_SERVICE } from './constats/constats-consommation.js';
import { IMAGE_BATEAU_PORTE_CONTENEURS } from './constats/constats-ressources.js';
import { IMAGE_PAYSAGE_AGRICOLE } from './constats/constats-terres-agricoles.js';

export interface Theme {
    id: IdTheme;
    titre: string;
    titreCourt: string;
    question: string;
    image: ImageModele;
}

const IMAGE_TRACTEUR: ImageModele = {
    url: '/images/constats/titres/tracteur-748x804.jpg',
    descriptionAlt: 'Tracteur',
    resolutionX: 748,
    resolutionY: 804
};

export const THEMES: Theme[] = [
    {
        id: 'risques-securite-alimentaire',
        titre: 'Risques sur la sécurité alimentaire',
        titreCourt: 'Introduction aux enjeux',
        question: 'Quels risques pour la sécurité alimentaire ?',
        image: IMAGE_TRACTEUR
    },
    {
        id: 'terres-agricoles',
        titre: 'Terres agricoles',
        titreCourt: 'Terres agricoles',
        question: 'Un territoire nourricier ?',
        image: IMAGE_PAYSAGE_AGRICOLE
    },
    {
        id: 'autonomie-alimentaire',
        titre: 'Autonomie alimentaire',
        titreCourt: 'Autonomie alimentaire',
        question: "D'où vient ce que vous mangez ?",
        image: IMAGE_POTIMARRON
    },
    {
        id: 'transformation-distribution',
        titre: 'Transformation & Distribution',
        titreCourt: 'Transformation & Distribution',
        question: 'Quels acteurs de la transformation et de la distribution ?',
        image: IMAGE_MARCHE
    },
    {
        id: 'consommation',
        titre: 'Consommation',
        titreCourt: 'Consommation',
        question: 'Que mange-t-on ?',
        image: IMAGE_SELF_SERVICE
    },
    {
        id: 'accessibilite',
        titre: "Accessibilité de l'alimentation",
        titreCourt: "Accessibilité de l'alimentation",
        question: "Tous égaux face à l'alimentation ?",
        image: IMAGE_RAYON_FRUITS
    },
    {
        id: 'agriculteurs',
        titre: 'Agriculteurs',
        titreCourt: 'Agriculteurs',
        question: 'Et les agriculteurs dans tout ça ?',
        image: IMAGE_AGRICULTEUR
    },
    {
        id: 'ressources',
        titre: 'Dépendances aux ressources et aux technologies',
        titreCourt: 'Dépendances aux ressources',
        question: 'La souveraineté alimentaire, une illusion ?',
        image: IMAGE_BATEAU_PORTE_CONTENEURS
    },
    {
        id: 'biodiversite',
        titre: 'Impacts des pratiques agricoles',
        titreCourt: 'Impacts des pratiques agricoles',
        question: 'Quelles pratiques agricoles ?',
        image: IMAGE_PAYSAGE_AGRICOLE_AVEC_DES_HAIES
    },
    {
        id: 'climat',
        titre: 'Climat et autres crises',
        titreCourt: 'Climat et autres crises',
        question: 'Quel temps demain ?',
        image: IMAGE_NUAGES
    }
];

export function getTheme(idTheme: IdTheme): Theme | undefined {
    return THEMES.find((i) => i.id === idTheme);
}
