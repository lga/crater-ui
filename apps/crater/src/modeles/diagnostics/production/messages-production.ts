export function calculerMessageSyntheseProduction(
    tauxAdequationBrutPourcent: number | null,
    tauxAdequationMoyenPonderePourcent: number | null,
    notePartSauBio: number | null,
    noteHvn: number | null
): string {
    if (tauxAdequationBrutPourcent === null || tauxAdequationMoyenPonderePourcent === null || notePartSauBio === null || noteHvn === null)
        return `Données non disponibles (voir les échelles supérieures telles que l'EPCI)`;

    const adverbe =
        calculerMessageSyntheseAdequationProductionConsommation(tauxAdequationBrutPourcent, tauxAdequationMoyenPonderePourcent).messageEstPositif !=
        calculerMessageSynthesePratiqueAgricoles(notePartSauBio, noteHvn).messageEstPositif
            ? ' mais '
            : ' et ';

    return (
        calculerMessageSyntheseAdequationProductionConsommation(tauxAdequationBrutPourcent, tauxAdequationMoyenPonderePourcent).message +
        adverbe +
        calculerMessageSynthesePratiqueAgricoles(notePartSauBio, noteHvn).message
    );
}

export function calculerMessageSyntheseAdequationProductionConsommation(
    tauxAdequationBrutPourcent: number,
    tauxAdequationMoyenPonderePourcent: number
): {
    message: string;
    messageEstPositif: boolean;
} {
    let message;
    let messageEstPositif = false;

    if (tauxAdequationMoyenPonderePourcent >= 95) {
        message = `Production <strong>suffisante</strong> pour couvrir la consommation`;
        messageEstPositif = true;
    } else {
        if (tauxAdequationBrutPourcent >= 100) {
            message = `Production élevée <strong>mais trop spécialisée</strong> pour couvrir la consommation`;
        } else {
            if (tauxAdequationMoyenPonderePourcent >= 70) {
                message = `Production <strong>presque suffisante</strong> pour couvrir la consommation`;
                messageEstPositif = true;
            } else if (tauxAdequationMoyenPonderePourcent >= 30) {
                message = `Production <strong>insuffisante</strong> pour couvrir la consommation`;
            } else {
                message = `Production <strong>nettement insuffisante</strong> pour couvrir la consommation`;
            }
        }
    }

    return { message, messageEstPositif };
}

export function calculerMessageSynthesePratiqueAgricoles(notePartSauBio: number, noteHvn: number): { message: string; messageEstPositif: boolean } {
    let message;
    let messageEstPositif = false;

    const noteMoyenne = (notePartSauBio + noteHvn) / 2;
    if (noteMoyenne <= 5) {
        message = `pratiques agricoles <strong>très préjudiciables</strong> à la biodiversité.`;
    } else if (noteMoyenne <= 7) {
        message = `pratiques agricoles <strong>préjudiciables</strong> à la biodiversité.`;
    } // entre 8 et 10
    else {
        message = `pratiques agricoles <strong>favorables</strong> à la biodiversité.`;
        messageEstPositif = true;
    }
    return { message, messageEstPositif };
}
