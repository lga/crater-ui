import './ChartTreemapCultures.js';
import '@lga/design-system/build/composants/Legende.js';

import { formaterNombreEnEntierString } from '@lga/base';
import { type ItemLegende, Legende } from '@lga/design-system/build/composants/Legende';
import { CSS_BREAKPOINT_MAX_WIDTH_MOBILE_PAYSAGE } from '@lga/design-system/build/styles/styles-breakpoints.js';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { css, html, LitElement, unsafeCSS } from 'lit';
import { customElement, property, state } from 'lit/decorators.js';

import type { DonneesTreemapCultures, OptionsChartTreemapCultures } from './ChartTreemapCultures';

export interface OptionsChartTreemapDoubleCultures {
    sauTotaleCulturesGaucheHa: number | null;
    sauTotaleCulturesDroiteHa: number | null;
    titreCulturesGauche: string;
    titreCulturesDroite: string;
    donneesTreemapCulturesGauche: DonneesTreemapCultures[];
    donneesTreemapCulturesDroite: DonneesTreemapCultures[];
    itemsLegende: ItemLegende[];
}

declare global {
    interface HTMLElementTagNameMap {
        'c-chart-treemap-double-cultures': ChartTreemapDoubleCultures;
    }
}

const LARGEUR_PAR_DEFAUT_PIXELS = 250;
const HAUTEUR_MAX_PIXELS = 350;
const HAUTEUR_BASCULEMENT_AFFICHAGE_CARRE_PIXELS = 25;
const TAILLE_MIN_CARRE_PIXELS = 15;

@customElement('c-chart-treemap-double-cultures')
export class ChartTreemapDoubleCultures extends LitElement {
    @state()
    optionsTreemapCulturesGauche!: OptionsChartTreemapCultures;

    @state()
    optionsTreemapCulturesDroite!: OptionsChartTreemapCultures;

    @property({ attribute: false })
    options?: OptionsChartTreemapDoubleCultures;

    @state()
    conserverProportions = true;

    static styles = [
        STYLES_DESIGN_SYSTEM,
        css`
            figure {
                margin: auto;
                max-width: 600px;
                text-align: center;
            }

            #charts {
                display: grid;
                grid-auto-flow: column;
                align-items: end;
                min-height: 350px;
            }

            .texte {
                margin: 0;
            }

            c-legende {
                margin: 1rem auto;
            }

            @media screen and (max-width: ${unsafeCSS(CSS_BREAKPOINT_MAX_WIDTH_MOBILE_PAYSAGE + 'px')}) {
                #charts {
                    display: block;
                }
            }
        `
    ];

    render() {
        return html`
            <figure>
                <div id="charts">
                    <div class="chart-treemap">
                        <c-chart-treemap-cultures
                            .options=${this.optionsTreemapCulturesGauche}
                            id="chart-treemap-cultures-gauche"
                        ></c-chart-treemap-cultures>
                        <p class="texte">
                            ${this.options?.titreCulturesGauche} : ${formaterNombreEnEntierString(this.options?.sauTotaleCulturesGaucheHa ?? null)} ha
                        </p>
                    </div>
                    <div class="chart-treemap">
                        <c-chart-treemap-cultures
                            .options=${this.optionsTreemapCulturesDroite}
                            id="chart-treemap-cultures-droite"
                        ></c-chart-treemap-cultures>
                        <p class="texte">
                            ${this.options?.titreCulturesDroite} : ${formaterNombreEnEntierString(this.options?.sauTotaleCulturesDroiteHa ?? null)} ha
                        </p>
                    </div>
                </div>
                <c-legende .items=${this.options?.itemsLegende} type="${Legende.TYPE_LEGENDE.discrete}"></c-legende>
                <div>
                    <label class="texte-moyen">Basculer l'affichage: </label>
                    <select id="selecteur" @change=${this.changerOptionSelecteur}>
                        <option value="conserver" ?selected=${this.conserverProportions}>Conserver proportions</option>
                        <option value="etirer" ?selected=${!this.conserverProportions}>Étirer</option>
                    </select>
                </div>
            </figure>
        `;
    }

    willUpdate(): void {
        if (this.options?.donneesTreemapCulturesGauche !== undefined && this.options.donneesTreemapCulturesDroite !== undefined) {
            this.optionsTreemapCulturesGauche = {
                donnees: this.options.donneesTreemapCulturesGauche,
                ...this.calculerOptionsTreemap(
                    this.conserverProportions,
                    this.options.sauTotaleCulturesGaucheHa ?? 0,
                    this.options.sauTotaleCulturesDroiteHa ?? 0
                )
            };
            this.optionsTreemapCulturesDroite = {
                donnees: this.options.donneesTreemapCulturesDroite,
                ...this.calculerOptionsTreemap(
                    this.conserverProportions,
                    this.options.sauTotaleCulturesDroiteHa ?? 0,
                    this.options.sauTotaleCulturesGaucheHa ?? 0
                )
            };
        }
    }

    private changerOptionSelecteur() {
        this.conserverProportions = !this.conserverProportions;
    }

    private calculerOptionsTreemap(
        conserverProportions: boolean,
        superficieTreemap: number,
        superficieTreemapAdjacent: number
    ): { hauteur: number; largeur: number; activerLabels: boolean } {
        if (!conserverProportions || superficieTreemap >= superficieTreemapAdjacent) {
            return { hauteur: HAUTEUR_MAX_PIXELS, largeur: LARGEUR_PAR_DEFAUT_PIXELS, activerLabels: true };
        } else {
            const ratio = superficieTreemap / superficieTreemapAdjacent;
            let hauteur = HAUTEUR_MAX_PIXELS * ratio;
            let largeur = LARGEUR_PAR_DEFAUT_PIXELS;
            if (hauteur < HAUTEUR_BASCULEMENT_AFFICHAGE_CARRE_PIXELS) {
                hauteur = largeur = Math.max(TAILLE_MIN_CARRE_PIXELS, Math.sqrt(hauteur * largeur));
            }
            return { hauteur: Math.round(hauteur), largeur: Math.round(largeur), activerLabels: ratio > 0.5 ? true : false };
        }
    }
}
