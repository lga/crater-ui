#!/bin/bash

# Récupérer le status des process s'ils ne sont pas "online"
LOG_API_DEV=$(pm2 list | grep api-dev | grep online -v)
LOG_API_PROD=$(pm2 list | grep api-prod | grep online -v)
LOG_API_PDF=$(pm2 list | grep api-pdf | grep online -v)
LOG_TEF_DEV=$(pm2 list | grep tef-dev | grep online -v)
LOG_TEF_PROD=$(pm2 list | grep tef-prod | grep online -v)


if [[ -n "$LOG_API_DEV" || -n "$LOG_API_PROD" || -n "$LOG_API_PDF" || -n "$LOG_TEF_DEV" || -n "$LOG_TEF_PROD" ]]; then
    FICHIER_TMP_MESSAGE=$(mktemp)
    printf "ALERTE - PROCESS DOWN :\n$LOG_API_DEV\n$LOG_API_PROD\n$LOG_API_PDF\n$LOG_TEF_DEV\n$LOG_TEF_PROD" > "$FICHIER_TMP_MESSAGE"

    ./post-mattermost.sh "$FICHIER_TMP_MESSAGE"

    rm -f "$FICHIER_TMP_MESSAGE"
fi

