import { css } from 'lit';

export const STYLES_ZONES_COMPLEMENTS_ACCUEIL = css`
    :host {
        display: block;
    }
    #contenu {
        display: flex;
        flex-direction: column;
        align-items: center;
        gap: calc(2 * var(--dsem));
    }
    main {
        display: flex;
        flex-direction: column;
        align-items: center;
        gap: calc(6 * var(--dsem));
        padding-top: var(--dsem4);
    }
    header {
        display: flex;
        flex-direction: column;
        align-items: center;
        gap: var(--dsem);
        color: var(--couleur-primaire);
        padding-bottom: var(--dsem);
    }
    header > svg {
        fill: var(--couleur-primaire);
    }

    img {
        height: auto;
    }
`;
