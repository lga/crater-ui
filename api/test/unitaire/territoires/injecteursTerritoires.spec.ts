import { beforeEach, describe, expect, it } from 'vitest';

import { Commune, Departement, Epci, Pays, Region, RegroupementCommunes } from '../../../src/territoires/domaines/entitesTerritoires';
import { ServicesTerritoires } from '../../../src/territoires/domaines/ServicesTerritoires';
import { InjecteursTerritoires } from '../../../src/territoires/InjecteursTerritoires';

const CHEMIN_DONNEES_TESTS_UNITAIRES_INPUT = `${__dirname}/../data/input`;

describe('Test unitaire injecteurs territoires', () => {
    let servicesTerritoires: ServicesTerritoires;
    let injecteursTerritoires: InjecteursTerritoires;

    beforeEach(() => {
        servicesTerritoires = new ServicesTerritoires();
        injecteursTerritoires = new InjecteursTerritoires(servicesTerritoires);
    });

    it('Charger les territoires', async () => {
        // when
        await injecteursTerritoires.injecterDonneesReferentielTerritoires(CHEMIN_DONNEES_TESTS_UNITAIRES_INPUT);
        // then
        expect(servicesTerritoires.rechercherTous().length).toEqual(8);

        const pays = servicesTerritoires.rechercher('france');
        expect(pays).toEqual({
            id: 'france',
            idCode: 'P-FR',
            idNom: 'france',
            nom: 'France',
            categorie: 'PAYS',
            idTerritoireParcel: '1',
            boundingBox: {
                latitudeMax: 51.09,
                latitudeMin: 41.33,
                longitudeMax: 9.56,
                longitudeMin: -5.14
            },
            genreNombre: 'FEMININ_SINGULIER',
            preposition: 'en'
        });
        expect(pays).toBeInstanceOf(Pays);

        const region = servicesTerritoires.rechercher('nouvelle-aquitaine');
        expect(region).toEqual({
            id: 'nouvelle-aquitaine',
            idCode: 'R-75',
            idNom: 'nouvelle-aquitaine',
            nom: 'Nouvelle-Aquitaine',
            categorie: 'REGION',
            idTerritoireParcel: '75',
            idPays: 'france',
            boundingBox: {
                latitudeMax: 10,
                latitudeMin: -10,
                longitudeMax: 10,
                longitudeMin: -10
            },
            genreNombre: 'FEMININ_SINGULIER',
            preposition: 'en'
        });
        expect(region).toBeInstanceOf(Region);

        const departement = servicesTerritoires.rechercher('landes');
        expect(departement).toEqual({
            id: 'landes',
            idCode: 'D-40',
            idNom: 'landes',
            nom: 'Landes',
            categorie: 'DEPARTEMENT',
            idTerritoireParcel: '40',
            idPays: 'france',
            idRegion: 'nouvelle-aquitaine',
            boundingBox: {
                latitudeMax: 10,
                latitudeMin: -10,
                longitudeMax: 10,
                longitudeMin: -10
            },
            genreNombre: 'FEMININ_PLURIEL',
            preposition: 'dans'
        });
        expect(departement).toBeInstanceOf(Departement);

        const epci = servicesTerritoires.rechercher('communaute-de-communes-d-aire-sur-l-adour');
        expect(epci).toEqual({
            id: 'communaute-de-communes-d-aire-sur-l-adour',
            idCode: 'E-200030435',
            idNom: 'communaute-de-communes-d-aire-sur-l-adour',
            nom: "CC d'Aire-sur-l'Adour",
            categorie: 'EPCI',
            sousCategorie: 'COMMUNAUTE_COMMUNES',
            idTerritoireParcel: '200',
            idPays: 'france',
            idRegion: 'nouvelle-aquitaine',
            idDepartement: 'landes',
            boundingBox: {
                latitudeMax: 10,
                latitudeMin: -10,
                longitudeMax: 10,
                longitudeMin: -10
            },
            genreNombre: 'FEMININ_SINGULIER',
            preposition: 'dans'
        });
        expect(epci).toBeInstanceOf(Epci);

        const pnr = servicesTerritoires.rechercher('pnr-fictif-pour-les-tests');
        expect(pnr).toEqual({
            id: 'pnr-fictif-pour-les-tests',
            idCode: 'PNR-1',
            idNom: 'pnr-fictif-pour-les-tests',
            nom: 'PNR Fictif pour les tests',
            categorie: 'REGROUPEMENT_COMMUNES',
            sousCategorie: 'PARC_NATUREL_REGIONAL',
            idTerritoireParcel: '100',
            idPays: 'france',
            idRegion: 'nouvelle-aquitaine',
            idDepartement: 'landes',
            boundingBox: {
                latitudeMax: 10,
                latitudeMin: -10,
                longitudeMax: 10,
                longitudeMin: -10
            },
            genreNombre: 'MASCULIN_SINGULIER',
            preposition: 'dans'
        });
        expect(pnr).toBeInstanceOf(RegroupementCommunes);

        const commune = servicesTerritoires.rechercher('latrille');
        expect(commune).toEqual({
            id: 'latrille',
            idCode: 'C-40146',
            idNom: 'latrille',
            nom: 'Latrille',
            categorie: 'COMMUNE',
            idTerritoireParcel: '301',
            idEpci: 'communaute-de-communes-d-aire-sur-l-adour',
            idDepartement: 'landes',
            idRegion: 'nouvelle-aquitaine',
            idPays: 'france',
            _codesPostaux: ['40800'],
            boundingBox: {
                latitudeMax: 10,
                latitudeMin: -10,
                longitudeMax: 10,
                longitudeMin: -10
            },
            genreNombre: 'FEMININ_SINGULIER',
            preposition: 'à'
        });
        expect(commune).toBeInstanceOf(Commune);
    });
});
