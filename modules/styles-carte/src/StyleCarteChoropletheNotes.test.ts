import { describe, expect, it } from 'vitest';

import { StyleCarteChoropletheNotes } from './StyleCarteChoropletheNotes';

describe('Test de la classe StyleCarteChoropletheNotes', () => {
    const style = new StyleCarteChoropletheNotes();

    it('Tester le calcul valeur texte avec unite ', () => {
        expect(style.calculerLibelleTooltip(null)).toEqual('-');
        expect(style.calculerLibelleTooltip(2.4)).toEqual('Score : 2 / 10');
    });
});
