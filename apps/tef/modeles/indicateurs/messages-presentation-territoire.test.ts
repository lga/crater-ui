import { describe, expect, it } from 'vitest';

import { calculerMessageIdentiteTerritoire, calculerMessageOccupationSolsEtAgricultureTerritoire } from './messages-presentation-territoire.js';

describe('Tests fonctions utils', () => {
    it('Test fonction messageIdentiteTerritoire', () => {
        expect(calculerMessageIdentiteTerritoire('Lyon', 'FEMININ_SINGULIER', 1, 25400, 60)).toEqual(
            'Lyon est peuplée de <strong>25 000 habitants</strong> (60 hab./km²).'
        );
        expect(calculerMessageIdentiteTerritoire('la communauté de communes de Lyon', 'FEMININ_SINGULIER', 1202, 2540000, 60)).toEqual(
            'La communauté de communes de Lyon est composée de <strong>1 202 communes</strong> et peuplée de <strong>2,5 millions habitants</strong> (60 hab./km²).'
        );
        expect(calculerMessageIdentiteTerritoire('les Hauts-de-France', 'MASCULIN_PLURIEL', 12, 2540000, 60)).toEqual(
            'Les Hauts-de-France sont composés de <strong>12 communes</strong> et peuplés de <strong>2,5 millions habitants</strong> (60 hab./km²).'
        );
    });

    it('Test fonction calculerMessageOccupationSolsEtAgricultureTerritoire', () => {
        expect(
            calculerMessageOccupationSolsEtAgricultureTerritoire(
                {
                    superficieAgricoleClcHa: 10,
                    superficieArtificialiseeClcHa: 10,
                    superficieNaturelleOuForestiereClcHa: 50,
                    superficieAgricoleRA2020Ha: 8,
                    superficieTotaleHa: 70
                },
                'GRANDES_CULTURES'
            )
        ).toEqual("C'est un territoire majoritairement <strong>naturel ou forestier</strong> avec une agriculture spécialisée en grandes cultures.");
        expect(
            calculerMessageOccupationSolsEtAgricultureTerritoire(
                {
                    superficieAgricoleClcHa: 10,
                    superficieArtificialiseeClcHa: 10,
                    superficieNaturelleOuForestiereClcHa: 50,
                    superficieAgricoleRA2020Ha: 8,
                    superficieTotaleHa: 70
                },
                'SANS_OTEX_MAJORITAIRE'
            )
        ).toEqual("C'est un territoire majoritairement <strong>naturel ou forestier</strong> avec une agriculture non spécialisée.");
    });
});
