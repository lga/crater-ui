import type { ArborescenceSite } from '~/modeles/navigation/MenuModele';

export const ARBORESCENCE_TEST: ArborescenceSite = [
    {
        type: 'LIEN_PAGE',
        libelle: 'Lien inactif',
        route: '#lien-inactif',
        estActif: false
    },
    {
        type: 'LIEN_PAGE',
        libelle: 'Lien actif',
        route: '#lien-actif',
        estActif: true
    },
    {
        type: 'SOUS_MENU',
        libelle: 'Menu avec sous-menu, actif',
        sousItems: [
            {
                type: 'LIEN_PAGE',
                libelle: 'Sous-menu 1 actif',
                route: '#sous-menu-1',
                estActif: true
            },
            {
                type: 'LIEN_PAGE',
                libelle: 'Sous-menu 2 inactif',
                route: '#sous-menu-2',
                estActif: false
            },
            {
                type: 'LIBELLE_SANS_LIEN',
                libelle: 'Titre section'
            },
            {
                type: 'LIEN_PAGE',
                libelle: 'Sous-menu dans section',
                route: '#sous-menu-3',
                estActif: false,
                estSousSection: true
            }
        ],
        estActif: true
    },
    {
        type: 'SOUS_MENU',
        libelle: 'Menu avec sous-menu, inactif',
        sousItems: [
            {
                type: 'LIEN_PAGE',
                libelle: 'Sous-menu 1',
                route: '#sous-menu-1',
                estActif: false
            }
        ],
        estActif: false
    }
];
