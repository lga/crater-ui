import type { DefinitionIndicateur } from '@lga/indicateurs';
import { type RouteLocationRaw, type RouteParams, useRoute } from 'vue-router/auto';

import { useGetStateIdTerritoire } from '#imports';
import { calculerComplementRouteDiagnostic } from '~/modeles/liens-utils.js';

export function useRouteDiagnostics(idTerritoire?: string, complementUrlCrater?: string): RouteLocationRaw<'diagnostics-idTerritoire'> {
    const route: RouteLocationRaw<'diagnostics-idTerritoire'> = {
        name: 'diagnostics-idTerritoire',
        // eslint-disable-next-line @typescript-eslint/prefer-nullish-coalescing
        params: { idTerritoire: idTerritoire },
        query: { complementUrlCrater: complementUrlCrater }
    };
    return route;
}

export function useRouteDiagnosticsIndicateur(indicateur: DefinitionIndicateur): RouteLocationRaw<'diagnostics-idTerritoire'> {
    return useRouteDiagnostics(useGetStateIdTerritoire().value, calculerComplementRouteDiagnostic(indicateur));
}

export function useRouteParamsIndicateurs(): RouteParams<'diagnostics-idTerritoire'> {
    return useRoute().params as RouteParams<'diagnostics-idTerritoire'>;
}
