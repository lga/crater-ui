import '../../charts/ChartTreemapConsommation.js';
import '../composants/ChapitreDomaine.js';
import '../../../commun/PageSablierCrater.js';

import { IDS_DOMAINES } from '@lga/indicateurs';
import { SA } from '@lga/indicateurs';
import { html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { EtatRapport, type Rapport } from '../../../../modeles/diagnostics/Rapport.js';
import type { OptionsTreemapConsommation } from '../../charts/ChartTreemapConsommation';
import type { DonneesPageDiagnostic } from '../../PageDiagnostic.js';
import { creerDonneesTreemapConsommation } from '../chapitres-outils.js';
import type { OptionsDomaineDetaille } from '../composants/ChapitreDomaine';

declare global {
    interface HTMLElementTagNameMap {
        'c-chapitre-part-alimentation-animale-dans-consommation': ChapitrePartAlimentationAnimaleDansConsommation;
    }
}

@customElement('c-chapitre-part-alimentation-animale-dans-consommation')
export class ChapitrePartAlimentationAnimaleDansConsommation extends LitElement {
    @property({ attribute: false })
    donneesPage?: DonneesPageDiagnostic;

    @property({ attribute: false })
    rapport?: Rapport;

    render() {
        if (this.rapport?.getEtat() === EtatRapport.PRET) {
            return html` <c-chapitre-domaine
                .options=${this.calculerOptionsChapitreDomaineEmpreinteSurfaceHumainsAnimaux(this.rapport, this.donneesPage)}
                nbLignesMessageMinimum="5"
            >
                <c-chart-treemap-consommation
                    .options=${this.calculerOptionsChartTreemapConsommation(this.rapport)}
                    slot="chart-chapitre-domaine"
                ></c-chart-treemap-consommation>
            </c-chapitre-domaine>`;
        } else {
            return html`<c-page-sablier-crater></c-page-sablier-crater>`;
        }
    }

    private calculerOptionsChapitreDomaineEmpreinteSurfaceHumainsAnimaux(
        rapport: Rapport,
        donneesPage: DonneesPageDiagnostic | undefined
    ): OptionsDomaineDetaille {
        return {
            domaine: SA.getDomaine(IDS_DOMAINES.consommationProduitsAnimaux)!,
            message: `<p>
                    Les aliments d'origine animale (viande, oeufs, produits laitiers) requièrent davantage de ressources et de terres cultivées que
                    les produits végétaux pour atteindre une valeur nutritive similaire.
                </p>
            ${rapport.getDiagnosticActif()?.consommation.messagePartAlimentationAnimaleDansConsommation}
            `,
            territoire: rapport.getTerritoireActif(),
            estPagePourTef: donneesPage?.estPagePourTef
        };
    }

    private calculerOptionsChartTreemapConsommation(rapport: Rapport): OptionsTreemapConsommation {
        const donneesTreemapConsommationAnimaux = creerDonneesTreemapConsommation(
            rapport.getDiagnosticActif()!.consommation.indicateurConsommation!.consommationAnimauxParGroupeCulture
        );
        const donneesTreemapConsommationHumains = creerDonneesTreemapConsommation(
            rapport.getDiagnosticActif()!.consommation.indicateurConsommation!.consommationHumainsParGroupeCulture
        );

        return {
            consommationAnimauxHa: rapport.getDiagnosticActif()!.consommation.indicateurConsommation!.consommationAnimauxHa ?? 0,
            consommationHumainsHa: rapport.getDiagnosticActif()!.consommation.indicateurConsommation!.consommationHumainsHa ?? 0,
            donneesTreemapConsommationAnimaux: donneesTreemapConsommationAnimaux,
            donneesTreemapConsommationHumains: donneesTreemapConsommationHumains
        };
    }
}
