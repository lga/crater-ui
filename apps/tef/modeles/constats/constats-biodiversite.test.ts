import { describe, expect, it } from 'vitest';

import { creerDonneesApi } from '../tests-utils';
import { calculerMessageCourtConstatPaysagesAgricoles, creerConstatHaies, creerConstatPaysagesAgricoles } from './constats-biodiversite.js';

describe('Tests constats Biodiversité', () => {
    it('Test de la fonction creerConstatHaies', () => {
        const constat = creerConstatHaies(creerDonneesApi({}));
        expect(constat.message.titre).toEqual("L’agriculture est l'activité qui impacte le plus la biodiversité.");
        expect(constat.message.texte).toEqual(
            'En France, les oiseaux et les insectes disparaissent du fait de la destruction des haies, de la simplification des cultures, et de l’usage intensif de pesticides et d’engrais. Les paysages s’uniformisent et la production agricole s’en trouve fragilisée.'
        );
        expect(constat.message.question).toEqual('Et si on faisait évoluer nos pratiques agricoles ?');

        expect(constat.infographie?.nomComposant).toEqual('InfographieImageChiffreCle');
    });

    it('Test de la fonction creerConstatPaysagesAgricoles', () => {
        let constat = creerConstatPaysagesAgricoles(creerDonneesApi({}), 10);
        expect(constat.message.titre).toEqual('Les pratiques agricoles dominantes de votre territoire contribuent au déclin de la biodiversité.');
        expect(constat.message.texte).toEqual(
            'Les oiseaux et les insectes disparaissent du fait de la destruction des haies, de la simplification des cultures, et de l’usage intensif de pesticides et d’engrais. Les paysages s’uniformisent et la production agricole s’en trouve fragilisée.'
        );
        expect(constat.message.question).toEqual('Et si on faisait évoluer nos pratiques agricoles ?');
        expect(constat.infographie?.nomComposant).toEqual('InfographieCarteHVN');

        constat = creerConstatPaysagesAgricoles(creerDonneesApi({}), 15);
        expect(constat.message.titre).toEqual('Les pratiques agricoles dominantes de votre territoire sont peu favorables à la biodiversité.');
        expect(constat.message.texte).toEqual(
            'Les oiseaux et les insectes disparaissent du fait de la destruction des haies, de la simplification des cultures, et de l’usage intensif de pesticides et d’engrais. Les paysages s’uniformisent et la production agricole s’en trouve fragilisée.'
        );
        expect(constat.message.question).toEqual('Et si on faisait évoluer nos pratiques agricoles ?');
        expect(constat.infographie?.nomComposant).toEqual('InfographieCarteHVN');

        constat = creerConstatPaysagesAgricoles(creerDonneesApi({}), 20);
        expect(constat.message.titre).toEqual('Les pratiques agricoles de votre territoire contribuent à préserver la biodiversité.');
        expect(constat.message.texte).toEqual(
            'Le maintien des haies et des prairies, les pratiques peu intensives comme l’agriculture biologique, permettent aux espèces sauvages d’habiter les zones cultivées. Leur présence améliore en retour la production agricole.'
        );
        expect(constat.message.question).toEqual('Et si on généralisait ces pratiques vertueuses ?');
        expect(constat.infographie?.nomComposant).toEqual('InfographieCarteHVN');

        constat = creerConstatPaysagesAgricoles(
            creerDonneesApi({
                codeProfilDiagnostic: 'ELEVAGE_INTENSIF'
            }),
            0
        );
        expect(constat.message.titre).toEqual('Les pratiques agricoles dominantes de votre territoire contribuent au déclin de la biodiversité.');
        expect(constat.message.texte).toEqual(
            'L’excès de nitrates (azote) provenant de certains élevages et cultures de la région entraîne la pollution des cours d’eau et des nappes phréatiques. Les impacts sur la vie aquatique et les coûts de traitement de l’eau potable sont importants.'
        );
        constat = creerConstatPaysagesAgricoles(
            creerDonneesApi({
                codeProfilDiagnostic: 'ELEVAGE_INTENSIF'
            }),
            25
        );
        expect(constat.message.titre).toEqual('Les paysages agricoles de votre territoire sont favorables à la biodiversité mais...');
        expect(constat.message.texte).toEqual(
            'L’excès de nitrates (azote) provenant de certains élevages et cultures de la région entraîne la pollution des cours d’eau et des nappes phréatiques. Les impacts sur la vie aquatique et les coûts de traitement de l’eau potable sont importants.'
        );
        expect(constat.infographie?.nomComposant).toEqual('InfographieCarteHVN');

        constat = creerConstatPaysagesAgricoles(
            creerDonneesApi({
                noduNormalise: 8
            }),
            0
        );
        expect(constat.message.titre).toEqual('Les pratiques agricoles dominantes de votre territoire contribuent au déclin de la biodiversité.');
        expect(constat.message.texte).toEqual(
            'L’usage de pesticides y est très important et les risques associés sont multiples : santé des travailleurs agricoles, qualité des nappes phréatiques et cours d’eau, disparition des insectes pollinisateurs…'
        );
        expect(constat.infographie?.nomComposant).toEqual('InfographieCarteHVN');
        constat = creerConstatPaysagesAgricoles(
            creerDonneesApi({
                noduNormalise: 8
            }),
            25
        );
        expect(constat.message.titre).toEqual('Les paysages agricoles de votre territoire sont favorables à la biodiversité mais...');
        expect(constat.message.texte).toEqual(
            'L’usage de pesticides y est très important et les risques associés sont multiples : santé des travailleurs agricoles, qualité des nappes phréatiques et cours d’eau, disparition des insectes pollinisateurs…'
        );
        expect(constat.infographie?.nomComposant).toEqual('InfographieCarteHVN');
    });

    it('Test de la fonction calculerMessageCourtConstatPaysagesAgricoles', () => {
        expect(calculerMessageCourtConstatPaysagesAgricoles(1.2, 3.5)).toEqual(
            '<p>1,2 % de la surface agricole est en agriculture biologique.</p><p>L’usage de pesticides est 3,5 fois plus intense que la moyenne nationale.</p>'
        );
        expect(calculerMessageCourtConstatPaysagesAgricoles(12, null)).toEqual('<p>12 % de la surface agricole est en agriculture biologique.</p>');
        expect(calculerMessageCourtConstatPaysagesAgricoles(null, 3.5)).toEqual(
            '<p>L’usage de pesticides est 3,5 fois plus intense que la moyenne nationale.</p>'
        );
        expect(calculerMessageCourtConstatPaysagesAgricoles(null, null)).toEqual(
            'Les oiseaux et les insectes disparaissent du fait de la destruction des haies, de la simplification des cultures, et de l’usage intensif de pesticides et d’engrais.'
        );
        expect(calculerMessageCourtConstatPaysagesAgricoles(null, 1.1)).toEqual(
            'Les oiseaux et les insectes disparaissent du fait de la destruction des haies, de la simplification des cultures, et de l’usage intensif de pesticides et d’engrais.'
        );
    });
});
