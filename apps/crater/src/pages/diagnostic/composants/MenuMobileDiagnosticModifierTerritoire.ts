import '@lga/commun/build/composants/champ-recherche-territoire/ChampRechercheTerritoire.js';
import '@lga/design-system/build/composants/Bouton.js';
import '@lga/design-system/build/composants/RadioBouton.js';

import { getApiBaseUrl } from '@lga/commun/build/env/config-baseurl';
import { Bouton } from '@lga/design-system/build/composants/Bouton.js';
import type { OptionsRadioBouton } from '@lga/design-system/build/composants/RadioBouton';
import { EvenementFermer } from '@lga/design-system/build/evenements/EvenementFermer.js';
import type { EvenementSelectionner } from '@lga/design-system/build/evenements/EvenementSelectionner';
import { ICONES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/icones-design-system.js';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { css, html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { PAGES_PRINCIPALES } from '../../../configuration/pages/declaration-pages.js';
import { EvenementSelectionnerCategorieTerritoire } from '../../../evenements/EvenementSelectionnerCategorieTerritoire';

declare global {
    interface HTMLElementTagNameMap {
        'c-menu-mobile-diagnostic-modifier-territoire': MenuMobileDiagnosticModifierTerritoire;
    }
}
@customElement('c-menu-mobile-diagnostic-modifier-territoire')
export class MenuMobileDiagnosticModifierTerritoire extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        css`
            :host {
                display: block;
                width: 100vw;
                height: 100vh;
            }

            div {
                background-color: var(--couleur-blanc);
                min-height: 100%;
            }

            header {
                border-bottom: var(--couleur-primaire) solid 2px;
                height: var(--hauteur-entete-principale);
                display: flex;
                align-items: center;
            }

            main {
                padding: calc(2 * var(--dsem));
            }

            section {
                padding: calc(1.5 * var(--dsem)) 0;
            }

            c-bouton {
                padding: 1rem;
            }
        `
    ];

    @property({ attribute: false })
    optionsEchellesTerritoriales?: OptionsRadioBouton;

    render() {
        return html`
            <div>
                <header>
                    <c-bouton @click="${this.fermer}" libelleTooltip="Fermer" type="${Bouton.TYPE.plat}"
                        >${ICONES_DESIGN_SYSTEM.flecheGauche}
                    </c-bouton>
                </header>
                <main>
                    <section>
                        <h2 class="texte-alternatif">Rechercher un territoire</h2>
                        <c-champ-recherche-territoire
                            apiBaseUrl=${getApiBaseUrl()}
                            @selectionnerTerritoire="${this.fermerParents}"
                            lienFormulaireDemandeAjoutTerritoire="${PAGES_PRINCIPALES.demandeAjoutTerritoire.getUrl()}"
                        ></c-champ-recherche-territoire>
                    </section>
                    <section>
                        <h2 class="texte-alternatif">Changer d'échelle</h2>
                        <c-radio-bouton
                            id="boutons-territoires"
                            @selectionner=${this.actionSelectionCategorieTerritoire}
                            .options=${this.optionsEchellesTerritoriales}
                        ></c-radio-bouton>
                    </section>
                </main>
            </div>
        `;
    }

    private fermer() {
        this.dispatchEvent(new EvenementFermer());
    }
    private fermerParents() {
        this.dispatchEvent(new EvenementFermer(true));
    }

    private actionSelectionCategorieTerritoire(event: Event) {
        const evenementSelectionner = event as EvenementSelectionner;
        this.dispatchEvent(new EvenementSelectionnerCategorieTerritoire(evenementSelectionner.detail.idItemSelectionne));
        evenementSelectionner.stopPropagation();
        this.fermerParents();
    }
}
