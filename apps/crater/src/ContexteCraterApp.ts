import './pages/commun/PageSablierCrater.js';

import { enregistrerSuiviPage } from '@lga/commun/build/matomo/matomo-utils';
import type { DonneesPageSimple } from '@lga/commun/build/modeles/pages/donnees-pages.js';
import type { ModelePage } from '@lga/commun/build/modeles/pages/ModelePage.js';
import { Page } from '@lga/commun/build/modeles/pages/Page.js';
import { GestionnaireErreur } from '@lga/commun/build/outils/GestionnaireErreur';
import { importDynamique } from '@lga/commun/build/outils/importDynamiqueDirective';
import { positionnerDonnesSEO } from '@lga/commun/build/outils/seo';
import { html } from 'lit';
import type { DirectiveResult } from 'lit/async-directive.js';
import { ifDefined } from 'lit/directives/if-defined.js';

import {
    PAGES_AIDE,
    PAGES_CARTE,
    PAGES_COMPRENDRE_LE_SA,
    PAGES_DIAGNOSTIC,
    PAGES_METHODOLOGIE,
    PAGES_PRINCIPALES
} from './configuration/pages/declaration-pages.js';
import type { DonneesPageCarte } from './pages/carte/PageCarte.js';
import type { DonneesPageDiagnostic, DonneesPageDiagnosticDomaine, DonneesPageDiagnosticMaillon } from './pages/diagnostic/PageDiagnostic.js';

export class ContexteCraterApp {
    private gestionnaireErreur = new GestionnaireErreur();

    public templatePageCourante: DirectiveResult = html`<c-page-sablier-crater></c-page-sablier-crater>`;
    public pageCourante = new Page<DonneesPageSimple>(PAGES_PRINCIPALES.accueil);

    //-------------------------------------------
    // Mécanismes communs et routeurs génériques
    //-------------------------------------------
    mettreEnErreur = (codeErreur: string, messageErreur: string) => {
        this.gestionnaireErreur.mettreEnErreur(codeErreur, messageErreur);
    };

    routerSiErreur = () => {
        if (this.gestionnaireErreur.estEnErreur()) {
            this.templatePageCourante = importDynamique(
                import('./pages/erreur/PageErreur.js'),
                html`<c-page-erreur
                    codeErreur=${ifDefined(this.gestionnaireErreur.erreur?.code)}
                    messageErreur="${ifDefined(this.gestionnaireErreur.erreur?.message)}"
                ></c-page-erreur>`,
                html`<c-page-sablier-crater></c-page-sablier-crater>`
            );
            this.gestionnaireErreur.annulerErreur();
        }
    };

    routerVersPageErreurNotFound = (urlPage: string) => {
        this.majPageCourante(PAGES_PRINCIPALES.erreur);
        this.gestionnaireErreur.mettreEnErreur('URL_INCONNUE', `La page demandée n'existe pas, l'adresse "${urlPage}" est inconnue.`);
        this.templatePageCourante = importDynamique(
            import('./pages/erreur/PageErreur.js'),
            html`<c-page-erreur
                codeErreur=${ifDefined(this.gestionnaireErreur.erreur?.code)}
                messageErreur="${ifDefined(this.gestionnaireErreur.erreur?.message)}"
            ></c-page-erreur>`,
            html`<c-page-sablier-crater></c-page-sablier-crater>`
        );
        this.gestionnaireErreur.annulerErreur();
    };

    private majPageCourante(modelePage: ModelePage<DonneesPageSimple>, donneesInitiales: DonneesPageSimple = {}) {
        const nouvellePage = new Page<DonneesPageSimple>(modelePage, donneesInitiales);
        this.resetScrollSiChangementDePage(nouvellePage);
        this.pageCourante = nouvellePage;
    }

    private resetScrollSiChangementDePage(nouvellePage: Page<DonneesPageSimple>) {
        if (this.pageCourante.getId() !== nouvellePage.getId()) {
            window.scrollTo({ top: 0, left: 0, behavior: 'auto' });
        }
    }

    majPostChangementPage() {
        positionnerDonnesSEO(this.pageCourante.getTitreLong(), this.pageCourante.getUrlCanonique(), this.pageCourante.getMetaDescription());
        enregistrerSuiviPage(this.pageCourante.getUrl(), document.title);
    }

    majDonneesPageCourante(donneesPage: DonneesPageSimple) {
        const anciennePage = new Page(this.pageCourante.modelePage, this.pageCourante.donnees);
        this.pageCourante.majDonnees(donneesPage);
        if (anciennePage.getUrl() !== this.pageCourante.getUrl()) {
            this.ajouterDansHistoriqueDeNavigation(this.pageCourante.getUrl());
            enregistrerSuiviPage(this.pageCourante.getUrl(), document.title);
        }
        positionnerDonnesSEO(this.pageCourante.getTitreLong(), this.pageCourante.getUrlCanonique(), this.pageCourante.getMetaDescription());
    }

    private ajouterDansHistoriqueDeNavigation(urlRelative: string) {
        const url = new URL(urlRelative, location.origin);
        history.pushState({ path: urlRelative }, '', url);
    }

    //------------------------------------
    // Routeurs vers les différentes pages
    //------------------------------------
    routerVersPageAccueil = (donneesPage: DonneesPageSimple) => {
        this.majPageCourante(PAGES_PRINCIPALES.accueil, donneesPage);
        this.templatePageCourante = importDynamique(
            import('./pages/accueil/PageAccueil.js'),
            html`<c-page-accueil idElementCibleScroll="${ifDefined(this.pageCourante.donnees.idElementCibleScroll)}"></c-page-accueil>`,
            html`<c-page-sablier-crater></c-page-sablier-crater>`
        );
    };

    routerVersPageDiagnosticAccueil = () => {
        this.majPageCourante(PAGES_PRINCIPALES.diagnostic);
        this.templatePageCourante = importDynamique(
            import('./pages/diagnostic/PageDiagnosticAccueil.js'),
            html`<c-page-diagnostic-accueil></c-page-diagnostic-accueil>`,
            html`<c-page-sablier-crater></c-page-sablier-crater>`
        );
    };

    routerVersPageDiagnosticSynthese = (donneesPage: DonneesPageDiagnostic) => {
        this.majPageCourante(PAGES_DIAGNOSTIC.synthese as ModelePage<DonneesPageSimple>, donneesPage);
        this.templatePageCourante = importDynamique(
            import('./pages/diagnostic/PageDiagnostic.js'),
            html`<c-page-diagnostic .donneesPage=${this.pageCourante.donnees as DonneesPageDiagnostic}></c-page-diagnostic>`,
            html`<c-page-sablier-crater></c-page-sablier-crater>`
        );
    };

    routerVersPageDiagnosticTerritoire = (donneesPage: DonneesPageDiagnostic) => {
        this.majPageCourante(PAGES_DIAGNOSTIC.territoire as ModelePage<DonneesPageSimple>, donneesPage);
        this.templatePageCourante = importDynamique(
            import('./pages/diagnostic/PageDiagnostic.js'),
            html` <c-page-diagnostic .donneesPage=${this.pageCourante.donnees as DonneesPageDiagnostic}></c-page-diagnostic>`,
            html` <c-page-sablier-crater></c-page-sablier-crater>`
        );
    };

    routerVersPageDiagnosticMaillon = (donneesPage: DonneesPageDiagnosticMaillon) => {
        this.majPageCourante(PAGES_DIAGNOSTIC.maillons as ModelePage<DonneesPageSimple>, donneesPage);
        this.templatePageCourante = importDynamique(
            import('./pages/diagnostic/PageDiagnostic.js'),
            html`<c-page-diagnostic .donneesPage=${this.pageCourante.donnees as DonneesPageDiagnostic}></c-page-diagnostic>`,
            html`<c-page-sablier-crater></c-page-sablier-crater>`
        );
    };

    routerVersPageDiagnosticDomaine = (donneesPage: DonneesPageDiagnosticDomaine) => {
        this.majPageCourante(PAGES_DIAGNOSTIC.domaines as ModelePage<DonneesPageSimple>, donneesPage);
        this.templatePageCourante = importDynamique(
            import('./pages/diagnostic/PageDiagnostic.js'),
            html`<c-page-diagnostic .donneesPage=${this.pageCourante.donnees as DonneesPageDiagnostic}></c-page-diagnostic>`,
            html`<c-page-sablier-crater></c-page-sablier-crater>`
        );
    };

    routerVersPageDiagnosticPdf = (donneesPage: DonneesPageDiagnostic) => {
        this.majPageCourante(PAGES_DIAGNOSTIC.pdf as ModelePage<DonneesPageSimple>, donneesPage);
        this.templatePageCourante = importDynamique(
            import('./pages/diagnostic/PageDiagnosticPdf.js'),
            html`<c-page-diagnostic-pdf .donneesPage=${this.pageCourante.donnees as DonneesPageDiagnostic}></c-page-diagnostic-pdf>`,
            html`<c-page-sablier-crater></c-page-sablier-crater>`
        );
    };

    routerVersPageCarte = () => {
        this.majPageCourante(PAGES_PRINCIPALES.carte);
        this.templatePageCourante = importDynamique(
            import('./pages/carte/PageCarteAccueil.js'),
            html`<c-page-carte-accueil></c-page-carte-accueil>`,
            html`<c-page-sablier-crater></c-page-sablier-crater>`
        );
    };

    routerVersPageCarteIndicateur = (donneesPage: DonneesPageCarte) => {
        this.majPageCourante(PAGES_CARTE.indicateurs as ModelePage<DonneesPageSimple>, donneesPage);
        this.templatePageCourante = importDynamique(
            import('./pages/carte/PageCarte.js'),
            html`<c-page-carte .donneesPage=${this.pageCourante.donnees as DonneesPageCarte}></c-page-carte>`,
            html`<c-page-sablier-crater></c-page-sablier-crater>`
        );
    };

    routerVersPageAideAccueil = () => {
        this.majPageCourante(PAGES_PRINCIPALES.aide);
        this.templatePageCourante = importDynamique(
            import('./pages/aide/PageAide.js'),
            html`<c-page-aide></c-page-aide>`,
            html`<c-page-sablier-crater></c-page-sablier-crater>`
        );
    };

    routerVersPageLeSystemeActuel = () => {
        this.majPageCourante(PAGES_COMPRENDRE_LE_SA.systemeActuel);
        this.templatePageCourante = importDynamique(
            import('./pages/aide/comprendre-le-systeme-alimentaire/PageAideSystemeActuel.js'),
            html`<c-page-aide-systeme-actuel></c-page-aide-systeme-actuel>`,
            html`<c-page-sablier-crater></c-page-sablier-crater>`
        );
    };
    routerVersPageDefaillancesVulnerabilites = () => {
        this.majPageCourante(PAGES_COMPRENDRE_LE_SA.defaillancesVulnerabilites);
        this.templatePageCourante = importDynamique(
            import('./pages/aide/comprendre-le-systeme-alimentaire/PageAideDefaillancesVulnerabilites.js'),
            html`<c-page-aide-defaillances-vulnerabilites></c-page-aide-defaillances-vulnerabilites>`,
            html`<c-page-sablier-crater></c-page-sablier-crater>`
        );
    };
    routerVersPageTransitionAgricoleAlimentaire = () => {
        this.majPageCourante(PAGES_COMPRENDRE_LE_SA.transitionAgricoleAlimentaire);
        this.templatePageCourante = importDynamique(
            import('./pages/aide/comprendre-le-systeme-alimentaire/PageAideTransitionAgricoleEtAlimentaire.js'),
            html`<c-page-aide-transition-agricole-et-alimentaire></c-page-aide-transition-agricole-et-alimentaire>`,
            html`<c-page-sablier-crater></c-page-sablier-crater>`
        );
    };

    routerVersPageMethodologiePresentation = (donneesPage: DonneesPageSimple) => {
        this.majPageCourante(PAGES_METHODOLOGIE.presentationGenerale, donneesPage);
        this.templatePageCourante = importDynamique(
            import('./pages/aide/methodologie/PageMethodologiePresentationGenerale.js'),
            html`<c-page-methodologie-presentation-generale
                idElementCibleScroll="${this.pageCourante.donnees.idElementCibleScroll ?? ''}"
            ></c-page-methodologie-presentation-generale>`,
            html`<c-page-sablier-crater></c-page-sablier-crater>`
        );
    };

    routerVersPageMethodologieTerresAgricoles = (donneesPage: DonneesPageSimple) => {
        this.majPageCourante(PAGES_METHODOLOGIE.terresAgricoles, donneesPage);
        this.templatePageCourante = importDynamique(
            import('./pages/aide/methodologie/PageMethodologieTerresAgricoles.js'),
            html`<c-page-methodologie-terres-agricoles
                idElementCibleScroll="${this.pageCourante.donnees.idElementCibleScroll ?? ''}"
            ></c-page-methodologie-terres-agricoles>`,
            html`<c-page-sablier-crater></c-page-sablier-crater>`
        );
    };
    routerVersPageMethodologieAgriculteursExploitations = (donneesPage: DonneesPageSimple) => {
        this.majPageCourante(PAGES_METHODOLOGIE.agriculteursExploitations, donneesPage);
        this.templatePageCourante = importDynamique(
            import('./pages/aide/methodologie/PageMethodologieAgriculteursExploitations.js'),
            html`<c-page-methodologie-agriculteurs-exploitations
                idElementCibleScroll="${this.pageCourante.donnees.idElementCibleScroll ?? ''}"
            ></c-page-methodologie-agriculteurs-exploitations>`,
            html`<c-page-sablier-crater></c-page-sablier-crater>`
        );
    };
    routerVersPageMethodologieIntrants = (donneesPage: DonneesPageSimple) => {
        this.majPageCourante(PAGES_METHODOLOGIE.intrants, donneesPage);
        this.templatePageCourante = importDynamique(
            import('./pages/aide/methodologie/PageMethodologieIntrants.js'),
            html`<c-page-methodologie-intrants
                idElementCibleScroll="${this.pageCourante.donnees.idElementCibleScroll ?? ''}"
            ></c-page-methodologie-intrants>`,
            html`<c-page-sablier-crater></c-page-sablier-crater>`
        );
    };
    routerVersPageMethodologieProduction = (donneesPage: DonneesPageSimple) => {
        this.majPageCourante(PAGES_METHODOLOGIE.production, donneesPage);
        this.templatePageCourante = importDynamique(
            import('./pages/aide/methodologie/PageMethodologieProduction.js'),
            html`<c-page-methodologie-production
                idElementCibleScroll="${this.pageCourante.donnees.idElementCibleScroll ?? ''}"
            ></c-page-methodologie-production>`,
            html`<c-page-sablier-crater></c-page-sablier-crater>`
        );
    };
    routerVersPageMethodologieTransformationDistribution = (donneesPage: DonneesPageSimple) => {
        this.majPageCourante(PAGES_METHODOLOGIE.transformationDistribution, donneesPage);
        this.templatePageCourante = importDynamique(
            import('./pages/aide/methodologie/PageMethodologieTransformationDistribution.js'),
            html`<c-page-methodologie-transformation-distribution
                idElementCibleScroll="${this.pageCourante.donnees.idElementCibleScroll ?? ''}"
            ></c-page-methodologie-transformation-distribution>`,
            html`<c-page-sablier-crater></c-page-sablier-crater>`
        );
    };
    routerVersPageMethodologieConsommation = (donneesPage: DonneesPageSimple) => {
        this.majPageCourante(PAGES_METHODOLOGIE.consommation, donneesPage);
        this.templatePageCourante = importDynamique(
            import('./pages/aide/methodologie/PageMethodologieConsommation.js'),
            html`<c-page-methodologie-consommation
                idElementCibleScroll="${this.pageCourante.donnees.idElementCibleScroll ?? ''}"
            ></c-page-methodologie-consommation>`,
            html`<c-page-sablier-crater></c-page-sablier-crater>`
        );
    };
    routerVersPageMethodologieSourcesDonnees = (donneesPage: DonneesPageSimple) => {
        this.majPageCourante(PAGES_METHODOLOGIE.sourcesDonnees, donneesPage);
        this.templatePageCourante = importDynamique(
            import('./pages/aide/methodologie/PageMethodologieSourcesDonnees.js'),
            html`<c-page-methodologie-sources-donnees
                idElementCibleScroll="${this.pageCourante.donnees.idElementCibleScroll ?? ''}"
            ></c-page-methodologie-sources-donnees>`,
            html`<c-page-sablier-crater></c-page-sablier-crater>`
        );
    };
    routerVersPageFAQ = (donneesPage: DonneesPageSimple) => {
        this.majPageCourante(PAGES_AIDE.faq, donneesPage);
        this.templatePageCourante = importDynamique(
            import('./pages/aide/faq/PageFAQ.js'),
            html`<c-page-faq idElementCibleScroll="${this.pageCourante.donnees.idElementCibleScroll ?? ''}"></c-page-faq>`,
            html`<c-page-sablier-crater></c-page-sablier-crater>`
        );
    };
    routerVersPageGlossaire = (donneesPage: DonneesPageSimple) => {
        this.majPageCourante(PAGES_AIDE.glossaire, donneesPage);
        this.templatePageCourante = importDynamique(
            import('./pages/aide/glossaire/PageGlossaire.js'),
            html`<c-page-glossaire idElementCibleScroll="${this.pageCourante.donnees.idElementCibleScroll ?? ''}"></c-page-glossaire>`,
            html`<c-page-sablier-crater></c-page-sablier-crater>`
        );
    };

    routerVersPageProjet = (donneesPage: DonneesPageSimple) => {
        this.majPageCourante(PAGES_PRINCIPALES.projet, donneesPage);
        this.templatePageCourante = importDynamique(
            import('./pages/projet/PageProjet.js'),
            html`<c-page-projet idElementCibleScroll="${this.pageCourante.donnees.idElementCibleScroll ?? ''}"></c-page-projet>`,
            html`<c-page-sablier-crater></c-page-sablier-crater>`
        );
    };

    routerVersPageMentionsLegales = (donneesPage: DonneesPageSimple) => {
        this.majPageCourante(PAGES_PRINCIPALES.mentionsLegales, donneesPage);
        this.templatePageCourante = importDynamique(
            import('./pages/mentions-legales/PageMentionsLegales.js'),
            html`<c-page-mentions-legales idElementCibleScroll="${this.pageCourante.donnees.idElementCibleScroll ?? ''}"></c-page-mentions-legales>`,
            html`<c-page-sablier-crater></c-page-sablier-crater>`
        );
    };

    routerVersPageApi = () => {
        this.majPageCourante(PAGES_PRINCIPALES.api);
        this.templatePageCourante = importDynamique(
            import('./pages/api/PageApi.js'),
            html`<c-page-api></c-page-api>`,
            html`<c-page-sablier-crater></c-page-sablier-crater>`
        );
    };

    routerVersPageContact = () => {
        this.majPageCourante(PAGES_PRINCIPALES.contact);
        this.templatePageCourante = importDynamique(
            import('./pages/contact/PageContact.js'),
            html`<c-page-contact></c-page-contact>`,
            html`<c-page-sablier-crater></c-page-sablier-crater>`
        );
    };
    routerVersPageDemandeAjoutTerritoire = () => {
        this.majPageCourante(PAGES_PRINCIPALES.demandeAjoutTerritoire);
        this.templatePageCourante = importDynamique(
            import('./pages/contact/PageDemandeAjoutTerritoire.js'),
            html`<c-page-demande-ajout-territoire></c-page-demande-ajout-territoire>`,
            html`<c-page-sablier-crater></c-page-sablier-crater>`
        );
    };
}
