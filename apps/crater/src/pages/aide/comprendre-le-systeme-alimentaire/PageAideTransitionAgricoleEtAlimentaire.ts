import '../../commun/template/TemplatePageAvecSommaire.js';
import './BoiteEnSavoirPlusSurLeSA.js';
import '@lga/design-system/build/composants/EncartPagesPrecedenteEtSuivante.js';

import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { css, html, LitElement } from 'lit';
import { customElement } from 'lit/decorators.js';

import { PAGES_COMPRENDRE_LE_SA, PAGES_PRINCIPALES } from '../../../configuration/pages/declaration-pages.js';
import imageAfterres from '../../../ressources/images/afterres2050.png';
import imageTYFA from '../../../ressources/images/tyfa2050.png';
import { STYLES_CRATER } from '../../commun/pages-styles';
import { construireLiensFilArianePagesComprendreLeSA, ITEMS_MENU_COMPRENDRE_LE_SA } from './comprendre-le-systeme-alimentaire-utils.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-page-aide-transition-agricole-et-alimentaire': PageAideTransitionAgricoleEtAlimentaire;
    }
}

@customElement('c-page-aide-transition-agricole-et-alimentaire')
export class PageAideTransitionAgricoleEtAlimentaire extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        STYLES_CRATER,
        css`
            #figures {
                display: flex;
                flex-wrap: wrap;
                justify-content: center;
                gap: var(--dsem);
            }
            figure img {
                height: 300px;
            }
            figcaption {
                text-align: center;
            }
        `
    ];

    render() {
        return html`
            <c-template-page-avec-sommaire
                idItemActifMenuPrincipal="${PAGES_PRINCIPALES.aide.getId()}"
                idItemActifMenuSommaire="${PAGES_COMPRENDRE_LE_SA.transitionAgricoleAlimentaire.getId()}"
                .itemsMenuSommaire="${ITEMS_MENU_COMPRENDRE_LE_SA}"
                .liensFilAriane="${construireLiensFilArianePagesComprendreLeSA(
                    'La transition agricole et alimentaire',
                    PAGES_COMPRENDRE_LE_SA.defaillancesVulnerabilites.getUrl()
                )}"
            >
                <div slot="contenu">${this.renderCoeurPage()}</div>
            </c-template-page-avec-sommaire>
        `;
    }

    private renderCoeurPage() {
        return html`
            <section class="texte-moyen">
                <h1>${PAGES_COMPRENDRE_LE_SA.transitionAgricoleAlimentaire.getTitreCourt()}</h1>
                <p>
                    Les études scientifiques convergent : le système agricole et alimentaire doit profondément se transformer pour assurer durablement
                    notre sécurité alimentaire.
                </p>
                <h2>Généraliser l'agroécologie</h2>
                <p>
                    Celle-ci repose sur trois grands principes. Réduire nos dépendances aux ressources dont la disponibilité est compromise (énergies
                    fossiles, engrais minéraux, eau). Préserver les milieux (sols, eau, biodiversité) pour favoriser le renouvellement de la fertilité
                    et la régulation des espèces indésirables. Diversifier les systèmes agricoles à tous les niveaux (génétique, productions,
                    paysages) pour les rendre plus résilients face à des perturbations multiples et imprévisibles.
                </p>
                <h2>Manger plus végétal</h2>
                <p>
                    En France, la moitié des terres arables sert à produire des plantes qui vont nourrir des animaux. Le rendement en termes de
                    calories est bien plus faible que si ces terres étaient cultivées directement pour l’alimentation humaine. Réduire la consommation
                    d’aliments d’origine animale et réserver les terres arables pour des cultures directement comestibles est notre principal levier
                    pour maintenir la disponibilité alimentaire malgré les baisses de rendements à venir.
                </p>
                <h2>Réduire les distances</h2>
                <p>
                    Aujourd’hui, la quasi-totalité de la production agricole d’un département français est exportée tandis que la quasi-totalité de la
                    nourriture consommée par ses habitants est importée. Ce système repose sur un flux continu de camions transportant les matières
                    agricoles brutes et les produits transformés. Sans pétrole abondant, cette organisation ne tient plus. Il est nécessaire de
                    reconstruire des filières territoriales pour les produits alimentaires de base.
                </p>
                <div id="figures">
                    <figure>
                        <img src="${imageAfterres}" width="226" height="320" alt="Scénario Afterres 2050" />
                    </figure>
                    <figure>
                        <img src="${imageTYFA}" width="455" height="640" alt="Scénario TYFA" />
                    </figure>
                </div>
                <figcaption class="texte-petit">
                    Deux scénarios prospectifs : Afterres2050 de l’association Solagro (échelle France) et TYFA (Ten Years For Agroecology) de l’IDDRI
                    (échelle Union Européenne). <br />
                    Crédits : Solagro (2016) Le scénario Afterres2050 version 2016 / Poux X. et Aubert P-M. (2018) Une Europe agroécologique en 2050 :
                    une agriculture multifonctionnelle pour une alimentation saine. Enseignement d’une modélisation du système alimentaire européen,
                    Iddri-AScA. Institut du Développement Durable et des Relations Internationales.
                </figcaption>
                <c-encart-pages-precedente-suivante
                    libellePagePrecedente=${PAGES_COMPRENDRE_LE_SA.defaillancesVulnerabilites.getTitreCourt()}
                    hrefPagePrecedente=${PAGES_COMPRENDRE_LE_SA.defaillancesVulnerabilites.getUrl()}
                ></c-encart-pages-precedente-suivante>
                <c-boite-en-savoir-plus-sur-le-sa></c-boite-en-savoir-plus-sur-le-sa>
            </section>
        `;
    }
}
