import type { Territoire } from '@lga/territoires';

import type { IndicateurSynthese } from '../IndicateurSynthese';
import type { Note } from '../Note';
import type { IndicateurConsommation } from '../production';
import { calculerMessageDetaillePartAlimentationAnimaleDansConsommation, calculerMessageDetailleTauxPauvrete } from './messages-consommation';

export class Consommation implements IndicateurSynthese {
    public consommationPays?: Consommation;

    constructor(
        private territoire: Territoire,
        public readonly note: Note,
        public readonly tauxPauvrete: number | null,
        public readonly indicateurConsommation: IndicateurConsommation | null
    ) {}

    setConsommationPays(consommationPays: Consommation) {
        this.consommationPays = consommationPays;
    }

    readonly messageSynthese = `Régime alimentaire <strong>riche et très carné</strong> qui induit un <strong>besoin important de surfaces agricoles</strong>.
Précarité alimentaire <strong>importante et en hausse</strong>.`;

    get messageTauxPauvrete() {
        return calculerMessageDetailleTauxPauvrete(this.territoire.nom, this.tauxPauvrete, this.consommationPays?.tauxPauvrete);
    }

    get messagePartAlimentationAnimaleDansConsommation() {
        return calculerMessageDetaillePartAlimentationAnimaleDansConsommation(
            this.territoire.nom,
            this.indicateurConsommation!.consommationHa,
            this.indicateurConsommation!.consommationAnimauxHa,
            this.indicateurConsommation!.pourcentageConsommationAnimauxSurConsommationTotale
        );
    }
}
