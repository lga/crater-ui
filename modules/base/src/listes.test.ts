import { describe, expect, it } from 'vitest';

import { deplacerElementEnPositionN, differenceListeAMoinsListeB, type Entite, verifierListeAIncluseDansListeB } from './listes.js';

describe('Test fonctions', () => {
    it('Test calcul difference listeA moins listeB', () => {
        const listeVide: Entite[] = [];
        const listeA: Entite[] = [{ id: '1' }, { id: '2' }, { id: '3' }];
        const listeB: Entite[] = [{ id: '2' }, { id: '5' }];

        expect(differenceListeAMoinsListeB(listeA, listeB)).toEqual([{ id: '1' }, { id: '3' }]);
        expect(differenceListeAMoinsListeB(listeB, listeA)).toEqual([{ id: '5' }]);
        expect(differenceListeAMoinsListeB(listeVide, listeA)).toEqual([]);
        expect(differenceListeAMoinsListeB(listeA, listeVide)).toEqual([{ id: '1' }, { id: '2' }, { id: '3' }]);
        expect(differenceListeAMoinsListeB(listeVide, listeVide)).toEqual([]);
    });

    it('Test deplacerElementEnPositionN', () => {
        const listeVide: Entite[] = [];
        const liste: Entite[] = [{ id: '1' }, { id: '2' }, { id: '3' }, { id: '4' }, { id: '5' }];

        expect(deplacerElementEnPositionN(liste, '4', 2)).toEqual([{ id: '1' }, { id: '4' }, { id: '2' }, { id: '3' }, { id: '5' }]);
        expect(deplacerElementEnPositionN(liste, '6', 2)).toEqual(liste);
        expect(deplacerElementEnPositionN(listeVide, '4', 2)).toEqual([]);
    });

    it('Test verifierListeAIncluseDansListeB', () => {
        expect(verifierListeAIncluseDansListeB([], ['1', '2'])).toEqual(true);
        expect(verifierListeAIncluseDansListeB(['1'], ['1', '2'])).toEqual(true);
        expect(verifierListeAIncluseDansListeB(['1', '2'], ['1', '2'])).toEqual(true);
        expect(verifierListeAIncluseDansListeB(['1', '2'], ['2', '1'])).toEqual(true);
        expect(verifierListeAIncluseDansListeB(['1', '2'], ['1', '2', '3'])).toEqual(true);
        expect(verifierListeAIncluseDansListeB(['1', '2'], ['1'])).toEqual(false);
    });
});
