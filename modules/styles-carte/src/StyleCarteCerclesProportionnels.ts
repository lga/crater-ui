import { COULEUR_VALEUR_NULL } from './couleurs-carte.js';
import { type AttributsStyleCarte, StyleCarte } from './StyleCarte';

export interface AttributsStyleCarteCerclesProportionnels extends AttributsStyleCarte {
    readonly facteurMultiplicatifTailleCercles: number;
    readonly couleurContours?: string;
    readonly couleurValeursPositives?: string;
    readonly couleurValeursNegatives?: string;
    readonly opacite?: number;
}

export class StyleCarteCerclesProportionnels extends StyleCarte {
    private facteurMultiplicatifTailleCercles: number;
    private couleurValeursPositives = 'green';
    private couleurValeursNegatives = 'red';
    opacite = 0.7;

    constructor(attributs: AttributsStyleCarteCerclesProportionnels) {
        super(attributs.couleurContours, attributs.calculerLibelleTooltipPersonnalise, attributs.unite);
        this.facteurMultiplicatifTailleCercles = attributs.facteurMultiplicatifTailleCercles;
        if (attributs.couleurValeursPositives) this.couleurValeursPositives = attributs.couleurValeursPositives;
        if (attributs.couleurValeursNegatives) this.couleurValeursNegatives = attributs.couleurValeursNegatives;
        if (attributs.opacite) this.opacite = attributs.opacite;
    }

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    calculerCouleurFondFeature(valeur?: number | string | null): string {
        return 'transparent';
    }

    calculerCouleurCercle(valeur: number | null): string {
        if (valeur === null) return COULEUR_VALEUR_NULL;
        return valeur >= 0 ? this.couleurValeursPositives : this.couleurValeursNegatives;
    }

    calculerRayon(valeur: number | null): number {
        return this.facteurMultiplicatifTailleCercles * Math.sqrt(Math.abs(valeur ?? 1));
    }
}
