import { formaterNombreSelonValeurString, type HtmlString, htmlstring } from '@lga/base';

import type { PosteEnergie } from './Energie.js';

export function calculerMessageEnergiePrincipal(
    nomTerritoire: string,
    energiePrimaireGJ: number | null,
    energiePrimaireGJParHa: number | null,
    energiePrimaireGJParHaPays?: number
): HtmlString {
    if (energiePrimaireGJ === null || energiePrimaireGJParHa === null)
        return htmlstring`Les données de consommation d'énergie ne sont pas disponibles.`;

    return htmlstring`Le territoire <em>${nomTerritoire}</em> consomme ${formaterNombreSelonValeurString(
        energiePrimaireGJ
    )} GJ d'énergie primaire par an pour les besoins de son agriculture (soit ${formaterNombreSelonValeurString(convertirGJEnMWh(energiePrimaireGJ))} MWh par an). Cela représente ${formaterNombreSelonValeurString(energiePrimaireGJParHa)} GJ d'énergie primaire par hectare de surface agricole${
        !energiePrimaireGJParHaPays
            ? htmlstring``
            : htmlstring`, soit <strong>${formaterNombreSelonValeurString(
                  energiePrimaireGJParHa / energiePrimaireGJParHaPays
              )} fois celui de la France métropolitaine</strong>`
    }.`;
}

export function calculerMessageEnergieSecondaire(postePrincipal?: PosteEnergie): HtmlString {
    if (!postePrincipal) return htmlstring``;
    return htmlstring`Le poste principal de consommation est <em>${postePrincipal.libelle}</em> qui représente ${formaterNombreSelonValeurString(
        postePrincipal.partPourcent
    )} % de la consommation totale. La répartition des différents postes est donnée dans la figure suivante :`;
}

export function convertirGJEnMWh(energieGJ: number | null) {
    if (energieGJ === null) return null;
    return energieGJ / 3.6;
}
