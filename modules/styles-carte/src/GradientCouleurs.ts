import { clip } from '@lga/base';

import { COULEUR_VALEUR_NULL } from './couleurs-carte.js';

interface ValeurCouleur {
    valeur: number;
    couleur: string;
}

export class GradientCouleurs {
    constructor(private seuilsCouleurs: ValeurCouleur[]) {
        this.seuilsCouleurs = seuilsCouleurs.map((couleur) => {
            return { valeur: couleur.valeur, couleur: this.nettoyerCodeCouleur(couleur.couleur) };
        });
    }

    calculerCouleur(valeur: number | null): string {
        if (valeur != null) {
            let indiceSeuilBas = 0;
            while (indiceSeuilBas < this.seuilsCouleurs.length - 2 && valeur >= this.seuilsCouleurs[indiceSeuilBas + 1].valeur) {
                indiceSeuilBas++;
            }
            return this.calculerGradientDeuxCouleurs(
                valeur,
                this.seuilsCouleurs[indiceSeuilBas].valeur,
                this.seuilsCouleurs[indiceSeuilBas + 1].valeur,
                this.seuilsCouleurs[indiceSeuilBas].couleur,
                this.seuilsCouleurs[indiceSeuilBas + 1].couleur
            );
        }
        return COULEUR_VALEUR_NULL;
    }

    get couleurBasse(): string {
        return this.calculerCouleur(this.seuilsCouleurs[0].valeur);
    }
    get couleurHaute(): string {
        return this.calculerCouleur(this.seuilsCouleurs[this.seuilsCouleurs.length - 1].valeur);
    }

    private nettoyerCodeCouleur(codeCouleur: string): string {
        return codeCouleur.replace(/#| /g, '');
    }

    private calculerGradientDeuxCouleurs(
        valeur: number,
        valeurMin: number,
        valeurMax: number,
        couleurValeurMin: string,
        couleurValeurMax: string
    ): string {
        valeur = clip(valeur, valeurMin, valeurMax);
        const ratio = (valeur - valeurMin) / (valeurMax - valeurMin);

        const r = calculerCodeCouleurInt256(couleurValeurMax.substring(0, 2), couleurValeurMin.substring(0, 2), ratio);
        const g = calculerCodeCouleurInt256(couleurValeurMax.substring(2, 4), couleurValeurMin.substring(2, 4), ratio);
        const b = calculerCodeCouleurInt256(couleurValeurMax.substring(4, 6), couleurValeurMin.substring(4, 6), ratio);

        return '#' + calculerCouleurHex(r) + calculerCouleurHex(g) + calculerCouleurHex(b);
    }
}

export function calculerCodeCouleurInt256(composanteCouleurMax: string, composanteCouleurMin: string, ratio: number): number {
    return Math.round(parseInt(composanteCouleurMax, 16) * ratio + parseInt(composanteCouleurMin, 16) * (1 - ratio));
}

export function calculerCouleurHex(codeCouleurInt256: number): string {
    const codeCouleurHexa = codeCouleurInt256.toString(16);
    return codeCouleurHexa.length === 1 ? '0' + codeCouleurHexa : codeCouleurHexa;
}
