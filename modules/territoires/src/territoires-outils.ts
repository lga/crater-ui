import type { CodeGenreNombreTerritoireApi, CodePrepositionTerritoireApi } from '@lga/specification-api';

export function calculerNomAvecDeterminant(nom: string, genreNombre: CodeGenreNombreTerritoireApi, prepositon: CodePrepositionTerritoireApi) {
    if (prepositon === 'à') {
        return nom;
    } else if (
        ('aeéèiïîoôuûüy'.includes(nom.charAt(0).toLowerCase()) ||
            (nom.charAt(0).toLowerCase() === 'h' && 'aeéèiïîoôuûüy'.includes(nom.charAt(1).toLowerCase()))) &&
        (genreNombre === 'FEMININ_SINGULIER' || genreNombre === 'MASCULIN_SINGULIER')
    )
        return "l'" + nom;
    else {
        const determinant = genreNombre === 'FEMININ_SINGULIER' ? 'la' : genreNombre === 'MASCULIN_SINGULIER' ? 'le' : 'les';
        return determinant + ' ' + nom;
    }
}

export function calculerNomAvecPrepositionA(nom: string, genreNombre: CodeGenreNombreTerritoireApi, prepositon: CodePrepositionTerritoireApi) {
    if (prepositon === 'en') {
        return 'en ' + nom;
    } else if (prepositon === 'dans') {
        return 'dans ' + calculerNomAvecDeterminant(nom, genreNombre, prepositon);
    } else {
        return 'à ' + nom;
    }
}

export function calculerNomAvecPrepositionDe(nom: string, genreNombre: CodeGenreNombreTerritoireApi, prepositon: CodePrepositionTerritoireApi) {
    return ('de ' + calculerNomAvecDeterminant(nom, genreNombre, prepositon)).replace('de les', 'des').replace('de le', 'du');
}
