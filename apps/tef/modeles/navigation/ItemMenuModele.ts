import type { RouteLocationRaw } from 'vue-router';

export interface ItemMenuBase {
    libelle: string;
}

export type ItemLienPage = ItemMenuBase & {
    type: 'LIEN_PAGE';
    route: RouteLocationRaw;
    estActif?: boolean;
    estSousSection?: boolean;
};

export type ItemLienHash = ItemMenuBase & {
    type: 'LIEN_HASH';
    hash: string;
    estActif?: boolean;
};

export type ItemLibelleSansLien = ItemMenuBase & {
    type: 'LIBELLE_SANS_LIEN';
};

export type ItemSousMenu = ItemLibelleSansLien | ItemLienPage;
