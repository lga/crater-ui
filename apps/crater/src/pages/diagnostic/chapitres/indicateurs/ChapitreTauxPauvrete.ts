import '../../charts/ChartBarres.js';
import '../composants/ChapitreDomaine.js';
import '../../../commun/PageSablierCrater.js';

import { IDS_DOMAINES } from '@lga/indicateurs';
import { SA } from '@lga/indicateurs';
import { html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { EtatRapport, type Rapport } from '../../../../modeles/diagnostics/Rapport.js';
import { Serie, SerieMultiple } from '../../charts/chart-outils.js';
import type { OptionsChartBarres } from '../../charts/ChartBarres';
import type { DonneesPageDiagnostic } from '../../PageDiagnostic.js';
import type { OptionsDomaineDetaille } from '../composants/ChapitreDomaine';

declare global {
    interface HTMLElementTagNameMap {
        'c-chapitre-taux-pauvrete': ChapitreTauxPauvrete;
    }
}

@customElement('c-chapitre-taux-pauvrete')
export class ChapitreTauxPauvrete extends LitElement {
    @property({ attribute: false })
    donneesPage?: DonneesPageDiagnostic;

    @property({ attribute: false })
    rapport?: Rapport;

    render() {
        if (this.rapport?.getEtat() === EtatRapport.PRET) {
            return html`<c-chapitre-domaine
                .options=${this.calculerOptionsChapitreDomaineTauxPauvrete(this.rapport, this.donneesPage)}
                nbLignesMessageMinimum="6"
            >
                <c-chart-barres .options=${this.calculerOptionsChartTauxPauvrete(this.rapport)} slot="chart-chapitre-domaine"></c-chart-barres>
            </c-chapitre-domaine>`;
        } else {
            return html`<c-page-sablier-crater></c-page-sablier-crater>`;
        }
    }

    private calculerOptionsChapitreDomaineTauxPauvrete(rapport: Rapport, donneesPage: DonneesPageDiagnostic | undefined): OptionsDomaineDetaille {
        return {
            domaine: SA.getDomaine(IDS_DOMAINES.pauvrete)!,
            message: rapport.getDiagnosticActif()!.consommation.messageTauxPauvrete,
            territoire: rapport.getTerritoireActif(),
            estPagePourTef: donneesPage?.estPagePourTef
        };
    }

    private calculerOptionsChartTauxPauvrete(rapport: Rapport): OptionsChartBarres {
        const labels = rapport.getDiagnostics()!.map((d) => d!.territoire.nom);
        return {
            series: new SerieMultiple([new Serie(rapport.getDiagnostics()!.map((d) => d!.consommation.tauxPauvrete))], labels),
            nomAxeOrdonnees: 'Taux de pauvreté (%)'
        };
    }
}
