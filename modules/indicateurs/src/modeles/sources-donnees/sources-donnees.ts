import { type HtmlString, htmlstring } from '@lga/base';

import { type FournisseurDonnee, fournisseursDonnees } from './fournisseurs-donnees';

export interface DefinitionSourceDonnee {
    id: string;
    nom: string;
    definition: HtmlString;
    annees: string;
    perimetreGeographique: string;
    fournisseur: FournisseurDonnee;
    url: string;
    description?: HtmlString;
    limites?: HtmlString;
}

export interface DefinitionSourceDonneeRetraitee {
    id: string;
    nom: string;
    source: DefinitionSourceDonnee;
    annees: string;
}

export interface DefinitionRapportEtude {
    id: string;
    nom: string;
    annee: number | string;
    auteur: string;
    url?: string;
}

export const SOURCES_DONNEES = {
    decoupage_communal: {
        id: 'decoupage_communal',
        nom: 'Découpage communal de la France',
        definition: htmlstring`Découpage communal de la France, et appartenances géographiques`,
        annees: '1er janvier 2024',
        perimetreGeographique: 'France métropolitaine + DROM',
        fournisseur: fournisseursDonnees.insee,
        url: 'https://www.insee.fr/fr/information/2028028'
    } as DefinitionSourceDonnee,

    codes_officiels_departements_et_regions: {
        id: 'codes_officiels_departements_et_regions',
        nom: 'Codes officiels des départements & régions',
        definition: htmlstring`Code officiel géographique`,
        annees: '1er janvier 2024',
        perimetreGeographique: 'France métropolitaine + DROM',
        fournisseur: fournisseursDonnees.insee,
        url: 'https://www.insee.fr/fr/information/6800675'
    } as DefinitionSourceDonnee,

    codes_postaux: {
        id: 'codes_postaux',
        nom: 'Codes postaux',
        definition: htmlstring`Base officielle des codes postaux (correspondance code INSEE ⇔ code postal)`,
        annees: '1er janvier 2024',
        perimetreGeographique: 'France (métropole et DOM), TOM et MONACO',
        fournisseur: fournisseursDonnees.laPoste,
        url: 'https://datanova.laposte.fr/datasets/laposte-hexasmal'
    } as DefinitionSourceDonnee,

    epcis: {
        id: 'epcis',
        nom: 'EPCI',
        definition: htmlstring`Définition des intercommunalités (EPCI)`,
        annees: '1er janvier 2024',
        perimetreGeographique: 'France métropolitaine + DROM',
        fournisseur: fournisseursDonnees.insee,
        url: 'https://www.insee.fr/fr/information/2510634'
    } as DefinitionSourceDonnee,

    communes_appartenance_bassins_de_vie: {
        id: 'communes_appartenance_bassins_de_vie',
        nom: "Communes d'appartenance des bassins de vie 2022",
        definition: htmlstring`Liste des communes des bassins de vie 2022 (BV2022)`,
        annees: '1er janvier 2024',
        perimetreGeographique: 'France métropolitaine + DROM',
        fournisseur: fournisseursDonnees.insee,
        url: 'https://www.insee.fr/fr/information/6676988'
    } as DefinitionSourceDonnee,

    communes_appartenance_pn: {
        id: 'communes_appartenance_pn',
        nom: "Communes d'appartenance des parcs nationaux (PN)",
        definition: htmlstring`Liste des communes des parcs nationaux (PN)`,
        annees: '1er janvier 2024',
        perimetreGeographique: 'France métropolitaine + DROM',
        fournisseur: fournisseursDonnees.observatoireDesTerritoires,
        url: 'https://www.observatoire-des-territoires.gouv.fr/pn-communes-des-parcs-nationaux'
    } as DefinitionSourceDonnee,

    communes_appartenance_pnr: {
        id: 'communes_appartenance_pnr',
        nom: "Communes d'appartenance des parcs naturels régionaux (PNR)",
        definition: htmlstring`Liste des communes des parcs naturels régionaux (PNR)`,
        annees: '1er janvier 2024',
        perimetreGeographique: 'France métropolitaine + DROM',
        fournisseur: fournisseursDonnees.observatoireDesTerritoires,
        url: 'https://www.observatoire-des-territoires.gouv.fr/pnr-communes-des-parcs-naturels-regionaux'
    } as DefinitionSourceDonnee,

    communes_appartenance_pays_et_petr: {
        id: 'communes_appartenance_pays_et_petr',
        nom: "Communes d'appartenance des PAYS et PETR",
        definition: htmlstring`Liste des communes des PAYS et PETR`,
        annees: '1er janvier 2024',
        perimetreGeographique: 'France métropolitaine + DROM',
        fournisseur: fournisseursDonnees.observatoireDesTerritoires,
        url: 'https://www.observatoire-des-territoires.gouv.fr/perimetre-des-pays-et-petr'
    } as DefinitionSourceDonnee,

    communes_appartenance_pat: {
        id: 'communes_appartenance_pat',
        nom: "Communes d'appartenance des Projets Alimentaires Territoriaux (PAT)",
        definition: htmlstring`Liste des communes des Projets Alimentaires Territoriaux (PAT)`,
        annees: 'téléchargé le 19/12/2024',
        perimetreGeographique: 'France métropolitaine',
        fournisseur: fournisseursDonnees.francePat,
        url: 'https://www.data.gouv.fr/fr/datasets/pat-projets-alimentaires-territoriaux-description'
    } as DefinitionSourceDonnee,

    communes_appartenance_scot: {
        id: 'communes_appartenance_scot',
        nom: "Communes d'appartenance des Schémas de Cohérence Territoriale (SCoT)",
        definition: htmlstring`Liste des communes des Schémas de Cohérence Territoriale (SCoT)`,
        annees: '1er janvier 2023',
        perimetreGeographique: 'France métropolitaine',
        fournisseur: fournisseursDonnees.federationScot,
        url: 'https://www.fedescot.org/donnees-scot-2023'
    } as DefinitionSourceDonnee,

    geographie_des_communes: {
        id: 'geographie_des_communes',
        nom: 'Géographie des communes',
        definition: htmlstring`Géographie des communes`,
        annees: '2024',
        perimetreGeographique: 'France métropolitaine y compris les DROM',
        fournisseur: fournisseursDonnees.ign,
        url: 'https://geoservices.ign.fr/adminexpress'
    } as DefinitionSourceDonnee,

    geographie_des_epcis: {
        id: 'geographie_des_epcis',
        nom: 'Géographie des EPCI',
        definition: htmlstring`Géographie des EPCI`,
        annees: '2024',
        perimetreGeographique: 'France métropolitaine y compris les DROM',
        fournisseur: fournisseursDonnees.ign,
        url: 'https://geoservices.ign.fr/adminexpress'
    } as DefinitionSourceDonnee,

    geographie_des_departements_et_regions: {
        id: 'geographie_des_departements_et_regions',
        nom: 'Géographie des départements et régions',
        definition: htmlstring`Géographie des départements et régions`,
        annees: '2024',
        perimetreGeographique: 'France métropolitaine y compris les DROM',
        fournisseur: fournisseursDonnees.ign,
        url: 'https://geoservices.ign.fr/adminexpress'
    } as DefinitionSourceDonnee,

    corineLandCover: {
        id: 'corine-land-cover',
        nom: "Occupation des sols de l'inventaire Corine Land Cover",
        definition: htmlstring`Inventaire biophysique de l'occupation des sols produit par interprétation visuelle d’images satellite (CORINE Land Cover)`,
        annees: '2018',
        perimetreGeographique: `Communes au 01/01/2016 - France métropolitaine`,
        fournisseur: fournisseursDonnees.ministereTransitionEcologique,
        url: 'https://www.statistiques.developpement-durable.gouv.fr/corine-land-cover-0'
    } as DefinitionSourceDonnee,

    otex: {
        id: 'otex',
        nom: 'Spécialisation territoriale de la production agricole en 2020',
        definition: htmlstring`Spécialisation territoriale de la production agricole en 2020 (OTEX en 12 postes)`,
        annees: '2020',
        perimetreGeographique: 'Communes au 01/01/2020 - France métropolitaine + DROM',
        fournisseur: fournisseursDonnees.agreste,
        url: 'https://stats.agriculture.gouv.fr/cartostat/#c=indicator&i=otex_2020_1.otefda20&t=A02&view=map15'
    } as DefinitionSourceDonnee,

    rpg: {
        id: 'rpg',
        nom: 'Registre Parcellaire Graphique',
        definition: htmlstring`Surfaces, géométries des parcelles et types de cultures`,
        annees: '2023',
        perimetreGeographique: 'France (métropole et DROM)',
        fournisseur: fournisseursDonnees.ign,
        url: 'https://geoservices.ign.fr/rpg',
        limites: htmlstring`<a href="https://www.geoportail.gouv.fr/actualites/les-usages-du-registre-parcellaire-graphique#!">
            Seules les parcelles appartenant à un exploitant les ayant déclarées dans sa demande de subventions à la PAC (Politique Agricole Communes)
            sont recensées.</a
        >`
    } as DefinitionSourceDonnee,

    sau_ra: {
        id: 'sau_ra',
        nom: 'Surfaces agricoles utiles issues du Recensement Agricole',
        definition: htmlstring`Surface agricole utile de chaque commune`,
        annees: '2020',
        perimetreGeographique: 'Communes au 01/01/2020 - France métropolitaine + DROM',
        fournisseur: fournisseursDonnees.agreste,
        url: 'https://stats.agriculture.gouv.fr/cartostat'
    } as DefinitionSourceDonnee,

    cheptels: {
        id: 'cheptels',
        nom: 'Cheptels',
        definition: htmlstring`Cheptels selon la taille du troupeau par commune.`,
        annees: '1988, 2000 et 2010',
        perimetreGeographique: `Communes, Départements, Régions (nouvelles) au 1e janvier 2011 vraisemblablement - France métropolitaine + DROM`,
        fournisseur: fournisseursDonnees.agreste,
        url: 'https://agreste.agriculture.gouv.fr/agreste-web/disaron/G_1010/detail/',
        limites: htmlstring`Les données sont assez anciennes et parfois sous secret statistique. Celles du recensement agricole 2020 sont progressivement publiées depuis le printemps 2022. Elles seront intégrées dans l'application quand elles seront disponibles.`
    } as DefinitionSourceDonnee,

    population_totale: {
        id: 'population_totale',
        nom: 'Population totale',
        definition: htmlstring`Populations légales`,
        annees: '1er janvier 2017',
        perimetreGeographique: 'Communes au 01/01/2019 - France métropolitaine + DROM hors Mayotte',
        fournisseur: fournisseursDonnees.insee,
        url: 'https://www.insee.fr/fr/statistiques/4265429?sommaire=4265511'
    } as DefinitionSourceDonnee,

    population_totale_historique: {
        id: 'population_totale_historique',
        nom: 'Historique des populations totales',
        definition: htmlstring`Recensement de la population - Base des principaux indicateurs. Résultats sur les naissances, les décès, la population, les
        résidences principales, les résidences secondaires et logements occasionnels, les caractéristiques des personnes des ménages`,
        annees: '1er janvier 1968, 1975, 1982, 1990, 1999, 2010 et 2015',
        perimetreGeographique: 'Communes au 01/01/2017 - France métropolitaine + DROM hors Mayotte',
        fournisseur: fournisseursDonnees.insee,
        url: 'https://www.insee.fr/fr/statistiques/3565661'
    } as DefinitionSourceDonnee,

    actifs_agricoles_permanents: {
        id: 'actifs_agricoles_permanents',
        nom: 'Actifs agricoles permanents',
        definition: htmlstring`Actifs agricoles permanents par commune. Cela inclut les chefs d'exploitations, conjoints et salariés permanents mais pas les
        salariés temporaires à l'échelle française.`,
        annees: 'Recensements de 1970, 1979, 1988, 2000 et 2010',
        perimetreGeographique: `Communes, Départements, Régions (nouvelles) au 1e janvier 2011 vraisemblablement - France métropolitaine + DROM`,
        fournisseur: fournisseursDonnees.agreste,
        url: 'https://agreste.agriculture.gouv.fr/agreste-web/disaron/G_2005/detail/',
        limites: htmlstring`La donnée sur le nombre d’actifs agricoles inclut les chefs d&#39;exploitations, conjoints et salariés permanents mais pas les
            salariés temporaires à l&#39;échelle française (“En 2018, en France métropolitaine, le travail agricole, mesuré en unité de travail annuel
            (UTA) , est assuré pour 57,3 % par les dirigeants actifs, pour 26,4 % par les autres actifs permanents (non salariés ou salariés) et pour
            16,3 % par les salariés saisonniers ou prestataires extérieurs à l’exploitation. La diminution de l’emploi de l’ensemble des actifs
            agricoles se poursuit (–1,0 % en moyenne annuelle entre 2010 et 2018)&quot; selon l’<a
                href="https://www.insee.fr/fr/statistiques/4277860?sommaire=4318291"
                >INSEE</a
            >). De plus, les données sont assez anciennes et parfois sous secret statistique. Celles du recensement agricole 2020 sont progressivement publiées depuis le printemps 2022. Elles seront intégrées dans l'application quand elles seront disponibles.`
    } as DefinitionSourceDonnee,

    exploitations_selon_age_chef_exploitation: {
        id: 'exploitations_selon_age_chef_exploitation',
        nom: "Exploitations selon l'âge du chef d'exploitation",
        definition: htmlstring`Nombre et superficie des exploitations par classes d'âge du chef d'exploitation (e.g. moins de 40 ans)`,
        annees: 'Recensements de 1970, 1979, 1988, 2000 et 2010',
        perimetreGeographique: 'Communes, Départements, Régions (nouvelles) au 1e janvier 2011 vraisemblablement - France métropolitaine + DROM',
        fournisseur: fournisseursDonnees.agreste,
        url: 'https://agreste.agriculture.gouv.fr/agreste-web/disaron/G_2004/detail/',
        limites: htmlstring`Les données sont assez anciennes et parfois sous secret statistique. Elles devraient être mise à jour en 2022 (mise à
        disposition au printemps 2022 des données du recensement agricole réalisé en 2020).`
    } as DefinitionSourceDonnee,

    exploitations_selon_taille_exploitation: {
        id: 'exploitations_selon_taille_exploitation',
        nom: 'Exploitations selon la classe de superficie',
        definition: htmlstring`Nombre et superficie des exploitations par classes de superficie (e.g. de 20 à 50 ha)`,
        annees: 'Recensements de 1970, 1979, 1988, 2000 et 2010',
        perimetreGeographique: 'Communes, Départements, Régions (nouvelles) au 1e janvier 2011 vraisemblablement - France métropolitaine + DROM',
        fournisseur: fournisseursDonnees.agreste,
        url: 'https://agreste.agriculture.gouv.fr/agreste-web/disaron/G_2003/detail/',
        limites: htmlstring` Les données sont assez anciennes et parfois sous secret statistique. Elles devraient être mise à jour en 2022 (mise à
        disposition au printemps 2022 des données du recensement agricole réalisé en 2020).`
    } as DefinitionSourceDonnee,

    artificialisation_sols: {
        id: 'artificialisation_sols',
        nom: 'Artificialisation des sols',
        definition: htmlstring`Surfaces artificialisées pour l’habitat, l’activité, le mix, et autre.`,
        annees: '2009 à 2021',
        perimetreGeographique: 'Communes au 01/01/2022 - France métropolitaine + DROM hors Mayotte',
        fournisseur: fournisseursDonnees.cerema,
        url: 'https://artificialisation.developpement-durable.gouv.fr',
        limites: htmlstring`La méthodologie de calcul est expliquée
            <a href="https://artificialisation.developpement-durable.gouv.fr/methodologie-production-des-donnees">ici</a>. Des limites y
            sont présentées telles que le fait que les fichiers fonciers ne traitent que les parcelles cadastrées (les routes ne sont ainsi en
            principe pas prises en compte bien qu’en pratique, certains espaces théoriquement dans le domaine public static readonly, y compris de
            grande ampleur (périphériques, routes nationales, certaines autoroutes,etc.) sont encore cadastrés). De même l’impact des bâtiments public
            static readonlys, des terrains militaires et des golfs est mal pris en compte.`
    } as DefinitionSourceDonnee,

    evolution_menages_emplois: {
        id: 'evolution_menages_emplois',
        nom: "Évolution du nombre d'emplois et de ménages",
        definition: htmlstring`Évolution du nombre d'emplois et de ménages.`,
        annees: '2009 à 2021',
        perimetreGeographique: 'Communes au 01/01/2022 - France métropolitaine + DROM hors Mayotte',
        fournisseur: fournisseursDonnees.cerema,
        url: 'https://artificialisation.developpement-durable.gouv.fr'
    } as DefinitionSourceDonnee,

    logements_totaux: {
        id: 'logements_totaux',
        nom: 'Nombre de logements totaux',
        definition: htmlstring`<a href="https://www.insee.fr/fr/metadonnees/definition/c1702">Nombre de logements totaux</a>`,
        annees: '2013 et 2018',
        perimetreGeographique: 'Communes au 01/01/2021 - France métropolitaine + DROM hors Mayotte',
        fournisseur: fournisseursDonnees.observatoireDesTerritoires,
        url: 'https://www.observatoire-des-territoires.gouv.fr/nombre-de-logements'
    } as DefinitionSourceDonnee,

    logements_vacants: {
        id: 'logements_vacants',
        nom: 'Nombre de logements vacants',
        definition: htmlstring`<a href="https://www.insee.fr/fr/metadonnees/definition/c1059">Nombre de logements vacants</a>`,
        annees: '2013 et 2018',
        perimetreGeographique: 'Communes au 01/01/2021 - France métropolitaine + DROM hors Mayotte',
        fournisseur: fournisseursDonnees.observatoireDesTerritoires,
        url: 'https://www.observatoire-des-territoires.gouv.fr/nombre-de-logements-vacants'
    } as DefinitionSourceDonnee,

    prelevements_irrigation: {
        id: 'prelevements_irrigation',
        nom: `Prélèvements en eau pour l'irrigation`,
        definition: htmlstring`Cette base de données porte sur les volumes annuels directement prélevés sur la ressource en eau, déclinés par localisation
            et catégorie d’usage d’eau (irrigation, eau potable…). Voir la
            <a href="https://bnpe.eaufrance.fr/presentation">présentation de la donnée sur le site de la BNPE</a>.`,
        annees: '2012 à 2020',
        perimetreGeographique: 'France métropolitaine et départements d’outre-mer',
        fournisseur: fournisseursDonnees.eauFrance,
        url: 'https://bnpe.eaufrance.fr/',
        limites: htmlstring` Les données ne sont pas complètes car :
            <ul>
                <li>
                    elles sont issues uniquement de la gestion des redevances par les agences et offices de l’eau, ce qui ne couvre pas l’ensemble des
                    producteurs de données ;
                </li>
                <li>seuls les volumes supérieurs à 10 000 m³ sont comptabilisés ;</li>
                <li>certains usages sont exonérés de redevance, notamment la lutte antigel des cultures pérennes.</li>
            </ul>
            Aussi, il n'est pas possible d’identifier finement les volumes d’eau utilisés par commune : la mesure du volume d’eau prélevé se fait au
            niveau d’un ouvrage, et ni le détail des volumes prélevés par points de prélèvement n’est fourni (un ouvrage regroupe plusieurs points de
            prélèvements qui sont potentiellement sur des communes différentes), ni la localisation des parcelles effectivement irriguées avec l’eau
            prélevée. Pour certains territoires disposant de vastes réseaux de canaux d'irrigation, la distance entre le point de prélèvement et la
            parcelle effectivement irriguée peut même couvrir plusieurs dizaines de kilomètres. C'est le cas par exemple dans les Pyrénées Orientales
            (canaux gravitaires permettant l'irrigation de la plaine du Roussillon), ou dans les Bouches-du-Rhône. En conséquence les données
            d’irrigation sont considérées comme fiables uniquement pour les niveaux Pays, Région, Déparement, et EPCI (fiabilité relative pour ce
            dernier niveau, la présence de canaux d'irrigation pouvant fausser les résultats).`
    } as DefinitionSourceDonnee,

    pratiques_irrigation: {
        id: 'pratiques_irrigation',
        nom: `Pratiques d'irrigation`,
        definition: htmlstring` Cette base de données porte sur les surfaces irriguées selon 15 types de cultures et 16 OTEX.`,
        annees: '2010',
        perimetreGeographique: 'Départements, régions - France entière',
        fournisseur: fournisseursDonnees.agreste,
        url: 'https://agreste.agriculture.gouv.fr/agreste-web/disaron/G_0051/detail/'
    } as DefinitionSourceDonnee,

    surfaces_riz: {
        id: 'surfaces_riz',
        nom: `Surfaces de riz`,
        definition: htmlstring` Ces données décrivent les surfaces de cultures de riz par département.`,
        annees: '2021',
        perimetreGeographique: 'Départements',
        fournisseur: fournisseursDonnees.franceagrimer,
        url: 'https://www.franceagrimer.fr/fam/content/download/71497/document/Bilan_riz_2021_2022.pdf?version=1'
    } as DefinitionSourceDonnee,

    donnees_secheresses: {
        id: 'donnees_secheresses',
        nom: `Arrêtés sécheresses`,
        definition: htmlstring` Cette base de données porte sur les arrêtés sécheresse qui sont pris par les préfets pour faire face à une insuffisance de
        la ressource en eau. Ces arrêtés entraînent des mesures exceptionnelles de limitation ou de suspension des usages de l'eau en application de
        l'article L.211-3 II-1° du code de l'environnement. La base contient la description des zones d'alertes (contours géographique) ainsi que la
        liste des arrêtés sècheresse.`,
        annees: '2012 à 2022',
        perimetreGeographique: 'France métropolitaine et départements d’outre-mer',
        fournisseur: fournisseursDonnees.propluvia,
        url: 'https://www.propluvia.developpement-durable.gouv.fr/propluviapublic/'
    } as DefinitionSourceDonnee,

    quantites_substances_actives: {
        id: 'qsa',
        nom: 'Quantités de Substances Actives achetées',
        definition: htmlstring`La quantité de substance active (QSA) achetée, pour une substance donnée, sur une année, par code postal (ou département) de
            l'acheteur. Voir la definition détaillée dans la
            <a href="https://agriculture.gouv.fr/telecharger/106547?token=90fe2c9e64650e3f9164076be113b1307327a103934e774ea1d818adacaf12ac"
                >méthodologie de calcul du <abbr title="NOmbre de Doses Unités">NODU</abbr></a
            >.`,
        annees: '2015 à 2020',
        perimetreGeographique: 'Codes postaux ou départements France métropolitaine + DROM',
        fournisseur: fournisseursDonnees.officeFrancaisBiodiversite,
        url: 'https://www.data.gouv.fr/fr/datasets/achats-de-pesticides-par-code-postal/',
        limites: htmlstring` La
            <abbr title="Banque Nationale des Ventes de produits phytopharmaceutiques par les Distributeurs agréés">BNVD</abbr>
            renseigne sur les achats des produits phytosanitaires pour un code postal acheteur (ou département) et une annee donnée. Les données par
            code postal acheteur ne reflètent ni le périmètre géographique réel de l'utilisation (produits utilisés sur un territoire différent que le
            code postal d’achat renseigné, qui est lié à la domiciliation de l’acheteur ; exemple des centrales d'achat, des exploitations étendues
            sur plusieurs communes ou EPCIs), ni l'année effective d'utilisation (achat puis stockage). Les données incluent également les achats de
            produits à usage non agricole (services de voiries des mairies – avant la loi Labbé – ou de la SNCF) qu'il conviendrait d'exclure (pour
            l'instant la qualité des données ne permet pas de facilement isoler ces usages).`
    } as DefinitionSourceDonnee,

    doses_unites_substances_actives: {
        id: 'du',
        nom: 'Doses Unités des substances actives',
        definition: htmlstring` La dose unité (DU) représente la dose homologuée pour une substance donnée. Voir la definition: détaillée dans la
            <a href="https://agriculture.gouv.fr/telecharger/106547?token=90fe2c9e64650e3f9164076be113b1307327a103934e774ea1d818adacaf12ac"
                >méthodologie de calcul du <abbr title="NOmbre de Doses Unités">NODU</abbr></a
            >.`,
        annees: '2017 et 2019',
        perimetreGeographique: '',
        fournisseur: fournisseursDonnees.ministereAgriculture,
        url: 'https://info.agriculture.gouv.fr',
        description: htmlstring` <ul>
            <li>
                <a href="https://info.agriculture.gouv.fr/gedei/site/bo-agri/document_administratif-fbafa36b-8218-40fc-a461-1b47df670dbe"
                    >Arrêté du 27/04/2017</a
                >
                définissant la méthodologie de calcul et la valeur des doses unité de référence des substances actives phytopharmaceutiques
            </li>
            <li>
                <a href="https://info.agriculture.gouv.fr/gedei/site/bo-agri/document_administratif-6e692406-4700-4623-81f9-77c929399b61"
                    >Arrêté du 18/12/2019</a
                >
                portant modification de l'arrêté 27 avril 2017 définissant la méthodologie de calcul et la valeur des doses unités de référence des
                substances actives phytopharmaceutiques. Contient une mise à jour des valeurs de
                <abbr title="Dose Unité">DU</abbr>
                par rapport à l’arrêté de 2017.
            </li>
        </ul>`,
        limites: htmlstring` Même en combinant les années 2017 et 2019, des Doses Unités sont manquantes pour certaines substances actives. `
    } as DefinitionSourceDonnee,

    hvn: {
        id: 'hvn',
        nom: 'Indicateur HVN',
        definition: htmlstring`Indicateur Haute Valeur Naturelle de SOLAGRO`,
        annees: '2017',
        perimetreGeographique: 'Communes au 1e janvier 2018 - France métropolitaine',
        fournisseur: fournisseursDonnees.solagro,
        url: 'https://solagro.org/nos-domaines-d-intervention/agroecologie/haute-valeur-naturelle'
    } as DefinitionSourceDonnee,

    surfaces_agricoles_bio: {
        id: 'sau_bio',
        nom: 'Surfaces biologiques',
        definition: htmlstring`Surfaces agricoles labellisées AB ou en cours de conversion`,
        annees: '2023',
        perimetreGeographique: 'Communes au 1e janvier 2024 - France métropolitaine',
        fournisseur: fournisseursDonnees.agenceBio,
        url: 'https://www.agencebio.org/vos-outils/les-chiffres-cles/',
        limites: htmlstring`Pour des raisons de confidentialité, les données ne sont pas disponibles pour les territoires pour lesquels il y a moins de 3
        exploitants en bio.`
    } as DefinitionSourceDonnee,

    consommation_sau: {
        id: 'consommation_sau',
        nom: 'Surfaces agricoles nécessaires pour couvrir la consommation de la population',
        definition: htmlstring`Surfaces agricoles nécessaires pour couvrir la consommation de la population`,
        annees: '2019',
        perimetreGeographique: 'Communes, EPCIs, Départements, Régions, France métropolitaine au 1e janvier 2019',
        fournisseur: fournisseursDonnees.parcel,
        url: 'https://parcel-app.org/',
        limites: htmlstring`Certains produits ne sont pas considérés :
        <ul>
        <li>Les produits qui ne possèdent pas de potentiel de production en France métropolitaine (café, cacao, fruits exotiques, etc.) ;</li>
        <li>L'alimentation animale importée (e.g. tourteaux de soja) ;</li>
        <li>Les boissons (bières, vins etc.) ;</li>
        <li>Les produits de la mer (difficulté de donner un indicateur d’empreinte spatiale) ;</li>
        </ul>
        Ils représentent environ 80% de l'alimentation totale en volume.`
    } as DefinitionSourceDonnee,

    base_permanente_equipements: {
        id: 'bpe',
        nom: 'Base permanente des équipements',
        definition: htmlstring`La base permanente des équipements (BPE) est une source statistique qui fournit le niveau d'équipements et de services rendus
        à la population sur un territoire.`,
        annees: '2020',
        perimetreGeographique: 'France entière (base géolocalisée)',
        fournisseur: fournisseursDonnees.insee,
        url: 'https://www.insee.fr/fr/statistiques/3568638?sommaire=3568656'
    } as DefinitionSourceDonnee,

    commerces_osm: {
        id: 'commerces_osm',
        nom: 'Base des commerces OSM',
        definition: htmlstring`Base de données ouverte et collaborative OpenStreetMap des commerces`,
        annees: '2021',
        perimetreGeographique: 'France entière (base géolocalisée)',
        fournisseur: fournisseursDonnees.openStreetMap,
        url: 'https://babel.opendatasoft.com/explore/dataset/osm-shop-fr/information/?disjunctive.type&disjunctive.region&disjunctive.departement&disjunctive.commune'
    } as DefinitionSourceDonnee,

    carroyage_insee: {
        id: 'carroyage_insee',
        nom: 'Données carroyées ',
        definition: htmlstring`Informations socio-économiques sur l'ensemble de la France discrétisée en carreaux de 200 mètres de côté.`,
        annees: '2015',
        perimetreGeographique: 'Carroyage INSEE 200m',
        fournisseur: fournisseursDonnees.insee,
        url: 'https://www.insee.fr/fr/statistiques/4176290?sommaire=4176305'
    } as DefinitionSourceDonnee,

    taux_pauvrete: {
        id: 'taux_pauvrete',
        nom: 'Taux de pauvreté (seuil à 60% du revenu médian)',
        definition: htmlstring`Le taux de pauvreté correspond à la proportion d'individus appartenant à des ménages dont le niveau de vie (après transferts,
        impôts et prestations sociales) est inférieur au seuil de 60 % du niveau de vie médian de l'ensemble de la population.`,
        annees: '2019',
        perimetreGeographique: 'Communes, EPCIs, Départements, Régions, France métropolitaine au 1e janvier 2020',
        fournisseur: fournisseursDonnees.insee,
        url: 'https://www.insee.fr/fr/statistiques/6036907'
    } as DefinitionSourceDonnee,

    climagri: {
        id: 'climagri',
        nom: 'Climagri',
        definition: htmlstring`Méthodologie de diagnostic énergie/gaz à effet de serre au service d'une démarche de territoire`,
        perimetreGeographique: 'France',
        fournisseur: fournisseursDonnees.ademe,
        url: 'https://librairie.ademe.fr/produire-autrement/3486-climagri-un-diagnostic-energie-gaz-a-effet-de-serre-au-service-d-une-demarche-de-territoire.html'
    } as DefinitionSourceDonnee,

    etude_serres: {
        id: 'surfaces_serres',
        nom: 'Étude CTIFL',
        definition: htmlstring`Surfaces de serres chauffées pour la culture des tomates et des concombres issues du rapport Évolution du parc de serres chauffées en tomate et concombre - Résultats de l'enquête CTIFL 2016`,
        annees: '2016',
        perimetreGeographique: 'départements, France',
        fournisseur: fournisseursDonnees.ctifl,
        url: 'https://www.ctifl.fr/evolution-du-parc-de-serres-chauffees-en-tomate-et-concombre-resultats-de-l-enquete-ctifl-2016-infos-ctifl-333'
    } as DefinitionSourceDonnee,

    ventes_engrais_azotes_synthese: {
        id: 'ventes_engrais_azotes_synthese',
        nom: "Ventes d'engrais azotés de synthèse",
        definition: htmlstring`Ventes d'engrais azotés de synthèse`,
        annees: 'Campagnes 2017/2018, 2018/2019 et 2019/2020',
        perimetreGeographique: `Régions de France métropolitaine`,
        fournisseur: fournisseursDonnees.unifa,
        url: 'https://www.unifa.fr/statistiques-du-secteur/les-statistiques-de-campagne-retrouvez-lhistorique-des-campagnes-de'
    } as DefinitionSourceDonnee,

    rica: {
        id: 'rica',
        nom: 'Réseau d’Information Comptable Agricole (RICA)',
        definition: htmlstring`Données de comptabilité agricole.`,
        annees: 'Toutes les années depuis 1968',
        perimetreGeographique: `France métropolitaine`,
        fournisseur: fournisseursDonnees.agreste,
        url: 'https://agreste.agriculture.gouv.fr/agreste-web/methodon/S-RICA/methodon/'
    } as DefinitionSourceDonnee
};

export const SOURCES_DONNEES_RETRAITEES = {
    cheptels_crater: {
        id: 'cheptels-crater',
        nom: 'Cheptels',
        annees: SOURCES_DONNEES.cheptels.annees,
        source: SOURCES_DONNEES.cheptels
    } as DefinitionSourceDonneeRetraitee,

    surfaces_agricoles_crater: {
        id: 'sau-crater',
        nom: 'Recensement Parcelaire Graphique',
        annees: SOURCES_DONNEES.rpg.annees,
        source: SOURCES_DONNEES.rpg
    } as DefinitionSourceDonneeRetraitee
};

export const RAPPORTS_ETUDES = {
    greniersAbondance: {
        id: 'lga',
        nom: 'Calculs internes',
        auteur: "Greniers d'Abondance"
    } as DefinitionRapportEtude,

    utopies: {
        id: 'utopies',
        nom: 'Rapport Autonomie alimentaire des villes',
        auteur: 'Utopies',
        annee: 2017,
        url: 'https://utopies.com/wp-content/uploads/2019/12/autonomie-alimentaire-des-villes-notedeposition12.pdf'
    } as DefinitionRapportEtude,

    aide_alimentaire: {
        id: 'aide-alimentaire',
        nom: 'La lutte contre la précarité alimentaire',
        auteur: 'Le Morvan F. et Wanecq T.',
        annee: 2019,
        url: 'https://www.vie-publique.fr/files/rapport/pdf/272871.pdf'
    } as DefinitionRapportEtude,

    obesite: {
        id: 'obesite',
        nom: 'Obésité en France',
        auteur: 'Wikipédia, basé entre autres sur INSEE et Odoxa',
        annee: '1981 à 2020',
        url: 'https://fr.wikipedia.org/wiki/Ob%C3%A9sit%C3%A9_en_France'
    } as DefinitionRapportEtude
};
