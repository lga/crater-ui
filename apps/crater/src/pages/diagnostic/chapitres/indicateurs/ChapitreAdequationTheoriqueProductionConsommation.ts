import '../../charts/ChartTreemapDoubleCultures.js';
import '@lga/design-system/build/composants/Tableau.js';
import '../composants/ChapitreDomaine.js';
import '../../../commun/PageSablierCrater.js';
import '../../../diagnostic/chapitres/composants/Figure.js';

import { formaterNombreEnEntierHtml } from '@lga/commun/build/composants/outils-composants';
import type { OptionsTableau } from '@lga/design-system/build/composants/Tableau';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { IDS_DOMAINES, INDICATEURS } from '@lga/indicateurs';
import { GroupeCulture } from '@lga/indicateurs';
import { SA } from '@lga/indicateurs';
import { html, LitElement, type TemplateResult } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { EtatRapport, type Rapport } from '../../../../modeles/diagnostics';
import { construireLienVersSourcesIndicateur } from '../../../commun/liens-utils.js';
import type { OptionsChartTreemapDoubleCultures } from '../../charts/ChartTreemapDoubleCultures';
import type { DonneesPageDiagnostic } from '../../PageDiagnostic.js';
import { creerDonneesTreemapConsommation, creerDonneesTreemapProduction } from '../chapitres-outils.js';
import type { OptionsDomaineDetaille } from '../composants/ChapitreDomaine';

declare global {
    interface HTMLElementTagNameMap {
        'c-chapitre-adequation-theorique-production-consommation': ChapitreAdequationTheoriqueProductionConsommation;
    }
}

@customElement('c-chapitre-adequation-theorique-production-consommation')
export class ChapitreAdequationTheoriqueProductionConsommation extends LitElement {
    static styles = [STYLES_DESIGN_SYSTEM];

    @property({ attribute: false })
    donneesPage?: DonneesPageDiagnostic;

    @property({ attribute: false })
    rapport?: Rapport;

    render() {
        if (this.rapport?.getEtat() === EtatRapport.PRET) {
            return html`<c-chapitre-domaine
                .options=${{
                    domaine: SA.getDomaine(IDS_DOMAINES.adequationProductionConsommation),
                    territoire: this.rapport.getTerritoireActif(),
                    estPagePourTef: this.donneesPage?.estPagePourTef
                } as OptionsDomaineDetaille}
                contenuAdhoc
            >
                <div slot="contenu">
                    <p>
                        Les estimations brutes de production et consommation sont présentées ci-dessous toutes cultures confondues, et sont exprimées
                        toutes deux en hectares de surface agricole :
                    </p>

                    <c-figure .titre="${construireLienVersSourcesIndicateur(INDICATEURS.adequationTheoriqueBruteProductionConsommation)}">
                        <c-tableau .options=${this.calculerOptionsTableauProductionBesoinsGlobal(this.rapport)} slot="chart-figure"></c-tableau>
                    </c-figure>
                    <p>
                        Analyser le ratio production / consommation sans prendre en compte les besoins par grande catégorie de culture peut masquer
                        des déséquilibres importants (manque ou production excessive pour certains secteurs). Le diagramme suivant illustre l'écart
                        entre production et consommation pour le territoire avec le détail par groupes de cultures :
                    </p>

                    <c-figure .titre="${construireLienVersSourcesIndicateur(INDICATEURS.adequationTheoriqueBruteProductionConsommation)}">
                        <c-chart-treemap-double-cultures
                            .options=${this.calculerOptionsChartTreemapProductionBesoins(this.rapport)}
                            slot="chart-figure"
                        ></c-chart-treemap-double-cultures>
                    </c-figure>

                    <p>
                        L'adéquation théorique globale représente de façon plus fine l'adéquation théorique entre production et consommation
                        puisqu'elle tient compte de la diversité des cultures nécessaire à une alimentation variée. Elle est calculée en faisant la
                        moyenne des adéquations théoriques de chaque groupe de culture plafonnées à 100% (pour ne pas tenir compte des surproductions)
                        et pondérées par leur part dans le total des consommations.
                    </p>
                    <c-figure .titre="${construireLienVersSourcesIndicateur(INDICATEURS.adequationTheoriqueMoyennePondereeProductionConsommation)}">
                        <c-tableau slot="chart-figure" .options=${this.obtenirTableauProductionBesoinsDetail(this.rapport)}></c-tableau>
                    </c-figure>
                </div>
            </c-chapitre-domaine>`;
        } else {
            return html`<c-page-sablier-crater></c-page-sablier-crater>`;
        }
    }

    private calculerOptionsTableauProductionBesoinsGlobal(rapport: Rapport): OptionsTableau {
        return {
            entetes: [html``, html`Production`, html`Consommation`, html`Ratio<br />production/<br />consommation`],
            contenuHtml: rapport
                .getDiagnostics()
                .map((d) => [
                    html`${d!.territoire.nom}`,
                    html`${formaterNombreEnEntierHtml(d!.surfaceAgricoleUtile.sauProductiveHa)} ha`,
                    html`${formaterNombreEnEntierHtml(d!.consommation.indicateurConsommation?.consommationHa)} ha`,
                    html`${formaterNombreEnEntierHtml(d!.production.tauxAdequationBrutPourcent)} %`
                ])!
        };
    }

    private calculerOptionsChartTreemapProductionBesoins(rapport: Rapport): OptionsChartTreemapDoubleCultures {
        const donneesTreemapProduction = creerDonneesTreemapProduction(rapport.getDiagnosticActif()!.surfaceAgricoleUtile.sauParGroupeCultureHorsSNC);
        const donneesTreemapConsommation = creerDonneesTreemapConsommation(
            rapport.getDiagnosticActif()!.consommation.indicateurConsommation!.consommationsParGroupeCultures
        );

        return {
            sauTotaleCulturesGaucheHa: rapport.getDiagnosticActif()!.surfaceAgricoleUtile.sauProductiveHa,
            sauTotaleCulturesDroiteHa: rapport.getDiagnosticActif()!.consommation.indicateurConsommation!.consommationHa,
            titreCulturesGauche: 'Production',
            titreCulturesDroite: 'Consommation',
            donneesTreemapCulturesGauche: donneesTreemapProduction,
            donneesTreemapCulturesDroite: donneesTreemapConsommation,
            itemsLegende: GroupeCulture.groupesHorsSNC.map((g) => {
                return { libelle: `${g.nom} (${g.nomCourt})`, couleur: g.couleur, motif: '' };
            })
        };
    }

    private obtenirTableauProductionBesoinsDetail(rapport: Rapport): OptionsTableau {
        const ligneEntete = [
            html``,
            html`<span>Consommation</span>`,
            html`<span>Part dans le régime alimentaire</span>`,
            html`<span>Adéquation théorique</span>`
        ];

        const formaterTauxAdequationBrut = (taux: number | null): TemplateResult => {
            if (taux === null) {
                return html`${formaterNombreEnEntierHtml(taux)}%`;
            } else if (taux <= 100) {
                return html`${formaterNombreEnEntierHtml(taux)}%`;
            } else {
                return html`>${formaterNombreEnEntierHtml(100)}% (<small>${formaterNombreEnEntierHtml(taux)} %</small>)`;
            }
        };

        const contenuTableau = rapport
            .getDiagnosticActif()!
            .consommation.indicateurConsommation!.consommationsParGroupeCultures.map((b) => [
                html`${b.nomGroupeCulture}`,
                html`${formaterNombreEnEntierHtml(b.consommationHa)} ha`,
                html`${formaterNombreEnEntierHtml(b.partDansBesoinsTotaux)} %`,
                html`${formaterTauxAdequationBrut(b.tauxAdequationBrutPourcent)}`
            ])!;

        const ligneFin = [
            html`Adéquation théorique globale`,
            html``,
            html``,
            html`${formaterNombreEnEntierHtml(rapport.getDiagnosticActif()?.production.tauxAdequationMoyenPonderePourcent)} %`
        ];
        return {
            entetes: ligneEntete,
            contenuHtml: contenuTableau,
            ligneFin: ligneFin
        };
    }
}
