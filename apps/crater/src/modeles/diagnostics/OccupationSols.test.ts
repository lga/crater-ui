import { describe, expect, it } from 'vitest';

import { OccupationSols } from './OccupationSols.js';

describe('Tests de la classe OccupationSols', () => {
    it('Valider le calcul des champs dynamiques', () => {
        const occupationSols = new OccupationSols(10, 3, 2, 5, 1);

        expect(occupationSols.partSuperficieAgricoleClcPourcent).toEqual(20);
    });

    it('Valider le calcul des champs dynamiques avec objet rempli de zero', () => {
        const occupationSols = new OccupationSols(0, 0, 0, 0, 0);
        expect(occupationSols.partSuperficieAgricoleClcPourcent).to.be.null;
    });
});
