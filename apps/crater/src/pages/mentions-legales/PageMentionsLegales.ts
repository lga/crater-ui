import '../commun/template/TemplatePageAccueil.js';
import '@lga/design-system/build/composants/Lien.js';
import '@lga/commun/build/matomo/GestionConsentementMatomo.js';

import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { css, html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { HASH_PROJET_LICENCE, PAGES_PRINCIPALES } from '../../configuration/pages/declaration-pages';
import { STYLES_CRATER } from '../commun/pages-styles';

declare global {
    interface HTMLElementTagNameMap {
        'c-page-mentions-legales': PageMentionsLegales;
    }
}

@customElement('c-page-mentions-legales')
export class PageMentionsLegales extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        STYLES_CRATER,
        css`
            main {
                padding-right: max((100vw - var(--largeur-maximum-contenu)) / 2, var(--dsem));
                padding-left: max((100vw - var(--largeur-maximum-contenu)) / 2, var(--dsem));
            }

            h1 {
                display: flex;
                flex-direction: column;
                align-items: center;
                gap: calc(var(--dsem) * 2);
                padding-bottom: calc(4 * var(--dsem));
            }

            h1 > svg {
                fill: var(--couleur-primaire);
            }
        `
    ];

    @property()
    idElementCibleScroll = '';

    render() {
        return html`
            <c-template-page-accueil idElementCibleScroll=${this.idElementCibleScroll}>
                <main slot="contenu">
                    <h1>Mentions légales</h1>

                    <h2>Généralités</h2>

                    <p>
                        Les informations décrites dans les paragraphes suivants concernent toutes les pages du site
                        <a href="https://crater.resiliencealimentaire.org">https://crater.resiliencealimentaire.org</a>.
                    </p>
                    <p>
                        Les informations contenues dans ce site sont soumises à la loi française et sont fournies sans garanties d’aucune sorte. Elles
                        sont sujettes à modification sans préavis.
                    </p>

                    <h2>Présentation du site</h2>

                    <p>
                        En vertu de l’article 6 de la loi n° 2004-575 du 21 juin 2004 pour la confiance dans l’économie numérique, il est précisé aux
                        utilisateurs du site
                        <a href="https://crater.resiliencealimentaire.org">https://crater.resiliencealimentaire.org</a> l’identité des différents
                        intervenants dans le cadre de sa réalisation et de son suivi.
                    </p>
                    <ul>
                        <li>
                            <strong>Propriétaire :</strong> Le site
                            <a href="https://crater.resiliencealimentaire.org">https://crater.resiliencealimentaire.org</a> est la propriété de
                            L'association Les Greniers d'Abondance, 4 rue Edouard Pailleron, 73000 Jabob-Bellecombette
                        </li>
                        <br />
                        <li>
                            <strong>Responsable technique : </strong> Association Les Greniers d'Abondance : 4 rue Edouard Pailleron, 73000
                            Jabob-Bellecombette
                        </li>
                        <br />
                        <li>
                            <strong>Développeurs web :</strong> Association Les Greniers d'Abondance : 4 rue Edouard Pailleron, 73000
                            Jabob-Bellecombette
                        </li>
                        <br />
                        <li>
                            <strong>Hébergeur :</strong> O2switch, 222-224 Boulevard Gustave Flaubert 63000 Clermont-Ferrand, Capital de 100000 euros,
                            Siret : 510 909 80700024.
                        </li>
                    </ul>

                    <h2>Conditions générales d’utilisation du site</h2>

                    <p>
                        L’utilisation du site <a href="https://crater.resiliencealimentaire.org">https://crater.resiliencealimentaire.org</a> implique
                        l’acceptation des conditions générales d’utilisation. Ces conditions sont susceptibles d’être modifiées ou complétées à tout
                        moment.<br /><br />
                        Ce site est gratuit et disponible pour tous. Il est normalement ouvert à tout moment aux utilisateurs. Une interruption pour
                        raison de maintenance technique peut être toutefois décidée.<br /><br />
                        Le site <a href="https://crater.resiliencealimentaire.org">https://crater.resiliencealimentaire.org</a> sera mis à jour
                        régulièrement.
                    </p>

                    <h2>Propriété intellectuelle et contrefaçons</h2>

                    <p>
                        De manière générale, les contenus (textes, images, identité visuelle...), les données et le code sont la propriété de
                        l’association Les Greniers d’Abondance et sont protégés à ce titre par les dispositions du Code de la propriété
                        intellectuelle. Tout internaute s’engage à ne les utiliser que dans le strict cadre offert par le Site.
                    </p>
                    <p>
                        La plupart de ces éléments sont publiés sous licence open source. Les conditions d'utilisation applicables sont détaillées
                        <c-lien href="${PAGES_PRINCIPALES.projet.getUrl({ idElementCibleScroll: HASH_PROJET_LICENCE })}">dans cette page</c-lien>.
                    </p>

                    <h2>Données personnelles</h2>
                    <h3>Informations personnelles collectées</h3>
                    <p>
                        L’internaute peut être amené à fournir certaines données personnelles en répondant aux formulaires qui lui sont proposés sur
                        le Site. La saisie de ces données est nécessaire au traitement de la demande de l’internaute, par l’association Les Greniers
                        d’Abondance.
                    </p>
                    <p>
                        L’association Les Greniers d’Abondance s’engage à respecter les dispositions de la loi n°78-17 du 6 janvier 1978 relative à
                        l’informatique, aux fichiers et aux libertés modifiées et au Règlement (UE) 2016/679 du Parlement européen et du Conseil du 27
                        avril 2016 dit « RGPD » et prendre toute précaution nécessaire pour préserver la sécurité des informations nominatives
                        confiées.
                    </p>

                    <h3>Analyse de trafic et cookies</h3>
                    <p>
                        En vue d’adapter le site aux demandes de nos visiteurs, nous analysons le trafic avec Matomo. Matomo génère un cookie avec un
                        identifiant unique, dont la durée de conservation est limitée à 13 mois. Les données recueillies (adresse IP, User-Agent…)
                        sont anonymisées et conservées pour une durée de 25 mois. Elles ne sont pas cédées à des tiers ni utilisées à d’autres fins.
                    </p>
                    <c-gestion-consentement-matomo></c-gestion-consentement-matomo>
                </main>
            </c-template-page-accueil>
        `;
    }
}
