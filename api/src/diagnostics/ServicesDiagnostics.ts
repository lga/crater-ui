import { creerDiagnosticApiVide } from '@lga/specification-api/build/outils/modelesDiagnosticsUtils';
import type { BoundingBoxApi, CodeCategorieTerritoireApi, DiagnosticApi } from '@lga/specification-api/build/specification/api';

import { estIntersectionVide, StockageMemoireIndexParId } from '../communs/servicesUtils';

export class ServicesDiagnostics {
    private stockageDiagnostics = new StockageMemoireIndexParId<DiagnosticApi>();

    ajouterDiagnostic(diagnostic: DiagnosticApi): void {
        this.stockageDiagnostics.save(diagnostic.idTerritoire, diagnostic);
    }

    rechercherOuCreerDiagnostic(idTerritoire: string, nomTerritoire: string, categorieTerritoire: CodeCategorieTerritoireApi): DiagnosticApi {
        let diagnostic = this.rechercher(idTerritoire);
        if (!diagnostic) {
            diagnostic = creerDiagnosticApiVide(idTerritoire, nomTerritoire, categorieTerritoire);
            this.ajouterDiagnostic(diagnostic);
        }
        return diagnostic;
    }

    rechercher(idTerritoire: string): DiagnosticApi | undefined {
        return this.stockageDiagnostics.findById(idTerritoire);
    }

    rechercherTous(): DiagnosticApi[] {
        return this.stockageDiagnostics.findAll();
    }

    rechercherTousParCategorie(codeCategorieTerritoire: CodeCategorieTerritoireApi): DiagnosticApi[] {
        return this.stockageDiagnostics.findAll().filter((d) => d.categorieTerritoire === codeCategorieTerritoire);
    }

    rechercherCommunesParDepartements(filtreIdsDepartements: string[] | string) {
        const diagnosticsToutesCommunes = this.rechercherTousParCategorie('COMMUNE');
        return diagnosticsToutesCommunes.filter((d) => filtreIdsDepartements.includes(d.idDepartement ?? ''));
    }

    rechercherTousParCategorieEtBoundingBox(codeCategorieTerritoireApi: CodeCategorieTerritoireApi, bbox: BoundingBoxApi): DiagnosticApi[] {
        const resultat = this.stockageDiagnostics.findAll().filter((d) => {
            return d.categorieTerritoire === codeCategorieTerritoireApi && !estIntersectionVide(bbox, d.boundingBoxTerritoire);
        });
        return resultat;
    }
}
