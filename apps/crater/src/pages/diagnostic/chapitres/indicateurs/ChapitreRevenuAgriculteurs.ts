import '../composants/ChapitreDomaine.js';
import '../../../commun/PageSablierCrater.js';

import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { IDS_DOMAINES } from '@lga/indicateurs';
import { SA } from '@lga/indicateurs';
import { html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { EtatRapport, type Rapport } from '../../../../modeles/diagnostics/Rapport.js';
import type { DonneesPageDiagnostic } from '../../PageDiagnostic.js';
import type { OptionsDomaineDetaille } from '../composants/ChapitreDomaine';

declare global {
    interface HTMLElementTagNameMap {
        'c-chapitre-revenu-agriculteurs': ChapitreRevenuAgriculteurs;
    }
}

@customElement('c-chapitre-revenu-agriculteurs')
export class ChapitreRevenuAgriculteurs extends LitElement {
    static styles = [STYLES_DESIGN_SYSTEM];

    @property({ attribute: false })
    donneesPage?: DonneesPageDiagnostic;

    @property({ attribute: false })
    rapport?: Rapport;

    render() {
        if (this.rapport?.getEtat() === EtatRapport.PRET) {
            return html`<c-chapitre-domaine
                .options=${{
                    domaine: SA.getDomaine(IDS_DOMAINES.revenuAgriculteurs),
                    territoire: this.rapport.getTerritoireActif(),
                    estPagePourTef: this.donneesPage?.estPagePourTef,
                    masquerLienMethodologie: true
                } as OptionsDomaineDetaille}
                contenuAdhoc
            >
                <div slot="contenu">
                    <p>
                        Sur la période 2017-2020, la moitié des moyennes et grandes exploitations agricoles françaises ont dégagé un revenu par
                        travailleur après cotisations sociales inférieur à 1 280 euros par mois.
                        <strong
                            >Ramené au temps de travail, la majorité des agriculteurs se rémunère donc à un taux horaire inférieur à 70 % du
                            SMIC</strong
                        >. Pour un quart des exploitations, le revenu dégagé par travailleur est inférieur à <strong>600 euros par mois</strong>, et
                        ce avant même d’éventuelles cotisations sociales. De l’autre côté de l’échelle, les 10 % d’exploitations les plus
                        rémunératrices parviennent à un revenu mensuel avant cotisations supérieur à 5 500 euros.
                    </p>
                    <p>
                        Pour en savoir plus, voir le rapport
                        <c-lien
                            href="https://publications.resiliencealimentaire.org/qui-veille-au-grain/partie-I_un-systeme-defaillant/#_des_agriculteurs_qui_ne_gagnent_pas_leur_vie"
                            >Qui veille grain&nbsp;?</c-lien
                        >.
                    </p>
                </div>
            </c-chapitre-domaine>`;
        } else {
            return html`<c-page-sablier-crater></c-page-sablier-crater>`;
        }
    }
}
