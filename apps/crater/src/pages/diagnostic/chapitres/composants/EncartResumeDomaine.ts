import '@lga/design-system/build/composants/BoiteAvecLien.js';
import '../../../commun/JaugeTroisClasses.js';

import { QualificatifEvolution, Signe } from '@lga/base';
import { ICONES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/icones-design-system';
import { CSS_BREAKPOINT_MAX_WIDTH_MOBILE_PAYSAGE } from '@lga/design-system/build/styles/styles-breakpoints.js';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system';
import { type Domaine, ICONES_HTML } from '@lga/indicateurs';
import { css, html, LitElement, type TemplateResult, unsafeCSS } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { unsafeHTML } from 'lit/directives/unsafe-html.js';

import { construireUrlPageDiagnosticDomaine } from '../../../../configuration/pages/pages-utils.js';
import type { Note } from '../../../../modeles/diagnostics/Note.js';
import { STYLES_CRATER } from '../../../commun/pages-styles.js';
import type { DonneesPageDiagnostic } from '../../PageDiagnostic.js';

export interface OptionsEncartResumeDomaine {
    domaine: Domaine;
    estPerimetreNational?: boolean;
    chiffresCles: ChiffreCle[];
    motSeparateurChiffresCles?: string;
    note?: Note;
    texteComplementaire?: string;
    donneesPage?: DonneesPageDiagnostic;
}

interface ChiffreCle {
    libelleAvantChiffre?: string;
    chiffre: string;
    icone?: string;
    libelleApresChiffre?: string;
    evolutionSigne?: Signe;
    evolutionQualificatif?: QualificatifEvolution;
    evolutionDescription?: string;
}

declare global {
    interface HTMLElementTagNameMap {
        'c-encart-resume-domaine': EncartResumeDomaine;
    }
}

@customElement('c-encart-resume-domaine')
export class EncartResumeDomaine extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        STYLES_CRATER,
        css`
            :host {
                display: block;
                width: 100%;
                margin-top: calc(2 * var(--dsem));
                margin-bottom: calc(2 * var(--dsem));
                --couleur-icone: var(--couleur-primaire-clair);
            }

            c-boite-avec-lien {
                max-width: 600px;
            }

            c-boite-avec-lien [slot='contenu'] {
                padding: calc(2 * var(--dsem));
            }

            #titre-et-jauge {
                display: flex;
                justify-content: space-between;
                align-items: center;
            }

            #titre {
                margin: 0;
            }

            c-jauge-trois-classes {
                margin-right: calc(2 * var(--dsem));
                --taille: 2;
                padding-top: calc(3px * var(--taille));
            }

            #texte-complementaire {
                padding-left: calc(2 * var(--dsem));
                color: var(--couleur-neutre);
            }

            #evolution {
                display: inline-flex;
                align-items: center;
                gap: calc(var(--dsem) / 2);
                padding-left: calc(2 * var(--dsem));
            }

            #evolution div {
                margin: 0;
            }

            .succes > svg,
            .succes {
                color: var(--couleur-succes);
                stroke: var(--couleur-succes);
            }

            .danger > svg,
            .danger {
                color: var(--couleur-danger);
                stroke: var(--couleur-danger);
            }

            .neutre > svg,
            .neutre {
                color: var(--couleur-neutre);
                stroke: var(--couleur-neutre);
            }

            .conteneur-deux-items-chiffres-cles {
                display: grid;
                grid-template-columns: auto 1fr 20% 1fr;
                align-items: center;
                margin: calc(3 * var(--dsem));
            }

            .conteneur-un-item-chiffres-cles {
                display: grid;
                grid-template-columns: auto 1fr;
                align-items: center;
                margin: calc(3 * var(--dsem));
                gap: calc(1.5 * var(--dsem));
            }

            #mot-separateur {
                color: var(--couleur-primaire);
                width: fit-content;
                margin-right: auto;
                margin-left: auto;
            }

            #icone {
                display: grid;
            }

            #icone svg {
                width: calc(9 * var(--dsem));
                height: calc(9 * var(--dsem));
            }

            .chiffre-cle {
                padding: 0 var(--dsem);
            }

            .libelle {
                color: var(--couleur-neutre-60);
            }

            .valeur {
                display: flex;
                align-items: center;
                gap: var(--dsem);
                color: var(--couleur-primaire);
                max-width: 300px;
                padding: var(--dsem) 0;
            }

            .valeur svg {
                fill: var(--couleur-primaire-clair);
                display: grid;
            }

            @media screen and (max-width: ${unsafeCSS(CSS_BREAKPOINT_MAX_WIDTH_MOBILE_PAYSAGE + 'px')}) {
                #icone {
                    display: none;
                }
                c-jauge-trois-classes {
                    margin-right: var(--dsem);
                }
                .conteneur-deux-items-chiffres-cles {
                    grid-template-columns: 1fr auto 1fr;
                    margin-left: 0;
                    margin-right: 0;
                }
                #mot-separateur {
                    margin-right: auto;
                    margin-left: auto;
                }
            }
        `
    ];

    @property({ attribute: false })
    options?: OptionsEncartResumeDomaine;

    render() {
        if (!this.options) return;

        const dispositionChiffresCles =
            this.options.chiffresCles.length > 1 ? 'conteneur-deux-items-chiffres-cles' : 'conteneur-un-item-chiffres-cles';

        return html`
            <c-boite-avec-lien
                id="${this.options.domaine.id}"
                libelleLien="Voir l'indicateur"
                href=${construireUrlPageDiagnosticDomaine(
                    this.options.domaine.id,
                    this.options.donneesPage?.idTerritoirePrincipal ?? '',
                    this.options.donneesPage?.idEchelleTerritoriale
                )}
                encadrement
                arrondi="moyen"
            >
                <div slot="contenu">
                    <div id="titre-et-jauge">
                        <h2 id="titre" class="texte-moyen gras">
                            ${this.options.estPerimetreNational ? html`${ICONES_HTML.drapeau_français} ` : ''}${this.options.domaine.libelle}
                        </h2>
                        ${this.options.note?.classe ? html`<c-jauge-trois-classes classe=${this.options.note.classe}></c-jauge-trois-classes>` : ''}
                    </div>
                    <div class=${dispositionChiffresCles}>
                        <div id="icone">${unsafeHTML(this.options.domaine.icone)}</div>
                        ${this.renderChiffresCles(this.options.chiffresCles)}
                    </div>
                    ${this.renderEvolution(this.options.chiffresCles[0])}
                    ${this.options.texteComplementaire
                        ? html`<div id="texte-complementaire" class="texte-petit-majuscule">${this.options.texteComplementaire}</div>`
                        : ''}
                </div>
            </c-boite-avec-lien>
        `;
    }

    private renderChiffresCles(chiffresCles: ChiffreCle[]) {
        return chiffresCles.map((c, i) => {
            const chiffreDebut = c.chiffre.split(' ')[0];
            const chiffreFin = c.chiffre.split(' ').slice(1).join(' ');
            const estChiffre = !isNaN(Number(c.chiffre[0])) || ['+', '-'].includes(c.chiffre[0]);
            return html`
                ${i !== 0 ? html`<div id="mot-separateur" class="texte-titre">${unsafeHTML(this.options!.motSeparateurChiffresCles)}</div> ` : ``}
                <div class="chiffre-cle">
                    <div class="libelle texte-petit">${unsafeHTML(c.libelleAvantChiffre)}</div>
                    <div class="valeur">
                        ${estChiffre ? html`<span class="chiffre-grand">${chiffreDebut}</span>` : ''}
                        <span class="unite-tres-grand">${unsafeHTML(estChiffre ? chiffreFin : c.chiffre)}</span>
                        <span>${unsafeHTML(c.icone)}</span>
                    </div>
                    <div class="libelle texte-petit">${unsafeHTML(c.libelleApresChiffre)}</div>
                </div>
            `;
        });
    }
    private renderEvolution(chiffreCle: ChiffreCle) {
        let icone: TemplateResult = html``;
        let couleur = 'neutre';
        if (chiffreCle.evolutionSigne === Signe.POSITIF) {
            icone = ICONES_DESIGN_SYSTEM.flecheHautDroit;
        } else if (chiffreCle.evolutionSigne === Signe.NEGATIF) {
            icone = ICONES_DESIGN_SYSTEM.flecheBasDroit;
        }
        if (chiffreCle.evolutionQualificatif === QualificatifEvolution.valide) {
            icone = ICONES_DESIGN_SYSTEM.check;
        } else if (chiffreCle.evolutionQualificatif === QualificatifEvolution.rate) {
            icone = ICONES_DESIGN_SYSTEM.croix;
        }
        if (chiffreCle.evolutionQualificatif === QualificatifEvolution.progres || chiffreCle.evolutionQualificatif === QualificatifEvolution.valide) {
            couleur = 'succes';
        } else if (
            chiffreCle.evolutionQualificatif === QualificatifEvolution.declin ||
            chiffreCle.evolutionQualificatif === QualificatifEvolution.rate
        ) {
            couleur = 'danger';
        }
        return chiffreCle.evolutionQualificatif
            ? html`
                  <div id="evolution" class="${couleur}">
                      ${icone}
                      <div class="texte-petit-majuscule">${unsafeHTML(chiffreCle.evolutionDescription)}</div>
                  </div>
              `
            : ``;
    }
}
