import { formaterNombreEnNDecimalesString, formaterNombreSelonValeurString, htmlstring, type ImageModele } from '@lga/base';
import { INDICATEURS } from '@lga/indicateurs';
import { recupererListe3InitiativesParConstats } from '@lga/initiatives';

import type { DonneesApi } from '../fonctions-utils.js';
import { calculerLibelleSourcesIndicateur, creerLienExterneHtmlBrut } from '../liens-utils.js';
import type { ConstatModele } from './ConstatModele.js';
import type { InfographieCarteHVNModele, InfographieImageChiffreCleModele } from './InfographieModele.js';

export const IMAGE_PAYSAGE_AGRICOLE_AVEC_DES_HAIES: ImageModele = {
    url: '/images/constats/titres/paysage-agricole-avec-haies-800x868.jpg',
    descriptionAlt: 'Paysage agricole avec des haies',
    resolutionX: 800,
    resolutionY: 868
};

const IMAGE_ABEILLE: ImageModele = {
    url: '/images/constats/titres/abeille-800x861.jpg',
    descriptionAlt: 'Abeille qui butine',
    resolutionX: 800,
    resolutionY: 861
};

export function creerConstatHaies(donneesApi: DonneesApi): ConstatModele<InfographieImageChiffreCleModele> {
    const infographie: InfographieImageChiffreCleModele = {
        nomComposant: 'InfographieImageChiffreCle',
        informationsMethodologie: {
            titre: 'Indice du maintien des haies',
            descriptif: htmlstring`La perte nette de haies est estimée à 23 500 km par an sur la période 2017- 2021 en France, malgré une politique de plantation d'environ 3 000 km/an. Ces chiffres proviennent du rapport du CGAAER ${creerLienExterneHtmlBrut('https://agriculture.gouv.fr/la-haie-levier-de-la-planification-ecologique', 'La haie levier de la planification écologique')}.`,
            estPerimetreNational: true
        },
        chiffreCle: {
            chiffre: (23500).toLocaleString('fr-FR'),
            unite: 'km',
            phrase: 'de haies en moins en 2022 en France. C’est plus qu’un Paris-Sydney...'
        },
        accroche: '110m haies c’était suffisant',
        image: {
            url: '/images/constats/infographies/haies-arrachees-416x448.png',
            descriptionAlt: 'Haies arrachées dans un champ',
            resolutionX: 416,
            resolutionY: 448,
            position: 'gauche'
        }
    };

    return {
        id: 'haies',
        idTheme: 'biodiversite',
        titre: {
            libelle: 'À quoi ressemblent nos campagnes ?',
            image: IMAGE_PAYSAGE_AGRICOLE_AVEC_DES_HAIES
        },
        message: {
            titre: `L’agriculture est l'activité qui impacte le plus la biodiversité.`,
            texte: `En France, les oiseaux et les insectes disparaissent du fait de la destruction des haies, de la simplification des cultures, et de l’usage intensif de pesticides et d’engrais. Les paysages s’uniformisent et la production agricole s’en trouve fragilisée.`,
            texteCourt: `En France, les oiseaux et les insectes disparaissent du fait de la destruction des haies, de la simplification des cultures, et de l’usage intensif de pesticides et d’engrais.`,
            question: 'Et si on faisait évoluer nos pratiques agricoles ?',
            initiatives: recupererListe3InitiativesParConstats(donneesApi.diagnosticApi.profils, ['haies'])
        },
        infographie: infographie
    };
}

export function creerConstatPaysagesAgricoles(donneesApi: DonneesApi, scoreHvn: number): ConstatModele<InfographieCarteHVNModele> {
    const SEUIL_HVN_MOYEN = 12;
    const SEUIL_HVN_BON = 18;

    const nodu = donneesApi.diagnosticApi.pesticides.noduNormalise;
    const NODU_FRANCE = 3.47;
    const ratioNodu = nodu !== null ? nodu / NODU_FRANCE : null;

    const SEUIL_NODU = 3;
    const estSeuilNoduDepasse = nodu !== null && nodu >= SEUIL_NODU ? true : false;
    const estSeuilNoduTresDepasse = nodu !== null && nodu >= 2 * SEUIL_NODU ? true : false;

    const partSauBioPourcent = donneesApi.diagnosticApi.pratiquesAgricoles.partSauBioPourcent;

    let type: 'DEFAUT' | 'PESTICIDES' | 'NITRATES' = 'DEFAUT';
    if (donneesApi.diagnosticApi.profils.codeProfilDiagnostic === 'ELEVAGE_INTENSIF') {
        type = 'NITRATES';
    } else if (estSeuilNoduDepasse) {
        type = 'PESTICIDES';
    }

    let messageTitre = '';
    let messageTexte =
        'Les oiseaux et les insectes disparaissent du fait de la destruction des haies, de la simplification des cultures, et de l’usage intensif de pesticides et d’engrais. Les paysages s’uniformisent et la production agricole s’en trouve fragilisée.';
    let messageQuestion = 'Et si on faisait évoluer nos pratiques agricoles ?';

    if (scoreHvn < SEUIL_HVN_MOYEN) {
        messageTitre = `Les pratiques agricoles dominantes de votre territoire contribuent au déclin de la biodiversité.`;
    } else if (scoreHvn < SEUIL_HVN_BON) {
        messageTitre = `Les pratiques agricoles dominantes de votre territoire sont peu favorables à la biodiversité.`;
    } else if (scoreHvn >= SEUIL_HVN_BON) {
        if (type === 'PESTICIDES' || type === 'NITRATES') {
            messageTitre = 'Les paysages agricoles de votre territoire sont favorables à la biodiversité mais...';
        } else {
            messageTitre = `Les pratiques agricoles de votre territoire contribuent à préserver la biodiversité.`;
            messageTexte = `Le maintien des haies et des prairies, les pratiques peu intensives comme l’agriculture biologique, permettent aux espèces sauvages d’habiter les zones cultivées. Leur présence améliore en retour la production agricole.`;
            messageQuestion = 'Et si on généralisait ces pratiques vertueuses ?';
        }
    }
    if (type === 'PESTICIDES') {
        messageTexte = `L’usage de pesticides y est${estSeuilNoduTresDepasse ? ' très' : ''} important et les risques associés sont multiples : santé des travailleurs agricoles, qualité des nappes phréatiques et cours d’eau, disparition des insectes pollinisateurs…`;
    } else if (type === 'NITRATES') {
        messageTexte = `L’excès de nitrates (azote) provenant de certains élevages et cultures de la région entraîne la pollution des cours d’eau et des nappes phréatiques. Les impacts sur la vie aquatique et les coûts de traitement de l’eau potable sont importants.`;
    }

    const infographie: InfographieCarteHVNModele = {
        nomComposant: 'InfographieCarteHVN',
        informationsMethodologie: {
            titre: 'Indicateur Haute Valeur Naturelle',
            descriptif: htmlstring`
            <p>${INDICATEURS.hvn.description}</p>
            <br>
            <p>Source : ${calculerLibelleSourcesIndicateur(INDICATEURS.hvn, true)}</p>`,
            urlComplementInformation: 'https://solagro.org/nos-domaines-d-intervention/agroecologie/haute-valeur-naturelle',
            libelleBouton: 'Méthode de calcul'
        },
        territoire: donneesApi.territoireApi
    };

    return {
        id: 'paysages-agricoles',
        idTheme: 'biodiversite',
        titre: {
            libelle: 'À quoi ressemblent vos campagnes ?',
            image: type === 'PESTICIDES' ? IMAGE_ABEILLE : IMAGE_PAYSAGE_AGRICOLE_AVEC_DES_HAIES
        },
        message: {
            titre: messageTitre,
            texte: messageTexte,
            texteCourt: calculerMessageCourtConstatPaysagesAgricoles(partSauBioPourcent, ratioNodu),
            question: messageQuestion,
            initiatives: recupererListe3InitiativesParConstats(donneesApi.diagnosticApi.profils, ['paysages-agricoles'])
        },
        infographie: infographie
    };
}

export function calculerMessageCourtConstatPaysagesAgricoles(partSauBioPourcent: number | null, ratioNodu: number | null) {
    const message1 =
        partSauBioPourcent !== null
            ? htmlstring`<p>${formaterNombreSelonValeurString(partSauBioPourcent)} % de la surface agricole est en agriculture biologique.</p>`
            : '';
    const message2 =
        ratioNodu !== null && ratioNodu > 1.5
            ? htmlstring`<p>L’usage de pesticides est ${formaterNombreEnNDecimalesString(ratioNodu, 1)} fois plus intense que la moyenne nationale.</p>`
            : '';

    if (message1 === '' && message2 === '')
        return 'Les oiseaux et les insectes disparaissent du fait de la destruction des haies, de la simplification des cultures, et de l’usage intensif de pesticides et d’engrais.';
    return message1 + message2;
}
