import { describe, expect, it } from 'vitest';

import { calculerLibelleTooltipGenerique } from './style-carte-utils';

describe('Test du module outils style carte', () => {
    it('Test calculerLibelleTooltipGenerique', () => {
        expect(calculerLibelleTooltipGenerique(null)).toEqual('Valeur : Non disponible');
        expect(calculerLibelleTooltipGenerique(2, 'm')).toEqual('Valeur : 2 m');
        expect(calculerLibelleTooltipGenerique(2, '')).toEqual('Valeur : 2');
        expect(calculerLibelleTooltipGenerique(2, '%')).toEqual('Valeur : 2%');
        expect(calculerLibelleTooltipGenerique(2.3)).toEqual('Valeur : 2,3');
        expect(calculerLibelleTooltipGenerique(5.6)).toEqual('Valeur : 5,6');
    });
});
