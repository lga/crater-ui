import '../composants/ChapitreDomaine.js';
import '../../../commun/PageSablierCrater.js';
import '../../charts/ChartBarresAvecCourbeTendance.js';
import '@lga/design-system/build/composants/Tableau.js';
import '../../charts/ChartTreemapCultures.js';
import '../../charts/ChartTreemapDoubleCultures.js';

import { formaterNombreSelonValeurString } from '@lga/base';
import type { OptionsTableau } from '@lga/design-system/build/composants/Tableau';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { INDICATEURS, SOURCES_DONNEES } from '@lga/indicateurs';
import { IDS_MAILLONS } from '@lga/indicateurs';
import { IDS_DOMAINES } from '@lga/indicateurs';
import { SA } from '@lga/indicateurs';
import { CategorieTerritoire } from '@lga/territoires';
import { css, html, LitElement, type TemplateResult } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { unsafeHTML } from 'lit/directives/unsafe-html.js';

import { construireUrlPageMethodologie } from '../../../../configuration/pages/pages-utils.js';
import { EvenementSelectionnerCategorieTerritoire } from '../../../../evenements/EvenementSelectionnerCategorieTerritoire.js';
import { COULEURS_CATEGORIES_CULTURES_IRRIGATION } from '../../../../modeles/diagnostics/eau/PratiquesIrrigation.js';
import { EtatRapport, type Rapport } from '../../../../modeles/diagnostics/Rapport.js';
import { construireLienVersMethodologieSourceDonnee, construireLienVersSourcesIndicateur } from '../../../commun/liens-utils.js';
import { Serie, SerieMultiple } from '../../charts/chart-outils.js';
import type { OptionsChartBarresAvecCourbeTendance } from '../../charts/ChartBarresAvecCourbeTendance';
import type { DonneesTreemapCultures } from '../../charts/ChartTreemapCultures';
import type { OptionsChartTreemapDoubleCultures } from '../../charts/ChartTreemapDoubleCultures';
import type { DonneesPageDiagnostic } from '../../PageDiagnostic.js';
import type { OptionsDomaineDetaille } from '../composants/ChapitreDomaine';

declare global {
    interface HTMLElementTagNameMap {
        'c-chapitre-irrigation': ChapitreIrrigation;
    }
}

@customElement('c-chapitre-irrigation')
export class ChapitreIrrigation extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        css`
            h3 {
                padding-top: calc(4 * var(--dsem));
            }

            .legende {
                text-align: center;
            }
        `
    ];

    @property({ attribute: false })
    donneesPage?: DonneesPageDiagnostic;

    @property({ attribute: false })
    rapport?: Rapport;

    render() {
        if (this.rapport?.getEtat() === EtatRapport.PRET) {
            return html`<c-chapitre-domaine
                .options=${{
                    domaine: SA.getDomaine(IDS_DOMAINES.irrigation),
                    territoire: this.rapport.getTerritoireActif(),
                    estPagePourTef: this.donneesPage?.estPagePourTef
                } as OptionsDomaineDetaille}
                contenuAdhoc
            >
                <div slot="contenu">
                    ${this.renderAvertissementQualiteDonnees()}

                    <c-message-domaine
                        .message=${this.rapport.getDiagnosticActif()?.intrants.eau.irrigation.messagePrelevementsIrrigation}
                        nbLignesMinimum="20"
                    ></c-message-domaine>
                    <p>Le graphique suivant présente l’évolution des volumes prélevés en m³ par an et la tendance correspondante :</p>
                    <c-figure .titre=${construireLienVersSourcesIndicateur(INDICATEURS.prelevementsIrrigation)}>
                        <c-chart-barres-avec-courbe-tendance
                            .options=${this.calculerOptionsChartIrrigation()}
                            slot="chart-figure"
                        ></c-chart-barres-avec-courbe-tendance>
                    </c-figure>
                    <h3>Pratiques d'irrigation</h3>
                    ${!this.rapport.getTerritoireActif()?.categorie.estAuMoinsEgaleA(CategorieTerritoire.Departement)
                        ? html`
                              <p>
                                  Les données ne sont pas disponibles à cette échelle, nous vous invitons à regarder le diagnostic du
                                  <c-lien @click=${this.actionSelectionDepartement}>département</c-lien> !
                              </p>
                          `
                        : html`
                              <p>
                                  Les figures suivantes présentent les pratiques d’irrigation du territoire pour différents groupes de culture en
                                  2010, en termes de superficies irriguées. Attention, il ne s'agit donc pas des volumes utilisés par chaque culture,
                                  cette donnée étant inconnue.
                              </p>
                              ${unsafeHTML(this.rapport.getDiagnosticActif()!.intrants.eau.irrigation.pratiquesIrrigation.messageSuperficieIrriguee)}
                              ${unsafeHTML(
                                  this.rapport.getDiagnosticActif()!.intrants.eau.irrigation.pratiquesIrrigation
                                      .messageCulturesLesPlusIntensementIrriguees
                              )}
                              ${unsafeHTML(
                                  this.rapport.getDiagnosticActif()!.intrants.eau.irrigation.pratiquesIrrigation.messageCultureLaPlusIrriguee
                              )}
                              <c-figure .titre=${construireLienVersSourcesIndicateur(INDICATEURS.pratiquesIrrigation)}>
                                  <c-chart-treemap-double-cultures
                                      .options=${this.calculerOptionsTreemapCultures(this.rapport)}
                                      slot="chart-figure"
                                  ></c-chart-treemap-double-cultures>
                              </c-figure>
                              <br />
                              <p>Toutes les informations sont reprises dans ce tableau :</p>
                              <c-tableau slot="chart-figure" .options=${this.calculerOptionsTableauPratiquesIrrigation()}></c-tableau>
                              <p class="legende texte-petit">${construireLienVersSourcesIndicateur(INDICATEURS.pratiquesIrrigation)}</p>
                          `}
                </div>
            </c-chapitre-domaine>`;
        } else {
            return html`<c-page-sablier-crater></c-page-sablier-crater>`;
        }
    }

    private renderAvertissementQualiteDonnees() {
        return !this.rapport?.getTerritoireActif()?.categorie.estAuMoinsEgaleA(CategorieTerritoire.Departement)
            ? html`
                  <p class="italique">
                      Note préalable : il est possible que le volume de prélèvements présenté pour ce territoire soit surestimé ou sous-estimé de
                      manière significative, en particulier si le territoire comporte des réseaux de canaux d'irrigation (ex: Pyrénées-Orientales,
                      Bouches-du-Rhône, etc.). Pour plus de détails, voir la
                      <c-lien href="${construireUrlPageMethodologie(IDS_MAILLONS.intrants, IDS_DOMAINES.irrigation)}">méthodologie de calcul</c-lien>
                      et les limites liées à la source de données
                      ${construireLienVersMethodologieSourceDonnee(SOURCES_DONNEES.prelevements_irrigation)}
                  </p>
              `
            : html``;
    }

    private calculerOptionsChartIrrigation(): OptionsChartBarresAvecCourbeTendance {
        return {
            series: new SerieMultiple(
                [
                    new Serie(
                        this.rapport?.getDiagnosticActif()?.intrants.eau.irrigation.irrigationIndicateursParAnnees.valeurs((i) => i.irrigationM3),
                        'Volumes prélevés'
                    )
                ],
                this.rapport?.getDiagnosticActif()?.intrants.eau.irrigation.irrigationIndicateursParAnnees.annees
            ),
            nomAxeOrdonnees: 'Volume [m³]'
        };
    }

    private calculerOptionsTreemapCultures(rapport: Rapport): OptionsChartTreemapDoubleCultures {
        const pratiquesIrrigation = rapport.getDiagnosticActif()?.intrants.eau.irrigation.pratiquesIrrigation;

        const pratiquesIrrigationParCultures = (pratiquesIrrigation?.pratiquesIrrigationParCultures ?? []).sort((a, b) => {
            return a.categorieCulture.localeCompare(b.categorieCulture) || a.culture.localeCompare(b.culture);
        });

        const donneesTreemapSurfacesIrriguees: DonneesTreemapCultures[] =
            pratiquesIrrigationParCultures.map((p) => {
                return {
                    code: '',
                    labelCourt: '',
                    labelLong: p.culture,
                    sauHa: p.sauIrrigueeHa,
                    couleur: p.couleur
                };
            }) ?? [];

        const donneesTreemapSurfacesNonIrriguees: DonneesTreemapCultures[] =
            pratiquesIrrigationParCultures.map((p) => {
                return {
                    code: '',
                    labelCourt: '',
                    labelLong: p.culture,
                    sauHa: p.sauNonIrrigueeHa ?? 0,
                    couleur: p.couleur
                };
            }) ?? [];

        return {
            sauTotaleCulturesGaucheHa: pratiquesIrrigation?.pratiquesIrrigationTotal.sauIrrigueeHa ?? 0,
            sauTotaleCulturesDroiteHa: pratiquesIrrigation?.pratiquesIrrigationTotal.sauNonIrrigueeHa ?? 0,
            titreCulturesGauche: 'Surfaces irriguées',
            titreCulturesDroite: 'Surfaces non irriguées',
            donneesTreemapCulturesGauche: donneesTreemapSurfacesIrriguees,
            donneesTreemapCulturesDroite: donneesTreemapSurfacesNonIrriguees,
            itemsLegende: COULEURS_CATEGORIES_CULTURES_IRRIGATION.map((g) => {
                return { libelle: g.categorieCulture, couleur: g.couleur };
            })
        };
    }

    private calculerOptionsTableauPratiquesIrrigation(): OptionsTableau {
        const contenuTableau: TemplateResult[][] = [];
        const pratiquesIrrigation = this.rapport?.getDiagnosticActif()?.intrants.eau.irrigation.pratiquesIrrigation;
        pratiquesIrrigation?.pratiquesIrrigationParCultures?.forEach((p) =>
            contenuTableau.push([
                html`${p.culture}`,
                html`${p.partSauIrrigueePourcent}`,
                html`${formaterNombreSelonValeurString(p.sauHa)}`,
                html`${p.poidsCultureSauTotalePourcent}`,
                html`${formaterNombreSelonValeurString(p.sauIrrigueeHa)}`,
                html`${p.poidsCultureSauIrrigueeTotalePourcent}`
            ])
        );
        contenuTableau.push([
            html`Total`,
            html`${formaterNombreSelonValeurString(pratiquesIrrigation?.pratiquesIrrigationTotal.partSauIrrigueePourcent ?? 0)}`,
            html`${formaterNombreSelonValeurString(pratiquesIrrigation?.pratiquesIrrigationTotal.sauHa)}`,
            html`100`,
            html`${formaterNombreSelonValeurString(pratiquesIrrigation?.pratiquesIrrigationTotal.sauIrrigueeHa)}`,
            html`100`
        ]);
        return {
            entetes: [
                html`Culture`,
                html`Part irriguée<br />[%]`,
                html`Superficie<br />[ha]`,
                html`Poids de la culture<br />dans la superficie totale [%]`,
                html`Superficie<br />irriguée<br />[ha]`,
                html`Poids de la culture<br />dans la superficie totale irriguée [%]`
            ],
            contenuHtml: contenuTableau,
            jaugesDansColonnes: [false, true, false, true, false, true]
        };
    }

    private actionSelectionDepartement() {
        this.dispatchEvent(
            new EvenementSelectionnerCategorieTerritoire(this.rapport?.getHierarchieTerritoires()?.departement?.categorie.codeCategorie ?? '')
        );
    }
}
