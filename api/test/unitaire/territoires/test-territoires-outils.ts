import type { CommuneApi, DepartementApi, RegroupementCommunesApi } from '@lga/specification-api';

import { Commune, Departement, Epci, Pays, RegroupementCommunes } from '../../../src/territoires/domaines/entitesTerritoires';

type OptionsCommune = Partial<CommuneApi> & {
    codesPostaux?: string[];
};
type OptionsRegroupementCommunes = Partial<RegroupementCommunesApi>;
type OptionsDepartement = Partial<DepartementApi>;

export function creerCommune(options: OptionsCommune) {
    return new Commune(
        {
            id: options.nom ?? '',
            idCode: options.nom ?? '',
            idNom: options.nom ?? '',
            nom: options.nom ?? '',
            categorie: 'COMMUNE',
            genreNombre: 'FEMININ_SINGULIER',
            preposition: 'dans',
            idTerritoireParcel: null,
            boundingBox: { latitudeMax: 10, latitudeMin: -10, longitudeMax: 10, longitudeMin: -10 }
        },
        '',
        '',
        '',
        '',
        options.codesPostaux ?? []
    );
}

export function creerEPCI(options: OptionsRegroupementCommunes) {
    return new Epci(
        {
            id: options.nom ?? '',
            idCode: options.nom ?? '',
            idNom: options.nom ?? '',
            nom: options.nom ?? '',
            categorie: 'EPCI',
            genreNombre: 'MASCULIN_SINGULIER',
            preposition: 'dans',
            idTerritoireParcel: null,
            boundingBox: { latitudeMax: 10, latitudeMin: -10, longitudeMax: 10, longitudeMin: -10 }
        },
        options.sousCategorie ?? 'COMMUNAUTE_COMMUNES',
        '',
        '',
        ''
    );
}

export function creerRegroupementCommunes(options: OptionsRegroupementCommunes) {
    return new RegroupementCommunes(
        {
            id: options.nom ?? '',
            idCode: options.nom ?? '',
            idNom: options.nom ?? '',
            nom: options.nom ?? '',
            categorie: 'REGROUPEMENT_COMMUNES',
            idTerritoireParcel: null,
            genreNombre: 'MASCULIN_SINGULIER',
            preposition: 'dans',
            boundingBox: { latitudeMax: 10, latitudeMin: -10, longitudeMax: 10, longitudeMin: -10 }
        },
        options.sousCategorie ?? 'PROJET_ALIMENTAIRE_TERRITORIAL',
        '',
        '',
        ''
    );
}

export function creerDepartement(options: OptionsDepartement) {
    return new Departement(
        {
            id: options.nom ?? '',
            idCode: options.nom ?? '',
            idNom: options.nom ?? '',
            nom: options.nom ?? '',
            categorie: 'DEPARTEMENT',
            idTerritoireParcel: null,
            genreNombre: 'MASCULIN_SINGULIER',
            preposition: 'dans',
            boundingBox: { latitudeMax: 10, latitudeMin: -10, longitudeMax: 10, longitudeMin: -10 }
        },
        '',
        ''
    );
}

export function creerPays() {
    return new Pays({
        id: 'France',
        idCode: 'P-FR',
        idNom: 'France',
        nom: 'Pays',
        categorie: 'PAYS',
        genreNombre: 'MASCULIN_SINGULIER',
        preposition: 'en',
        idTerritoireParcel: null,
        boundingBox: { latitudeMax: 10, latitudeMin: -10, longitudeMax: 10, longitudeMin: -10 }
    });
}
