import { calculerEvolutionPourcent, htmlstring, type ImageModele } from '@lga/base';
import { INDICATEURS } from '@lga/indicateurs';
import { recupererListe3InitiativesParConstats } from '@lga/initiatives';
import type { CodeProfilDiagnosticApi } from '@lga/specification-api';

import type { DonneesApi } from '../fonctions-utils.js';
import { estimerPopulation } from '../fonctions-utils.js';
import { calculerLibelleSourcesIndicateur, creerLienExterneHtmlBrut } from '../liens-utils.js';
import type { ConstatModele } from './ConstatModele.js';
import type {
    InfographieEvolutionPopulationAgricoleModele,
    InfographieImageMessageModele,
    InformationsMethodologieInfographieModele
} from './InfographieModele.js';

type AnneeNombreAgriculteurs = 1988 | 2010 | 2040;

export const IMAGE_AGRICULTEUR: ImageModele = {
    url: '/images/constats/titres/agriculteur-800x868.jpg',
    descriptionAlt: "Agriculteur en train d'arroser",
    resolutionX: 800,
    resolutionY: 868
};

export function creerConstatEvolutionPopulationAgricole(donneesApi: DonneesApi): ConstatModele<InfographieEvolutionPopulationAgricoleModele> {
    const donneesEvolutionPopulationAgricole = calculerDonneesEvolutionPopulationAgricole(
        donneesApi.diagnosticApi.populationAgricole.populationAgricole1988 ?? 0,
        donneesApi.diagnosticApi.populationAgricole.populationAgricole2010 ?? 0
    );

    const evolutionNombreAgriculteursPourcent =
        calculerEvolutionPourcent(
            donneesApi.diagnosticApi.populationAgricole.populationAgricole1988,
            donneesApi.diagnosticApi.populationAgricole.populationAgricole2010
        ) ?? 0;

    const informationsMethodologie = calculerInformationsMethodologieConstatEvolutionPopulationAgricole([1988, 2010, 2040]);

    return {
        id: 'evolution-population-agricole',
        idTheme: 'agriculteurs',
        titre: {
            libelle: 'Les agriculteurs dans tout ça ?',
            image: IMAGE_AGRICULTEUR
        },
        message: {
            titre: calculerMessageTitreConstatEvolutionPopulationAgricole(evolutionNombreAgriculteursPourcent),
            texte: calculerMessageTexteConstatEvolutionPopulationAgricole(
                evolutionNombreAgriculteursPourcent,
                donneesApi.diagnosticApi.profils.codeProfilDiagnostic
            ),
            texteCourt:
                evolutionNombreAgriculteursPourcent < -5
                    ? 'Les exploitations s’agrandissent et deviennent de plus en plus difficiles à transmettre.'
                    : '',
            question: calculerQuestionConstatPopulationAgricole(evolutionNombreAgriculteursPourcent),
            initiatives: recupererListe3InitiativesParConstats(donneesApi.diagnosticApi.profils, ['evolution-population-agricole'])
        },
        infographie: {
            nomComposant: 'InfographieEvolutionPopulationAgricole',
            informationsMethodologie: informationsMethodologie,
            accroche: `En ${donneesEvolutionPopulationAgricole.annees[donneesEvolutionPopulationAgricole.annees.length - 1]}, qui veillera au grain ?`,
            nombreAgriculteurs: donneesEvolutionPopulationAgricole.nombreAgriculteurs,
            annees: donneesEvolutionPopulationAgricole.annees,
            nomTerritoire: donneesApi.territoireApi.nom
        }
    };
}

export interface EvolutionAgriculteurs {
    annees: number[];
    nombreAgriculteurs: number[];
}

export function calculerDonneesEvolutionPopulationAgricole(nombreAgriculteurs1988: number, nombreAgriculteurs2010: number): EvolutionAgriculteurs {
    const annees: AnneeNombreAgriculteurs[] = [1988, 2010, 2040];
    const estimationNombreAgriculteurs = estimerPopulation(nombreAgriculteurs1988, nombreAgriculteurs2010, annees[0], annees[1], annees[2]);
    const nombreAgriculteurs = [nombreAgriculteurs1988, nombreAgriculteurs2010, estimationNombreAgriculteurs];
    return { annees: annees, nombreAgriculteurs: nombreAgriculteurs };
}

export function calculerInformationsMethodologieConstatEvolutionPopulationAgricole(annees: number[]): InformationsMethodologieInfographieModele {
    return {
        titre: 'Évolution de la population agricole',
        descriptif: htmlstring`
        <p>Le nombre d’agriculteurs en ${annees[0]} et ${annees[1]} provient des recensements agricoles effectués par le ministère de l’agriculture. Il s’agit du nombre d'actifs agricoles permanents (salariés et exploitants). La valeur pour l’année ${annees[2]} a été calculée en prolongeant la tendance observée entre ${annees[0]} et ${annees[1]}.</p>
        <br>
        <p>Source : ${calculerLibelleSourcesIndicateur(INDICATEURS.nombreActifsAgricoles1988)} et ${calculerLibelleSourcesIndicateur(INDICATEURS.nombreActifsAgricoles2010)}</p>`,
        libelleBouton: 'Chiffres'
    };
}

export function calculerMessageTitreConstatEvolutionPopulationAgricole(evolutionNombreAgriculteurs: number) {
    let message = '';
    if (evolutionNombreAgriculteurs < -50) {
        message = 'La population agricole est en très forte baisse.';
    } else if (evolutionNombreAgriculteurs < -30) {
        message = 'La population agricole est en forte baisse.';
    } else if (evolutionNombreAgriculteurs < -5) {
        message = 'La population agricole est en baisse.';
    } else if (evolutionNombreAgriculteurs < 5) {
        message = 'La population agricole est stable.';
    } else {
        message = 'La population agricole est en hausse !';
    }
    return message;
}

export function calculerMessageTexteConstatEvolutionPopulationAgricole(
    evolutionNombreAgriculteurs: number,
    codeProfilDiagnostic: CodeProfilDiagnosticApi
) {
    let message = '';
    if (evolutionNombreAgriculteurs < -5) {
        message = 'Revenus faibles au vu du travail fourni et investissements très lourds n’incitent pas à se lancer.';

        if (codeProfilDiagnostic === 'VITICULTURE') {
            message +=
                ' La spécialisation viticole fait en plus grimper le prix des terres. Difficile dans ces conditions de s’installer avec des projets visant à nourrir la population locale.';
        } else {
            message +=
                ' En l’absence de candidats à qui transmettre sa ferme, ce sont souvent les voisins qui récupèrent les terres. Les exploitations s’agrandissent et deviennent encore plus difficiles à transmettre. Un parfait cercle vicieux.';
        }
    } else {
        message = 'Votre territoire fait figure d’exception en France où la tendance est à une forte baisse de la population agricole.';
    }
    return message;
}

export function calculerQuestionConstatPopulationAgricole(evolutionNombreAgriculteurs: number) {
    let accroche;
    if (evolutionNombreAgriculteurs < -5) {
        accroche = 'Et si on facilitait l’installation des agriculteurs qui nous nourrissent ?';
    } else {
        accroche = 'Et si on allait encore plus loin ?';
    }

    return accroche;
}

export function creerConstatRevenus(donneesApi: DonneesApi): ConstatModele<InfographieImageMessageModele> {
    return {
        id: 'revenus',
        idTheme: 'agriculteurs',
        titre: {
            libelle: 'Les agriculteurs dans tout ça ?',
            image: {
                url: '/images/constats/titres/agriculteur-800x868.jpg',
                descriptionAlt: "Agriculteur en train d'arroser",
                resolutionX: 800,
                resolutionY: 868
            }
        },
        message: {
            titre: 'L’agriculture, un métier difficile et souvent peu rémunéré au vu du travail fourni.',
            texte: htmlstring`Ce n’est pas le seul : saisonniers, ouvriers des chaînes de transformation alimentaire, caristes ou caissiers de supermarché sont des métiers essentiels, éprouvants mais peu valorisés.`,
            question: 'Et si on améliorait leurs conditions de travail ?',
            initiatives: recupererListe3InitiativesParConstats(donneesApi.diagnosticApi.profils, ['revenus'])
        },
        infographie: {
            nomComposant: 'InfographieImageMessage',
            informationsMethodologie: {
                titre: 'Conditions de travail des agriculteurs',
                descriptif: htmlstring`
            <p>Les chiffres sont issus des rapports suivants :</p>
            <p>Retraites : ${creerLienExterneHtmlBrut('https://drees.solidarites-sante.gouv.fr/publications-documents-de-reference/panoramas-de-la-drees/les-retraites-et-les-retraites-edition', 'Les retraités et les retraites. Chapitre 5')}, DREES (2020).</p>
            <p>Temps de travail : ${creerLienExterneHtmlBrut('https://agriculture.gouv.fr/actifagri-de-lemploi-lactivite-agricole-determinants-dynamiques-et-trajectoires', 'Actif’Agri. Transformations des emplois et des activités en agriculture')}, ministère de l’agriculture (2019).</p>
            `,
                estPerimetreNational: true,
                libelleBouton: 'Chiffres'
            },
            image: {
                url: '/images/constats/infographies/offre-emploi-agriculteur-495x492.png',
                descriptionAlt: 'Offre d’emploi pour agriculteur',
                resolutionX: 495,
                resolutionY: 492,
                styleBords: 'carre'
            },
            accroche: "Un métier d'avenir ?"
        }
    };
}
