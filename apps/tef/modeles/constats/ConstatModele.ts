import type { HtmlString, ImageModele } from '@lga/base';
import type { IdConstatAvecAbsenceDonnees, IdTheme } from '@lga/indicateurs';
import type { InitiativeAvecScore } from '@lga/initiatives';

import type { InfographieModele } from './InfographieModele.js';

export interface ConstatMessageModele {
    titre: string;
    texte: HtmlString | string;
    texteCourt?: HtmlString | string | null;
    question: string;
    initiatives: InitiativeAvecScore[];
}

export interface ConstatModele<I extends InfographieModele> {
    id: IdConstatAvecAbsenceDonnees;
    idTheme: IdTheme;
    titre: {
        libelle: string;
        image: ImageModele;
    };
    message: ConstatMessageModele;
    infographie?: I;
}
