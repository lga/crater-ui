import { arrondirANDecimales, calculerEvolutionParPasDeTemps } from '@lga/base';
import type { Territoire } from '@lga/territoires';

import type { IndicateurSynthese } from '../IndicateurSynthese';
import type { Note } from '../Note';
import {
    calculerMessageDetaillePolitiqueAmenagement,
    calculerMessagePartLogementsVacants,
    calculerMessageSyntheseTerresAgricoles,
    MessageRythmeArtificialisation,
    MessageSauParHabitant
} from './messages-terres-agricoles';
import type { PolitiqueAmenagement } from './PolitiqueAmenagement';

export const SEUIL_ARTIFICIALISATION_HA_QUASI_NULLE_OU_NEGATIVE = 0.1; // Seuil en hectare à partir duquel on considère qu'il n'y a pas d'artificialisation

export class TerresAgricoles implements IndicateurSynthese {
    public terresAgricolesPays?: TerresAgricoles;

    constructor(
        private territoire: Territoire,
        public readonly note: Note,
        public readonly superficieTotaleHa: number | null,
        public readonly artificialisation5ansHa: number | null,
        public readonly evolutionMenagesEmplois5ans: number | null,
        public readonly sauParHabitantM2: number | null,
        public readonly besoinsAssietteActuelleHa: number | null,
        public readonly besoinsAssietteDemitarienneHa: number | null,
        public readonly population: number | null,
        public readonly rythmeArtificialisationSauPourcent: number | null,
        public readonly politiqueAmenagement: PolitiqueAmenagement,
        public readonly partLogementsVacants2013Pourcent: number | null,
        public readonly partLogementsVacants2018Pourcent: number | null
    ) {}

    setTerresAgricolesPays(terresAgricolesPays: TerresAgricoles) {
        this.terresAgricolesPays = terresAgricolesPays;
    }

    get messageSynthese() {
        return calculerMessageSyntheseTerresAgricoles(
            this.note,
            this.sauParHabitantM2,
            this.besoinsParHabitantAssietteActuelleM2,
            this.besoinsParHabitantAssietteDemitarienneM2,
            this.artificialisation5ansHa
        );
    }

    get besoinsParHabitantAssietteActuelleM2(): number | null {
        if (this.population && this.besoinsAssietteActuelleHa) {
            return arrondirANDecimales((this.besoinsAssietteActuelleHa * 10000) / this.population, 0);
        }
        return null;
    }

    get besoinsParHabitantAssietteDemitarienneM2(): number | null {
        if (this.population && this.besoinsAssietteDemitarienneHa) {
            return arrondirANDecimales((this.besoinsAssietteDemitarienneHa * 10000) / this.population, 0);
        }
        return null;
    }

    get messageSauParHabitant() {
        return MessageSauParHabitant.construireMessage(
            this.territoire.nom,
            this.sauParHabitantM2,
            this.besoinsParHabitantAssietteActuelleM2,
            this.besoinsParHabitantAssietteDemitarienneM2
        );
    }

    get messageRythmeArtificialisation() {
        return MessageRythmeArtificialisation.construireMessage(
            this.territoire.nom,
            this.rythmeArtificialisationSauPourcent,
            this.terresAgricolesPays!.rythmeArtificialisationSauPourcent
        );
    }

    get chiffreRythmeArtificialisation() {
        const SUPERFICIE_TERRAIN_FOOTBALL_HECTARE = 7000 / 10000;
        let artificialisationAnnuelleEnNombreDeTerrainsDeFootball;
        this.artificialisation5ansHa === null
            ? (artificialisationAnnuelleEnNombreDeTerrainsDeFootball = null)
            : (artificialisationAnnuelleEnNombreDeTerrainsDeFootball = this.artificialisation5ansHa / 5 / SUPERFICIE_TERRAIN_FOOTBALL_HECTARE);
        return calculerEvolutionParPasDeTemps(artificialisationAnnuelleEnNombreDeTerrainsDeFootball);
    }

    get ratioArtificialisation5ansSurSuperficieTerritoirePourcent(): number | null {
        if (this.artificialisation5ansHa === null || this.superficieTotaleHa === null) {
            return null;
        }
        return arrondirANDecimales((this.artificialisation5ansHa / this.superficieTotaleHa) * 100, 1);
    }

    get messagePolitiqueAmenagement() {
        return calculerMessageDetaillePolitiqueAmenagement(
            this.territoire.nom,
            this.politiqueAmenagement,
            this.artificialisation5ansHa,
            this.evolutionMenagesEmplois5ans,
            this.ratioArtificialisation5ansSurSuperficieTerritoirePourcent
        );
    }

    get messagePartLogementsVacants() {
        return calculerMessagePartLogementsVacants(this.territoire.nom, this.partLogementsVacants2013Pourcent, this.partLogementsVacants2018Pourcent);
    }

    get evolutionPartLogementsVacants2013a2018Points() {
        if (this.partLogementsVacants2013Pourcent === null || this.partLogementsVacants2018Pourcent === null) return null;
        return this.partLogementsVacants2018Pourcent - this.partLogementsVacants2013Pourcent;
    }
}
