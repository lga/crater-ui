import '@lga/design-system/build/composants/Bouton.js';

import { Bouton } from '@lga/design-system/build/composants/Bouton.js';
import { EvenementFermer } from '@lga/design-system/build/evenements/EvenementFermer.js';
import { ICONES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/icones-design-system.js';
import { css, html, LitElement } from 'lit';
import { customElement } from 'lit/decorators.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-template-panneau': TemplatePanneau;
    }
}

@customElement('c-template-panneau')
export class TemplatePanneau extends LitElement {
    static styles = css`
        :host {
            display: block;
            height: 100%;
            width: 100%;
            position: relative;
        }

        header {
            z-index: 1;
            position: fixed;
            display: flex;
            align-items: center;
            justify-content: right;
            width: 100%;
            height: var(--hauteur-c-entete);
            background-color: var(--couleur-blanc);
            border-bottom: 2px solid var(--couleur-primaire);
        }

        div {
            background-color: var(--couleur-blanc);
            padding-top: calc(var(--hauteur-c-entete) + 2 * var(--dsem));
            min-height: 100vh;
        }

        #bouton-fermer {
            padding-right: 1rem;
        }
    `;

    render() {
        return html`
            <header>
                <c-bouton id="bouton-fermer" @click=${this.fermer} libelleTooltip="Fermer" type="${Bouton.TYPE.plat}">
                    ${ICONES_DESIGN_SYSTEM.croix}
                </c-bouton>
            </header>
            <div>
                <slot name="contenu-panneau"></slot>
            </div>
        `;
    }

    private fermer() {
        this.dispatchEvent(new EvenementFermer());
    }
}
