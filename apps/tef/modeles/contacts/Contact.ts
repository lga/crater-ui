import { z } from 'zod';

const ProfilSchema = z.enum(['ELU', 'PORTEUR_PROJET', 'ASSOCIATION_OU_COLLECTIF', 'CITOYEN', 'AUTRE']);
export type Profil = z.infer<typeof ProfilSchema>;

export const contactSchema = z.object({
    email: z.string({ message: 'Champ obligatoire' }).email('Email non valide'),
    territoire: z.string().optional(),
    profil: ProfilSchema.optional()
});

export type Contact = z.infer<typeof contactSchema>;
