import '@lga/design-system/public/theme-defaut/theme-defaut.css';
import '../../../src/pages/diagnostic/charts/ChartBarres.js';

import { html } from 'lit';

import { Serie, SerieMultiple } from '../../../src/pages/diagnostic/charts/chart-outils.js';
import type { OptionsChartBarres } from '../../../src/pages/diagnostic/charts/ChartBarres';

describe('Test ChartBarres', () => {
    context('Mode desktop', () => {
        beforeEach(() => {
            cy.viewport(1024, 768);
        });

        it('chart series mutliples', () => {
            const options: OptionsChartBarres = {
                series: new SerieMultiple(
                    [
                        new Serie([100, 150, 210, 180, 200], 'Série 1'),
                        new Serie([110, 160, 220, 190, 210], 'Série 2'),
                        new Serie([120, 170, 230, 200, 220], 'Série 3')
                    ],
                    ['Commune AAAAAAAAAAAA', 'EPCI BBBBBBBB', 'DEPARTEMENT CCCCCCCCC', 'd', 'e']
                ),
                nomAxeOrdonnees: 'Nom axe ordonnées',
                annotations: {
                    yaxis: [
                        {
                            y: 200,
                            borderColor: 'var(--couleur-neutre-80)',
                            strokeDashArray: 5,
                            label: {
                                style: {
                                    color: 'var(--couleur-blanc)',
                                    background: 'var(--couleur-neutre-40)'
                                },
                                text: 'Annotation à y=200'
                            }
                        }
                    ]
                }
            };
            cy.mount<'c-chart-barres'>(html` <c-chart-barres .options=${options}></c-chart-barres> `);

            cy.get('c-chart-barres').find('figure').find('.apexcharts-canvas').invoke('height').should('be.equal', 400);
            cy.get('c-chart-barres').find('figure').find('.apexcharts-canvas').invoke('width').should('be.equal', 800);
        });
    });
});
