import { calculerUrlsSitemap } from './config/urls-sitemaps';

const API_BASE_URL_DEV = 'https://api.resiliencealimentaire.org/dev/';
const API_BASE_URL_PROD = 'https://api.resiliencealimentaire.org/';

const CRATER_BASE_URL_DEV = 'https://crater-tef-dev.resiliencealimentaire.org';

export default defineNuxtConfig({
    components: {
        dirs: [] // Désactiver l'auto import pour les composants
    },

    css: ['~/assets/css/theme-tef/theme-tef.css', '@lga/design-system/public/theme-defaut/theme-defaut.css'],

    colorMode: {
        preference: 'light',
        fallback: 'light'
    },

    compatibilityDate: '2024-07-11',

    devServer: {
        port: 8077,
        host: '0.0.0.0'
    },

    devtools: { enabled: true },

    experimental: {
        typedPages: true,
        defaults: {
            nuxtLink: {
                // Pour éviter de charger inutilement des pages (pas de prefetch => chargement uniquement au moment du clic)
                prefetch: false
            }
        }
    },

    image: {
        format: ['avif', 'webp', 'png']
    },

    imports: {
        autoImport: false //  Désactiver l'autoimport pour les composables & utilities
    },

    modules: [
        //  FIXME : Et corriger également le pb de dépendance de lit en double (voir warning dans console
        // FIXME : et également l'exception qui remonte sur la page projet => apparemment c'est lié au conflit avec lit ?
        ['nuxt-ssr-lit', { litElementPrefix: ['c-'] }],
        'nuxt-svgo',
        '@nuxt/image',
        '@nuxt/test-utils/module',
        '@nuxt/ui',
        '@nuxtjs/seo'
    ],

    nitro: {
        preset: 'node-cluster'
    },

    routeRules: {
        // Voir https://blog.risingstack.com/nuxt-3-rendering-modes/
        // - isr: true : adapté pour les pages qui ne font pas appel à l'api. Ces pages sont générées à la volée et conservées en cache jusqu'au prochain déploiement
        // - swr: true : pour les pages qui font appel à l'api crater (ex: page diag). Fonctionne comme isr, mais si l'api retourne une réponse différente, la page sera mise à jour à la prochaine requête. Permet de prendre en compte à chaud des updates dans l'api sans redémarrer l'ui...
        '/': { isr: true },
        '/**': { swr: true }, // swr par défaut pour toutes les autres pages
        '/_ipx/**': {
            // nécessaire sinon les images ne sont pas générées, voir https://github.com/nuxt/image/issues/1400 et https://github.com/nuxt/nuxt/issues/27307
            swr: false,
            cache: false
        },
        '/api/**': {
            // nécessaire sinon sinon le body est undefined dans les defineEventHandler
            swr: false,
            cache: false
        },
        '/_debug/**': { robots: false, prerender: false },
        '/confirmation-inscription-newsletter': { robots: false },
        '/cartes': { robots: false },
        '/methodologie_v2': { robots: false }
    },

    runtimeConfig: {
        // Config par défaut, utilisée en dev local. Cette conf est écrasée lors du déploiement (voir ecosystem.config.js)
        cleApiBrevo: '__CLE_API_BREVO__', // Pour une utilisation en local, il faut définir un fichier ui/apps/tef/.env avec cette valeur
        public: {
            apiBaseUrl: API_BASE_URL_DEV,
            craterBaseUrl: CRATER_BASE_URL_DEV,
            siteEnv: 'developpement', // Rq : nuxt-seo semble ne pas passer par le runtimeConfig. Il faut déclarer en plus une var d'env NUXT_PUBLIC_SITE_ENV
            brevo: {
                idListeSouscription: 64, // ID de la liste utilisée pour les env de test, voir https://app.brevo.com/contact/list-listing
                idTemplateMail: 203, // https://my.brevo.com/camp/template/203/message-setup
                urlPageConfirmation: 'https://dev.territoiresfertiles.fr/confirmation-inscription-newsletter'
            },
            matomo: {
                siteId: 'AUCUN' // Par défaut, tracking désactivé pour le dev en local
                //                siteId: '10' // Si besoin de tester, activer cette ligne (10=ID SITE MATOMO TeF DEV)
            }
        }
    },

    site: {
        // Config du module nuxt site-config.
        // Les variables ci-dessous sont automatiquement injectées en tant que nuxt runtimeConfig
        url: 'http://localhost:8077', // A rédéfinir via la var d'env NUXT_PUBLIC_SITE_URL (voir ecosystem.config.js)
        name: 'Territoires Fertiles - LOCALHOST', // Idem via NUXT_PUBLIC_SITE_NAME
        description:
            'Évaluez en un clic les enjeux de transition agricole et alimentaire spécifiques à votre commune, votre intercommunalité, votre département...',
        defaultLocale: 'fr'
    },

    sitemap: {
        // On construit toujours le sitemap à partir des données de prod
        urls: () => calculerUrlsSitemap(API_BASE_URL_PROD)
    },

    tailwindcss: {
        cssPath: ['~/assets/css/tailwind.css', { injectPosition: 'last' }],
        configPath: 'tailwind.config',
        exposeConfig: {
            level: 4, // Le niveau 4 permet d'importer spécifiquement des sous ensembles de la configuration (ex : la palette _primaire) => optimal pour le tree shaking
            alias: '#tailwind-config'
        },
        viewer: { exportViewer: true }
    },

    typescript: {
        strict: true,
        typeCheck: true
    },

    vite: {
        build: {
            rollupOptions: {
                output: {
                    manualChunks(id) {
                        // Utiliser npm run build:analyze pour voir les chunks générés et ceux qu'il est pertinent d'isoler, attention peut casser le build, bien checker avec un serve
                        const modulesAIsoler = ['apexcharts', '@turf', '@iconify', 'vue-router', '@headlessui'];
                        for (const nomModule of modulesAIsoler) {
                            if (id.includes(`node_modules/${nomModule}`)) {
                                return nomModule;
                            }
                        }
                        // Manual chunks pour les modules de l'application sauf pour 'commun' car le composant lit du champ de recherche ne fonctionne plus
                        if (id.includes('/ui/modules/') && !id.includes('/ui/modules/commun/')) {
                            const indexMDeModules = id.indexOf('/ui/modules/') + '/ui/'.length;
                            const indexPremierCharApresSlashDeModules = id.indexOf('/ui/modules/') + '/ui/modules/'.length;
                            const indexFinNomSousDossierModules = id.indexOf('/', indexPremierCharApresSlashDeModules);
                            const nomChunkModuleLga = id.slice(indexMDeModules, indexFinNomSousDossierModules);
                            return nomChunkModuleLga;
                        }
                    }
                }
            }
        }
    },

    vue: {
        compilerOptions: {
            // Déclarer explicitement quels élements sont des webcomponents pour éviter les warning vue dans la console
            isCustomElement: (tag) => tag.startsWith('c-')
        }
    }
});
