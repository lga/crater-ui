import './TitreChapitreDomaine.js';
import './MessageDomaine.js';
import './Figure.js';

import type { Domaine } from '@lga/indicateurs';
import { SA } from '@lga/indicateurs';
import type { Territoire } from '@lga/territoires';
import { EchelleTerritoriale } from '@lga/territoires';
import { css, html, LitElement } from 'lit';
import { customElement, property, state } from 'lit/decorators.js';
import { ifDefined } from 'lit/directives/if-defined.js';

import { construireUrlPageCarteTerritoire } from '../../../../configuration/pages/pages-utils.js';
import { construireLienVersSourcesIndicateur } from '../../../commun/liens-utils.js';

export interface OptionsDomaineDetaille {
    domaine: Domaine;
    territoire: Territoire | undefined;
    message?: string;
    masquerLienMethodologie?: boolean;
    estPagePourTef?: boolean;
}

declare global {
    interface HTMLElementTagNameMap {
        'c-chapitre-domaine': ChapitreDomaine;
    }
}

@customElement('c-chapitre-domaine')
export class ChapitreDomaine extends LitElement {
    static styles = css`
        :host {
            width: 100%;
        }
    `;

    @property({ type: Number })
    nbLignesMessageMinimum = 0;

    @property({ attribute: false })
    options?: OptionsDomaineDetaille;

    @property({ type: Boolean })
    contenuAdhoc = false;

    @state()
    idIndicateurCarte = '';

    willUpdate() {
        this.idIndicateurCarte = this.options?.domaine.indicateursDetailles[0].id ?? '';
    }

    render() {
        if (!this.options) return '';
        return html`
            <main>
                <c-titre-chapitre-domaine
                    .domaine=${this.options.domaine}
                    lienCarte=${this.estVisibleLienCarte()
                        ? construireUrlPageCarteTerritoire(this.idIndicateurCarte, this.options.territoire?.id ?? '')
                        : ''}
                    ?masquerLienMethodologie=${this.options.masquerLienMethodologie}
                    ?estPagePourTef=${this.options?.estPagePourTef}
                >
                </c-titre-chapitre-domaine>
                <c-message-domaine message=${ifDefined(this.options.message)} nbLignesMinimum=${this.nbLignesMessageMinimum}></c-message-domaine>
                ${!this.contenuAdhoc
                    ? html` <c-figure .titre=${construireLienVersSourcesIndicateur(this.options.domaine.indicateursDetailles.at(-1)!)}>
                          <slot name="chart-chapitre-domaine" slot="chart-figure"></slot>
                      </c-figure>`
                    : html`<slot name="contenu"></slot>`}
            </main>
        `;
    }

    private estVisibleLienCarte() {
        return (
            !this.options?.estPagePourTef &&
            EchelleTerritoriale.listerTousLesCodeCategories.includes(this.options?.territoire?.categorie.codeCategorie ?? '') &&
            SA.getIndicateurCarte(this.idIndicateurCarte) !== undefined
        );
    }
}
