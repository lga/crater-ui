import './SablierCrater.js';

import { css, html, LitElement } from 'lit';
import { customElement } from 'lit/decorators.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-page-sablier-crater': PageSablierCrater;
    }
}

@customElement('c-page-sablier-crater')
export class PageSablierCrater extends LitElement {
    static styles = css`
        :host {
            display: block;
            height: 100vh;
            background-color: var(--couleur-blanc);
        }
    `;

    render() {
        return html`
            <main>
                <c-sablier-crater></c-sablier-crater>
            </main>
        `;
    }
}
