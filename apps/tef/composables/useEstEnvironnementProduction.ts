import { useRuntimeConfig } from '#imports';

export function useEstEnvironnementProduction() {
    const config = useRuntimeConfig();
    const siteEnv = config.public.siteEnv;
    return siteEnv === 'production';
}
