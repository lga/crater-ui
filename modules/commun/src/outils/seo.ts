export function positionnerDonnesSEO(titre: string, urlCanonique: string, metaDescription: string | undefined) {
    document.title = titre;
    positionnerLienCanoniqueHeader(urlCanonique);
    positionnerMetaDescriptionHeader(metaDescription);
}

function positionnerLienCanoniqueHeader(urlCanoniqueRelative: string) {
    const lienCanonique = (document.getElementById('lien-url-canonique') as HTMLLinkElement) || document.createElement('link');
    lienCanonique.id = 'lien-url-canonique';
    lienCanonique.rel = 'canonical';
    lienCanonique.href = `${document.location.origin}${urlCanoniqueRelative}`;
    document.head.appendChild(lienCanonique);
}

function positionnerMetaDescriptionHeader(metaDescription: string | undefined) {
    if (metaDescription !== undefined) {
        const elementMetaDescription = (document.getElementById('meta-description') as HTMLMetaElement) || document.createElement('meta');
        elementMetaDescription.id = 'meta-description';
        elementMetaDescription.name = 'description';
        elementMetaDescription.content = metaDescription;
        document.head.appendChild(elementMetaDescription);
    } else {
        const elementMetaDescription = document.getElementById('meta-description') as HTMLMetaElement;
        if (elementMetaDescription) {
            elementMetaDescription.remove();
        }
    }
}
