#!/bin/bash

# telecharger les certificats (ici il faut que /var/www/html soit exposé via un site statique apache sinon marche pas)
sudo certbot certonly --webroot -w /var/www/html -d api.resiliencealimentaire.org -d territoiresfertiles.org  -d www.territoiresfertiles.org -d dev.territoiresfertiles.org -d api.territoiresfertiles.fr -d territoiresfertiles.com -d www.territoiresfertiles.com -d territoiresfertiles.fr -d www.territoiresfertiles.fr -d dev.territoiresfertiles.fr