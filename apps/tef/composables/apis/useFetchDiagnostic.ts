import type { DiagnosticApi } from '@lga/specification-api';

import { useFetchAvecGestionErreur, useGetApiBaseUrl } from '@/composables';

export async function useFetchDiagnostic(idTerritoire: string): Promise<DiagnosticApi> {
    const apiBaseUrl = useGetApiBaseUrl();
    const url = `${apiBaseUrl}/crater/api/diagnostics/${idTerritoire}`;
    return useFetchAvecGestionErreur<DiagnosticApi>(url);
}
