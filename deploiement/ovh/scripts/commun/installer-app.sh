#!/bin/bash
# Install d'une nouvelle version d'une application node

echo "Execution  de : 'installer-app.sh $1 $2'"
echo "  -> Installer une nouvelle version de l'application $1 sur l'environnement $2"
if [ $# -ne 2 ]; then
  echo 1>&2 "$0: Erreur, merci de renseigner les 2 paramètres obligatoires : NOM_APP (ex : api ou tef), et ENV_CIBLE_BUILD (prod ou dev)"
  exit 2
fi

NOM_APP=$1
ENV_CIBLE_BUILD=$2

if [ $ENV_CIBLE_BUILD != "dev" ] && [ $ENV_CIBLE_BUILD != "prod" ]; then
  echo 1>&2 "$0: Erreur, la valeur du paramètre ENV_CIBLE_BUILD doit être 'dev' ou 'prod'"
  exit 2
fi

# pour récuperer l'env node (défini dans bashrc, mais pas lu lors de l'execution du script a distance)
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm

#
# MAJ APACHE
#
# Purge des anciennes confs
sudo a2dissite crater-api.conf > /dev/null 2>&1
sudo rm -rf /etc/apache2/sites-available/crater-api.conf
#Install des nouvelles
sudo cp /home/ubuntu/lga/ovh/config/apache/apps.conf /etc/apache2/sites-available/apps.conf
sudo a2ensite apps.conf
sudo systemctl reload apache2

# FIX 24/05/2024 : on ne supprime plus le cache à chaque livraision (meilleures perfs)
# Par contre attention à l'évolution du nom des images : Si on renomme une image avec un nom qui existait déjà, et qui est donc déjà dans le cache, elle ne sera pas remplacée
## Reset dossier de cache apache a chaque relivraison de l'app
#sudo rm -rf /var/cache/apache2/mod_cache_disk/$NOM_APP-$ENV_CIBLE_BUILD
#sudo mkdir -p /var/cache/apache2/mod_cache_disk/$NOM_APP-$ENV_CIBLE_BUILD
#sudo chown -R www-data:www-data /var/cache/apache2/mod_cache_disk/$NOM_APP-$ENV_CIBLE_BUILD

# si le dossier de cache apache des images n'existe pas, on le crée
if [ ! -d /var/cache/apache2/mod_cache_disk/$NOM_APP-$ENV_CIBLE_BUILD ]; then
  sudo mkdir -p /var/cache/apache2/mod_cache_disk/$NOM_APP-$ENV_CIBLE_BUILD
  sudo chown -R www-data:www-data /var/cache/apache2/mod_cache_disk/$NOM_APP-$ENV_CIBLE_BUILD
fi

#
# Maj conf pm2 et redemarrage en faisant un reload du fichier de conf
#
pm2 stop  /home/ubuntu/lga/ovh/config/pm2/ecosystem.config.js --only $NOM_APP-$ENV_CIBLE_BUILD
pm2 reload /home/ubuntu/lga/ovh/config/pm2/ecosystem.config.js --only $NOM_APP-$ENV_CIBLE_BUILD
