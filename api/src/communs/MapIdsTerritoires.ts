import { lireCelluleString, lireCelluleStringOuNull, type TraiterCellules } from './injecteursUtils';

export class MapIdsTerritoires {
    private correspondanceIdCodeVersIdNom = new Map<string, string>();

    private ajouterTerritoire(idCodeTerritoire: string, idNomTerritoire: string): void {
        this.correspondanceIdCodeVersIdNom.set(idCodeTerritoire, idNomTerritoire);
    }

    lireCelluleIdTerritoire(map: Map<string, string>): string {
        const idCodeTerritoire = lireCelluleString(map, 'id_territoire');
        return this.correspondanceIdCodeVersIdNom.get(idCodeTerritoire)!;
    }

    lireCelluleIdTerritoireSupra(map: Map<string, string>, nomColonneIdTerritoireSupra: string): string | null {
        const idCodeTerritoire: string | null = lireCelluleStringOuNull(map, nomColonneIdTerritoireSupra);
        if (!idCodeTerritoire) {
            return null;
        } else {
            return this.correspondanceIdCodeVersIdNom.get(idCodeTerritoire) ?? null;
        }
    }

    enregistrerCorrespondanceIdsTerritoires: TraiterCellules = (mapCellules) => {
        const idCodeTerritoire = lireCelluleString(mapCellules, 'id_territoire');
        const idNomTerritoire = lireCelluleString(mapCellules, 'id_nom_territoire');
        if (!idCodeTerritoire || !idNomTerritoire) {
            throw new Error(
                `Erreur critique lors de la construction de la table de correspondance des idTerritoires : au moins un des id est undefined id_territoire=${idCodeTerritoire}, id_nom_territoire=${idNomTerritoire}`
            );
        }
        this.ajouterTerritoire(idCodeTerritoire, idNomTerritoire);
    };
}
