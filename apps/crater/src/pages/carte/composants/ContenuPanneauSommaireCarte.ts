import './ListeChoixIndicateursCarte.js';
import '@lga/commun/build/composants/champ-recherche-territoire/ChampRechercheTerritoire.js';
import '@lga/design-system/build/composants/RadioBouton.js';

import { getApiBaseUrl } from '@lga/commun/build/env/config-baseurl';
import type { OptionsRadioBouton } from '@lga/design-system/build/composants/RadioBouton';
import { EvenementFermer } from '@lga/design-system/build/evenements/EvenementFermer.js';
import type { EvenementSelectionner } from '@lga/design-system/build/evenements/EvenementSelectionner';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { EchelleTerritoriale } from '@lga/territoires';
import { css, html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { PAGES_PRINCIPALES } from '../../../configuration/pages/declaration-pages.js';
import { EvenementSelectionnerCategorieTerritoire } from '../../../evenements/EvenementSelectionnerCategorieTerritoire';

declare global {
    interface HTMLElementTagNameMap {
        'c-contenu-panneau-sommaire-carte': ContenuPanneauSommaireCarte;
    }
}

@customElement('c-contenu-panneau-sommaire-carte')
export class ContenuPanneauSommaireCarte extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        css`
            :host {
                display: block;
                width: 100vw;
                height: 100%;
            }

            main {
                padding: 0 calc(2 * var(--dsem));
            }
            section {
                padding: calc(1.5 * var(--dsem)) 0;
            }
        `
    ];

    @property()
    idIndicateurActif?: string;

    @property({ attribute: false })
    optionsBoutonsTerritoires?: OptionsRadioBouton;

    render() {
        return html`
            <main>
                <section>
                    <h2 class="texte-alternatif">Rechercher un territoire</h2>
                    <c-champ-recherche-territoire
                        apiBaseUrl=${getApiBaseUrl()}
                        codesCategorieTerritoire="${EchelleTerritoriale.listerTousLesCodeCategories.join(' ')}"
                        data-cy="champ-recherche-territoire"
                        @selectionnerTerritoire="${this.fermerParents}"
                        lienFormulaireDemandeAjoutTerritoire="${PAGES_PRINCIPALES.demandeAjoutTerritoire.getUrl()}"
                    ></c-champ-recherche-territoire>
                </section>
                <section>
                    <h2 class="texte-alternatif">Changer d'échelle</h2>
                    <c-radio-bouton
                        @selectionner="${this.actionSelectionCategorieTerritoire}"
                        .options="${this.optionsBoutonsTerritoires}"
                    ></c-radio-bouton>
                </section>
                <section>
                    <h2 class="texte-alternatif">Changer d'indicateur</h2>
                    <c-liste-choix-indicateurs-carte
                        .idIndicateurActif="${this.idIndicateurActif}"
                        @selectionner="${this.fermerParents}"
                    ></c-liste-choix-indicateurs-carte>
                </section>
            </main>
        `;
    }

    private fermerParents() {
        this.dispatchEvent(new EvenementFermer(true));
    }

    private actionSelectionCategorieTerritoire(event: Event) {
        const evenementSelectionner = event as EvenementSelectionner;
        this.dispatchEvent(new EvenementSelectionnerCategorieTerritoire(evenementSelectionner.detail.idItemSelectionne));
        evenementSelectionner.stopPropagation();
        this.fermerParents();
    }
}
