import '../composants/ChapitreDomaine.js';
import '../../../commun/PageSablierCrater.js';

import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { IDS_DOMAINES } from '@lga/indicateurs';
import { SA } from '@lga/indicateurs';
import { html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { EtatRapport, type Rapport } from '../../../../modeles/diagnostics/Rapport.js';
import type { DonneesPageDiagnostic } from '../../PageDiagnostic.js';
import type { OptionsDomaineDetaille } from '../composants/ChapitreDomaine';

declare global {
    interface HTMLElementTagNameMap {
        'c-chapitre-part-population-obese': ChapitrePartPopulationObese;
    }
}

@customElement('c-chapitre-part-population-obese')
export class ChapitrePartPopulationObese extends LitElement {
    static styles = [STYLES_DESIGN_SYSTEM];

    @property({ attribute: false })
    donneesPage?: DonneesPageDiagnostic;

    @property({ attribute: false })
    rapport?: Rapport;

    render() {
        if (this.rapport?.getEtat() === EtatRapport.PRET) {
            return html`<c-chapitre-domaine
                .options=${{
                    domaine: SA.getDomaine(IDS_DOMAINES.obesite),
                    territoire: this.rapport.getTerritoireActif(),
                    estPagePourTef: this.donneesPage?.estPagePourTef,
                    masquerLienMethodologie: true
                } as OptionsDomaineDetaille}
                contenuAdhoc
            >
                <p slot="contenu">
                    En France, l'obésité touche environ 8,5 millions de personnes ce qui représente 17% de le population. Ce taux a triplé en 30 ans,
                    il était de 5,85% en 1992.
                </p>
            </c-chapitre-domaine>`;
        } else {
            return html`<c-page-sablier-crater></c-page-sablier-crater>`;
        }
    }
}
