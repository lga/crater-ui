import '@lga/design-system/public/theme-defaut/theme-defaut.css';
import '../../../src/pages/commun/JaugeTroisClasses.js';

import { html } from 'lit';

describe('Test BarreMenuTerritoire', () => {
    it('Test affichage mobile', () => {
        // given
        cy.viewport(500, 500);
        cy.mount<'c-jauge-trois-classes'>(html` <c-jauge-trois-classes classe="3"></c-jauge-trois-classes> `);
        // then
        cy.get('c-jauge-trois-classes').invoke('width').should('be.approximately', 60, 10);
        cy.get('c-jauge-trois-classes').invoke('height').should('be.approximately', 32, 5);
        cy.get('c-jauge-trois-classes').get('.jauge-item').should('have.length', 3);
        cy.get('#triangle').invoke('css', 'grid-column').should('eq', '1');
        cy.get('#jauge-1').should('have.css', 'background-color', 'rgb(20, 184, 166)');
        cy.get('#legende-1').contains('A');
    });
});
