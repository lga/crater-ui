import '@lga/design-system/public/theme-defaut/theme-defaut.css';
import '../../../src/pages/diagnostic/charts/ChartTreemapConsommation';

import { html } from 'lit';

import { occitanie } from '../../fixtures/treemapConsommation';

const HAUTEUR_MAX_PIXELS = 350;

describe('Test ChartTreemapConsommation', () => {
    beforeEach(() => {
        cy.viewport(1024, 768);
    });

    it('cas occitanie', () => {
        cy.mount<'c-chart-treemap-consommation'>(html` <c-chart-treemap-consommation .options=${occitanie}></c-chart-treemap-consommation> `);

        const consommationTotalHa = occitanie.consommationAnimauxHa + occitanie.consommationHumainsHa;
        const hauteurTreemapAnimauxAttendu = Math.round((occitanie.consommationAnimauxHa / consommationTotalHa) * HAUTEUR_MAX_PIXELS);
        const hauteurTreemapHumainsAttendu = Math.round((occitanie.consommationHumainsHa / consommationTotalHa) * HAUTEUR_MAX_PIXELS);

        cy.get('#chart-treemap-consommation-animaux')
            .find('figure')
            .find('.apexcharts-canvas')
            .invoke('height')
            .should('be.approximately', hauteurTreemapAnimauxAttendu, 1);
        cy.get('#chart-treemap-consommation-humains')
            .find('figure')
            .find('.apexcharts-canvas')
            .invoke('height')
            .should('be.approximately', hauteurTreemapHumainsAttendu, 1);
        cy.get('.chart-treemap').invoke('height').should('be.approximately', HAUTEUR_MAX_PIXELS, 1);
    });
});
