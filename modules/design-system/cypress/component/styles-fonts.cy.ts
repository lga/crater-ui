import '../../public/theme-defaut/theme-defaut.css';

import { html } from 'lit';

import { STYLES_DESIGN_SYSTEM } from '../../src/styles/styles-design-system';

describe('Test styles fonts', () => {
    it('Vérifier les polices de caractères', () => {
        cy.mount<'div'>(html`
            <style>
                ${STYLES_DESIGN_SYSTEM}
            </style>
            <div>
                <p class="texte-petit">Texte petit</p>
                <p class="texte-petit-majuscule">Texte petit majuscule</p>
                <p class="texte-moyen">Texte moyen</p>
                <p class="texte-titre">Texte titre</p>
                <p class="texte-alternatif">Texte alternatif</p>
                <p class="titre-petit">Titre petit</p>
                <p class="titre-moyen">Titre moyen</p>
                <p class="titre-large">Titre large</p>
            </div>
        `);

        cy.get('.texte-petit') // yields <nav>
            .should('have.css', 'font-family') // yields 'sans-serif'
            .and('match', /font-family-open-sans/);
        cy.get('.texte-petit-majuscule') // yields <nav>
            .should('have.css', 'font-family') // yields 'sans-serif'
            .and('match', /font-family-open-sans/);
        cy.get('.texte-moyen') // yields <nav>
            .should('have.css', 'font-family') // yields 'sans-serif'
            .and('match', /font-family-open-sans/);
        cy.get('.texte-titre') // yields <nav>
            .should('have.css', 'font-family') // yields 'sans-serif'
            .and('match', /font-family-open-sans/);
        cy.get('.texte-alternatif') // yields <nav>
            .should('have.css', 'font-family') // yields 'sans-serif'
            .and('match', /font-family-open-sans/);
        cy.get('.titre-petit') // yields <nav>
            .should('have.css', 'font-family') // yields 'sans-serif'
            .and('match', /font-family-open-sans/);
        cy.get('.titre-moyen') // yields <nav>
            .should('have.css', 'font-family') // yields 'sans-serif'
            .and('match', /font-family-eigthies-comeback/);
        cy.get('.titre-large') // yields <nav>
            .should('have.css', 'font-family') // yields 'sans-serif'
            .and('match', /font-family-eigthies-comeback/);
    });
});
