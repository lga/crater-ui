import { fusionner } from '@lga/base';
import type { ApexOptions } from 'apexcharts';
import ApexCharts from 'apexcharts';
import { css, html, LitElement } from 'lit';
import { customElement, property, query } from 'lit/decorators.js';

import { APEX_FONT_STYLE_CONFIG, APEX_OPTIONS_GLOBALES } from './chart-config';
import { formaterLabelNombreEleve, ModeFiltrageSeries, type SerieMultiple } from './chart-outils';

export interface OptionsChartLignesDeuxAxesY {
    series: SerieMultiple;
    categoriesXaxis: number[];
    nomsSeriesYaxis: string[];
    titreYaxisGauche: string;
    titreYaxisDroite: string;
    couleurYaxisGauche: string;
    couleurYaxisGaucheBis: string;
    couleurYaxisDroite: string;
}

declare global {
    interface HTMLElementTagNameMap {
        'c-chart-lignes-2-axes-y': ChartLignesDeuxAxesY;
    }
}

@customElement('c-chart-lignes-2-axes-y')
export class ChartLignesDeuxAxesY extends LitElement {
    private chart?: ApexCharts;

    @property({ attribute: false })
    options?: OptionsChartLignesDeuxAxesY;

    @query('figure')
    private figure?: HTMLElement;

    static styles = css`
        figure {
            margin: auto;
            max-width: 600px;
        }
    `;

    render() {
        return html`
            <div>
                <figure></figure>
            </div>
        `;
    }

    async updated() {
        if (this.chart) {
            this.chart.updateOptions(this.calculerApexOptions(this.options), true, true);
        } else {
            this.chart = new ApexCharts(this.figure, this.calculerApexOptions(this.options));
            await this.chart.render();
            // Le premier render d'apex ne fonctionne pas dans lit (diagramme vide). Contournement : on attend la fin du render, et on fait un update
            this.chart.updateOptions(this.calculerApexOptions(this.options), true, true);
        }
    }

    private calculerApexOptions(options: OptionsChartLignesDeuxAxesY | undefined): ApexOptions {
        if (!options) return APEX_OPTIONS_GLOBALES;

        const apexOptionsLocales = {
            series: options.series.filtrerValeursNull(ModeFiltrageSeries.SUPPRIMER_SERIES_AVEC_AU_MOINS_UNE_VALEUR_NULL).versSerieApexSingleValue(),
            colors: [options.couleurYaxisGauche, options.couleurYaxisGaucheBis, options.couleurYaxisDroite],
            chart: {
                type: 'line',
                height: 400,
                stacked: false
            },
            stroke: {
                width: [3, 2, 3],
                dashArray: [0, 3, 0]
            },
            xaxis: {
                categories: options.categoriesXaxis
            },
            yaxis: [
                {
                    min: 0,
                    seriesName: options.nomsSeriesYaxis[0],
                    axisTicks: {
                        show: true
                    },
                    labels: {
                        formatter: (labelInitial: string) => formaterLabelNombreEleve(labelInitial)
                    },
                    title: {
                        text: options.titreYaxisGauche,
                        style: {
                            fontSize: APEX_FONT_STYLE_CONFIG.fontSize,
                            fontWeight: APEX_FONT_STYLE_CONFIG.fontWeight
                        }
                    }
                },
                {
                    seriesName: options.nomsSeriesYaxis[1],
                    axisTicks: {
                        show: false
                    },
                    labels: {
                        show: false
                    }
                },
                {
                    seriesName: options.nomsSeriesYaxis[2],
                    opposite: true,
                    min: 0,
                    axisTicks: {
                        show: true
                    },
                    labels: {
                        formatter: (labelInitial: string) => formaterLabelNombreEleve(labelInitial)
                    },
                    title: {
                        text: options.titreYaxisDroite,
                        style: {
                            fontSize: APEX_FONT_STYLE_CONFIG.fontSize,
                            fontWeight: APEX_FONT_STYLE_CONFIG.fontWeight
                        }
                    }
                }
            ],
            tooltip: {
                y: {
                    formatter: (labelInitial: string) => formaterLabelNombreEleve(labelInitial)
                }
            }
        };

        return fusionner(APEX_OPTIONS_GLOBALES, apexOptionsLocales);
    }
}
