import { type RouteLocationRaw, type RouteParams, useRoute } from 'vue-router/auto';

export function useRouteMonTerritoire(idTerritoire: string): RouteLocationRaw<'idTerritoire'> {
    const route: RouteLocationRaw<'idTerritoire'> = {
        name: 'idTerritoire',
        params: { idTerritoire }
    };
    return route;
}

export function useRouteParamsMonTerritoire(): RouteParams<'idTerritoire'> {
    return useRoute().params as RouteParams<'idTerritoire'>;
}
