import type { IdTheme } from '@lga/indicateurs';

export interface ThematiqueModele {
    idTheme: IdTheme;
}
