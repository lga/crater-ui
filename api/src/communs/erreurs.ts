export class ErreurRessourceNonTrouvee extends Error {}

export class ErreurRequeteIncorrecte extends Error {}

export class ErreurTemporairementIndisponible extends Error {}

export class ErreurTechnique extends Error {}
