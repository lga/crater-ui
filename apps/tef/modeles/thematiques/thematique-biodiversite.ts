import { formaterNombreEnEntierString, formaterNombreSelonValeurString } from '@lga/base';
import { INDICATEURS, type ValeurIndicateur } from '@lga/indicateurs';

import type { DonneesApi } from '../fonctions-utils.js';
import type { ThematiqueModele } from './ThematiqueModele.js';

export interface ThematiqueBiodiversiteModele extends ThematiqueModele {
    indicateurHVN: ValeurIndicateur;
    indicateurNodu: ValeurIndicateur;
    indicateurPartBio: ValeurIndicateur;
}

export function creerThematiqueBiodiversite(donneesApi: DonneesApi): ThematiqueBiodiversiteModele {
    return {
        idTheme: 'biodiversite',
        indicateurHVN: {
            indicateur: INDICATEURS.hvn,
            chiffre: formaterNombreEnEntierString(donneesApi.diagnosticApi.pratiquesAgricoles.indicateurHvn.indiceTotal),
            unite: '/30'
        },
        indicateurNodu: {
            indicateur: INDICATEURS.noduNormalise,
            chiffre: formaterNombreSelonValeurString(donneesApi.diagnosticApi.pesticides.noduNormalise),
            unite: 'traitements à la dose maximale',
            chiffreComplement: 'en moyenne par an.'
        },
        indicateurPartBio: {
            indicateur: INDICATEURS.partSAUBio,
            chiffre: formaterNombreSelonValeurString(donneesApi.diagnosticApi.pratiquesAgricoles.partSauBioPourcent),
            unite: '%',
            chiffreComplement: 'de surfaces en Agriculture Biologique ou en conversion.'
        }
    };
}
