import { supprimerEspacesRedondantsEtRetoursLignes } from '@lga/base';
import { ICONES_HTML } from '@lga/indicateurs';
import { describe, expect, it } from 'vitest';

import { Note } from '../Note';
import {
    calculerMessagePartPopulationDependanteVoiture,
    calculerMessagePartTerritoireDependantVoiture,
    calculerMessageSynthese
} from './messages-transformation-distribution';

describe('Tests des fonctions de calcul des messages de Proxmité aux commerces', () => {
    it('Calculer les messages de synthese', () => {
        expect(supprimerEspacesRedondantsEtRetoursLignes(calculerMessageSynthese(new Note(null), 10, 20))).toBe(
            supprimerEspacesRedondantsEtRetoursLignes(`Données non disponibles (voir les échelles supérieures telles que l'EPCI)`)
        );
        expect(supprimerEspacesRedondantsEtRetoursLignes(calculerMessageSynthese(new Note(1), 10, 20))).toBe(
            supprimerEspacesRedondantsEtRetoursLignes(
                `<strong>10&nbsp;% de la population</strong> est théoriquement dépendante de la voiture pour ses achats alimentaires.`
            )
        );
    });

    it('Calculer les messages PartPopulationDependanteVoiture', () => {
        expect(supprimerEspacesRedondantsEtRetoursLignes(calculerMessagePartPopulationDependanteVoiture('NomEPCI', null, 20))).toBe(
            supprimerEspacesRedondantsEtRetoursLignes('Les données sont indisponibles pour ce territoire.')
        );
        expect(supprimerEspacesRedondantsEtRetoursLignes(calculerMessagePartPopulationDependanteVoiture('NomEPCI', 10, 20))).toBe(
            supprimerEspacesRedondantsEtRetoursLignes(
                `
            Sur le territoire <em>NomEPCI</em>, 
            <strong>10&nbsp;% de la population</strong> est théoriquement dépendante de la voiture pour ses achats alimentaires.
            <br/>En France ${ICONES_HTML.drapeau_français}, c'est 20&nbsp;% de la population.
            `
            )
        );
    });

    it('Calculer les messages PartTerritoireDependantVoiture', () => {
        expect(supprimerEspacesRedondantsEtRetoursLignes(calculerMessagePartTerritoireDependantVoiture('NomEPCI', 'EPCI', null, 20))).toBe(
            supprimerEspacesRedondantsEtRetoursLignes(
                `
                Les données sont indisponibles pour ce territoire.
                `
            )
        );
        expect(supprimerEspacesRedondantsEtRetoursLignes(calculerMessagePartTerritoireDependantVoiture('NomEPCI', 'EPCI', 10, 20))).toBe(
            supprimerEspacesRedondantsEtRetoursLignes(
                `
                Dans <strong>10&nbsp;% des communes du territoire</strong>, 
                plus de la moitié de la population est théoriquement dépendante de la voiture pour ses achats alimentaires. 
                <br/>En France ${ICONES_HTML.drapeau_français}, c'est 20&nbsp;% des communes qui sont dans cette situation.
            `
            )
        );
    });
});
