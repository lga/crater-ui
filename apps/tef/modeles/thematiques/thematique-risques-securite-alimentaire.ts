import type { ThematiqueModele } from './ThematiqueModele.js';

export function creerThematiqueRisquesSecuriteAlimentaire(): ThematiqueModele {
    return {
        idTheme: 'risques-securite-alimentaire'
    };
}
