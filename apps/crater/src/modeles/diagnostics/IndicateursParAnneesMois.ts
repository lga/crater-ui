import { IndicateursParAnnees } from './IndicateursParAnnees';

interface IndicateurAnneeMoisN {
    anneeMois: Date;
}

export const JUILLET = 6;
export const AOUT = 7;

export class IndicateursParAnneesMois<I extends IndicateurAnneeMoisN> {
    private listeIndicateursParAnneesMois: I[] = [];

    constructor(indicateursParAnneesMois: I[]) {
        this.listeIndicateursParAnneesMois = indicateursParAnneesMois.sort((ia1, ia2) => {
            return ia1.anneeMois.getTime() - ia2.anneeMois.getTime();
        });
    }

    get anneesMois(): Date[] {
        return this.listeIndicateursParAnneesMois.map((ia) => ia.anneeMois);
    }

    valeurs(getterAttribut: (ia: I) => number | null): (number | null)[] {
        return this.listeIndicateursParAnneesMois.map((ia) => getterAttribut(ia));
    }

    valeursMoyennesParAnnees(
        moisAConserver: number[],
        getterAttribut: (ia: I) => number | null,
        anneeMin?: number,
        anneeMax?: number
    ): IndicateursParAnnees<{ annee: number; valeurMoyenne: number | null }> {
        const sommesParAnnees = new Map<number, number | null>();
        const estMoisAConserver = (iam: IndicateurAnneeMoisN) => moisAConserver.includes(iam.anneeMois.getMonth());
        this.listeIndicateursParAnneesMois.forEach((iam) => {
            if (estMoisAConserver(iam)) {
                const annee = iam.anneeMois.getFullYear();
                const valeur = getterAttribut(iam);
                const valeurPrecedente = sommesParAnnees.get(annee);
                if (valeurPrecedente === undefined) {
                    sommesParAnnees.set(annee, valeur);
                } else if (valeurPrecedente === null || valeur === null) {
                    sommesParAnnees.set(annee, null);
                } else {
                    sommesParAnnees.set(annee, valeurPrecedente + valeur);
                }
            }
        });

        const anneeMinCalculee = anneeMin ? anneeMin : Math.min(...sommesParAnnees.keys());
        const anneeMaxCalculee = anneeMax ? anneeMax : Math.max(...sommesParAnnees.keys());
        const moyennesParAnnees: { annee: number; valeurMoyenne: number | null }[] = [];
        for (let annee = anneeMinCalculee; annee <= anneeMaxCalculee; annee++) {
            if (sommesParAnnees.has(annee)) {
                const valeurMoyenne = sommesParAnnees.get(annee) === null ? null : sommesParAnnees.get(annee)! / moisAConserver.length;
                moyennesParAnnees.push({ annee: annee, valeurMoyenne: valeurMoyenne });
            } else {
                moyennesParAnnees.push({ annee: annee, valeurMoyenne: 0 });
            }
        }
        return new IndicateursParAnnees<{ annee: number; valeurMoyenne: number | null }>(moyennesParAnnees);
    }
}
