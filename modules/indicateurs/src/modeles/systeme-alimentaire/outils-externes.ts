import { OutilExterne } from '../../modeles/systeme-alimentaire/OutilExterne';

export const IDS_OUTILS_EXTERNES = {
    tableauDeBord: 'Tableau-de-bord',
    parcel: 'PARCEL',
    parcelPourTerresAgricoles: 'PARCEL-Terres-Agricoles',
    parcelPourAgriculteursEtExploitations: 'PARCEL-Agriculteurs-Exploitations',
    parcelPourProduction: 'PARCEL-Production',
    parcelPourConsommation: 'PARCEL-Consommation',
    cartofriches: 'CARTOFRICHES',
    climagri: 'Climagri',
    geoportail: 'Geoportail',
    osae: 'OSAE',
    territoiresBio: 'Territoires bio',
    ObSAT: 'ObSAT',
    transiscope: 'Transiscope',
    etudeIDDRI: 'Etude IDDRI'
};

export const DONNEES_OUTILS_EXTERNES: OutilExterne[] = [
    new OutilExterne(
        IDS_OUTILS_EXTERNES.tableauDeBord,
        `Tableau de bord de la résilience alimentaire`,
        `Développer et piloter une stratégie de résilience alimentaire`,
        `Quelle stratégie de résilience alimentaire ?`,
        'https://resiliencealimentaire.org/tableau-de-bord/'
    ),
    new OutilExterne(
        IDS_OUTILS_EXTERNES.cartofriches,
        `CARTOFRICHES`,
        `Outil d'aide au recensement à l’échelle nationale des friches (industrielles, commerciales, d’habitat, tertiaires, etc.) développé par le CEREMA`,
        'Quelles friches sur mon territoire ?',
        'https://cartofriches.cerema.fr/cartofriches/'
    ),
    new OutilExterne(
        IDS_OUTILS_EXTERNES.climagri,
        `Démarche Climagri`,
        `Réaliser un diagnostic des consommations d’énergie et des émissions de gaz à effet de serre des secteurs agricole et forestier à l’échelle territoriale`,
        `Quelle consommation fine d'énergie pour la production agricole de mon territoire ?`,
        'https://agirpourlatransition.ademe.fr/entreprises/aides-financieres/2024/demarches-climagrir-diagnostic-animation'
    ),
    new OutilExterne(
        IDS_OUTILS_EXTERNES.geoportail,
        `Géoportail de l'IGN`,
        `Cartographie des données du Récensement Parcellaire Graphique de 2018`,
        'Où et que cultive-t-on sur mon territoire ?',
        'https://www.geoportail.gouv.fr/donnees/registre-parcellaire-graphique-rpg-2018'
    ),
    new OutilExterne(
        IDS_OUTILS_EXTERNES.parcel,
        `PARCEL`,
        `Simulateur d'empreintes alimentaires et agricoles`,
        'Quels effets de changements de régime alimentaire ou pratiques par exemple ?',
        'https://parcel-app.org/'
    ),
    new OutilExterne(
        IDS_OUTILS_EXTERNES.parcelPourTerresAgricoles,
        `PARCEL`,
        `Simulateur d'empreintes alimentaires et agricoles`,
        'Quelles surfaces agricoles sont nécessaires selon différents scénarios d’évolution ?',
        'https://parcel-app.org/resultats-pour-lalimentation-relocalisee?idterritoire=1'
    ),
    new OutilExterne(
        IDS_OUTILS_EXTERNES.parcelPourAgriculteursEtExploitations,
        `PARCEL`,
        `Simulateur d'empreintes alimentaires et agricoles`,
        'Quels emplois agricoles sont nécessaires selon différents scénarios d’évolution ?',
        'https://parcel-app.org/emplois-crees-par-la-relocalisation?idterritoire=1'
    ),
    new OutilExterne(
        IDS_OUTILS_EXTERNES.parcelPourProduction,
        `PARCEL`,
        `Simulateur d'empreintes alimentaires et agricoles`,
        `Quels effets d’une relocalisation ou de la modification des pratiques agricoles ?`,
        'https://parcel-app.org/impacts-ecologiques-de-la-relocatisation?idterritoire=1'
    ),
    new OutilExterne(
        IDS_OUTILS_EXTERNES.parcelPourConsommation,
        `PARCEL`,
        `Simulateur d'empreintes alimentaires et agricoles`,
        `Quels effets d'un changement de régime alimentaire ?`,
        'https://parcel-app.org/resultats-de-votre-relocalisation?idterritoire=1'
    ),
    new OutilExterne(
        IDS_OUTILS_EXTERNES.osae,
        `OSAE, osez l'agroécologie`,
        `Site internet de SOLAGRO`,
        `En savoir plus sur l'agroécologie :`,
        'https://osez-agroecologie.org/'
    ),
    new OutilExterne(
        IDS_OUTILS_EXTERNES.territoiresBio,
        `Territoires bio`,
        `Plateforme dédiée aux collectivités locales engagées en faveur de la transition agricole et alimentaire, développée par la FNAB`,
        'En apprendre plus sur la bio :',
        'https://territoiresbio.fr/'
    ),
    new OutilExterne(
        IDS_OUTILS_EXTERNES.ObSAT,
        `Observatoire des Systèmes Alimentaires Territorialisés (ObSAT)`,
        `Pour identifier les modes de distribution en circuit court et les magasins vendant des produits locaux`,
        'Quelle place des circuits courts et/ou de proximité sur mon territoire ?',
        'https://obsat.org/?ObSAT'
    ),
    new OutilExterne(
        IDS_OUTILS_EXTERNES.transiscope,
        `Transiscope`,
        `Pour identifier notamment des commerces durables`,
        'Quels commerces durables sur mon territoire ?',
        'https://transiscope.org/carte-des-alternatives/'
    ),
    new OutilExterne(
        IDS_OUTILS_EXTERNES.etudeIDDRI,
        `Étude de l'IDDRI`,
        `L’empreinte énergétique et carbone de l’alimentation en France - de la production à la consommation, voir en particulier la partie relative aux déplacements des ménages pour leurs achats et la restauration.`,
        `Quelle est l'empreinte énergétique et carbone de l’alimentation en France ?`,
        'https://www.iddri.org/fr/publications-et-evenements/rapport/lempreinte-energetique-et-carbone-de-lalimentation-en-france-de'
    )
];
