#!/bin/bash

echo "Execution  de : 'vider-cache-apache.sh $1 $2'"
echo "  -> Suppression et recréation du dossier de cache apache pour l'application $1 sur l'environnement $2"
if [ $# -ne 2 ]; then
  echo 1>&2 "$0: Erreur, merci de renseigner les 2 paramètres obligatoires : NOM_APP (ex : api ou tef), et ENV_CIBLE_BUILD (prod ou dev)"
  exit 2
fi

NOM_APP=$1
ENV_CIBLE_BUILD=$2

if [ $ENV_CIBLE_BUILD != "dev" ] && [ $ENV_CIBLE_BUILD != "prod" ]; then
  echo 1>&2 "$0: Erreur, la valeur du paramètre ENV_CIBLE_BUILD doit être 'dev' ou 'prod'"
  exit 2
fi

## Reset dossier de cache apache a chaque relivraison de l'app
echo "  -> emplacement du dossier : /var/cache/apache2/mod_cache_disk/$NOM_APP-$ENV_CIBLE_BUILD"
sudo rm -rf /var/cache/apache2/mod_cache_disk/$NOM_APP-$ENV_CIBLE_BUILD
sudo mkdir -p /var/cache/apache2/mod_cache_disk/$NOM_APP-$ENV_CIBLE_BUILD
sudo chown -R www-data:www-data /var/cache/apache2/mod_cache_disk/$NOM_APP-$ENV_CIBLE_BUILD