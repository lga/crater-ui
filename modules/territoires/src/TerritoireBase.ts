import type { BoundingBoxApi, CodeGenreNombreTerritoireApi, CodePrepositionTerritoireApi } from '@lga/specification-api';

import type { CategorieTerritoire } from './CategorieTerritoire';
import { Territoire } from './Territoire';
import { calculerNomAvecDeterminant, calculerNomAvecPrepositionA, calculerNomAvecPrepositionDe } from './territoires-outils.js';

export class TerritoireBase extends Territoire {
    constructor(
        public readonly id: string,
        public readonly nom: string,
        public readonly categorie: CategorieTerritoire,
        public readonly genreNombre: CodeGenreNombreTerritoireApi,
        public readonly prepositon: CodePrepositionTerritoireApi,
        public readonly boundingBox: BoundingBoxApi
    ) {
        super(id, nom, categorie);
    }

    get nomTerritoireAvecDeterminant(): string {
        return calculerNomAvecDeterminant(this.nom, this.genreNombre, this.prepositon);
    }

    get nomTerritoireAvecPrepositionA(): string {
        return calculerNomAvecPrepositionA(this.nom, this.genreNombre, this.prepositon);
    }

    get nomTerritoireAvecPrepositionDe(): string {
        return calculerNomAvecPrepositionDe(this.nom, this.genreNombre, this.prepositon);
    }
}
