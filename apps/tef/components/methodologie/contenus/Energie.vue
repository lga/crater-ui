<template>
    <div :id="IDS_DOMAINES.energie">
        <TitreH3>Les indicateurs de dépendance à l'énergie</TitreH3>

        <TitreH4>Pourquoi des indicateurs sur la consommation d'énergie du secteur agricole&nbsp;?</TitreH4>
        <TexteP
        >Un système alimentaire, considéré du point de vue de l’énergie, est un système qui fournit aux humains de l’énergie contenue dans les
            produits alimentaires grâce aux transformations de matières permises par l’énergie solaire (photosynthèse) et l’énergie utilisée à chaque
            étape du processus de production, transformation et distribution des produits alimentaires.</TexteP
        >
        <TexteP
        >Cette dernière peut avoir des sources différentes : le carburant pour les machines agricoles et les camions de transport, l’électricité
            pour le chauffage, l’éclairage des bâtiments d’élevage, etc.</TexteP
        >
        <TexteP
        >Aujourd’hui, ces sources d’énergie sont essentiellement fossiles. Cette situation est la conséquence de la révolution agricole des pays
            du Nord au tournant des années 60. Elles sont passées d’une origine animale (traction animale, force humaine) à fossile, grâce à la
            disponibilité en pétrole et en gaz et le développement conjoint du machinisme agricole et des engrais de synthèse.</TexteP
        >
        <TexteP
        >Constatant depuis des années la volatilité des prix des énergies fossiles, et n’ayant pour seule perspective leur raréfaction générale,
            le développement d’un indicateur de dépendance pour le système alimentaire français paraît nécessaire tant pour évaluer la menace que pour
            sensibiliser à cet enjeu majeur. Les conséquences lourdes de la guerre en Ukraine sur les approvisionnements en engrais et ses
            prolongements dans les prix des denrées agricoles montrent que la dépendance énergétique du secteur agricole est bel et bien un enjeu de
            sécurité alimentaire. D’ici 2050, la production de pétrole des principaux fournisseurs de l’UE aura baissé de moitié (source : rapport The
            Shift Project 2022). Il faut donc s’attendre à une diminution sérieuse des approvisionnements.</TexteP
        >

        <TitreH4>Périmètres des indicateurs</TitreH4>
        <TexteP
        >Les postes d’énergie suivant sont estimés :
            <ul>
                <li>
                    Énergie directe : carburants pour les machines agricoles, chauffage des bâtiments d’élevage, chauffage des serres, irrigation.
                </li>
                <li>
                    Énergies indirectes : production du matériel agricole, des engrais azotés, de l’alimentation importée de l'étranger pour le
                    bétail.
                </li>
            </ul>
        </TexteP>
        <TexteP
        >L'énergie est dite indirecte quand elle concerne un produit qui n'est pas directement utilisé comme source d'énergie – un carburant par
            exemple – mais en a nécéssité pour le produire. Les engrais azotés de synthèse sont ainsi une énergie indirecte et désignent l'énergie
            qu'il a fallu pour les produire.</TexteP
        >
        <TexteP
        >Ces postes couvrent 90% de la consommation totale à l'échelle de la France. Les 10% restants rassemblent par exemple le séchage et la
            conservation des produits agricoles ou la production des produits phytosanitaires.</TexteP
        >
        <TexteP
        >Les résultats sont exprimés en énergie primaire (EP) car cela permet de comparer et sommer les énergies directe et indirecte (qui est
            forcément exprimée sous forme d’énergie primaire).</TexteP
        >

        <TitreH4>Principe de calcul des indicateurs</TitreH4>
        <TexteP
        >Le calcul des différents postes de consommation d'énergie est largement adaptée de la
            <TexteLien
                url="https://librairie.ademe.fr/produire-autrement/3486-climagri-un-diagnostic-energie-gaz-a-effet-de-serre-au-service-d-une-demarche-de-territoire.html"
            >méthodologie Climagri</TexteLien
            >
            développée par l'<TexteLien url="https://www.ademe.fr">ADEME</TexteLien>. Cette adaptation a été réalisée avec l'aide précieuse de
            <TexteLien url="https://solagro.org/">SOLAGRO</TexteLien>.</TexteP
        >

        <TitreH4>Carburants pour les tracteurs</TitreH4>

        <TitreH5>Données d'entrée</TitreH5>
        <TexteP>
            <ul>
                <li>Surfaces agricoles utiles issues du <LienMethodologieSourceDonnee :sourceDonnee="SOURCES_DONNEES.rpg" /> [ha]</li>
                <li>
                    Coefficients de consommation énergétique par catégories de cultures issus de
                    <LienMethodologieSourceDonnee :sourceDonnee="SOURCES_DONNEES.climagri" /> [L/ha]
                </li>
                <li>
                    Coefficient de conversion d’un litre de carburant en énergie primaire issus de
                    <LienMethodologieSourceDonnee :sourceDonnee="SOURCES_DONNEES.climagri" /> [GJ/L]
                </li>
            </ul>
        </TexteP>

        <TitreH5>Méthode de calcul</TitreH5>
        <TexteFormule>
            <ol>
                <li>
                    Calcul de la consommation de carburant pour chaque surface de culture :
                    <br />consommation [L] = SAU de la culture [ha] * coefficient de consommation [L/ha]
                </li>
                <li>Agrégation par communes</li>
                <li>
                    Conversion en énergie primaire en utilisant le coefficient de conversion :
                    <br />énergie [GJ] = consommation [L] * coefficient de conversion [GJ/L]
                </li>
                <li>Agrégation aux échelons supérieurs</li>
            </ol>
        </TexteFormule>

        <TitreH5>Limites</TitreH5>
        <TexteP
        >Les pratiques culturales (labour, semis direct...) ne sont pas prises en compte. Les coefficients utilisés reflètent donc des pratiques
            moyennes en France.</TexteP
        >

        <TitreH4>Matériel agricole</TitreH4>

        <TexteP
        >Le principe de calcul est exactement le même que pour les carburants des tracteurs en utilisant d'autres coefficients de consommation
            énergétique.</TexteP
        >

        <TitreH4>Irrigation</TitreH4>

        <TitreH5>Données d'entrée</TitreH5>
        <TexteP>
            <ul>
                <li>
                    <TexteLien :url="useRouteMethodologieIntrants(IDS_DOMAINES.eau)"
                    >Prélèvements annuels d’eau utilisés en irrigation non gravitaire par territoires [m³]</TexteLien
                    >
                </li>
                <li>
                    Coefficient de consommation énergétique moyen de l’irrigation issu de
                    <LienMethodologieSourceDonnee :sourceDonnee="SOURCES_DONNEES.climagri" /> [kWh/m³]
                </li>
                <li>
                    Coefficients de répartition par sources d’énergie (électricité vs fioul) issus de
                    <LienMethodologieSourceDonnee :sourceDonnee="SOURCES_DONNEES.climagri" /> [%]
                </li>
                <li>
                    Coefficients de conversion des sources d’énergie entre énergies finale et primaire issus de
                    <LienMethodologieSourceDonnee :sourceDonnee="SOURCES_DONNEES.climagri" /> [GJ/GJ]
                </li>
            </ul>
        </TexteP>

        <TitreH5>Méthode de calcul</TitreH5>
        <TexteFormule>
            <ol>
                <li>
                    Calcul de la consommation énergétique finale :
                    <br />consommation [GJ] = volume_irrigation_hors_gravitaire [m³] * coefficient_consommation_energetique_irrigation_hors_gravitaire
                    [kWh/m³] * coefficient_conversion_energie [GJ/kWh]
                </li>
                <li>Ventilation par sources d’énergie</li>
                <li>Conversion en énergie primaire</li>
            </ol>
        </TexteFormule>

        <TitreH5>Limites</TitreH5>
        <TexteP
        >Il n’y a pas de données de répartition des systèmes d’irrigation à l’échelle de la France ou plus fin, et pas de corrélation évidente
            avec les cultures. En conséquence on utilise un coefficient qui correspond à la moyenne France, calculé sur base des volumes d’irrigation
            totaux par types d’irrigation hors gravitaire estimés dans le diagnostic Climagri France.</TexteP
        >

        <TitreH4>Chauffage des serres</TitreH4>
        <TexteP
        >En France, les cultures sous serres chauffées concernent surtout une partie des surfaces de tomates, de concombres, de fraises, de melons
            et de l’horticulture. On distingue essentiellement deux types de serres chauffées : les serres en maraîchage (utilisées surtout pour les
            tomates et concombres) qui consomment entre 200 et 400 kWh/m² et les tunnels hors gel (utilisés surtout pour les fraises, melons) qui
            consomment plutôt autour de 25 kWh/m². Seule l’énergie utilisée pour le chauffage des serres en maraîchage pour les tomates et concombres
            est évaluée, faute de données disponibles pour les fraises, melons et l’horticulture. On estime que la consommation énergétique de ces
            derniers représente une part inférieure à 20% du total (source : diagnostic Climagri France).</TexteP
        >

        <TitreH5>Données d'entrée</TitreH5>
        <TexteP>
            <ul>
                <li>
                    Surfaces de serres chauffées pour les tomates et concombres par départements issues de l'<LienMethodologieSourceDonnee
                        :sourceDonnee="SOURCES_DONNEES.etude_serres"
                    />
                    [m²]
                </li>
                <li><LienMethodologieSurfacesAgricoles libelle="surfaces agricoles productives"></LienMethodologieSurfacesAgricoles> [ha]</li>
                <li>
                    Consommations moyennes des serres (chauffage + électricité) par zones géographiques issus de l'<LienMethodologieSourceDonnee
                        :sourceDonnee="SOURCES_DONNEES.etude_serres"
                    />
                    [kWh/m²]
                </li>
                <li>
                    Coefficients de conversion des sources d’énergie entre énergies finale et primaire issus de
                    <LienMethodologieSourceDonnee :sourceDonnee="SOURCES_DONNEES.climagri" /> [GJ/GJ]
                </li>
            </ul>
        </TexteP>

        <TitreH5>Méthode de calcul</TitreH5>
        <TexteFormule>
            <ol>
                <li>
                    Calcul de la consommation énergétique pour chaque source d’énergie au niveau départemental&nbsp;:
                    <br />consommation [GJ] = surface_serre [ha] * 10 000 [m²/ha] * coefficient_consommation_energetique_finale [kWh/m²] *
                    coefficient_conversion_energie [GJ/kWh]
                </li>
                <li>Conversion en énergie primaire</li>
                <li>Calcul aux échelons supérieurs (pas de répartition infra-départementale)</li>
            </ol>
        </TexteFormule>
        <TitreH5>Limites</TitreH5>
        <TexteP>L’estimation ne concerne que les concombres et tomates et n’est pas disponible à l'échelle infra-départementale.</TexteP>

        <TitreH4>Bâtiments d’élevage : chauffage & climatisation, électricité et fioul hors chauffage</TitreH4>

        <TitreH5>Données d'entrée</TitreH5>
        <TexteP>
            <ul>
                <li><LienMethodologieCheptels /> [têtes]</li>
                <li>
                    Coefficients de consommation énergétique pour le chauffage des bâtiments par catégories d’animaux issus de
                    <LienMethodologieSourceDonnee :sourceDonnee="SOURCES_DONNEES.climagri" /> [kWh/tête]
                </li>
                <li>
                    Coefficients définissant le mix énergétique pour le chauffage des bâtiments par catégories d’animaux issus de
                    <LienMethodologieSourceDonnee :sourceDonnee="SOURCES_DONNEES.climagri" /> [%]
                </li>
                <li>
                    Coefficients de consommation énergétique d'électricité des bâtiments d’élevage par catégories d’animaux issus de
                    <LienMethodologieSourceDonnee :sourceDonnee="SOURCES_DONNEES.climagri" /> [kWh/tête]
                </li>
                <li>
                    Coefficients définissant le mix électrique France issus de
                    <LienMethodologieSourceDonnee :sourceDonnee="SOURCES_DONNEES.climagri" /> [%]
                </li>
                <li>
                    Coefficients de consommation énergétique de fioul des bâtiments d’élevage pour chaque catégorie d’animaux issus de
                    <LienMethodologieSourceDonnee :sourceDonnee="SOURCES_DONNEES.climagri" /> [L/tête/jour]
                </li>
                <li>
                    Temps de présence moyen en bâtiment d’élevage pour chaque catégorie d’animaux issus de
                    <LienMethodologieSourceDonnee :sourceDonnee="SOURCES_DONNEES.climagri" /> [jour]
                </li>
                <li>
                    Coefficients de conversion des énergies finale vers primaire issus de
                    <LienMethodologieSourceDonnee :sourceDonnee="SOURCES_DONNEES.climagri" />
                </li>
            </ul>
        </TexteP>

        <TitreH5>Méthode de calcul de la consommation énergétique du chauffage</TitreH5>
        <TexteFormule>
            <ol>
                <li>
                    Conversion des données de cheptel Agreste en données de cheptel Climagri. Pour cela, on utilise la répartition fine des
                    différentes catégories d'animaux du RA 2020 (total France métropolitaine) pour reconstituer chaque catégorie Climagri à partir
                    d’une ou plusieurs catégories Agreste.
                </li>
                <li>
                    Calcul de la consommation énergétique totale pour chaque catégorie d’animaux Climagri par multiplication des cheptels avec les
                    coefficients d’énergie
                </li>
                <li>Ventilation par source d’énergie en utilisant les coefficients de mix énergétique</li>
                <li>Conversion en énergie primaire en utilisant les coefficients de conversion</li>
            </ol>
        </TexteFormule>

        <TitreH5>Méthode de calcul de la consommation d'électricité hors chauffage (machines de traite et autres machines, éclairage...)</TitreH5>
        <TexteFormule>
            Même principe que précédemment en utilisant les coefficients de consommation énergétique pour l’électricité en bâtiments d’élevage et le
            mix électrique de la France.
        </TexteFormule>

        <TitreH5
        >Méthode de calcul de la consommation de fioul des bâtiments d’élevage (carburant consommé par les véhicules, notamment pour le transport
            de la nourriture)</TitreH5
        >
        <TexteFormule>
            Même principe que précédemment en utilisant les coefficients d’énergie pour la consommation de fioul des bâtiments d’élevage et les temps
            de présence moyens des animaux.
        </TexteFormule>

        <TitreH4>Engrais azotés</TitreH4>

        <TitreH5>Données d'entrée</TitreH5>
        <TexteP>
            <ul>
                <li>
                    <LienMethodologieFluxAzote libelle="Besoins en azote minéral de synthèse pour chaque commune obtenu par le modèle MacDyn-Fs" />
                    [kgN/an]
                </li>
                <li><LienMethodologieSourceDonnee :sourceDonnee="SOURCES_DONNEES.ventes_engrais_azotes_synthese" /> par types d'engrais [tN/an]</li>
                <li>
                    Coefficients de consommation énergétique par types d’engrais issus de
                    <LienMethodologieSourceDonnee :sourceDonnee="SOURCES_DONNEES.climagri" /> [GJ/tN]
                </li>
            </ul>
        </TexteP>

        <TitreH5>Méthode de calcul</TitreH5>
        <TexteFormule>
            <ol>
                <li>
                    Calcul d’un coefficient de coût énergétique moyen de fabrication des fertilisants azotés (tous types confondus) [GJ_EP/tN] :
                    <ul>
                        <li>
                            Calcul de la part de chaque type d’engrais dans le total des ventes en utilisant les statistiques globales au niveau
                            France (pour s’affranchir des biais liés à la différence entre lieux d’achat et lieux d’utilisation) et en lissant les
                            résultats sur 3 ans (pour réduire l’effet de stock) [tN]
                        </li>
                        <li>Calcul de la moyenne des coûts énergétiques pondérée par les tonnages par types d’engrais [GJ/tN]</li>
                    </ul>
                </li>
                <li>
                    Calcul du coût énergétique total par communes par multiplication des besoins en azote minéral de la commune avec le coût
                    énergétique moyen [GJ]
                </li>
                <li>Calcul aux échelons supérieurs</li>
            </ol>
        </TexteFormule>

        <TitreH4>Alimentation animale importée de l’étranger</TitreH4>

        <TitreH5>Données d'entrée</TitreH5>
        <TexteP>
            <ul>
                <li>
                    <LienMethodologieFluxAzote
                        libelle="Importations nettes de nourriture par communes et par groupes d’aliments provenant du modèle MacDyn-FS"
                    />
                    [kgN]
                </li>
                <li>
                    Importations nationales de nourriture de l’étranger par types d’aliments (FAOSTAT - échelle France - valeur de 2020) [tonnes de
                    matière brute, tMB]
                </li>
                <li>
                    Coefficients de consommation énergétique liée à la production de nourriture animale à l’étranger par types d’aliments issus de
                    <LienMethodologieSourceDonnee :sourceDonnee="SOURCES_DONNEES.climagri" /> [GJ/tN]
                </li>
                <li>
                    Importations nationales de nourriture de l’étranger par types d’aliments (FAOSTAT - échelle France - valeur de 2020) [tonnes de
                    matière brute, tMB]
                </li>
                <li>
                    Correspondances entre les différentes typologies d’aliments : classifications EFESE (groupes d’aliments: fourrages, concentrés
                    énergétiques, concentrés protéiques), Climagri et FAOSTAT (types d’aliments: colza grain, tourteaux de soja…)
                </li>
            </ul>
        </TexteP>

        <TitreH5>Méthode de calcul</TitreH5>
        <TexteFormule>
            <ol>
                <li>
                    Estimation de l’importation nette de nourriture animale depuis l’étranger en azote au niveau des communes par groupes d’aliments
                    [tN] :
                    <ul>
                        <li>
                            Pour cela, on calcule le total France des importations nettes de nourriture des communes par groupes d’aliments = solde
                            d’importation
                        </li>
                        <li>
                            Pour chacun des groupes d’aliments, si le solde est positif (ie que l’on a besoin d’importer de l’étranger), on calcule le
                            même total que précédemment mais pour les communes dont les importations sont positives = total d’importation
                        </li>
                        <li>
                            On en déduit la part des importations nettes des communes importatrices qui provient de l’étranger : part = solde
                            d’importation / total d’importation
                        </li>
                        <li>Cette part est appliquée aux importations nettes pour obtenir les importations nettes depuis l’étranger</li>
                    </ul>
                </li>
                <li>
                    Estimation des coefficients de consommation énergétique liée à la production de nourriture animale importée de l’étranger par
                    groupes d’aliments [GJ/tN]:
                    <ul>
                        <li>
                            À partir des teneurs en azote par quantité de matière brute : on convertit les coefficients de consommation énergétique en
                            GJ/tN et les importations nationales de nourriture de l’étranger par types d’aliments en tN
                        </li>
                        <li>
                            On calcule finalement les coefficients énergétiques par groupes d’aliments en faisant la moyenne des coefficients par
                            types d’aliments, pondérée par la part du type d’aliment dans le groupe d’aliment
                        </li>
                    </ul>
                </li>
                <li>
                    Estimation de la consommation énergétique liée à l’import de nourriture animale au niveau des communes par multiplication des
                    importations nettes de nourriture animale depuis l’étranger par les coefficients énergétiques [GJ]
                </li>
                <li>Calcul aux échelons supérieurs</li>
            </ol>
        </TexteFormule>

        <TitreH4>Calcul de l'indicateur Consommation d'énergie primaire de l'agriculture par hectare agricole</TitreH4>
        <TexteP>
            Cette indicateur est calculé sur base de l'ensemble des consommations calculées comme décrit ci-dessus :
            <TexteFormule>
                consommation_energetique_primaire_par_hectare [GJ/ha] = ∑ consommations_energetiques_par_postes [GJ] / surface_agricole_utile [ha]
            </TexteFormule>
        </TexteP>

        <TitreH4>Calcul de la note globale pour la dépendance à l'énergie</TitreH4>
        <TexteP
        >La note est calculée à partir de l'indicateur consommation_energetique_primaire_par_hectare. La distribution des valeurs de cet
            indicateur pour les départements permet d'obtenir des seuils de référence : le minimum (min), le premier quartile (Q1), la médiane (Q2),
            et le troisième quartile (Q3). À partir de ces seuils les notes sont calculées comme suit :
            <TexteFormule>
                <div>si consommation_energetique_primaire_par_hectare = min alors note = 10</div>
                <div>si consommation_energetique_primaire_par_hectare = Q1 alors note = 7.5</div>
                <div>si consommation_energetique_primaire_par_hectare = Q2 alors note = 5</div>
                <div>si consommation_energetique_primaire_par_hectare = Q3 alors note = 2.5</div>
                <div>si consommation_energetique_primaire_par_hectare >= Q3 + 1.5 * (Q3 - Q1) alors note = 0</div>
                <div>si consommation_energetique_primaire_par_hectare est entre 2 seuils, la note est calculée par interpolation linéaire</div>
            </TexteFormule>
        </TexteP>
    </div>
</template>

<script setup lang="ts">
import { IDS_DOMAINES, SOURCES_DONNEES } from '@lga/indicateurs';

import TexteFormule from '~/components/base/TexteFormule.vue';
import TexteLien from '~/components/base/TexteLien.vue';
import TexteP from '~/components/base/TexteP.vue';
import TitreH3 from '~/components/base/TitreH3.vue';
import TitreH4 from '~/components/base/TitreH4.vue';
import TitreH5 from '~/components/base/TitreH5.vue';
import { useRouteMethodologieIntrants } from '~/composables/index.js';

import LienMethodologieCheptels from '../liens/LienMethodologieCheptels.vue';
import LienMethodologieFluxAzote from '../liens/LienMethodologieFluxAzote.vue';
import LienMethodologieSourceDonnee from '../liens/LienMethodologieSourceDonnee.vue';
import LienMethodologieSurfacesAgricoles from '../liens/LienMethodologieSurfacesAgricoles.vue';
</script>
