import { describe, expect, it } from 'vitest';

import {
    calculerEtFormaterEvolutionString,
    calculerEtFormaterPourcentageString,
    calculerEvolutionPourcent,
    formaterEvolutionPourcentString,
    formaterNombreEnEntierString,
    formaterNombreEnNDecimalesString,
    formaterNombreSelonValeurString,
    rendreInitialeMajuscule,
    supprimerEspacesRedondantsEtRetoursLignes,
    supprimerSlashFinal
} from './formats';

describe('Tests des fonctions de formats.ts', () => {
    it("Vérifier que la locale fr-FR est présente dans l'environnement de test", () => {
        // Selon l'installation nodejs utilisé pour lancer ces tests, la locale fr-FR peut manquer (par ex selon l'image docker choisie sur framagit)
        // Voir https://nodejs.org/api/intl.html
        const locales = ['fr-FR', 'locale-inconnue'];
        expect(Intl.DateTimeFormat.supportedLocalesOf(locales).join(', ')).toEqual('fr-FR');
    });

    it('Fonction formaterNombreEnEntierString', () => {
        expect(formaterNombreEnEntierString(null)).toEqual('-');
        expect(formaterNombreEnEntierString(NaN)).toEqual('-');
        expect(formaterNombreEnEntierString(undefined)).toEqual('-');
        expect(formaterNombreEnEntierString(5)).toEqual('5');
        expect(formaterNombreEnEntierString(-5)).toEqual('-5');
        expect(formaterNombreEnEntierString(5.1)).toEqual('5');
        expect(formaterNombreEnEntierString(1000)).toEqual('1 000');
        expect(formaterNombreEnEntierString(1234567)).toEqual('1 234 567');
    });
    it('Fonction formaterNombreEnNDecimalesString', () => {
        expect(formaterNombreEnNDecimalesString(null, 0)).toEqual('-');
        expect(formaterNombreEnNDecimalesString(NaN, 0)).toEqual('-');
        expect(formaterNombreEnNDecimalesString(undefined, 0)).toEqual('-');
        expect(formaterNombreEnNDecimalesString(1.234, 0)).toEqual('1');
        expect(formaterNombreEnNDecimalesString(1.234, 1)).toEqual('1,2');
        expect(formaterNombreEnNDecimalesString(1.234, 2)).toEqual('1,23');
    });
    it('Fonction formaterNombreSelonValeurString', () => {
        expect(formaterNombreSelonValeurString(null)).toEqual('-');
        expect(formaterNombreSelonValeurString(NaN)).toEqual('-');
        expect(formaterNombreSelonValeurString(undefined)).toEqual('-');
        expect(formaterNombreSelonValeurString(5)).toEqual('5');
        expect(formaterNombreSelonValeurString(-5)).toEqual('-5');
        expect(formaterNombreSelonValeurString(5, true)).toEqual('+5');
        expect(formaterNombreSelonValeurString(5, false)).toEqual('5');
        expect(formaterNombreSelonValeurString(-5, true)).toEqual('-5');
        expect(formaterNombreSelonValeurString(-5, false)).toEqual('-5');
        expect(formaterNombreSelonValeurString(0)).toEqual('0');
        expect(formaterNombreSelonValeurString(0.0001234)).toEqual('0');
        expect(formaterNombreSelonValeurString(0.001234)).toEqual('0,001');
        expect(formaterNombreSelonValeurString(0.01234)).toEqual('0,012');
        expect(formaterNombreSelonValeurString(0.1234)).toEqual('0,12');
        expect(formaterNombreSelonValeurString(0.5678)).toEqual('0,57');
        expect(formaterNombreSelonValeurString(1.234)).toEqual('1,2');
        expect(formaterNombreSelonValeurString(12.3)).toEqual('12');
        expect(formaterNombreSelonValeurString(123.4)).toEqual('120');
        expect(formaterNombreSelonValeurString(1234.5)).toEqual('1 200');
        expect(formaterNombreSelonValeurString(123456.7)).toEqual('120 000');
        expect(formaterNombreSelonValeurString(567800)).toEqual('570 000');
        expect(formaterNombreSelonValeurString(1234567.8)).toEqual('1,2 millions');
        expect(formaterNombreSelonValeurString(12345678.9)).toEqual('12 millions');
        expect(formaterNombreSelonValeurString(56780000)).toEqual('57 millions');
        expect(formaterNombreSelonValeurString(1234567891.2)).toEqual('1,2 milliards');
    });

    it('Fonction calculerEvolutionPourcent', () => {
        expect(calculerEvolutionPourcent(null, 10)).toEqual(null);
        expect(calculerEvolutionPourcent(5, null)).toEqual(null);
        expect(calculerEvolutionPourcent(NaN, 10)).toEqual(null);
        expect(calculerEvolutionPourcent(5, NaN)).toEqual(null);
        expect(calculerEvolutionPourcent(undefined, 10)).toEqual(null);
        expect(calculerEvolutionPourcent(5, undefined)).toEqual(null);
        expect(calculerEvolutionPourcent(10, 0)).toEqual(-100);
        expect(calculerEvolutionPourcent(0, 10)).toEqual(+Infinity);
        expect(calculerEvolutionPourcent(10, 15)).toEqual(50);
        expect(calculerEvolutionPourcent(10, 25)).toEqual(150);
        expect(calculerEvolutionPourcent(15, 10)).toEqual(-33.33);
    });

    it('Fonction formaterEvolutionPourcentString', () => {
        expect(formaterEvolutionPourcentString(null)).toEqual('-%');
        expect(formaterEvolutionPourcentString(NaN)).toEqual('-%');
        expect(formaterEvolutionPourcentString(undefined)).toEqual('-%');
        expect(formaterEvolutionPourcentString(0)).toEqual('+0%');
        expect(formaterEvolutionPourcentString(50)).toEqual('+50%');
        expect(formaterEvolutionPourcentString(150)).toEqual('x2,5');
        expect(formaterEvolutionPourcentString(-0.33)).toEqual('-0,33%');
        expect(formaterEvolutionPourcentString(-33.33)).toEqual('-33%');
    });

    it('Fonction calculerEtFormaterEvolutionString', () => {
        expect(calculerEtFormaterEvolutionString(null, 10)).toEqual('-%');
        expect(calculerEtFormaterEvolutionString(5, null)).toEqual('-%');
        expect(calculerEtFormaterEvolutionString(NaN, 10)).toEqual('-%');
        expect(calculerEtFormaterEvolutionString(5, NaN)).toEqual('-%');
        expect(calculerEtFormaterEvolutionString(undefined, 10)).toEqual('-%');
        expect(calculerEtFormaterEvolutionString(5, undefined)).toEqual('-%');
        expect(calculerEtFormaterEvolutionString(10, 15)).toEqual('+50%');
        expect(calculerEtFormaterEvolutionString(10, 25)).toEqual('x2,5');
        expect(calculerEtFormaterEvolutionString(15, 10)).toEqual('-33%');
    });

    it('Fonction calculerEtFormaterPourcentageString', () => {
        expect(calculerEtFormaterPourcentageString(null, null)).toEqual('-');
        expect(calculerEtFormaterPourcentageString(null, 0)).toEqual('-');
        expect(calculerEtFormaterPourcentageString(0, null)).toEqual('-');
        expect(calculerEtFormaterPourcentageString(1, 2)).toEqual('50');
    });

    it('Fonction supprimerEspacesEtRetoursChariotsMultiple', () => {
        expect(supprimerEspacesRedondantsEtRetoursLignes('a b')).toEqual('a b');
        expect(supprimerEspacesRedondantsEtRetoursLignes('a  b')).toEqual('a b');
        expect(
            supprimerEspacesRedondantsEtRetoursLignes(
                `
            a   
            b
            `
            )
        ).toEqual(`a b`);
        expect(
            supprimerEspacesRedondantsEtRetoursLignes(
                `

                a  

                b

                `
            )
        ).toEqual(`a b`);
    });

    it('Fonction supprimerSlashFinal', () => {
        expect(supprimerSlashFinal('a/b/')).toEqual('a/b');
        expect(supprimerSlashFinal('a/b')).toEqual('a/b');
        expect(supprimerSlashFinal('')).toEqual('');
    });

    it('Fonction rendreInitialeMajuscule', () => {
        expect(rendreInitialeMajuscule('')).toEqual('');
        expect(rendreInitialeMajuscule('nom')).toEqual('Nom');
    });
});
