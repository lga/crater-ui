import { htmlstring, type ImageModele } from '@lga/base';
import { INDICATEURS } from '@lga/indicateurs';
import { recupererListe3InitiativesParConstats } from '@lga/initiatives';
import type { CodeProfilDiagnosticApi } from '@lga/specification-api';

import type { DonneesApi } from '../fonctions-utils.js';
import type { ConstatModele } from './ConstatModele';
import type { InfographieImageChiffreCleModele } from './InfographieModele.js';

export const IMAGE_POTIMARRON: ImageModele = {
    url: '/images/constats/titres/potimarron-800x868.jpg',
    descriptionAlt: 'Potimarron provenance Pérou en vente sur un étalage',
    resolutionX: 800,
    resolutionY: 868
};

export const IMAGE_MARCHE: ImageModele = {
    url: '/images/constats/titres/marche-800x861.jpg',
    descriptionAlt: 'Marché',
    resolutionX: 800,
    resolutionY: 861
};

export function creerConstatDistances(donneesApi: DonneesApi): ConstatModele<InfographieImageChiffreCleModele> {
    const message = calculerMessageTitreEtTexteConstatDistances(donneesApi.diagnosticApi.profils.codeProfilDiagnostic);

    return {
        id: 'distances',
        idTheme: 'autonomie-alimentaire',
        titre: {
            libelle: 'D’où vient ce que vous mangez ?',
            image: IMAGE_POTIMARRON
        },
        message: {
            titre: message.titre,
            texte: message.texte,
            question: 'Et si on développait des filières de proximité ?',
            initiatives: recupererListe3InitiativesParConstats(donneesApi.diagnosticApi.profils, ['distances'])
        },
        infographie: {
            nomComposant: 'InfographieImageChiffreCle',
            informationsMethodologie: {
                titre: INDICATEURS.distanceMoyenneParcourueAliments.libelle,
                descriptif: INDICATEURS.distanceMoyenneParcourueAliments.description,
                descriptifDetaille: INDICATEURS.distanceMoyenneParcourueAliments.descriptionDetaillee,
                estPerimetreNational: true
            },
            chiffreCle: {
                chiffre: (INDICATEURS.distanceMoyenneParcourueAliments.valeur ?? 0).toLocaleString('fr-FR'),
                unite: INDICATEURS.distanceMoyenneParcourueAliments.unite,
                phrase: 'distance moyenne parcourue par votre nourriture'
            },
            accroche: 'On the road again, again !',
            image: {
                url: '/images/constats/infographies/file-camions-sur-autoroute-416x448.png',
                descriptionAlt: "File de camions sur l'autoroute",
                resolutionX: 416,
                resolutionY: 448,
                position: 'gauche'
            }
        }
    };
}

export function calculerMessageTitreEtTexteConstatDistances(codeProfilDiagnostic: CodeProfilDiagnosticApi) {
    return codeProfilDiagnostic === 'URBAIN'
        ? {
              titre: 'L’essentiel de la nourriture consommée a parcouru de longues distances.',
              texte: htmlstring`Votre alimentation dépend donc d’un flux incessant de camions. Autant dire que le moindre grain de sable dans le système de transport peut donner des sueurs froides.`
          }
        : {
              titre: 'Votre territoire ne consomme pas ce qu’il produit.',
              texte: htmlstring`Votre alimentation dépend donc d’un flux incessant de camions. Autant dire que le moindre grain de sable dans le système de transport peut donner des sueurs froides.`
          };
}
