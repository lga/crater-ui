import '@lga/design-system/build/composants/Legende.js';
import '@lga/design-system/build/composants/Tableau.js';
import '@lga/design-system/build/composants/Lien.js';
import '@lga/design-system/build/composants/EncartPagesPrecedenteEtSuivante.js';
import '../charts/ChartBarreEmpileeHorizontale.js';
import '../charts/ChartTreemapCultures.js';
import '../charts/ChartTreemapCheptels.js';
import './composants/Figure.js';

import { formaterNombreEnEntierString, formaterNombreSelonValeurString } from '@lga/base';
import { formaterNombreEnEntierHtml } from '@lga/commun/build/composants/outils-composants';
import { lien } from '@lga/design-system/build/composants/Lien.js';
import type { OptionsTableau } from '@lga/design-system/build/composants/Tableau';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import type { ItemLegende } from '@lga/design-system/src/composants/Legende';
import { CSS_BREAKPOINT_MAX_WIDTH_MOBILE_PAYSAGE } from '@lga/design-system/src/styles/styles-breakpoints.js';
import { GLOSSAIRE, SOURCES_DONNEES, SOURCES_DONNEES_RETRAITEES } from '@lga/indicateurs';
import { IDS_MAILLONS } from '@lga/indicateurs';
import { GroupeCulture } from '@lga/indicateurs';
import { SA } from '@lga/indicateurs';
import { CategorieTerritoire } from '@lga/territoires';
import { css, html, LitElement, unsafeCSS } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { ifDefined } from 'lit/directives/if-defined.js';
import { unsafeHTML } from 'lit/directives/unsafe-html.js';

import { PAGES_METHODOLOGIE } from '../../../configuration/pages/declaration-pages';
import { construireUrlPageDiagnosticMaillon, construireUrlPageMethodologieSourceDonneeRetraitee } from '../../../configuration/pages/pages-utils';
import type { Rapport } from '../../../modeles/diagnostics';
import { type Cheptel, DEFINITIONS_CHEPTELS } from '../../../modeles/diagnostics/Cheptels.js';
import {
    construireLienVersMethodologieSourceDonnee,
    construireLienVersMethodologieSourceDonneeRetraitee,
    construireLienVersMethodologieSources
} from '../../commun/liens-utils.js';
import { STYLES_CRATER } from '../../commun/pages-styles';
import type { ItemBarreEmpileeHorizontale } from '../charts/ChartBarreEmpileeHorizontale';
import type { DonneesTreemapCheptels, OptionsTreemapCheptels } from '../charts/ChartTreemapCheptels';
import type { OptionsChartTreemapCultures } from '../charts/ChartTreemapCultures';
import { STYLES_CHAPITRES_DIAGNOSTIC } from '../diagnostic-utils.js';
import type { DonneesPageDiagnostic } from '../PageDiagnostic';
import { creerDonneesTreemapProduction } from './chapitres-outils.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-chapitre-presentation-territoire': ChapitrePresentationTerritoire;
    }
}

@customElement('c-chapitre-presentation-territoire')
export class ChapitrePresentationTerritoire extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        STYLES_CRATER,
        STYLES_CHAPITRES_DIAGNOSTIC,
        css`
            h1 {
                color: var(--couleur-primaire-sombre);
            }

            section.PDF c-encart-pages-precedente-suivante {
                display: none;
            }

            c-legende {
                margin: 1rem auto;
            }

            #legende-otex {
                text-align: center;
                padding-bottom: 30px;
            }

            #figure-sau-cheptels {
                display: grid;
                grid-template-areas: 'chart1 chart2' 'texte1 texte2' 'legende1 legende2';
            }

            #chart1 {
                min-height: 350px;
                grid-area: chart1;
                align-content: flex-end;
            }
            #chart2 {
                min-height: 350px;
                grid-area: chart2;
                align-content: flex-end;
            }
            #texte1 {
                grid-area: texte1;
                text-align: center;
            }
            #texte2 {
                grid-area: texte2;
                text-align: center;
            }
            #legende1 {
                grid-area: legende1;
            }
            #legende2 {
                grid-area: legende2;
            }

            @media screen and (max-width: ${unsafeCSS(CSS_BREAKPOINT_MAX_WIDTH_MOBILE_PAYSAGE + 'px')}) {
                #figure-sau-cheptels {
                    display: flex;
                    flex-direction: column;
                    align-items: center;
                }

                #chart1,
                #chart2 {
                    margin: auto;
                }
            }
        `
    ];

    @property({ attribute: false })
    donneesPage?: DonneesPageDiagnostic;

    @property({ attribute: false })
    rapport?: Rapport;

    @property({ type: Boolean })
    affichagePDF? = false;

    private readonly nomChapitre = 'Présentation du territoire';

    render() {
        if (!this.rapport) return;

        const nomTerritoire = this.rapport.getTerritoireActif()?.nom ?? '';
        const informationNbCommunes =
            this.rapport.getDiagnosticActif()!.nbCommunes > 1
                ? `est composé de ${formaterNombreEnEntierString(this.rapport.getDiagnosticActif()?.nbCommunes)} communes. Il `
                : '';
        const superficie = formaterNombreSelonValeurString(this.rapport.getDiagnosticActif()?.occupationSols.superficieTotaleHa);
        const population = formaterNombreSelonValeurString(this.rapport.getDiagnosticActif()?.population);
        const densitePopulation = formaterNombreSelonValeurString(this.rapport.getDiagnosticActif()?.densitePopulationHabParKM2);
        const rapportDensitePopulationSurPays = formaterNombreSelonValeurString(
            this.rapport.getDiagnosticActif()?.rapportDensitePopulationHabParM2SurPays
        );
        const superficieAgricoleClcHa = formaterNombreSelonValeurString(this.rapport.getDiagnosticActif()?.occupationSols.superficieAgricoleClcHa);
        const partSuperficieAgricoleClcPourcent = formaterNombreSelonValeurString(
            this.rapport.getDiagnosticActif()?.occupationSols.partSuperficieAgricoleClcPourcent
        );
        const superficieAgricoleRAHa = formaterNombreSelonValeurString(this.rapport.getDiagnosticActif()?.occupationSols.superficieAgricoleRA2020Ha);
        const superficieAgricoleRPGHa = formaterNombreSelonValeurString(this.rapport.getDiagnosticActif()?.surfaceAgricoleUtile.sauTotaleHa);

        return html`
            <section class="${this.affichagePDF ? 'PDF' : ''}">
                <h1>${this.nomChapitre}</h1>
                <h2>Informations générales</h2>
                <p>
                    Le territoire <em>${nomTerritoire}</em> ${informationNbCommunes} est peuplé de
                    ${population} habitants sur une surface de ${superficie} hectares soit une densité de population de ${densitePopulation} hab./km²
                    (= ${rapportDensitePopulationSurPays} fois la densité de la France métropolitaine).
                </p>
                <p>${unsafeHTML(this.rapport.getDiagnosticActif()?.messageOtex)}</p>
                ${
                    this.rapport.getTerritoireActif()!.categorie !== CategorieTerritoire.Commune
                        ? html` <c-chart-barre-empilee-horizontale
                                  id="barre-otex"
                                  .items=${this.calculerOptionsBarreOtex(this.rapport)}
                                  unite="commune(s)"
                              ></c-chart-barre-empilee-horizontale>
                              <p id="legende-otex" class="texte-petit">
                                  Répartition des communes du territoire par OTEX (12 postes), d'après les données de
                                  ${construireLienVersMethodologieSourceDonnee(SOURCES_DONNEES.otex)}. Une carte est disponible sur
                                  ${lien(SOURCES_DONNEES.otex.url, 'Cartostat')}.
                              </p>`
                        : ''
                }
                <p>
                    La surface agricole totale est de ${superficieAgricoleClcHa} hectares (soit ${partSuperficieAgricoleClcPourcent} % de la superficie
                    totale) selon l'${construireLienVersMethodologieSourceDonnee(SOURCES_DONNEES.corineLandCover)}, ${superficieAgricoleRAHa}
                    hectares selon les ${construireLienVersMethodologieSourceDonnee(SOURCES_DONNEES.sau_ra)} et ${superficieAgricoleRPGHa}
                    hectares selon le ${construireLienVersMethodologieSourceDonneeRetraitee(SOURCES_DONNEES_RETRAITEES.surfaces_agricoles_crater)}.
                </p>
                <p>
                    Dans CRATer, nous utilisons les surfaces issues du ${SOURCES_DONNEES_RETRAITEES.surfaces_agricoles_crater.nom} car elles sont données par type de culture (blé dur, tomate...). Nous les séparons en deux catégories :
                </p>
                </p>
                <c-tableau .options=${this.calculerOptionsTableauPresentationTerritoire(this.rapport)}></c-tableau>
                <p>
                    Les surfaces agricoles peu productives rassemblent les jachères, les estives & landes et diverses cultures particulières. Cette
                    distinction est faite dans le but de tenir compte du fait que certaines terres agricoles ne participent pas (par exemple les
                    jachères) ou peu (par exemple les alpages de haute altitude) à la production agricole annuelle d'un territoire.
                </p>
                <p><strong>Ces surfaces dites peu productives sont exclues de tous les calculs de CRATer, sauf mention contraire.</strong></p>
                <p>
                    Pour en savoir plus sur la nomenclature et les limites sur ces surfaces agricoles, veuillez consulter la
                    ${lien(construireUrlPageMethodologieSourceDonneeRetraitee(SOURCES_DONNEES_RETRAITEES.surfaces_agricoles_crater), 'méthodologie')}.
                </p>

                <h2>Détail des surfaces agricoles et des cheptels</h2>
                <p>
                    La surface agricole totale est de
                    ${formaterNombreSelonValeurString(this.rapport.getDiagnosticActif()?.surfaceAgricoleUtile.sauTotaleHa)} ha et le cheptel s'élève à
                    ${formaterNombreSelonValeurString(this.rapport.getDiagnosticActif()?.cheptels.ugbTotal)}
                    <abbr title="${GLOSSAIRE.ugb.definition}">Unités Gros Bétail</abbr>
                    (soit ${formaterNombreSelonValeurString(this.rapport.getDiagnosticActif()?.cheptels.tetesTotal)} têtes)
                    ${
                        this.rapport.getDiagnosticActif()?.ugbParHa
                            ? html`soit un ratio de ${formaterNombreSelonValeurString(this.rapport.getDiagnosticActif()?.ugbParHa)} UGB/ha`
                            : ''
                    }.
                    Cette valeur permet d'estimer le niveau d'intensité de l'élevage, qui devient particulièrement important lorsqu'on commence à
                    dépasser 1.5 UGB/ha.
                </p>
                <p>
                    Le graphique suivant détaille les surfaces agricoles par groupe de culture (détaillé par catégorie de culture au survol de la
                    souris) et des cheptels par type de cheptel :
                </p>
                <c-figure
                    .titre=${construireLienVersMethodologieSources([
                        { source: SOURCES_DONNEES_RETRAITEES.surfaces_agricoles_crater },
                        { source: SOURCES_DONNEES_RETRAITEES.cheptels_crater }
                    ])}
                >
                    <div id="figure-sau-cheptels" slot="chart-figure">
                        <c-chart-treemap-cultures
                            id="chart1"
                            .options=${this.calculerOptionsChartTreemapCultures(this.rapport)}
                        ></c-chart-treemap-cultures>
                        <p id="texte1">
                            <strong>Surfaces agricoles</strong> :
                            ${formaterNombreEnEntierString(this.rapport?.getDiagnosticActif()?.surfaceAgricoleUtile.sauTotaleHa ?? null)} ha
                        </p>
                        <c-legende id="legende1" .items=${this.calculerItemsLegendeChartTreemapCultures()}></c-legende>
                        <c-chart-treemap-cheptels
                            id="chart2"
                            .options=${this.calculerOptionsChartTreemapCheptels(this.rapport)}
                        ></c-chart-treemap-cheptels>
                        <p id="texte2">
                            <strong>Cheptels</strong> : ${formaterNombreEnEntierString(this.rapport?.getDiagnosticActif()?.cheptels.ugbTotal ?? null)}
                            Unités Gros Bétail
                        </p>
                        <c-legende id="legende2" .items=${this.calculerItemsLegendeChartTreemapCheptels()}></c-legende>
                    </div>
                </c-figure>

                <p>
                    Des informations sur le mode de calcul des données de cheptels sont disponibles
                    ${lien(
                        PAGES_METHODOLOGIE.presentationGenerale.getUrl({
                            idElementCibleScroll: SOURCES_DONNEES_RETRAITEES.cheptels_crater.id
                        }),
                        'ici'
                    )}.
                </p>
                <c-encart-pages-precedente-suivante
                    libellePageSuivante=${ifDefined(SA.getMaillon(IDS_MAILLONS.terresAgricoles)?.nom)}
                    hrefPageSuivante=${construireUrlPageDiagnosticMaillon(
                        IDS_MAILLONS.terresAgricoles,
                        this.donneesPage?.idTerritoirePrincipal ?? '',
                        this.donneesPage?.idEchelleTerritoriale
                    )}
                ></c-encart-pages-precedente-suivante>
            </section>
        `;
    }

    private calculerOptionsTableauPresentationTerritoire(rapport: Rapport): OptionsTableau {
        return {
            entetes: [html``, html`Superficie`, html`Part`],
            contenuHtml: [
                [
                    html`Surface productive`,
                    html`${formaterNombreEnEntierHtml(rapport.getDiagnosticActif()!.surfaceAgricoleUtile.sauProductiveHa)} ha`,
                    html`${formaterNombreEnEntierHtml(rapport.getDiagnosticActif()!.surfaceAgricoleUtile.pourcentageSauProductive)} %`
                ],
                [
                    html`Surface peu productive`,
                    html`${formaterNombreEnEntierHtml(rapport.getDiagnosticActif()?.surfaceAgricoleUtile.sauPeuProductiveHa)} ha`,
                    html`${formaterNombreEnEntierHtml(rapport.getDiagnosticActif()?.surfaceAgricoleUtile.pourcentageSauPeuProductive)} %`
                ]
            ],
            ligneFin: [
                html`Total`,
                html`${formaterNombreEnEntierHtml(rapport.getDiagnosticActif()?.surfaceAgricoleUtile.sauTotaleHa)} ha`,
                html`${formaterNombreEnEntierHtml(100)} %`
            ]
        };
    }

    private calculerOptionsBarreOtex(rapport: Rapport): ItemBarreEmpileeHorizontale[] {
        const nbCommunesOtex12Postes = rapport.getDiagnosticActif()?.otex!.nbCommunesOtex12Postes;
        if (!nbCommunesOtex12Postes) return [];
        return nbCommunesOtex12Postes.map(
            (i) =>
                ({
                    valeur: i.valeur,
                    libelle: i.otex.libelle,
                    couleur: i.otex.couleur
                }) as ItemBarreEmpileeHorizontale
        );
    }

    private calculerOptionsChartTreemapCultures(rapport: Rapport): OptionsChartTreemapCultures {
        const donnees = creerDonneesTreemapProduction(rapport.getDiagnosticActif()!.surfaceAgricoleUtile.listeSauParGroupeCulture);

        return {
            donnees: donnees,
            hauteur: 350
        };
    }

    private calculerItemsLegendeChartTreemapCultures(): ItemLegende[] {
        return GroupeCulture.tous.map((g) => {
            return { libelle: `${g.nom} (${g.nomCourt})`, couleur: g.couleur, motif: '' };
        });
    }

    private calculerOptionsChartTreemapCheptels(rapport: Rapport): OptionsTreemapCheptels {
        const donnees = rapport.getDiagnosticActif()?.cheptels.listeCheptels.map(
            (i: Cheptel) =>
                ({
                    code: i.libelle,
                    couleur: i.couleur,
                    ugb: i.ugb,
                    tetes: i.tetes
                }) as DonneesTreemapCheptels
        );
        return {
            donnees: donnees!,
            hauteur: 350 * Math.min(1.5, rapport.getDiagnosticActif()?.ugbParHa ?? 1)
        };
    }

    private calculerItemsLegendeChartTreemapCheptels(): ItemLegende[] {
        return Object.values(DEFINITIONS_CHEPTELS).map((i) => {
            return { libelle: `${i.libelle} (${i.code.slice(0, 2).toUpperCase()})`, couleur: i.couleur };
        });
    }
}
