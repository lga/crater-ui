import { defineConfig } from 'cypress';

import viteConfig from './vite.config.cypress.component.js';
export default defineConfig({
    includeShadowDom: true,
    component: {
        devServer: {
            framework: 'vue',
            bundler: 'vite',
            viteConfig
        }
    },
    e2e: {
        baseUrl: 'http://localhost:8088/'
    }
});
