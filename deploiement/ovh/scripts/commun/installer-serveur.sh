#!/bin/bash

## INSTALL INITIALE DU SERVEUR - à ne faire qu'une seule fois

# Timezone
sudo timedatectl set-timezone Europe/Paris

# APACHE
sudo apt-get install apache2
# Activer ssl & proxy pour apache
sudo a2enmod ssl
sudo a2ensite default-ssl
sudo a2enmod proxy
sudo a2enmod proxy_http
sudo a2enmod filter
sudo a2enmod deflate
sudo a2enmod brotli
# FIXME : Attention les 4 modules suivants n'ont pas été installés lors de la dernière exec de ce script (reinstall d'un serveur from scratch) => a checker le prochain coup
sudo a2enmod cache
sudo a2enmod cache_disk
sudo a2enmod headers
sudo a2enmod expires
sudo systemctl reload apache2

#installer cerbot qui permet d'installer facilement des certificats lets encrypt
sudo apt update
sudo apt install certbot

# installer nvm pour gérer facilement les versions de node
wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash

# activer NVM
source ~/.bashrc

# activer NVM dans le shell courant (le .bashrc n'est pas pris en compte dans le script)
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

# installer la version de node
nvm install 20

# utiliser node 20 par defaut dans tous les shells
nvm alias default 20

#installer pm2 (gestion et monitoring des process en prod)
npm install pm2 -g

# configurer pm2 pour qu'il redémarre a chaque fois (commande obtenue en faisant "pm2 startup")
NODE_SCRIPT_PATH=$(which node)
NODE_BIN_DIR=$(dirname "${NODE_SCRIPT_PATH}")
NODE_VERSION_DIR=$(dirname "${NODE_BIN_DIR}")
sudo env PATH=$PATH:$NODE_BIN_DIR $NODE_VERSION_DIR/lib/node_modules/pm2/bin/pm2 startup systemd -u ubuntu --hp /home/ubuntu

# Installer les certificats lets encrypt
./renouveler-certificat.sh

#
# Installation de l'api pdf (puppeteer-html-to-pdf-converter)
#
../pdf/installer-api-pdf.sh

#
# Configurer les healthchecks mattermost
#
./healthchecks-mattermost/configurer-crontab-healthchecks.sh