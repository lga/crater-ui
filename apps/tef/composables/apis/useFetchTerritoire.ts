import type { TerritoireApi } from '@lga/specification-api';

import { useFetchAvecGestionErreur, useGetApiBaseUrl } from '@/composables';

export function useFetchTerritoire(idTerritoire: string): Promise<TerritoireApi> {
    const apiBaseUrl = useGetApiBaseUrl();
    const url = `${apiBaseUrl}/crater/api/territoires/${idTerritoire}`;
    return useFetchAvecGestionErreur<TerritoireApi>(url);
}
