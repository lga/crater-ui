export const COULEUR_CARTE_GRIS_CONTOURS = '#47474780';

export const COULEUR_CARTE_BEIGE = 'fff4d7';
export const COULEUR_CARTE_MARRON = '#635452';
export const COULEUR_CARTE_BLEU = '#315a5b';
export const COULEUR_CARTE_VERT = '#1b4d3e';
export const COULEUR_CARTE_BLEU_PETROLE = '#014160';
export const COULEUR_CARTE_ROUGE = '#851605';
export const COULEUR_CARTE_ORANGE = '#8c5e00';

export const COULEUR_VALEUR_NULL = '#d7d7d7';
