import type { DonneesPageSimple } from './donnees-pages';
import { ModelePage, type OptionsModelePage } from './ModelePage';

export interface OptionsModelePageStatique {
    url: string;
    titreCourt: string;
    urlCanonique?: string;
    titreLong?: string;
    metaDescription?: string;
}

export class ModelePageSimple extends ModelePage<DonneesPageSimple> {
    constructor(optionsModelePageStatique: OptionsModelePageStatique) {
        const optionsModelePage: OptionsModelePage<DonneesPageSimple> = {
            url: () => optionsModelePageStatique.url,
            titreCourt: optionsModelePageStatique.titreCourt,
            urlCanonique: optionsModelePageStatique.urlCanonique ? () => optionsModelePageStatique.urlCanonique! : undefined,
            titreLong: optionsModelePageStatique.titreLong ? () => optionsModelePageStatique.titreLong! : undefined,
            metaDescription: optionsModelePageStatique.metaDescription ? () => optionsModelePageStatique.metaDescription! : undefined
        };
        super(optionsModelePage);
    }

    getId(donneesPageSimple?: DonneesPageSimple): string {
        return super.getId(donneesPageSimple ?? ({} as DonneesPageSimple));
    }
    getUrl(donneesPageSimple?: DonneesPageSimple): string {
        return super.getUrl(donneesPageSimple ?? ({} as DonneesPageSimple));
    }
    getTitreLong(donneesPageSimple?: DonneesPageSimple): string {
        return super.getTitreLong(donneesPageSimple ?? ({} as DonneesPageSimple));
    }
    getUrlCanonique(donneesPageSimple?: DonneesPageSimple): string {
        return super.getUrlCanonique(donneesPageSimple ?? ({} as DonneesPageSimple));
    }
    getMetaDescription(donneesPageSimple?: DonneesPageSimple): string | undefined {
        return super.getMetaDescription(donneesPageSimple ?? ({} as DonneesPageSimple));
    }
}
