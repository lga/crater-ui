const LISTE_COMPOSANTS_NUXT_UI = [
    'UAccordion', 'UAlert', 'UAvatar', 'UAvatarGroup', 'UBadge', 'UButton', 'UButtonGroup', 'UCarousel', 'UChip', 'UDropdown', 'UIcon', 'UKbd', 'ULink', 'UMeter', 'UMeterGroup', 'UProgress', 'UCheckbox', 'UForm', 'UFormGroup', 'UInput', 'UInputMenu', 'URadio', 'URadioGroup', 'URange', 'USelect', 'USelectMenu', 'UTextarea', 'UToggle', 'UTable', 'UCard', 'UContainer', 'UDivider', 'USkeleton', 'UBreadcrumb', 'UCommandPalette', 'UCommandPaletteGroup', 'UHorizontalNavigation', 'UPagination', 'UTabs', 'UVerticalNavigation', 'UContextMenu', 'UModal', 'UModals', 'UNotification', 'UNotifications', 'UPopover', 'USlideover', 'USlideovers', 'UTooltip']

module.exports = {
    root: true,
    extends: ['@nuxt/eslint-config', 'prettier'],
    plugins: ['simple-import-sort'],
    rules: {
        // Activer certaines règles typescript.
        // Permettre l'utilisation des NNBSP dans string et template
        'no-irregular-whitespace': [ 'error', { skipStrings: true, skipTemplates: true } ],
        // Solution temporaire en attendant d'activer plugin:@typescript-eslint/strict-type-checked et plugin:@typescript-eslint/stylistic-type-checked
        // (conflit avec @nust/eslint-config si on les active sans mieux gérer la config)
        '@typescript-eslint/no-explicit-any': 'error',
        '@typescript-eslint/no-unused-vars': 'error',
        '@typescript-eslint/no-non-null-assertion': 'error',
        '@typescript-eslint/no-extraneous-class': 'error',
        // Forcer le tri (on utilise ici le plugin 'simple-import-sort'; TODO voir si la règle eslint sort-imports peut faire l'affaire)
        'simple-import-sort/imports': 'error',
        'simple-import-sort/exports': 'error',
        // Forcer les imports inline et interdire les imports avec side effects => sinon on a des erreurs avec vite-plugin-vue (voir https://github.com/vitejs/vite-plugin-vue/issues/253#issuecomment-1719305521)
        "@typescript-eslint/consistent-type-imports": ["error", { "fixStyle": "inline-type-imports" }],
        "@typescript-eslint/no-import-type-side-effects": "error",
        // Config vue faite ici, car globale
        // Vue Desactiver les règles pas pertinentes
        'vue/multi-word-component-names': 'off',
        'vue/attribute-hyphenation': 'off',
        'vue/v-on-event-hyphenation': 'off',
        // Vue Ajuster les règles
        'vue/html-indent': ['warn', 4], // valeur par défaut = 2, conflit avec prettier...
        // Vue Activer des règles supplémentaires
        'vue/block-lang': [
            'error',
            {
                script: {
                    lang: 'ts'
                }
            }
        ],
        'vue/block-order': [
            'error',
            {
                order: ['template', 'script', 'style']
            }
        ],
        'vue/define-emits-declaration': 'error',
        'vue/define-props-declaration': 'error',
        "vue/no-undef-components": ["error",{
            "ignorePatterns": [
                'c-',  // Ignorer les composants lit car l'import ne mentionne par le nom du composant, et eslint ne le voit pas
                'apexchart', // Le composant apexchart est déclaré globalement dans le fichier apexchart.client.js
                ...LISTE_COMPOSANTS_NUXT_UI // Ignorer les composants de Nuxt UI car ils sont importés automatiquement, et l'import explicite "import UTabs from '#components'" ne fonctionne pas (erreur 500, le composant est tree shaké)
            ]
        }],
        'vue/no-empty-component-block': 'error',
        'vue/no-ref-object-reactivity-loss': 'error',
        'vue/no-required-prop-with-default': 'error',
        'vue/no-root-v-if': 'error',
        'vue/no-template-target-blank': 'warn', // Pas error car NuxtLink ajoute lui même rel="noopener noreferrer" avec target="_blank"
        // "vue/no-undef-components": "error", // Inactif car conflit avec auto import et global nuxt
        'vue/no-useless-mustaches': 'error',
        'vue/no-useless-v-bind': 'error',
        'vue/require-macro-variable-name': 'error',
        'vue/require-typed-ref': 'error',
        // TODO :  a activer avec les règles defineXxx pour forcer la déclaration explicite de l'interface des composants
        // 'vue/require-explicit-slots': 'error',
        // "vue/require-macro-variable-name": ["error", {
        //     "defineProps": "props",
        //     "defineEmits": "emit",
        //     "defineSlots": "slots",
        //     "useSlots": "slots",
        //     "useAttrs": "attrs"
        // }]
    },
    overrides: [
        // Config, activation / désactivation de règles par module & app
        {
            files: ['modules/specification-api/**/*.{js,ts,vue}', 'api/**/*.{js,ts,vue}'],
            env: {
                node: true
            }
        },
        {
            files: ['modules/{design-system, commun}/*.{js,ts,vue}', 'apps/**/*.{js,ts,vue}'],
            env: {
                browser: true
            }
        },
        {
            files: ['modules/**/*.{js,ts}', 'api/**/*.{js,ts}', 'apps/crater/**/*.{js,ts}'],
            extends: [
                'plugin:@typescript-eslint/strict-type-checked',
                'plugin:@typescript-eslint/stylistic-type-checked',
                'plugin:wc/recommended',
                'plugin:lit/recommended'
            ],
            parserOptions: {
                project: './tsconfig.eslint.json',
                tsconfigRootDir: __dirname
            },
            rules: {
                // Règles à réactiver progressivement (en l'état plusieurs centaines d'erreurs)
                '@typescript-eslint/unbound-method': 'off',
                '@typescript-eslint/no-non-null-assertion': 'off', // Interdire les ! => off pour l'instant, il faudrait passer partout dans le code pour l'activer
                '@typescript-eslint/no-unsafe-return': 'off',
                '@typescript-eslint/no-unsafe-argument': 'off',
                '@typescript-eslint/no-unsafe-member-access': 'off',
                '@typescript-eslint/no-unsafe-assignment': 'off',
                '@typescript-eslint/no-unsafe-call': 'off',
                '@typescript-eslint/no-floating-promises': 'off',
                '@typescript-eslint/no-unnecessary-condition': 'off',
                '@typescript-eslint/prefer-nullish-coalescing': 'off',
                '@typescript-eslint/no-extraneous-class': 'off',
                '@typescript-eslint/no-useless-template-literals': 'off',
                '@typescript-eslint/no-unnecessary-type-assertion': 'off'
            }
        },
        {
            files: ['apps/tef/**/*.{js,ts}'],
            extends: ['plugin:@typescript-eslint/strict-type-checked', 'plugin:@typescript-eslint/stylistic-type-checked'],
            parserOptions: {
                project: 'apps/tef/tsconfig.json',
                tsconfigRootDir: __dirname
            }
        }
    ]
};
