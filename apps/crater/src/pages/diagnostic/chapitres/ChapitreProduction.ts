import '../../commun/TimeLine.js';
import '../charts/ChartBarres.js';
import '../charts/ChartTreemapCultures.js';
import '../charts/ChartTreemapDoubleCultures.js';
import './composants/CommentAgir.js';
import './composants/ChapitreDomaine.js';
import './composants/EncartResumeDomaine.js';
import './composants/PourquoiCestImportant.js';
import './composants/VoirEgalement.js';
import './SyntheseMaillon.js';
import '@lga/design-system/build/composants/EncartPagesPrecedenteEtSuivante.js';

import { formaterNombreEnEntierString, formaterNombreSelonValeurString } from '@lga/base';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { INDICATEURS } from '@lga/indicateurs';
import { IDS_MAILLONS } from '@lga/indicateurs';
import { IDS_DOMAINES } from '@lga/indicateurs';
import { SA } from '@lga/indicateurs';
import { html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { ifDefined } from 'lit/directives/if-defined.js';

import { construireUrlPageDiagnosticMaillon } from '../../../configuration/pages/pages-utils.js';
import type { Rapport } from '../../../modeles/diagnostics';
import { STYLES_CRATER } from '../../commun/pages-styles';
import { STYLES_CHAPITRES_DIAGNOSTIC } from '../diagnostic-utils.js';
import type { DonneesPageDiagnostic } from '../PageDiagnostic.js';
import { creerOptionsSyntheseMaillon } from './chapitres-outils.js';
import type { OptionsEncartResumeDomaine } from './composants/EncartResumeDomaine';

declare global {
    interface HTMLElementTagNameMap {
        'c-chapitre-production': ChapitreProduction;
    }
}

@customElement('c-chapitre-production')
export class ChapitreProduction extends LitElement {
    static styles = [STYLES_DESIGN_SYSTEM, STYLES_CRATER, STYLES_CHAPITRES_DIAGNOSTIC];

    @property({ attribute: false })
    donneesPage?: DonneesPageDiagnostic;

    @property({ attribute: false })
    rapport?: Rapport;

    @property({ type: Boolean })
    affichagePDF? = false;

    private readonly maillon = SA.getMaillon(IDS_MAILLONS.production);

    render() {
        if (!this.rapport) return;

        return html`
            <section class="${this.affichagePDF ? 'PDF' : ''}">
                <h1>${this.maillon?.nom}</h1>
                <c-synthese-maillon
                    .options=${creerOptionsSyntheseMaillon(
                        this.rapport,
                        this.rapport.getDiagnosticActif()!.production,
                        IDS_MAILLONS.production,
                        INDICATEURS.scoreProduction.id
                    )}
                >
                </c-synthese-maillon>
                <h2 id="pourquoi-cest-important">Pourquoi c'est important ?</h2>
                <c-pourquoi-cest-important .maillon=${this.maillon}></c-pourquoi-cest-important>

                <h2 id="etat-lieux">Quel est l'état des lieux ?</h2>
                <div class="domaines-resumes">
                    <p>La production du territoire pourrait-elle théoriquement couvrir la consommation des habitants ?</p>
                    <c-timeline></c-timeline>
                    <div class="bloc-domaines-resumes">
                        <c-encart-resume-domaine
                            .options=${this.calculerOptionsEncartResumeDomaineAdequationTheoriqueProductionConsommation(
                                this.rapport,
                                this.donneesPage
                            )}
                        ></c-encart-resume-domaine>
                    </div>
                    <p>Quelle part de la production est réellement consommée localement ?</p>
                    <c-timeline></c-timeline>
                    <div class="bloc-domaines-resumes">
                        <c-encart-resume-domaine
                            .options=${this.calculerOptionsEncartResumeDomainePartProductionExportee(this.donneesPage)}
                        ></c-encart-resume-domaine>
                    </div>
                    <p>Est ce que les pratiques agricoles sont respectueuses de la biodiversité ?</p>
                    <c-timeline></c-timeline>
                    <div class="bloc-domaines-resumes">
                        <c-encart-resume-domaine
                            .options=${this.calculerOptionsEncartResumeDomainePartSauBio(this.rapport, this.donneesPage)}
                        ></c-encart-resume-domaine>
                        <c-encart-resume-domaine
                            .options=${this.calculerOptionsEncartResumeDomaineIndiceHvn(this.rapport, this.donneesPage)}
                        ></c-encart-resume-domaine>
                    </div>
                </div>

                <c-voir-egalement
                    .liensDomaines=${[
                        {
                            question: 'Quelle intensité d’usage en pesticides ?',
                            idDomaine: IDS_DOMAINES.qsaNodu
                        },
                        {
                            question: `Quels besoins en surfaces agricoles pour nourrir les animaux d’élevage ?`,
                            idDomaine: IDS_DOMAINES.consommationProduitsAnimaux
                        }
                    ]}
                    .donneesPage=${this.donneesPage}
                ></c-voir-egalement>

                <h2 id="leviers-action">Comment ma collectivité peut-elle s&#39;améliorer ?</h2>
                <c-comment-agir
                    .maillon=${this.maillon}
                    idTerritoireParcel=${ifDefined(this.rapport.getDiagnosticActif()?.idTerritoireParcel ?? undefined)}
                ></c-comment-agir>

                <c-encart-pages-precedente-suivante
                    libellePagePrecedente=${ifDefined(SA.getMaillon(IDS_MAILLONS.intrants)?.nom)}
                    hrefPagePrecedente=${construireUrlPageDiagnosticMaillon(
                        IDS_MAILLONS.intrants,
                        this.donneesPage?.idTerritoirePrincipal ?? '',
                        this.donneesPage?.idEchelleTerritoriale
                    )}
                    libellePageSuivante=${ifDefined(SA.getMaillon(IDS_MAILLONS.transformationDistribution)?.nom)}
                    hrefPageSuivante=${construireUrlPageDiagnosticMaillon(
                        IDS_MAILLONS.transformationDistribution,
                        this.donneesPage?.idTerritoirePrincipal ?? '',
                        this.donneesPage?.idEchelleTerritoriale
                    )}
                ></c-encart-pages-precedente-suivante>
            </section>
        `;
    }

    private calculerOptionsEncartResumeDomaineAdequationTheoriqueProductionConsommation(
        rapport: Rapport,
        donneesPage: DonneesPageDiagnostic | undefined
    ): OptionsEncartResumeDomaine {
        return {
            domaine: SA.getDomaine(IDS_DOMAINES.adequationProductionConsommation)!,
            chiffresCles: [
                {
                    chiffre: formaterNombreSelonValeurString(rapport.getDiagnosticActif()!.production.tauxAdequationMoyenPonderePourcent) + ' %',
                    libelleApresChiffre: 'de la consommation actuelle pourrait en théorie être couverte par la production locale'
                }
            ],
            donneesPage: donneesPage
        };
    }

    private calculerOptionsEncartResumeDomainePartProductionExportee(donneesPage: DonneesPageDiagnostic | undefined): OptionsEncartResumeDomaine {
        return {
            domaine: SA.getDomaine(IDS_DOMAINES.importationExportationConsommationProduction)!,
            estPerimetreNational: true,
            chiffresCles: [
                {
                    libelleAvantChiffre: "à l'échelle d'un bassin de vie",
                    chiffre: 'plus de 90 %',
                    libelleApresChiffre: 'de la production est exportée'
                },
                {
                    libelleAvantChiffre: 'et dans le même temps',
                    chiffre: 'plus de 90 %',
                    libelleApresChiffre: 'des produits consommés sont importés'
                }
            ],
            donneesPage: donneesPage
        };
    }

    private calculerOptionsEncartResumeDomainePartSauBio(
        rapport: Rapport,
        donneesPage: DonneesPageDiagnostic | undefined
    ): OptionsEncartResumeDomaine {
        return {
            domaine: SA.getDomaine(IDS_DOMAINES.bio)!,
            chiffresCles: [
                {
                    libelleAvantChiffre: '&nbsp',
                    chiffre: formaterNombreSelonValeurString(rapport.getDiagnosticActif()!.production.partSauBioPourcent) + ' %',
                    libelleApresChiffre: 'de la SAU en bio ou en cours de conversion'
                }
            ],
            donneesPage: donneesPage
        };
    }

    private calculerOptionsEncartResumeDomaineIndiceHvn(
        rapport: Rapport,
        donneesPage: DonneesPageDiagnostic | undefined
    ): OptionsEncartResumeDomaine {
        return {
            domaine: SA.getDomaine(IDS_DOMAINES.hvn)!,
            chiffresCles: [
                {
                    libelleAvantChiffre: '&nbsp',
                    chiffre: formaterNombreEnEntierString(rapport.getDiagnosticActif()!.production.indicateurHvn.indiceTotal) + ' /30',
                    libelleApresChiffre: 'score permettant de caractériser les systèmes agricoles qui maintiennent un haut niveau de biodiversité '
                }
            ],
            donneesPage: donneesPage
        };
    }
}
