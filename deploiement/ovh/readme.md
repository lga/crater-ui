# Scripts d'install : 

* `installer-serveur.sh` : permet de configurer un serveur from scratch (ubuntu 22.04). A faire uniquement la première fois, ou après réinit du serveur
* `renouveler-certificat.sh` : permet de renouveler les certificats SSL, à faire la 1ere fois, puis éventuellement quand les certificats sont expirés (mais normalement certbot se charge de le faire automatiquement)
* `installer-app.sh` : build et configure les applications dev et prod, à faire si besoin après une livraison manuelle (normallement pm2 s'en charge arpès un update des fichiers par le CI/CD) 
* `restart-dev.sh` et `restart-prod.sh` : pour redémarrer manuellement les applications dev et prod


# Install / réinstall complète du serveur

* Depuis l'interface OVH, réinstaller le serveur avec une version Ubuntu. Choisir la clé SSH à associer au serveur (reutiliser la clé présente sur sa machine dans ~/.ssh, ou bien en recréer une nouvelle si besoin et l'uploader sur l'espace client OVH)
* quand le serveur est réinitialisé, faire depuis sa machine locale :
    * Oublier l'empreinte de l'ancien VPS : `ssh-keygen -f "/home/lionel/.ssh/known_hosts" -R "vps-e5fb237a.vps.ovh.net"`
    * Depuis le dossier lga/ui/deploiement, copier les fichiers sur le VPS : `scp -r ovh ubuntu@vps-e5fb237a.vps.ovh.net:./lga`
* Puis se connecter sur le VPS : `ssh ubuntu@vps-e5fb237a.vps.ovh.net`
* Et lancer l'install serveur dans lga/scripts/commun. S'assurer que tout fonctionne bien et répondre aux questions posées.
* Enfin il faut redéployer l'ensemble des apps : pour cela, depuis framagit, dans le menu Operate/Environments, on peut redéployer les environnements un par un

# Commandes utiles 

## Certificats

* `sudo certbot certificates` : pour voir les certificats SSL installés et leur date d'expiration
* `sudo certbot --dry-run renew` : pour tester le renouvellement des certificats SSL (sans les renouveler)
* `systemctl list-timers | grep certbot` : voir le statut du timer et sa prochaine exécution
* `systemctl status certbot.timer` : pour voir si le timer de renouvellement des certificats est bien configuré
* `systemctl status certbot.service` : pour voir si le service de renouvellement des certificats est bien configuré, et le résultat de la dernière exécution
* `sudo certbot delete --cert-name=api.resiliencealimentaire.org` : supprimer un certificat (peut etre nécessaire, car si on change les noms de domaine, le renouvelement recrée un certificat avec les nouveaux noms, qui s'appelle api.resiliencealimentaire.org-0001, etc.)

## PM2 

* `pm2 list` : pour voir les applications en cours d'exécution
* `pm2 logs` : pour voir les logs des applications en cours d'exécution
* `pm2 monit` : pour voir les stats des applications en cours d'exécution
* `pm2 reload /home/ubuntu/lga/ovh/config/pm2/ecosystem.config.js` : recharge le fichier ecosystem, et redémarre toutes les apps (voir redemarrer-apps.sh)
* `pm2 save` : sauvegarder la liste de toutes les apps en cours pour pouvoir les restaurer en cas de crash ou reboot serveur (voir redemarrer-apps.sh)

## Monitoring général

* `htop` : pour voir les process actifs, l'utilisation mémoire, etc... 

## SSH

* Gestion des clés SSH, voir document sur architecture et hébergement du projet 
* `scp -r ovh ubuntu@vps-e5fb237a.vps.ovh.net:./lga/` : pour copier un dossier local sur le serveur (ici pousser le dossier ovh sur le serveur, par ex pour tester des modifs de scripts)

# Documents utiles

Voir document "Guide hébergement TEF-CRATer"