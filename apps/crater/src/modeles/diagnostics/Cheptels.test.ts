import { describe, expect, it } from 'vitest';

import { Cheptel, Cheptels } from './Cheptels';

describe('Tests de Cheptel', () => {
    it('Test classe Cheptel', () => {
        const cheptel = new Cheptel('CAPRINS', 10, true, 3);

        expect(cheptel.code).toEqual('CAPRINS');
        expect(cheptel.estEstime).toEqual(true);
        expect(cheptel.tetes).toEqual(10);
        expect(cheptel.ugb).toEqual(3);
    });

    it('Test classe Cheptel', () => {
        const cheptels = new Cheptels([new Cheptel('BOVINS', 1200, true, 120), new Cheptel('CAPRINS', 500, true, 50)]);

        expect(cheptels.tetesTotal).toEqual(1700);
        expect(cheptels.ugbTotal).toEqual(170);
    });
});
