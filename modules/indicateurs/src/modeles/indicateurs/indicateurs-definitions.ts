import { type HtmlString, htmlstring } from '@lga/base';
import {
    COULEUR_CARTE_BEIGE,
    COULEUR_CARTE_BLEU,
    COULEUR_CARTE_BLEU_PETROLE,
    COULEUR_CARTE_MARRON,
    COULEUR_CARTE_ORANGE,
    COULEUR_CARTE_ROUGE,
    COULEUR_CARTE_VERT,
    GradientCouleurs,
    StyleCarteCerclesProportionnels,
    StyleCarteChorochromatique,
    StyleCarteChoroplethe,
    StyleCarteChoropletheNotes
} from '@lga/styles-carte';

import { RAPPORTS_ETUDES, SOURCES_DONNEES, SOURCES_DONNEES_RETRAITEES } from '../sources-donnees/sources-donnees.js';
import { DEFINITIONS_POSTES_ENERGIE } from './energie.js';
import type { DefinitionIndicateur } from './indicateurs.js';
import { styleCarteHVNChorochromatique } from './pratiques-agricoles.js';

//TODO a factoriser? (existe dans tef)
const creerLienExterneHtmlBrut = (url: string, texte: string): HtmlString =>
    htmlstring`<a href="${url}" target="_blank" rel="noopener noreferrer" class='underline hover:no-underline hover:text-accent'>${texte}</a>`;

const occupationSols: DefinitionIndicateur = {
    id: 'occupation-sols',
    libelle: 'Occupation des sols',
    description: htmlstring`L'occupation des sols donne la répartition de l'usage des sols selon trois catégories : agricole, artificialisé et naturel/forestier.`,
    unite: '%',
    sources: [{ source: SOURCES_DONNEES.corineLandCover }],
    nomIndicateurRequeteApi: 'politiqueFonciere.rythmeArtificialisationSauPourcent'
};

const otexMajoritaire5Postes: DefinitionIndicateur = {
    id: 'otex-majoritaire-5-postes',
    libelle: 'Spécialisation majoriatire (5 postes)',
    description: htmlstring`Il s'agit de l'OTEX majoritaire (5 postes) du territoire calculé selon la répartition des OTEX des communes du territoire.`,
    unite: '',
    sources: [{ source: SOURCES_DONNEES.otex }]
};

const sauParHabitant: DefinitionIndicateur = {
    id: 'surface-agricole-utile-par-habitant',
    libelle: 'Surface agricole utile par habitant',
    description: htmlstring`Cet indicateur correspond au ratio entre la surface agricole productive et le nombre d'habitants.
    <br>La surface agricole nécessaire par habitant varie fortement en fonction du régime alimentaire, des conditions pédoclimatiques du territoire et des pratiques agricoles. Aujourd'hui en France, elle s'élève à environ 4000 m²/hab pour le régime alimentaire actuel, 2500 m²/hab pour un régime alimentaire moins riche en protéines et produits animaux et 1700
        m²/hab pour un régime alimentaire très végétal (voir Le revers de notre assiette, Solagro (2019).`,
    unite: 'm²/hab',
    sources: [
        { source: SOURCES_DONNEES.rpg, anneesMobilisees: '2017' },
        { source: SOURCES_DONNEES.population_totale, anneesMobilisees: '2017' }
    ],
    nomIndicateurRequeteApi: 'politiqueFonciere.sauParHabitantM2',
    styleCarte: new StyleCarteChoroplethe({
        gradientCouleurs: new GradientCouleurs([
            { valeur: 0, couleur: '#ffffff' },
            { valeur: 4000, couleur: COULEUR_CARTE_MARRON }
        ]),
        labels: [
            { valeur: 0, label: '0' },
            { valeur: 1000, label: '1000' },
            { valeur: 2000, label: '2000' },
            { valeur: 3000, label: '3000' },
            { valeur: 4000, label: '4000+' }
        ]
    })
};

const rythmeArtificialisation: DefinitionIndicateur = {
    id: 'rythme-artificialisation',
    libelle: "Rythme d'artificialisation",
    description: htmlstring`Cet indicateur correspond au ratio entre le nombre d'hectares agricoles, naturels et forestiers qui ont été artificialisés
    en cinq ans et la surface agricole utile productive. Cette valeur doit tendre vers zéro dans le cadre d'un objectif de zéro artificialisation.`,
    unite: '%',
    sources: [
        { source: SOURCES_DONNEES.artificialisation_sols, anneesMobilisees: '2013-2018' },
        { source: SOURCES_DONNEES.rpg, anneesMobilisees: '2017' }
    ],
    nomIndicateurRequeteApi: 'politiqueFonciere.rythmeArtificialisationSauPourcent',
    styleCarte: new StyleCarteChoroplethe({
        gradientCouleurs: new GradientCouleurs([
            { valeur: 0, couleur: '#ffffff' },
            { valeur: 5, couleur: COULEUR_CARTE_MARRON }
        ]),
        labels: [
            { valeur: 0, label: '0' },
            { valeur: 1, label: '1' },
            { valeur: 2, label: '2' },
            { valeur: 3, label: '3' },
            { valeur: 4, label: '4' },
            { valeur: 5, label: '5+' }
        ]
    })
};

const politiqueAmenagement: DefinitionIndicateur = {
    id: 'politique-amenagement',
    libelle: "Politique d'aménagement",
    description: htmlstring`Cet indicateur, qualitatif, se base sur le total
        d'hectares agricoles, naturels et forestiers qui ont été artificialisés sur une période donnée au regard de l'évolution du nombre de
        ménages et d'emplois.`,
    unite: '%',
    sources: [
        { source: SOURCES_DONNEES.artificialisation_sols, anneesMobilisees: '2013-2018' },
        { source: SOURCES_DONNEES.evolution_menages_emplois, anneesMobilisees: '2013-2018' }
    ]
};

const partLogementsVacants: DefinitionIndicateur = {
    id: 'part-logements-vacants',
    libelle: 'Part de logements vacants',
    description: htmlstring`La part de logements vacants correspond au ratio entre le nombre de logements vacants et le nombre total de logements.`,
    unite: '%',
    sources: [{ source: SOURCES_DONNEES.logements_vacants }, { source: SOURCES_DONNEES.logements_totaux }],
    nomIndicateurRequeteApi: 'politiqueFonciere.partLogementsVacants2018Pourcent',
    styleCarte: new StyleCarteChoroplethe({
        gradientCouleurs: new GradientCouleurs([
            { valeur: 0, couleur: '#ffffff' },
            { valeur: 15, couleur: COULEUR_CARTE_MARRON }
        ]),
        labels: [
            { valeur: 0, label: '0' },
            { valeur: 5, label: '5' },
            { valeur: 10, label: '10' },
            { valeur: 15, label: '15+' }
        ]
    })
};

const scoreTerresAgricoles: DefinitionIndicateur = {
    id: 'score-terres-agricoles',
    libelle: 'Score du maillon Terres agricoles',
    description: htmlstring`Ce score évalue la capacité du territoire à préserver ses terres agricoles, en particulier face à l'artificialisation.
    Il se base sur la surface agricole par habitant et le rythme d'artificialisation des sols`,
    unite: '',
    sources: [{ source: sauParHabitant }, { source: rythmeArtificialisation }],
    nomIndicateurRequeteApi: 'politiqueFonciere.note',
    styleCarte: new StyleCarteChoropletheNotes()
};

const nombreActifsAgricoles1988: DefinitionIndicateur = {
    id: 'nombre-actifs-agricoles',
    libelle: "Nombre d'actifs agricoles permanents en 1988",
    description: htmlstring`Cet indicateur correspond au nombre de travailleurs agricoles permanents en 1988.`,
    unite: '',
    sources: [{ source: SOURCES_DONNEES.actifs_agricoles_permanents, anneesMobilisees: '1988' }]
};

const nombreActifsAgricoles2010: DefinitionIndicateur = {
    id: 'nombre-actifs-agricoles',
    libelle: "Nombre d'actifs agricoles permanents en 2010",
    description: htmlstring`Cet indicateur correspond au nombre de travailleurs agricoles permanents en 2010.`,
    unite: '',
    sources: [{ source: SOURCES_DONNEES.actifs_agricoles_permanents, anneesMobilisees: '2010' }]
};

const partActifsAgricoles: DefinitionIndicateur = {
    id: 'part-actifs-agricoles',
    libelle: 'Part des actifs agricoles permanents dans la population totale',
    description: htmlstring`Cet indicateur correspond au ratio entre le nombre de travailleurs agricoles permanents et la population totale.`,
    unite: '%',
    sources: [
        { source: SOURCES_DONNEES.actifs_agricoles_permanents, anneesMobilisees: '1988 et 2010' },
        { source: SOURCES_DONNEES.population_totale, anneesMobilisees: '2017' },
        { source: SOURCES_DONNEES.population_totale_historique, anneesMobilisees: '1990' }
    ],
    nomIndicateurRequeteApi: 'populationAgricole.partPopulationAgricole2010Pourcent',
    styleCarte: new StyleCarteChoroplethe({
        gradientCouleurs: new GradientCouleurs([
            { valeur: 0, couleur: '#ffffff' },
            { valeur: 15, couleur: COULEUR_CARTE_BLEU }
        ]),
        labels: [
            { valeur: 0, label: '0' },
            { valeur: 5, label: '5' },
            { valeur: 10, label: '10' },
            { valeur: 15, label: '15+' }
        ]
    })
};

const revenuAgriculteurs: DefinitionIndicateur = {
    id: 'revenu-agriculteurs',
    libelle: `Revenu des agriculteurs`,
    description: htmlstring`Cet indicateur donne le revenu maximal par travailleur du quart des exploitations françaises les moins rémunératrices.`,
    unite: '€',
    sources: [{ source: SOURCES_DONNEES.rica, anneesMobilisees: '2017 à 2020' }],
    estIndicateurNational: true
};

const agesChefsExploitation: DefinitionIndicateur = {
    id: 'ages-chefs-exploitations',
    libelle: `Âge des chefs d'exploitation`,
    description: htmlstring`Cet indicateur donne le nombre de chefs d'exploitation par classes d'âge.`,
    unite: 'ans',
    sources: [{ source: SOURCES_DONNEES.exploitations_selon_age_chef_exploitation, anneesMobilisees: '2010' }]
};

const superficiesExploitations: DefinitionIndicateur = {
    id: 'superficies-exploitations',
    libelle: 'Nombre et superficie des exploitations',
    description: htmlstring`Cet indicateur donne le nombre totale et la superficie associée des exploitations par classes de superficie.`,
    sources: [{ source: SOURCES_DONNEES.exploitations_selon_taille_exploitation, anneesMobilisees: '2010' }],
    unite: 'ha'
};

const superficieMoyenneExploitations: DefinitionIndicateur = {
    id: 'superficie-moyenne-exploitations',
    libelle: 'Superficie moyenne des exploitations',
    description: htmlstring`Cet indicateur donne la superficie moyenne des exploitations.`,
    unite: 'ha',
    sources: [{ source: SOURCES_DONNEES.exploitations_selon_taille_exploitation, anneesMobilisees: '2010' }],
    nomIndicateurRequeteApi: 'populationAgricole.sauMoyenneParExploitationHa2010',
    styleCarte: new StyleCarteChoroplethe({
        gradientCouleurs: new GradientCouleurs([
            { valeur: 0, couleur: '#ffffff' },
            { valeur: 150, couleur: COULEUR_CARTE_BLEU }
        ]),
        labels: [
            { valeur: 0, label: '0' },
            { valeur: 50, label: '50' },
            { valeur: 100, label: '100' },
            { valeur: 150, label: '150+' }
        ]
    })
};

const scoreAgriculteursExploitations: DefinitionIndicateur = {
    id: `score-agriculteurs-exploitations`,
    libelle: 'Score du maillon Agriculteurs et exploitations',
    description: htmlstring`Ce score évalue l'effectif et la dynamique de renouvellement de la population agricole. Il se base sur la part d'actifs
    agricoles dans la population et son évolution sur 30 années passées.`,
    unite: '',
    sources: [{ source: partActifsAgricoles }],
    nomIndicateurRequeteApi: 'populationAgricole.note',
    styleCarte: new StyleCarteChoropletheNotes()
};

const consommationEnergetiqueParHectare: DefinitionIndicateur = {
    id: 'consommation-energetique-par-hectare',
    libelle: "Consommation d'énergie primaire de l'agriculture par hectare agricole",
    description: htmlstring`Cet indicateur correspond au ratio entre la consommation annuelle d'énergie primaire de l'agriculture et la surface agricole du territoire. Les postes de dépense énergétique considérés sont : les carburants des tracteurs, l'irrigation, le chauffage des serres, les bâtiments d'élevage, la production du matériel agricole, la production des engrais azotés et la production de l'alimentation animale importée de l'étranger.`,
    unite: 'GJ/ha',
    sources: [{ source: SOURCES_DONNEES.climagri }],
    nomIndicateurRequeteApi: 'energie.energiePrimaireGJParHa',
    styleCarte: new StyleCarteChoroplethe({
        gradientCouleurs: new GradientCouleurs([
            { valeur: 0, couleur: '#ffffff' },
            { valeur: 40, couleur: COULEUR_CARTE_ORANGE }
        ]),
        labels: [
            { valeur: 0, label: '0' },
            { valeur: 10, label: '10' },
            { valeur: 20, label: '20' },
            { valeur: 30, label: '30' },
            { valeur: 40, label: '40+' }
        ]
    })
};

const postePrincipalEnergie: DefinitionIndicateur = {
    id: 'poste-principal-energie',
    libelle: "Poste principal d'énergie primaire consommée pour l'agriculture",
    description: htmlstring`Cet indicateur permet d'identifier le poste principal d'énergie primaire consommée pour l'agriculture du territoire.`,
    unite: '',
    sources: [{ source: SOURCES_DONNEES.climagri }],
    nomIndicateurRequeteApi: 'energie.codePostePrincipal',
    styleCarte: new StyleCarteChorochromatique({
        classes: DEFINITIONS_POSTES_ENERGIE.map((d) => {
            return {
                code: d.code,
                libelle: d.libelleCourt,
                couleur: d.couleur
            };
        })
    })
};

const scoreEnergie: DefinitionIndicateur = {
    id: 'score-energie',
    libelle: "Score de dépendance à l'énergie",
    description: htmlstring`Ce score évalue la dépendance à l'énergie, sur base de l'indicateur de consommation d'énergie primaire par hectare de surface agricole.`,
    unite: '',
    sources: [{ source: consommationEnergetiqueParHectare }],
    nomIndicateurRequeteApi: 'energie.note'
};

const prelevementsIrrigation: DefinitionIndicateur = {
    id: 'prelevements-irrigation',
    libelle: `Prélèvements en eau pour l'irrigation`,
    description: htmlstring`<p>Cet indicateur donne les valeurs annuelles moyennes de prélèvement en eau sur la période 2016-2020.</p>
    <p>
        Remarque : en raison de limites sur la source des données, il est possible que le volume de prélèvements soit surestimé ou
        sous-estimé de manière significative pour certaines communes ou EPCI.
    </p>`,
    unite: 'm3/an',
    sources: [
        { source: SOURCES_DONNEES.prelevements_irrigation, anneesMobilisees: '2015 à 2020' },
        { source: SOURCES_DONNEES_RETRAITEES.surfaces_agricoles_crater }
    ],
    nomIndicateurRequeteApi: 'eau.irrigation.irrigationM3',
    styleCarte: new StyleCarteCerclesProportionnels({
        couleurValeursPositives: 'blue',
        facteurMultiplicatifTailleCercles: 0.003
    })
};

const tendanceEvolutionPrelevementsIrrigation: DefinitionIndicateur = {
    id: 'tendance-evolution-irrigation',
    libelle: `Tendance d'évolution des prélèvements en eau pour l'irrigation`,
    description: htmlstring`
    <p>Il s’agit de la tendance moyenne observée sur les prélèvements en eau pour l’irrigation entre 2012 et 2020, rapportée à une période de 10 ans.</p>`,
    descriptionDetaillee: htmlstring`
    <p>Il s’agit de la tendance moyenne observée sur les prélèvements en eau pour l’irrigation entre 2012 et 2020, rapportée à une période de 10 ans.</p>
    <p>Pour cela, on réalise une régression linéaire sur les données de 2012 à 2020 que l’on utilise ensuite pour estimer des valeurs en 2012 et 2022. Ces deux valeurs servent alors de référence pour calculer l’évolution moyenne sur 10 ans.</p>`,
    unite: '%',
    sources: [{ source: SOURCES_DONNEES.prelevements_irrigation, anneesMobilisees: '2012 à 2020' }]
};

const pratiquesIrrigation: DefinitionIndicateur = {
    id: 'pratiques-irrigation',
    libelle: `Pratiques d'irrigation`,
    description: htmlstring`<p>Cet indicateur donne diverses informations sur les pratiques d'irrigation à l'échelle des départements et régions.</p>`,
    unite: '',
    sources: [
        { source: SOURCES_DONNEES.pratiques_irrigation, anneesMobilisees: '2010' },
        { source: SOURCES_DONNEES.surfaces_riz, anneesMobilisees: '2021' }
    ]
};

const alertesSecheresse: DefinitionIndicateur = {
    id: 'alertes-secheresse',
    libelle: `Part moyenne du territoire sous arrêté sécheresse en période estivale`,
    description: htmlstring`Cet indicateur correspond à la part du territoire qui a été sous arrêté sécheresse des eaux superficielles en moyenne
    sur les mois de juillet-août 2016-2020.`,
    unite: '%',
    sources: [{ source: SOURCES_DONNEES.donnees_secheresses, anneesMobilisees: '2016-2020' }],
    nomIndicateurRequeteApi: 'eau.arretesSecheresse.tauxImpactArretesSecheressePourcent',
    styleCarte: new StyleCarteChoroplethe({
        gradientCouleurs: new GradientCouleurs([
            { valeur: 0, couleur: '#ffffff' },
            { valeur: 100, couleur: COULEUR_CARTE_ROUGE }
        ]),
        labels: [
            { valeur: 0, label: '0' },
            { valeur: 20, label: '20' },
            { valeur: 40, label: '40' },
            { valeur: 60, label: '60' },
            { valeur: 80, label: '80' },
            { valeur: 100, label: '100' }
        ]
    })
};

const scoreEau: DefinitionIndicateur = {
    id: 'score-eau',
    libelle: 'Score de dépendance à la ressource eau pour l’irrigation',
    description: htmlstring`Ce score évalue la dépendance à la ressource eau pour l’irrigation, sur base des scores données aux indicateurs
        <strong>Prélèvement en eau pour l'irrigation</strong> et <strong>Alertes sécheresse des eaux superficielles</strong>.`,
    unite: '',
    sources: [{ source: prelevementsIrrigation }, { source: alertesSecheresse }],
    nomIndicateurRequeteApi: 'eau.note',
    styleCarte: new StyleCarteChoropletheNotes()
};

const qsa: DefinitionIndicateur = {
    id: 'qsa',
    libelle: 'Quantités de substances actives achetées',
    description: htmlstring`Cet indicateur <abbr title="Quantité de Substances Actives">QSA</abbr>, exprimé en kg, correspond à la quantité totale de
        substances actives qui ont été achetées sur le territoire. Seules les substances considérées comme cancérigènes, mutagène ou
        reprotoxiques, dangereuses pour la santé ou dangereuses pour l'environnement sont prises en compte (ie toutes les classifications de
        toxicité sauf "Autre"). Il est calculé en réalisant une moyenne triennale (la moyenne triennale de l'année N est calculée par moyenne des années N,
        N-1 et N-2).`,
    unite: 'kg',
    sources: [{ source: SOURCES_DONNEES.quantites_substances_actives }]
};

const nodu: DefinitionIndicateur = {
    id: 'pesticides-nodu',
    libelle: 'Nombre de doses unités équivalent',
    description: htmlstring`Cet indicateur <abbr title="NOmbre de Doses Unités">NODU</abbr>, exprimé en hectares, est calculé pour chaque substance active en faisant le
        ratio de la <abbr title="Quantité de Substances Actives">QSA</abbr> avec la <abbr title="Dose Unité">DU</abbr> de la substance active.
        Cette DU correspond à la dose maximale applicable sur un hectare. Le calcul du NODU permet donc de comparer et d'additionner des
        substances actives qui n'ont pas le même impact à quantité égale utilisée. Certaines substances n'ont pas de DU et sont donc exclues de
        facto du calcul. Aussi, comme il est calculé à partir du <abbr title="Quantité de Substances Actives">QSA</abbr>, seules les substances
        prises en compte dans le calcul de ce dernier le sont également ici. Il est calculé en réalisant une moyenne triennale (la moyenne triennale de l'année N est calculée par moyenne des années N,
        N-1 et N-2).
        <br />N. B. : des valeurs supérieures à 10 peuvent signifier une surestimation du <abbr title="NOmbre de Doses Unités">NODU</abbr>`,
    unite: 'ha',
    sources: [{ source: SOURCES_DONNEES.quantites_substances_actives }, { source: SOURCES_DONNEES.doses_unites_substances_actives }]
};

const noduNormalise: DefinitionIndicateur = {
    id: 'pesticides-intensite-usage',
    libelle: `Intensité d'usage de pesticides`,
    description: htmlstring` Cet indicateur peut s’interpréter comme le nombre moyen de traitements par pesticides que reçoivent les terres agricoles du
    territoire, en tenant compte de la toxicité des produits employés. Il permet de faire des comparaisons entre territoires et/ou périodes
    temporelles différent(e)s.`,
    unite: '',
    sources: [
        { source: nodu, anneesMobilisees: '2018-2020' },
        { source: SOURCES_DONNEES.sau_ra, anneesMobilisees: '2020' }
    ],
    nomIndicateurRequeteApi: 'pesticides.noduNormalise',
    styleCarte: new StyleCarteChoroplethe({
        gradientCouleurs: new GradientCouleurs([
            { valeur: 0, couleur: '#ffffff' },
            { valeur: 5, couleur: '#e2a582' },
            { valeur: 10, couleur: '#c44b06' }
        ]),
        labels: [
            { valeur: 0, label: '0' },
            { valeur: 2, label: '2' },
            { valeur: 4, label: '4' },
            { valeur: 6, label: '6' },
            { valeur: 8, label: '8' },
            { valeur: 10, label: '10+' }
        ]
    })
};

const scorePesticides: DefinitionIndicateur = {
    id: 'score-pesticides',
    libelle: 'Score de dépendance aux pesticides',
    description: htmlstring`Ce score évalue la dépendance aux pesticides, sur base de l'indicateur d'intensité d'usage de pesticides.`,
    unite: '',
    sources: [{ source: noduNormalise }]
};

const scoreIntrants: DefinitionIndicateur = {
    id: 'score-intrants',
    libelle: 'Score du maillon Intrants',
    description: htmlstring`Ce score évalue la dépendance aux intrants de l'agriculture du territoire, en se basant sur le niveau d'usage des
    pesticides, de l'eau pour l'irrigation et de l'énergie.`,
    unite: '',
    sources: [{ source: scoreEnergie }, { source: scoreEau }, { source: scorePesticides }],
    nomIndicateurRequeteApi: 'intrants.note',
    styleCarte: new StyleCarteChoropletheNotes()
};

const messageLimiteTauxAdequationProductionConsommation = htmlstring`<strong>Attention</strong>, il ne s'agit pas de la part de la consommation réellement couverte par la production locale. Les flux
        logistiques sont aujourd'hui totalement dissociés de la disponibilité locale, si bien qu'à l'échelle d'un bassin de vie, presque toute la
        production est généralement exportée, et tous les biens consommés sont importés depuis d'autres territoires.
        <br>Aussi, l'indicateur se base sur les surfaces de production vs de consommation, dont les périmètres sont différents (voir les limites des sources de données considérées).`;

const adequationTheoriqueBruteProductionConsommation: DefinitionIndicateur = {
    id: 'adequation-theorique-brute-production-consommation',
    libelle: 'Adéquation théorique brute entre production et consommation',
    description: htmlstring`Cet indicateur représente la part de la consommation du territoire qui pourrait en théorie être couverte par sa propre production agricole, dans le cadre du régime alimentaire actuel.
    <br/>${messageLimiteTauxAdequationProductionConsommation}`,
    unite: '%',
    sources: [
        { source: SOURCES_DONNEES_RETRAITEES.surfaces_agricoles_crater },
        { source: SOURCES_DONNEES.consommation_sau, anneesMobilisees: '2019' }
    ]
};

const adequationTheoriqueBruteProductionConsommationAssietteDemitarienne: DefinitionIndicateur = {
    id: 'adequation-theorique-brute-production-consommation-assiette-demitarienne',
    libelle: 'Adéquation théorique brute entre production et consommation demitarienne',
    description: htmlstring`Cet indicateur représente la part de la consommation du territoire qui pourrait en théorie être couverte par sa propre
        production agricole, si les habitants du territoire adoptaient en moyenne un régime demitarien (diminution de moité des produits animaux). <br />`,
    unite: '%',
    sources: [
        { source: SOURCES_DONNEES_RETRAITEES.surfaces_agricoles_crater },
        { source: SOURCES_DONNEES.consommation_sau, anneesMobilisees: '2019' }
    ]
};

const adequationTheoriqueMoyennePondereeProductionConsommation: DefinitionIndicateur = {
    id: 'adequation-theorique-moyenne-ponderee-production-consommation',
    libelle: 'Adéquation théorique entre production et consommation',
    description: htmlstring`Cet indicateur représente la part de la consommation du territoire qui pourrait en théorie être couverte par sa propre
        production.
        <br/>${messageLimiteTauxAdequationProductionConsommation}`,
    unite: '%',
    sources: [
        { source: SOURCES_DONNEES_RETRAITEES.surfaces_agricoles_crater },
        { source: SOURCES_DONNEES.consommation_sau, anneesMobilisees: '2019' }
    ],
    nomIndicateurRequeteApi: 'production.tauxAdequationMoyenPonderePourcent',
    styleCarte: new StyleCarteChoroplethe({
        gradientCouleurs: new GradientCouleurs([
            { valeur: 0, couleur: '#ffffff' },
            { valeur: 100, couleur: COULEUR_CARTE_VERT }
        ]),
        labels: [
            { valeur: 0, label: '0' },
            { valeur: 20, label: '20' },
            { valeur: 40, label: '40' },
            { valeur: 60, label: '60' },
            { valeur: 80, label: '80' },
            { valeur: 100, label: '100' }
        ]
    })
};

const partProductionExportee: DefinitionIndicateur = {
    id: 'part-production-exportee',
    libelle: 'Part de la production exportée et part de la consommation importée',
    description: htmlstring`Estimation obtenue après analyse de 100 aires urbaines françaises. Pas de détail par territoire.`,
    unite: '',
    sources: [{ source: RAPPORTS_ETUDES.utopies }],
    estIndicateurNational: true
};

const partSAUBio: DefinitionIndicateur = {
    id: 'part-surface-agricole-en-bio',
    libelle: 'Part de surface agricole labellisée Agriculture Biologique',
    description: htmlstring`Cet indicateur correspond à la part de la surface agricole labellisée agriculture biologique (ou en conversion) dans la
        surface agricole totale du territoire.
        <br />
        L'agriculture biologique répond à un cahier des charges qui incorpore plusieurs <strong>pratiques agroécologiques</strong> et fait l'objet
        d'un suivi régulier.`,
    unite: '%',
    sources: [{ source: SOURCES_DONNEES.surfaces_agricoles_bio }, { source: SOURCES_DONNEES.rpg }],
    nomIndicateurRequeteApi: 'production.partSauBioPourcent',
    styleCarte: new StyleCarteChoroplethe({
        gradientCouleurs: new GradientCouleurs([
            { valeur: 0, couleur: '#ffffff' },
            { valeur: 100, couleur: COULEUR_CARTE_VERT }
        ]),
        labels: [
            { valeur: 0, label: '0' },
            { valeur: 20, label: '20' },
            { valeur: 40, label: '40' },
            { valeur: 60, label: '60' },
            { valeur: 80, label: '80' },
            { valeur: 100, label: '100' }
        ]
    })
};

const hvn: DefinitionIndicateur = {
    id: 'haute-valeur-naturelle',
    libelle: 'Indicateur HVN (Haute Valeur Naturelle)',
    description: htmlstring`Cet indicateur caractérise les systèmes agricoles qui maintiennent un
        haut niveau de biodiversité.
        <br />
        <p>Trois dimensions sont prises en compte :</p>
        <ul>
            <li>la diversité des assolements, qui indique la variété des cultures présentes sur les fermes ;</li>
            <li>l'extensivité des pratiques (faible niveau d'intrants, pesticides et engrais chimiques) ;</li>
            <li>la présence d’éléments du paysage à intérêt agroécologique, tels que des haies ou des prairies permanentes.</li>
        </ul>
        <p>N. B. : La valeur de l’indicateur est à prendre avec précaution lorsque la surface agricole du territoire considéré est inférieure à 300 ha.</p>`,
    unite: '',
    sources: [{ source: SOURCES_DONNEES.hvn, anneesMobilisees: '2017' }],
    nomIndicateurRequeteApi: 'pratiquesAgricoles.indicateurHvn.indiceTotal',
    nomIndicateurComplementaireRequeteApi: 'surfaceAgricoleUtile.sauProductiveHa',
    styleCarte: styleCarteHVNChorochromatique
};

const scoreProduction: DefinitionIndicateur = {
    id: 'score-production',
    libelle: 'Score du maillon Production',
    description: htmlstring`Ce score évalue la capacité du territoire à produire une alimentation répondant à la consommation des habitants du
    territoire tout en utilisant des pratiques agricoles qui soutiennent la biodiversité.`,
    unite: '',
    sources: [{ source: adequationTheoriqueMoyennePondereeProductionConsommation }, { source: hvn }, { source: partSAUBio }],
    nomIndicateurRequeteApi: 'production.note',
    styleCarte: new StyleCarteChoropletheNotes()
};

const distanceMoyenneParcourueAliments: DefinitionIndicateur = {
    id: 'distance-moyenne-parcourue-aliments',
    libelle: 'Distance moyenne parcourue par les denrées alimentaires',
    description: htmlstring`
    <p>Ce chiffre est obtenu en comparant le trafic annuel total en France lié aux denrées alimentaires (uniquement les produits finis) et la quantité moyenne totale de denrées alimentaires consommée par un Français sur une année.</p>
    <br>
    <p>Source : Territoires Fertiles, à partir des données de fret présentées dans l’étude ${creerLienExterneHtmlBrut('https://www.iddri.org/fr/publications-et-evenements/rapport/lempreinte-energetique-et-carbone-de-lalimentation-en-france-de', 'L’empreinte énergétique et carbone de l’alimentation en France (IDDRI, 2019)')}</p>
    `,
    descriptionDetaillee: htmlstring`
    <p>Le calcul de la distance moyenne parcourue par les denrées alimentaires ne porte que sur les produits finis, susceptibles d’être achetés par les consommateurs. Il n’intègre donc pas les étapes de transport intermédiaires entre les champs, les zones de stockage et les usines de transformation, ni le transport des aliments pour animaux. Il se fait de la manière suivante :</p>
    <ul>
        <li>on estime d’un côté la quantité d’aliments consommés par jour grâce aux données de l’étude ${creerLienExterneHtmlBrut('https://www.anses.fr/fr/content/inca-3-evolution-des-habitudes-et-modes-de-consommation-de-nouveaux-enjeux-en-mati%C3%A8re-de', 'INCA 3')}, elle s’élève à 1,4 kg par personne en moyenne hors eau de boisson</li>
        <li>en faisant l’hypothèse que les emballages représentent en moyenne 10 % du poids des aliments achetés et avec un taux de perte global de ${creerLienExterneHtmlBrut('https://www.inrae.fr/actualites/infographie-pertes-gaspillages-alimentaires', '10 %')} aux étapes de distribution et consommation, la quantité de produits alimentaires devant être acheminés dans les magasins est de 1,8 kg par personne par jour soit 42 millions de tonnes par an pour 65,7 millions de français</li>
        <li>de l’autre côté, on estime la quantité de transport nécessaire pour acheminer les produits alimentaires depuis leur site de production vers les commerces à partir des données de la figure 15 de l’étude ${creerLienExterneHtmlBrut('https://www.iddri.org/fr/publications-et-evenements/rapport/lempreinte-energetique-et-carbone-de-lalimentation-en-france-de', 'L’empreinte énergétique et carbone de l’alimentation en France (IDDRI, 2019)')} en essayant de prendre en compte uniquement les produits finis susceptibles d’être achetés par les consommateurs, cette quantité de transport s’élève à 50 Gt.km</li>
        <li>puis on fait le ratio entre la quantité de transport nécessaire et le poids global des produits à transporter pour obtenir une distance moyenne.</li>
    </ul>`,
    valeur: 1200,
    unite: 'km',
    sources: [{ source: RAPPORTS_ETUDES.greniersAbondance }],
    estIndicateurNational: true
};

const partPopulationDependanteVoiture: DefinitionIndicateur = {
    id: 'part-population-dependante-voiture',
    libelle: 'Part de la population théoriquement dépendante de la voiture pour ses achats alimentaires',
    description: htmlstring`Cet indicateur évalue la part de la population théoriquement dépendante de la voiture pour accéder à un ensemble
    représentatif de commerces alimentaires (ie ne disposant pas de 3 commerces de type différent accessibles à vélo depuis le domicile).`,
    unite: '%',
    sources: [
        { source: SOURCES_DONNEES.commerces_osm, anneesMobilisees: '2021' },
        { source: SOURCES_DONNEES.carroyage_insee, anneesMobilisees: '2015' }
    ],
    nomIndicateurRequeteApi: 'proximiteCommerces.partPopulationDependanteVoiturePourcent',
    styleCarte: new StyleCarteChoroplethe({
        gradientCouleurs: new GradientCouleurs([
            { valeur: 0, couleur: '#ffffff' },
            { valeur: 100, couleur: COULEUR_CARTE_BLEU_PETROLE }
        ]),
        labels: [
            { valeur: 0, label: '0' },
            { valeur: 20, label: '20' },
            { valeur: 40, label: '40' },
            { valeur: 60, label: '60' },
            { valeur: 80, label: '80' },
            { valeur: 100, label: '100' }
        ]
    })
};

const partTerritoireDependantVoiture: DefinitionIndicateur = {
    id: 'part-territoire-dependant-voiture',
    libelle: 'Part du territoire dont la population est en majorité théoriquement dépendante de la voiture pour ses achats alimentaires',
    description: htmlstring`Cet indicateur évalue la part du territoire dont plus de la moitié de la population est théoriquement dépendante de la
    voiture pour accéder à un ensemble représentatif de commerces alimentaires (ie ne disposant pas de 3 commerces de type différent accessibles à
    vélo depuis le domicile).`,
    unite: '%',
    sources: [
        { source: SOURCES_DONNEES.commerces_osm, anneesMobilisees: '2021' },
        { source: SOURCES_DONNEES.carroyage_insee, anneesMobilisees: '2015' }
    ],
    nomIndicateurRequeteApi: 'proximiteCommerces.partTerritoireDependantVoiturePourcent',
    styleCarte: new StyleCarteChoroplethe({
        gradientCouleurs: new GradientCouleurs([
            { valeur: 0, couleur: '#ffffff' },
            { valeur: 100, couleur: COULEUR_CARTE_BLEU_PETROLE }
        ]),
        labels: [
            { valeur: 0, label: '0' },
            { valeur: 20, label: '20' },
            { valeur: 40, label: '40' },
            { valeur: 60, label: '60' },
            { valeur: 80, label: '80' },
            { valeur: 100, label: '100' }
        ]
    })
};

const distancesPlusProchesCommerces: DefinitionIndicateur = {
    id: 'distances-plus-proches-commerces',
    libelle: 'Distances moyennes aux plus proches commerces par type de commerce alimentaire',
    description: htmlstring`Cet indicateur mesure la distance moyenne à vol d'oiseau entre le domicile et le plus proche commerce d'un type donné.`,
    unite: 'km',
    sources: [
        { source: SOURCES_DONNEES.commerces_osm, anneesMobilisees: '2021' },
        { source: SOURCES_DONNEES.carroyage_insee, anneesMobilisees: '2015' }
    ]
};

const scoreTransformationDistribution: DefinitionIndicateur = {
    id: 'score-transformation-distribution',
    libelle: `Score du maillon Transformation et Distribution`,
    description: htmlstring`Ce score évalue le niveau d'indépendance aux énergies fossiles des consommateurs pour leurs achats alimentaires. Il se
    base sur la part de la population indépendante de la voiture pour ses achats alimentaires.`,
    unite: '',
    sources: [{ source: partPopulationDependanteVoiture }],
    nomIndicateurRequeteApi: 'proximiteCommerces.note',
    styleCarte: new StyleCarteChoropletheNotes()
};

const partAlimentationAnimaleDansConsommation: DefinitionIndicateur = {
    id: 'part-alimentation-animale-dans-consommation',
    libelle: "Part de l'alimentation d'origine animale dans l'empreinte en surface de la consommation",
    description: htmlstring`Cet indicateur mesure la part de l'alimentation d'origine animale dans l'empreinte en surface de la consommation totale des
    habitants (selon le régime alimentaire actuel moyen d'un Français).`,
    unite: '%',
    sources: [{ source: SOURCES_DONNEES.consommation_sau, anneesMobilisees: '2019' }]
};

const tauxPauvrete: DefinitionIndicateur = {
    id: 'taux-pauvrete',
    libelle: 'Taux de pauvreté',
    description: htmlstring`${SOURCES_DONNEES.taux_pauvrete.definition}`,
    unite: '%',
    sources: [{ source: SOURCES_DONNEES.taux_pauvrete, anneesMobilisees: '2019' }],
    nomIndicateurRequeteApi: 'consommation.tauxPauvrete60Pourcent',
    styleCarte: new StyleCarteChoroplethe({
        gradientCouleurs: new GradientCouleurs([
            { valeur: 0, couleur: COULEUR_CARTE_BEIGE },
            { valeur: 30, couleur: COULEUR_CARTE_ROUGE }
        ]),
        labels: [
            { valeur: 0, label: '0' },
            { valeur: 5, label: '5' },
            { valeur: 10, label: '10' },
            { valeur: 15, label: '15' },
            { valeur: 20, label: '20' },
            { valeur: 25, label: '25' },
            { valeur: 30, label: '30+' }
        ]
    })
};

const aideAlimentaire: DefinitionIndicateur = {
    id: 'aide-alimentaire',
    libelle: "Recours à l'aide alimentaire",
    description: htmlstring`Statistique valable à l'échelle nationale, pas de détail par territoire.`,
    unite: '',
    sources: [{ source: RAPPORTS_ETUDES.aide_alimentaire }],
    estIndicateurNational: true
};

const partPopulationObese: DefinitionIndicateur = {
    id: 'part-population-obese',
    libelle: "Part de la population touchée par l'obésité",
    description: htmlstring`Statistique valable à l'échelle nationale, pas de détail par territoire.`,
    unite: '%',
    estIndicateurNational: true,
    sources: [{ source: RAPPORTS_ETUDES.obesite }]
};

const scoreConsommation: DefinitionIndicateur = {
    id: 'score-consommation',
    libelle: 'Score du maillon Consommation',
    description: htmlstring`Aucun score n'est actuellement disponible mais un message, donné à l'échelle nationale, évalue le régime alimentaire actuel
    moyen des Français ainsi que la précarité alimentaire.`,
    unite: '',
    sources: [],
    estIndicateurNational: true
};

type ID_INDICATEURS =
    | 'occupationSols'
    | 'otexMajoritaire5Postes'
    | 'scoreTerresAgricoles'
    | 'sauParHabitant'
    | 'politiqueAmenagement'
    | 'rythmeArtificialisation'
    | 'partLogementsVacants'
    | 'scoreAgriculteursExploitations'
    | 'nombreActifsAgricoles1988'
    | 'nombreActifsAgricoles2010'
    | 'partActifsAgricoles'
    | 'revenuAgriculteurs'
    | 'agesChefsExploitation'
    | 'superficiesExploitations'
    | 'superficieMoyenneExploitations'
    | 'scoreIntrants'
    | 'scoreEnergie'
    | 'consommationEnergetiqueParHectare'
    | 'postePrincipalEnergie'
    | 'scoreEau'
    | 'prelevementsIrrigation'
    | 'tendanceEvolutionPrelevementsIrrigation'
    | 'pratiquesIrrigation'
    | 'alertesSecheresse'
    | 'qsa'
    | 'nodu'
    | 'noduNormalise'
    | 'scoreProduction'
    | 'adequationTheoriqueBruteProductionConsommation'
    | 'adequationTheoriqueBruteProductionConsommationAssietteDemitarienne'
    | 'adequationTheoriqueMoyennePondereeProductionConsommation'
    | 'partProductionExportee'
    | 'partSAUBio'
    | 'hvn'
    | 'distanceMoyenneParcourueAliments'
    | 'scoreTransformationDistribution'
    | 'partPopulationDependanteVoiture'
    | 'partTerritoireDependantVoiture'
    | 'distancesPlusProchesCommerces'
    | 'scoreConsommation'
    | 'partAlimentationAnimaleDansConsommation'
    | 'tauxPauvrete'
    | 'aideAlimentaire'
    | 'partPopulationObese';

export const INDICATEURS: Record<ID_INDICATEURS, DefinitionIndicateur> = {
    occupationSols,
    otexMajoritaire5Postes,
    scoreTerresAgricoles,
    sauParHabitant,
    politiqueAmenagement,
    rythmeArtificialisation,
    partLogementsVacants,
    scoreAgriculteursExploitations,
    nombreActifsAgricoles1988,
    nombreActifsAgricoles2010,
    partActifsAgricoles,
    revenuAgriculteurs,
    agesChefsExploitation,
    superficiesExploitations,
    superficieMoyenneExploitations,
    scoreIntrants,
    scoreEnergie,
    consommationEnergetiqueParHectare,
    postePrincipalEnergie,
    scoreEau,
    prelevementsIrrigation,
    tendanceEvolutionPrelevementsIrrigation,
    pratiquesIrrigation,
    alertesSecheresse,
    qsa,
    nodu,
    noduNormalise,
    scoreProduction,
    adequationTheoriqueBruteProductionConsommation,
    adequationTheoriqueBruteProductionConsommationAssietteDemitarienne,
    adequationTheoriqueMoyennePondereeProductionConsommation,
    partProductionExportee,
    partSAUBio,
    hvn,
    distanceMoyenneParcourueAliments,
    scoreTransformationDistribution,
    partPopulationDependanteVoiture,
    partTerritoireDependantVoiture,
    distancesPlusProchesCommerces,
    scoreConsommation,
    partAlimentationAnimaleDansConsommation,
    tauxPauvrete,
    aideAlimentaire,
    partPopulationObese
};
