import { describe, expect, it } from 'vitest';

import { IDS_DOMAINES } from '../domaines/domaines.js';
import { INDICATEURS } from '../indicateurs/indicateurs-definitions.js';
import { SA } from './systeme-alimentaire.js';

describe('Test de la classe SystemeAlimentaire', () => {
    it('Test des getter', () => {
        expect(SA.getDomaineParIdDomaineOuIdIndicateurDetaille('inconnu')).toEqual(undefined);
        expect(SA.getDomaineParIdDomaineOuIdIndicateurDetaille(IDS_DOMAINES.adequationProductionConsommation)?.id).toEqual(
            IDS_DOMAINES.adequationProductionConsommation
        );
        expect(SA.getDomaineParIdDomaineOuIdIndicateurDetaille(INDICATEURS.adequationTheoriqueBruteProductionConsommation.id)?.id).toEqual(
            IDS_DOMAINES.adequationProductionConsommation
        );
    });
});
