import '../../charts/ChartLignes.js';
import '../composants/ChapitreDomaine.js';
import '../../../commun/PageSablierCrater.js';

import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { INDICATEURS } from '@lga/indicateurs';
import { IDS_MAILLONS } from '@lga/indicateurs';
import { IDS_DOMAINES } from '@lga/indicateurs';
import { SA } from '@lga/indicateurs';
import { CategorieTerritoire } from '@lga/territoires';
import { html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { PAGES_METHODOLOGIE } from '../../../../configuration/pages/declaration-pages.js';
import { construireUrlPageMethodologie } from '../../../../configuration/pages/pages-utils.js';
import { EtatRapport, type Rapport } from '../../../../modeles/diagnostics/Rapport.js';
import { construireLienVersSourcesIndicateur } from '../../../commun/liens-utils.js';
import { getVariableCss, Serie, SerieMultiple } from '../../charts/chart-outils.js';
import type { OptionsChartLignes } from '../../charts/ChartLignes';
import type { DonneesPageDiagnostic } from '../../PageDiagnostic.js';
import type { OptionsDomaineDetaille } from '../composants/ChapitreDomaine';

declare global {
    interface HTMLElementTagNameMap {
        'c-chapitre-nodu-normalise': ChapitreNoduNormalise;
    }
}

@customElement('c-chapitre-nodu-normalise')
export class ChapitreNoduNormalise extends LitElement {
    static styles = [STYLES_DESIGN_SYSTEM];

    @property({ attribute: false })
    donneesPage?: DonneesPageDiagnostic;

    @property({ attribute: false })
    rapport?: Rapport;

    render() {
        if (this.rapport?.getEtat() === EtatRapport.PRET) {
            return html`<c-chapitre-domaine .options=${this.calculerOptionsChapitreDomaineNoduNormalise(this.rapport, this.donneesPage)} contenuAdhoc>
                <div slot="contenu">
                    ${this.renderAvertissementQualiteDonnees()}

                    <c-message-domaine
                        .message=${`Cet indicateur correspond au ratio entre le <c-lien href=${PAGES_METHODOLOGIE.intrants.getUrl()}#${
                            IDS_DOMAINES.noduNormalise
                        }>NODU</c-lien> et la surface agricole du territoire. Il peut s’interpréter comme le nombre moyen de traitements de pesticides utilisés à leur dosage maximal autorisé que reçoivent les terres agricoles du territoire.`}
                        nbLignesMinimum="5"
                    ></c-message-domaine>
                    <c-figure .titre="${construireLienVersSourcesIndicateur(INDICATEURS.noduNormalise)}">
                        <c-chart-lignes .options=${this.calculerOptionsChartNoduNormalise(this.rapport)} slot="chart-figure"></c-chart-lignes>
                    </c-figure>
                </div>
            </c-chapitre-domaine>`;
        } else {
            return html`<c-page-sablier-crater></c-page-sablier-crater>`;
        }
    }

    private calculerOptionsChapitreDomaineNoduNormalise(rapport: Rapport, donneesPage: DonneesPageDiagnostic | undefined): OptionsDomaineDetaille {
        return {
            domaine: SA.getDomaine(IDS_DOMAINES.noduNormalise)!,
            territoire: rapport.getTerritoireActif(),
            estPagePourTef: donneesPage?.estPagePourTef
        };
    }

    private renderAvertissementQualiteDonnees() {
        return this.rapport?.getTerritoireActif()?.categorie.codeCategorie === CategorieTerritoire.Commune.codeCategorie
            ? html`
                  <p class="italique">
                      Note préalable : les valeurs données à l'échelle communale correspondent aux valeurs de l'EPCI d'appartenance des communes. Pour
                      plus de détails, voir la
                      <c-lien href="${construireUrlPageMethodologie(IDS_MAILLONS.intrants, IDS_DOMAINES.noduNormalise)}"
                          >méthodologie de calcul</c-lien
                      >. Pour avoir une idée du détail à l'échelle communale, nous vous invitons à regarder la
                      <c-lien href="https://solagro.org/nos-domaines-d-intervention/agroecologie/carte-pesticides-adonis">carte Adonis</c-lien>
                      produite par SOLAGRO sur la base d'une méthodologie différente.
                  </p>
              `
            : html``;
    }

    private calculerOptionsChartNoduNormalise(rapport: Rapport): OptionsChartLignes {
        return {
            series: new SerieMultiple(
                rapport.getDiagnostics().map((d) => {
                    return new Serie(d!.intrants.pesticides.indicateursParAnnees.nodusNormalises, d!.territoire.nom);
                })
            ),
            xaxisCategories: rapport.getDiagnosticPays()?.intrants.pesticides.indicateursParAnnees.annees ?? [],
            yaxisTitre: 'NODU normalisé [-]',
            couleurs: [
                getVariableCss('--c-ligne-serie-1'),
                getVariableCss('--c-ligne-serie-2'),
                getVariableCss('--c-ligne-serie-3'),
                getVariableCss('--c-ligne-serie-4'),
                getVariableCss('--c-ligne-serie-5')
            ]
        };
    }
}
