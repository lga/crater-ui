import type { CodeProfilAgricultureApi, CodeProfilCollectiviteApi } from '@lga/specification-api';

import type { JokerProfilAgriculture, JokerProfilCollectivite } from './Jokers';

export type TagProfilAgriculture = CodeProfilAgricultureApi | JokerProfilAgriculture;
export type TagProfilCollectivite = CodeProfilCollectiviteApi | JokerProfilCollectivite;
export type TagProfil = TagProfilAgriculture | TagProfilCollectivite;
