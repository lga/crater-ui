import { EvenementNaviguer } from '@lga/commun/build/evenements/EvenementNaviguer';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system';
import type { Maillon } from '@lga/indicateurs';
import { IDS_OUTILS_EXTERNES } from '@lga/indicateurs';
import { SA } from '@lga/indicateurs';
import { css, html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { STYLES_CRATER } from '../../../commun/pages-styles';

declare global {
    interface HTMLElementTagNameMap {
        'c-comment-agir': CommentAgir;
    }
}

@customElement('c-comment-agir')
export class CommentAgir extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        STYLES_CRATER,
        css`
            h3 {
                margin-bottom: 0.5rem;
            }

            h4 {
                margin-top: 0.1rem;
                margin-bottom: 0.3rem;
                color: var(--couleur-primaire);
            }

            h5 {
                margin-top: 0.05rem;
            }

            a {
                text-decoration: none;
                cursor: pointer;
            }

            p {
                color: var(--couleur-neutre-60);
                margin: 0;
            }

            .lot-leviers {
                display: flex;
                flex-wrap: wrap;
                flex-direction: row;
                padding-left: calc(2 * var(--dsem));
            }

            .levier {
                min-width: 250px;
                max-width: 500px;
                padding: var(--dsem);
                display: grid;
                flex-grow: 1;
                margin-top: var(--dsem);
            }

            .encart {
                display: flex;
                flex-direction: column;
                justify-content: space-between;
                min-height: 80px;
                background-color: var(--couleur-blanc);
                border: 1px solid var(--couleur-neutre-20);
                border-left: solid 3px var(--couleur-accent);
                margin-top: var(--dsem);
                padding: var(--dsem);
            }

            .encart:hover {
                filter: brightness(95%);
            }

            .ouvrir {
                text-align: right;
                color: var(--couleur-neutre-40);
            }

            .encart:hover .ouvrir {
                color: var(--couleur-accent);
            }

            .titre {
                color: var(--couleur-primaire);
            }
        `
    ];

    @property({ attribute: false })
    maillon?: Maillon;

    @property()
    idTerritoireParcel?: string | null;

    render() {
        return html` <div>
            <h3>En précisant le diagnostic et en construisant une stratégie alimentaire</h3>
            <div class="lot-leviers">
                ${this.maillon?.outilsExternes.map(
                    (oe) =>
                        html` <div class="levier">
                            <h6>${oe.question}</h6>
                            <a
                                class="encart"
                                href=${oe.lien.includes(SA.getOutilExterne(IDS_OUTILS_EXTERNES.parcel)!.lien)
                                    ? this.idTerritoireParcel
                                        ? oe.lien.replace('idterritoire=1', `idterritoire=${this.idTerritoireParcel}`)
                                        : SA.getOutilExterne(IDS_OUTILS_EXTERNES.parcel)!.lien
                                    : oe.lien}
                                target="_blank"
                            >
                                <h6 class="titre">${oe.titre}</h6>
                                <p>${oe.description}</p>
                                <div class="ouvrir texte-petit">→ Ouvrir</div>
                            </a>
                        </div>`
                )}
                <div></div>
            </div>
            <h3>En déclinant les voies de résilience</h3>
            <div class="lot-leviers">
                ${this.maillon?.voiesResilience.map(
                    (vr) =>
                        html` <div class="levier">
                            <h6>${vr.question}</h6>
                            <a
                                class="encart"
                                @click="${() => {
                                    this.naviguer(`${vr.lien}`, '_blank');
                                }}"
                            >
                                <h6 class="titre">Agir à l’échelle du territoire</h6>
                                <p>${vr.description}</p>
                                <div class="ouvrir texte-petit">→ Ouvrir</div>
                            </a>
                        </div>`
                )}
                <div></div>
            </div>
        </div>`;
    }

    private naviguer(url: string, cible = '_self') {
        this.dispatchEvent(new EvenementNaviguer(url, cible));
    }
}
