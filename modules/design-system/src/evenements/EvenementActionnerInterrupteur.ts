import { EvenementPersonnalisable } from './EvenementPersonnalisable';

export class EvenementActionnerInterrupteur extends EvenementPersonnalisable<{ estActive: boolean }> {
    static ID = 'actionnerInterrupteur';
    constructor(estActive: boolean) {
        super(EvenementActionnerInterrupteur.ID, { estActive: estActive });
    }
}
