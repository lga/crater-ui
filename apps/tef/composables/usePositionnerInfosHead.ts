import type { RouteLocationRaw } from 'vue-router/auto';

import { usePathVersUrlAbsolue, useRouteVersUrlAbsolue } from '@/composables';
import { useHead, useRouter, useSeoMeta } from '#imports';

export interface ChampsHead {
    titre: string | (() => string);
    description?: string | (() => string);
    routeUrlCanonique?: RouteLocationRaw;
    titreRS?: string | (() => string);
}

export function usePositionnerInfosHead(champsHead: ChampsHead) {
    const router = useRouter();
    const routePageCourante = router.currentRoute;

    const urlCanonique = champsHead.routeUrlCanonique
        ? useRouteVersUrlAbsolue(champsHead.routeUrlCanonique)
        : useRouteVersUrlAbsolue(routePageCourante.value);

    useHead({
        title: champsHead.titre,
        link: [
            {
                rel: 'canonical',
                href: urlCanonique
            }
        ]
    });

    // Lorsque qu'on laisse les champs à undefined, on bascule sur le fallback de nuxt-seo
    useSeoMeta({
        description: champsHead.description,
        ogUrl: urlCanonique,
        ogType: 'website',
        ogTitle: champsHead.titreRS,
        ogDescription: champsHead.description,
        ogImage: usePathVersUrlAbsolue('/images/accueil/territoires-fertiles-apercu-RS-2400x1260.png'),
        twitterCard: 'summary_large_image',
        twitterSite: urlCanonique,
        twitterImage: usePathVersUrlAbsolue('/images/accueil/territoires-fertiles-apercu-RS-1120x600.png'),
        twitterTitle: champsHead.titreRS,
        twitterDescription: champsHead.description
    });
}
