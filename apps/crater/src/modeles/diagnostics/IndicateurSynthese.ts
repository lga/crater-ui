import type { Note } from './Note';

export interface IndicateurSynthese {
    note: Note;
    messageSynthese: string;
}
