import { css } from 'lit';

export const STYLES_BADGES = css`
    .badge {
        background-color: var(--couleur-primaire);
        color: var(--couleur-blanc);
        border-radius: var(--dsem);
        width: fit-content;
        padding: calc(0.5 * var(--dsem)) calc(1 * var(--dsem));
    }
`;
