import type {
    AnaloguesClimatiquesApi,
    CodeNiveauRisquePrecariteAlimentaireApi,
    CodeOtexMajoritaire5PostesApi,
    CodeProfilDiagnosticApi,
    OccupationSolsApi,
    TerritoireBaseApi
} from '@lga/specification-api';
import type { SauParGroupeCultureApi } from '@lga/specification-api';
import type { BesoinsParGroupeCultureApi } from '@lga/specification-api';

import { creerDonneesApiVide } from './constats/constats-tests-utils';
import type { DonneesApi } from './fonctions-utils.js';

export function creerDonneesApi(options: {
    territoire?: TerritoireBaseApi;
    codeProfilDiagnostic?: CodeProfilDiagnosticApi;
    population?: number | null;
    occupationSols?: OccupationSolsApi;
    codeOtexMajoritaire5Postes?: CodeOtexMajoritaire5PostesApi;
    tauxAdequationAssietteActuelleBrutPourcent?: number | null;
    besoinsHaAssietteActuelle?: number | null;
    tauxAdequationAssietteDemitarienneBrutPourcent?: number | null;
    tauxAdequationMoyenPonderePourcent?: number | null;
    besoinsParGroupeCultureAssietteActuelle?: BesoinsParGroupeCultureApi[];
    besoinsParGroupeCultureAssietteDemitarienne?: BesoinsParGroupeCultureApi[];
    artificialisation5ansHa?: number | null;
    rythmeArtificialisationSauPourcent?: number | null;
    populationAgricole1988?: number | null;
    populationAgricole2010?: number | null;
    irrigationM32012?: number | null;
    irrigationM32022?: number | null;
    indicateurHvnIndiceTotal?: number | null;
    noduNormalise?: number | null;
    partPopulationDependanteVoiturePourcent?: number | null;
    analoguesClimatiques?: AnaloguesClimatiquesApi;
    sauParGroupeCulture?: SauParGroupeCultureApi[];
    sauProductiveHa?: number;
    codeNiveauRisquePrecariteAlimentaire?: CodeNiveauRisquePrecariteAlimentaireApi;
}): DonneesApi {
    const donneesApi = creerDonneesApiVide();
    if (options.territoire) donneesApi.territoireApi = options.territoire;
    donneesApi.diagnosticApi.population = options.population ?? null;
    if (options.occupationSols) donneesApi.diagnosticApi.occupationSols = options.occupationSols;
    if (options.codeOtexMajoritaire5Postes) donneesApi.diagnosticApi.codeOtexMajoritaire5Postes = options.codeOtexMajoritaire5Postes;
    donneesApi.diagnosticApi.profils.codeProfilDiagnostic = options.codeProfilDiagnostic ?? 'DEFAUT';
    donneesApi.diagnosticApi.productionsBesoins.besoinsAssietteActuelle.tauxAdequationBrutPourcent =
        options.tauxAdequationAssietteActuelleBrutPourcent ?? null;
    donneesApi.diagnosticApi.productionsBesoins.besoinsAssietteActuelle.besoinsHa = options.besoinsHaAssietteActuelle ?? null;
    donneesApi.diagnosticApi.productionsBesoins.besoinsAssietteDemitarienne.tauxAdequationBrutPourcent =
        options.tauxAdequationAssietteDemitarienneBrutPourcent ?? null;
    donneesApi.diagnosticApi.productionsBesoins.besoinsAssietteActuelle.tauxAdequationMoyenPonderePourcent =
        options.tauxAdequationMoyenPonderePourcent ?? null;
    donneesApi.diagnosticApi.productionsBesoins.besoinsAssietteActuelle.besoinsParGroupeCulture =
        options.besoinsParGroupeCultureAssietteActuelle ?? [];
    donneesApi.diagnosticApi.productionsBesoins.besoinsAssietteDemitarienne.besoinsParGroupeCulture =
        options.besoinsParGroupeCultureAssietteDemitarienne ?? [];
    donneesApi.diagnosticApi.politiqueFonciere.artificialisation5ansHa = options.artificialisation5ansHa ?? null;
    donneesApi.diagnosticApi.politiqueFonciere.rythmeArtificialisationSauPourcent = options.rythmeArtificialisationSauPourcent ?? null;
    donneesApi.diagnosticApi.populationAgricole.populationAgricole1988 = options.populationAgricole1988 ?? null;
    donneesApi.diagnosticApi.populationAgricole.populationAgricole2010 = options.populationAgricole2010 ?? null;
    donneesApi.diagnosticApi.eau.irrigation.indicateursParAnnees = [
        {
            annee: 2012,
            irrigationM3: options.irrigationM32012 ?? null,
            irrigationMM: null
        },
        {
            annee: 2022,
            irrigationM3: options.irrigationM32022 ?? null,
            irrigationMM: null
        }
    ];
    donneesApi.diagnosticApi.proximiteCommerces.partPopulationDependanteVoiturePourcent = options.partPopulationDependanteVoiturePourcent ?? null;
    donneesApi.diagnosticApi.pratiquesAgricoles.indicateurHvn.indiceTotal = options.indicateurHvnIndiceTotal ?? null;
    donneesApi.diagnosticApi.pesticides.noduNormalise = options.noduNormalise ?? null;
    donneesApi.diagnosticApi.climat.analoguesClimatiques = options.analoguesClimatiques ?? {};
    if (options.sauParGroupeCulture) donneesApi.diagnosticApi.surfaceAgricoleUtile.sauParGroupeCulture = options.sauParGroupeCulture;
    if (options.sauProductiveHa) donneesApi.diagnosticApi.surfaceAgricoleUtile.sauProductiveHa = options.sauProductiveHa;
    donneesApi.diagnosticApi.profils = {
        codeProfilDiagnostic: options.codeProfilDiagnostic ?? 'DEFAUT',
        codeProfilCollectivite: 'PAYS',
        codeProfilAgriculture: 'NON_SPECIALISE'
    };
    if (options.codeNiveauRisquePrecariteAlimentaire)
        donneesApi.diagnosticApi.consommation.codeNiveauRisquePrecariteAlimentaire = options.codeNiveauRisquePrecariteAlimentaire;

    return donneesApi;
}
