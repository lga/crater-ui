import { formaterNombreSelonValeurString, htmlstring, type ImageModele } from '@lga/base';
import { INDICATEURS } from '@lga/indicateurs';
import { recupererListe3InitiativesParConstats } from '@lga/initiatives';

import type { DonneesApi } from '../fonctions-utils.js';
import { calculerTendanceEvolutionPrelevementEauPourcent } from '../fonctions-utils.js';
import { calculerLibelleSourcesIndicateur, creerLienExterneHtmlBrut } from '../liens-utils.js';
import type { ConstatModele } from './ConstatModele';
import type { InfographieImageChiffreCleModele, InfographieImageMessageModele } from './InfographieModele.js';

export const IMAGE_BATEAU_PORTE_CONTENEURS: ImageModele = {
    url: `/images/constats/titres/porte-conteneurs-800x868.jpg`,
    descriptionAlt: 'Bateau porte-conteneurs',
    resolutionX: 800,
    resolutionY: 868
};

export function creerConstatDependancesRessources(donneesApi: DonneesApi): ConstatModele<InfographieImageMessageModele> {
    return {
        id: 'dependances-ressources',
        idTheme: 'ressources',
        titre: {
            libelle: 'La souveraineté alimentaire, une illusion ?',
            image: IMAGE_BATEAU_PORTE_CONTENEURS
        },
        message: {
            titre: 'La production de notre nourriture dépend des énergies fossiles et d’autres ressources critiques importées.',
            texte: htmlstring`Leur approvisionnement va subir des contraintes dans les années à venir. Moins d’engrais azotés ou du pétrole plus cher, ce sont des rendements qui diminuent et des coûts de production qui augmentent.`,
            texteCourt: htmlstring`Prix volatiles et <strong>approvisionnements fragiles</strong> impactent le coût de notre alimentation.`,
            question: 'Et si on aidait les fermes à regagner de l’autonomie ?',
            initiatives: recupererListe3InitiativesParConstats(donneesApi.diagnosticApi.profils, ['dependances-ressources'])
        },
        infographie: {
            nomComposant: 'InfographieImageMessage',
            informationsMethodologie: {
                titre: 'Consommation d’engrais, de pétrole et de pesticides',
                descriptif: htmlstring`La France consomme chaque année pour sa production agricole : 
            8,5 millions de tonnes d’engrais minéraux, 
            69 600 tonnes de pesticides, 
            25 millions de tonnes équivalent pétrole. 
            Nous avons rapporté ces chiffres aux nombres d’habitants divisé par 365 jours. 
            Les données proviennent de l’Observatoire national de la fertilisation minérale et organique, GraphAgri et l’ADEME.`,
                estPerimetreNational: true,
                libelleBouton: 'Chiffres'
            },
            message: htmlstring`Ce qui se cache derrière <em class='not-italic'>une seule journée</em> de votre alimentation`,
            image: {
                url: '/images/constats/infographies/plateau-repas-intrants-1025x650.png',
                descriptionAlt: 'Plateau repas montrant le contenu de nos repas en pétrole, engrais et pesticides',
                resolutionX: 1025,
                resolutionY: 650,
                espacerImageEtMessage: true,
                largeurPrefereePx: 500
            }
        }
    };
}

export const sourceImportationSoja = htmlstring`Données utilisées pour le calcul : importations nettes de tourteaux de soja = 2,9 millions de tonnes (FAOSTAT, moyenne 2018-2022) ; importations nettes de fèves de soja = 430 000 tonnes (FAOSTAT, moyenne 2018-2022) ; rendement de trituration = 0,8.`;
export const chiffreImportationSojaMillionsTonnes = '3,2';

export function creerConstatAlimentationAnimale(donneesApi: DonneesApi): ConstatModele<InfographieImageChiffreCleModele> {
    return {
        id: 'alimentation-animale-importee',
        idTheme: 'ressources',
        titre: {
            libelle: 'La souveraineté alimentaire, une illusion ?',
            image: IMAGE_BATEAU_PORTE_CONTENEURS
        },
        message: {
            titre: 'L’élevage local est tributaire des importations d’aliments.',
            texte: htmlstring`La culture du soja participe à la destruction de la forêt amazonienne, un des écosystèmes les plus riches de la planète. La dépendance des élevages français à ces importations les rend vulnérables face à une envolée des prix ou à des restrictions commerciales.`,
            question: 'Et si on aidait les fermes à regagner de l’autonomie ?',
            initiatives: recupererListe3InitiativesParConstats(donneesApi.diagnosticApi.profils, ['alimentation-animale-importee'])
        },
        infographie: {
            nomComposant: 'InfographieImageChiffreCle',
            informationsMethodologie: {
                titre: 'Importation du soja',
                descriptif: sourceImportationSoja,
                estPerimetreNational: true
            },
            chiffreCle: {
                chiffre: chiffreImportationSojaMillionsTonnes,
                unite: 'millions',
                phrase: "de tonnes de soja sont importées chaque année pour l'alimentation animale en France."
            },
            image: {
                url: '/images/constats/infographies/deforestation-416x448.png',
                descriptionAlt: 'Forêt avec de nombreux arbres abattus',
                resolutionX: 416,
                resolutionY: 448,
                position: 'gauche'
            },
            accroche: 'L’envers du décor'
        }
    };
}

export const sourceEngrais = htmlstring`Résultats 2021 de l’observatoire national de la fertilisation minérale et organique. ${creerLienExterneHtmlBrut('https://anpea.com/wp-content/uploads/2022/12/2021ANPEA-Observatoire-FERTI.pdf', 'Voir le document en ligne')}.`;
export const chiffreEngraisTonnes = 8500000;

export const anneeInitialeConstatEau = 2012;
export const anneeFinaleConstatEau = 2022;

export function creerConstatPrelevementsEau(donneesApi: DonneesApi): ConstatModele<InfographieImageChiffreCleModele> {
    const irrigationIndicateursParAnnees = donneesApi.diagnosticApi.eau.irrigation.indicateursParAnnees;
    const intervalleAnnees = anneeFinaleConstatEau - anneeInitialeConstatEau;
    const evolutionPrelementEau = formaterNombreSelonValeurString(
        calculerTendanceEvolutionPrelevementEauPourcent(irrigationIndicateursParAnnees, anneeInitialeConstatEau, anneeFinaleConstatEau),
        true
    );

    const partMaisGrainDansSauProductive =
        ((donneesApi.diagnosticApi.surfaceAgricoleUtile.sauParGroupeCulture
            .find((i) => i.codeGroupeCulture === 'CER')
            ?.sauParCulture.find((i) => i.codeCulture === 'P-MAG')?.sauHa ?? 0) /
            donneesApi.diagnosticApi.surfaceAgricoleUtile.sauProductiveHa) *
        100;
    const complementNomCulture = partMaisGrainDansSauProductive >= 10 ? ', en particulier du maïs grain,' : '';

    return {
        id: 'prelevements-eau',
        idTheme: 'ressources',
        titre: {
            libelle: 'Un accès à l’eau garanti ?',
            image: {
                url: '/images/constats/titres/piscine-champ-ble-800x868.jpg',
                descriptionAlt: 'Piscine devant un champ de blé',
                resolutionX: 800,
                resolutionY: 868
            }
        },
        message: {
            titre: 'L’accès à l’eau est en train de devenir un sujet majeur.',
            texte: htmlstring`L’irrigation des cultures${complementNomCulture} et la consommation d'eau des habitants ne cessent d’augmenter. Demain, avec des sécheresses qui s’accentuent, il faudra faire des choix pour savoir qui aura accès à l’eau.`,
            question: 'Et si on réservait l’irrigation aux cultures essentielles ?',
            initiatives: recupererListe3InitiativesParConstats(donneesApi.diagnosticApi.profils, ['prelevements-eau'])
        },
        infographie: {
            nomComposant: 'InfographieImageChiffreCle',
            informationsMethodologie: {
                titre: 'Consommation d’eau pour l’agriculture',
                descriptif: htmlstring`
                ${INDICATEURS.tendanceEvolutionPrelevementsIrrigation.descriptionDetaillee ?? ''}
                <br>
                <p>Source : ${calculerLibelleSourcesIndicateur(INDICATEURS.tendanceEvolutionPrelevementsIrrigation)}</p>`
            },
            chiffreCle: {
                chiffre: evolutionPrelementEau,
                unite: '%',
                phrase: `d'eau consommée pour l'agriculture en ${intervalleAnnees} ans sur votre territoire`
            },
            image: {
                url: '/images/constats/infographies/sol-secheresse-416x448.png',
                descriptionAlt: 'Sol craquelé par la sécheresse',
                resolutionX: 416,
                resolutionY: 448,
                position: 'gauche'
            },
            accroche: 'Ça s’arrose ?'
        }
    };
}
