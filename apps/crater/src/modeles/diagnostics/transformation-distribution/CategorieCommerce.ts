export class CategorieCommerce {
    static readonly CommerceGeneraliste = new CategorieCommerce('COMMERCE_GENERALISTE', 'Commerce généraliste', 1, false, '#444242');
    static readonly BoulangeriePatisserie = new CategorieCommerce('BOULANGERIE_PATISSERIE', 'Boulangerie / Pâtisserie', 2, false, '#f1c232');
    static readonly BoucheriePoissonnerie = new CategorieCommerce('BOUCHERIE_POISSONNERIE', 'Boucherie / Poissonnerie', 3, false, '#cc0000');
    static readonly CommerceSpecialise = new CategorieCommerce('COMMERCE_SPECIALISE', 'Autre commerce spécialisé', 4, false, '#3d85c6');
    static readonly TroisTypesCommerces = new CategorieCommerce('TOUS_PLUS_LOIN_3EME_QUARTILE', 'Distance pour 3 types de commerces', -1, true);

    private constructor(
        public readonly code: string,
        public readonly libelle: string,
        private ordre: number,
        public categorieAgregee: boolean,
        public couleur?: string
    ) {}

    static get listerToutes(): CategorieCommerce[] {
        const liste = Object.values(CategorieCommerce).filter((c) => c instanceof CategorieCommerce);
        return liste.sort((c1, c2) => c1.comparer(c2));
    }

    static fromString(code: string): CategorieCommerce {
        const categorieCommerce = this.listerToutes.find((c) => c.code === code);
        if (categorieCommerce) return categorieCommerce;

        throw new RangeError(`Valeur incorrecte : le code catégorie "${code}" ne correspondent à aucun des codes de l'énumération`);
    }

    comparer(categorie: CategorieCommerce) {
        return this.ordre - categorie.ordre;
    }
}
