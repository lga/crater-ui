import ExifReader from 'exifreader';
import fs from 'fs';
import path from 'path';

import type { ImageDefinition } from '../src';

// Les fichiers images sont stockés dans un dossier public d'une app web (ici TEF)
// (copié depuis https://nuage.resiliencealimentaire.org/apps/files/files/215941?dir=/Territoires%20Fertiles/01-UX-UI/04_Images/01_img_prod)
// Le nom des fichiers images doit respecter le format suivant : "img0123-Lorem ipsum.jpg", afin d'extraire "img0123" qui est l'id de l'image
// Les metadonnées suivantes doivent être renseignées :
//    - le champ IPTC "Titre" (ou "Headline") qui sera utilisé comme description alternative
//    - le champ IPTC "Credit" : contient le nom de l'auteur / la structure à créditer pour la photo
//    - le champ IPTC "Source" : l'url de la source
//
// Les 2 variables ci dessous permettent au script de parcourir ces fichiers images et de générer un nom d'image utilisable pour construire ensuite des urls (ex:  '/images/galerie/img001-xxxx.jpg')
const DOSSIER_APP_PUBLIC = '../../../apps/tef/public';
const SOUS_DOSSIER_IMAGES_GALERIE = '/images/galerie';
const DOSSIER_APP_PUBLIC_IMAGES_GALERIE = path.join(DOSSIER_APP_PUBLIC, SOUS_DOSSIER_IMAGES_GALERIE);

const FICHIER_TS_A_GENERER = '../src/images-definitions.ts';
const FICHIER_HTML_TEST_IMAGE = '../build/page-test-images.html';
if (!fs.existsSync('../build')) {
    fs.mkdirSync('../build');
}

interface ErreurLectureImage {
    nomFichier: string;
    erreur: string;
}
const erreurs: ErreurLectureImage[] = [];

const estFichierImage = (nomFichier: string) => {
    const nomFichierMinuscules = nomFichier.toLowerCase();
    return (
        nomFichierMinuscules.endsWith('.jpg') ||
        nomFichierMinuscules.endsWith('.jpeg') ||
        nomFichierMinuscules.endsWith('.webp') ||
        nomFichierMinuscules.endsWith('.png')
    );
};

const formaterVersStringUtf8 = (valeur: unknown): string => {
    const convertirString = (valeur: string): string => {
        return Buffer.from(valeur.replace(/\n/g, ' '), 'latin1').toString('utf-8');
    };
    if (typeof valeur === 'string') {
        return convertirString(valeur);
    } else if (Array.isArray(valeur)) {
        return convertirString(valeur.join(', '));
    } else {
        return convertirString(String(valeur));
    }
};

const extraireId = (nomFichier: string) => {
    const regexMatcher = nomFichier.match(/^img\d{4}(?=_)/);
    if (regexMatcher) {
        return regexMatcher[0];
    } else {
        erreurs.push({ nomFichier, erreur: `Nom de fichier incorrect, pas d'id image trouvé dans le préfixe : ${nomFichier}` });
        return '';
    }
};

const extraireUrlRelative = (cheminFichier: string) => {
    return cheminFichier.replace(DOSSIER_APP_PUBLIC, '');
};

const extraireTagNumber = (nomFichier: string, nomTag: string, valeurTag: number | undefined) => {
    if (valeurTag) {
        return valeurTag;
    } else {
        erreurs.push({
            nomFichier,
            erreur: `Valeur du tag ${nomTag} incorrecte. Il faut une valeur numérique non nulle (valeur actuelle = ${valeurTag})`
        });
        return 0;
    }
};

const extraireTagString = (
    nomFichier: string,
    nomTag: string,
    valeurTag: string | ExifReader.XmpTags | number[] | ExifReader.XmpTag[] | undefined
) => {
    const valeurTagString = formaterVersStringUtf8(valeurTag);
    if (valeurTagString) {
        return valeurTagString;
    } else {
        erreurs.push({
            nomFichier,
            erreur: `Valeur du tag ${nomTag} incorrecte. Il faut une valeur texte non vide (valeur actuelle = ${valeurTagString})`
        });
        return '';
    }
};

const construireImageDefinition = (cheminFichier: string, nomFichier: string) => {
    const bufferFichier = fs.readFileSync(cheminFichier);
    const tags = ExifReader.load(bufferFichier);
    const imageDefinition: ImageDefinition = {
        id: extraireId(nomFichier),
        resolutionX: extraireTagNumber(nomFichier, 'Image Width', tags['Image Width']?.value),
        resolutionY: extraireTagNumber(nomFichier, 'Image Height', tags['Image Height']?.value),
        url: extraireUrlRelative(cheminFichier),
        descriptionAlt: extraireTagString(nomFichier, 'Headline', tags.Headline?.value),
        credit: extraireTagString(nomFichier, 'Credit', tags.Credit?.value),
        source: extraireTagString(nomFichier, 'Source', tags.Source?.value)
    };
    return imageDefinition;
};

const construireImagesDefinitions = () => {
    const imagesDefinitions: ImageDefinition[] = [];
    const nomFichiersImages = fs.readdirSync(DOSSIER_APP_PUBLIC_IMAGES_GALERIE);

    for (const nomFichierImage of nomFichiersImages) {
        if (estFichierImage(nomFichierImage)) {
            const imageDefinition = construireImageDefinition(path.join(DOSSIER_APP_PUBLIC_IMAGES_GALERIE, nomFichierImage), nomFichierImage);
            imagesDefinitions.push(imageDefinition);
        }
    }
    return imagesDefinitions;
};

const genererBlocTsImageDefinition = (image: ImageDefinition): string => {
    return `export const ${image.id.toUpperCase()}: ImageDefinition = {
    id: '${image.id}',
    url: '${image.url}',
    descriptionAlt: \`${image.descriptionAlt}\`,
    resolutionX: ${image.resolutionX},
    resolutionY: ${image.resolutionY},
    credit: \`${image.credit}\`,
    source: \`${image.source}\`
};`;
};

const genererFichierTsImagesDefinitions = (fichierTsAGenerer: string, imagesDefinitions: ImageDefinition[]) => {
    const contenuFichierTs = `// Fichier généré automatiquement par le script generer-definitions-images.ts
import type { ImageDefinition } from './ImageDefinition';

${imagesDefinitions.map(genererBlocTsImageDefinition).join('\n\n')}`;

    fs.writeFileSync(fichierTsAGenerer, contenuFichierTs);
    console.log(`Fichier créé : ${fichierTsAGenerer}`);
};

const genererPageTestImages = (fichierHtmlAGenerer: string, imagesDefinitions: ImageDefinition[]) => {
    const contenuPageHtml = `
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Page de test des images</title>
</head>
<body style='display: flex;gap:10px;justify-content: center;flex-wrap: wrap'>
        ${imagesDefinitions
            .map(
                (image) => `
        <article style='width: 250px'>
            <img style='width: 100%;height: auto' src="../../../apps/tef/public${image.url}" alt="${image.descriptionAlt}" width="${image.resolutionX}" height="${image.resolutionY}">
            <p style='font-size: 12px;width: 100%'>
            Url: ${image.url}<br>
            Description: ${image.descriptionAlt}<br>
            ${image.resolutionX}x${image.resolutionY}<br>
            Crédit: ${image.credit}<br>
            Source: ${image.source}
            </p>
        </article>
        `
            )
            .join('\n')}
</body>
</html>
    `;

    fs.writeFileSync(fichierHtmlAGenerer, contenuPageHtml);
    console.log(`Fichier ${fichierHtmlAGenerer} créé`);
};

try {
    erreurs.length = 0;

    if (fs.existsSync(FICHIER_TS_A_GENERER)) {
        fs.unlinkSync(FICHIER_TS_A_GENERER);
    }
    const imagesDefinitions: ImageDefinition[] = construireImagesDefinitions();

    if (erreurs.length > 0) {
        console.error(
            `ERREURS : Abandon lors de la génération du fichier ${FICHIER_TS_A_GENERER}, les metadonnnées des fichiers suivant sont incorrectes`,
            erreurs.map((erreur) => `${erreur.nomFichier} : ${erreur.erreur}`).join('\n')
        );
        process.exit(1);
    }

    genererFichierTsImagesDefinitions(FICHIER_TS_A_GENERER, imagesDefinitions);
    genererPageTestImages(FICHIER_HTML_TEST_IMAGE, imagesDefinitions);
} catch (error) {
    console.error(`Erreur lors de la génération du fichier ${FICHIER_TS_A_GENERER}`, error);
}
