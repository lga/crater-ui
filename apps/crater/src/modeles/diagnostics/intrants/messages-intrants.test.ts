import { describe, expect, it } from 'vitest';

import { Note } from '../Note';
import { calculerMessageSyntheseIntrants } from './messages-intrants';

describe(`Tests calculs message intrants`, () => {
    it('Calculer le message intrants', () => {
        expect(calculerMessageSyntheseIntrants(new Note(1.4), new Note(1.4), new Note(1.4))).toEqual(
            "Dépendance <strong>très marquée</strong> à l'énergie, à l'eau d'irrigation et aux pesticides."
        );
        expect(calculerMessageSyntheseIntrants(new Note(1.4), new Note(7), new Note(5))).toEqual(
            "Dépendance <strong>très marquée</strong> à l'énergie, <strong>marquée</strong> aux pesticides et <strong>relativement faible</strong> à l'eau d'irrigation."
        );
        expect(calculerMessageSyntheseIntrants(new Note(7), new Note(5), new Note(1.4))).toEqual(
            "Dépendance <strong>très marquée</strong> aux pesticides, <strong>marquée</strong> à l'eau d'irrigation et <strong>relativement faible</strong> à l'énergie."
        );
        expect(calculerMessageSyntheseIntrants(new Note(1.4), new Note(7), new Note(7))).toEqual(
            "Dépendance <strong>très marquée</strong> à l'énergie et <strong>relativement faible</strong> à l'eau d'irrigation et aux pesticides."
        );
        expect(calculerMessageSyntheseIntrants(new Note(1.4), new Note(1.4), new Note(7))).toEqual(
            "Dépendance <strong>très marquée</strong> à l'énergie et à l'eau d'irrigation et <strong>relativement faible</strong> aux pesticides."
        );
        expect(calculerMessageSyntheseIntrants(new Note(1.4), new Note(5), new Note(null))).toEqual(
            "Dépendance <strong>très marquée</strong> à l'énergie et <strong>marquée</strong> à l'eau d'irrigation."
        );
        expect(calculerMessageSyntheseIntrants(new Note(1.4), new Note(null), new Note(5))).toEqual(
            "Dépendance <strong>très marquée</strong> à l'énergie et <strong>marquée</strong> aux pesticides."
        );
        expect(calculerMessageSyntheseIntrants(new Note(null), new Note(1.4), new Note(5))).toEqual(
            "Dépendance <strong>très marquée</strong> à l'eau d'irrigation et <strong>marquée</strong> aux pesticides."
        );
        expect(calculerMessageSyntheseIntrants(new Note(null), new Note(null), new Note(5))).toEqual(
            'Dépendance <strong>marquée</strong> aux pesticides.'
        );
        expect(calculerMessageSyntheseIntrants(new Note(null), new Note(null), new Note(null))).toEqual('Données non disponibles.');
    });
});
