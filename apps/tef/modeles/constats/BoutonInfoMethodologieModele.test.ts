import { describe, expect, it } from 'vitest';

import { calculerLibelleBoutonInfoMethodologie, calculerLibelleMentionNationalBoutonInfoMethodologie } from './BoutonInfoMethodologieModele.js';

describe('Tests BoutonInfoMethodologieModele', () => {
    it('Test de la fonction calculerLibelleMentionNationalBoutonInfoMethodologie', () => {
        expect(calculerLibelleMentionNationalBoutonInfoMethodologie('Chiffre')).toEqual('Chiffre national');
        expect(calculerLibelleMentionNationalBoutonInfoMethodologie('Chiffres')).toEqual('Chiffres nationaux');
        expect(calculerLibelleMentionNationalBoutonInfoMethodologie('Méthode de calcul')).toEqual('');
    });
    it('Test de la fonction calculerLibelleBoutonInfoMethodologie', () => {
        expect(calculerLibelleBoutonInfoMethodologie('Chiffre', true)).toEqual(" - d'où vient-il");
        expect(calculerLibelleBoutonInfoMethodologie('Chiffre', false)).toEqual("D'où vient ce chiffre");
        expect(calculerLibelleBoutonInfoMethodologie('Chiffres', true)).toEqual(" - d'où viennent-ils");
        expect(calculerLibelleBoutonInfoMethodologie('Chiffres', false)).toEqual("D'où viennent ces chiffres");
        expect(calculerLibelleBoutonInfoMethodologie('Méthode de calcul', true)).toEqual('Méthode de calcul');
        expect(calculerLibelleBoutonInfoMethodologie('Méthode de calcul', false)).toEqual('Méthode de calcul');
    });
});
