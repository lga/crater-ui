import { type RouteLocationRaw, type RouteParams, useRoute } from 'vue-router/auto';

export type TypeModuleParcel = 'Emplois' | 'Relocalisation' | 'ImpactsEcologiques' | 'ChangementAlimentation' | undefined;

export function useRouteSimulateur(idTerritoire?: string | null, typeModule?: TypeModuleParcel): RouteLocationRaw<'simulateur-idTerritoire'> {
    const route: RouteLocationRaw<'simulateur-idTerritoire'> = {
        name: 'simulateur-idTerritoire',
        // eslint-disable-next-line @typescript-eslint/prefer-nullish-coalescing
        params: { idTerritoire: idTerritoire === null ? undefined : idTerritoire },
        query: { typeModule: typeModule }
    };
    return route;
}

export function useRouteParamsSimulateur(): RouteParams<'simulateur-idTerritoire'> {
    return useRoute().params as RouteParams<'simulateur-idTerritoire'>;
}
