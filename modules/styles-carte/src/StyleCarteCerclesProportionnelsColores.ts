import { COULEUR_VALEUR_NULL } from './couleurs-carte.js';
import type { FonctionCalculerLibelleTooltip, SeuilLegende } from './style-carte-utils.js';
import { type AttributsStyleCarteCerclesProportionnels, StyleCarteCerclesProportionnels } from './StyleCarteCerclesProportionnels.js';
import type { ClasseCarteChorochromatique, FonctionNombreVersCodeClasse } from './StyleCarteChorochromatique.js';

export interface AttributsStyleCarteCerclesProportionnelsColores extends AttributsStyleCarteCerclesProportionnels {
    readonly classes: ClasseCarteChorochromatique[];
    readonly fonctionNombreVersCodeClasse?: FonctionNombreVersCodeClasse;
    readonly calculerLibelleTooltipRayonCercle?: FonctionCalculerLibelleTooltip;
}

export class StyleCarteCerclesProportionnelsColores extends StyleCarteCerclesProportionnels {
    readonly classes: ClasseCarteChorochromatique[];
    readonly estIndicateurContinu = false;
    readonly fonctionNombreVersCodeClasse?: FonctionNombreVersCodeClasse;
    readonly calculerLibelleTooltipRayonCercle?: FonctionCalculerLibelleTooltip;

    constructor(attributs: AttributsStyleCarteCerclesProportionnelsColores) {
        super(attributs);
        this.classes = attributs.classes;
        this.fonctionNombreVersCodeClasse = attributs.fonctionNombreVersCodeClasse;
        if (attributs.calculerLibelleTooltipRayonCercle) this.calculerLibelleTooltipRayonCercle = attributs.calculerLibelleTooltipRayonCercle;
    }

    calculerCodeClasse(valeurCouleur: number | string | null): string | number | null {
        if (typeof valeurCouleur === 'number') {
            return this.fonctionNombreVersCodeClasse ? this.fonctionNombreVersCodeClasse(valeurCouleur) : null;
        }
        return valeurCouleur;
    }

    calculerCouleurCercle(valeurCouleur: number | string | null): string {
        const classeCarte = this.classes.find((classe) => classe.code === this.calculerCodeClasse(valeurCouleur));
        if (classeCarte === undefined || classeCarte === null) {
            return COULEUR_VALEUR_NULL;
        } else {
            return classeCarte.couleur;
        }
    }

    get seuilsLegende(): SeuilLegende[] {
        return this.classes.map((classe) => {
            return { libelle: classe.libelle, couleur: classe.couleur };
        });
    }
}
