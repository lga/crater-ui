import type { IdTheme } from '@lga/indicateurs';
import { type Component, defineAsyncComponent } from 'vue';

const AsyncThematiqueTerresAgricoles = defineAsyncComponent<Component>(
    () => import('~/components/diagnostics/thematiques/ThematiqueTerresAgricoles.vue')
);
const AsyncThematiqueAutonomieAlimentaire = defineAsyncComponent<Component>(
    () => import('~/components/diagnostics/thematiques/ThematiqueAutonomieAlimentaire.vue')
);
const AsyncThematiqueTransformationDistribution = defineAsyncComponent<Component>(
    () => import('~/components/diagnostics/thematiques/ThematiqueTransformationDistribution.vue')
);
const AsyncThematiqueAccessibilite = defineAsyncComponent<Component>(
    () => import('~/components/diagnostics/thematiques/ThematiqueAccessibilite.vue')
);
const AsyncThematiqueConsommation = defineAsyncComponent<Component>(() => import('~/components/diagnostics/thematiques/ThematiqueConsommation.vue'));
const AsyncThematiqueAgriculteurs = defineAsyncComponent<Component>(() => import('~/components/diagnostics/thematiques/ThematiqueAgriculteurs.vue'));
const AsyncThematiqueRessources = defineAsyncComponent<Component>(() => import('~/components/diagnostics/thematiques/ThematiqueRessources.vue'));
const AsyncThematiqueBiodiversite = defineAsyncComponent<Component>(() => import('~/components/diagnostics/thematiques/ThematiqueBiodiversite.vue'));
const AsyncThematiqueClimat = defineAsyncComponent<Component>(() => import('~/components/diagnostics/thematiques/ThematiqueClimat.vue'));
export function useImporterComposantThematique(idTheme: IdTheme) {
    switch (idTheme) {
        case 'risques-securite-alimentaire':
            return null;
        case 'terres-agricoles':
            return AsyncThematiqueTerresAgricoles;
        case 'autonomie-alimentaire':
            return AsyncThematiqueAutonomieAlimentaire;
        case 'transformation-distribution':
            return AsyncThematiqueTransformationDistribution;
        case 'accessibilite':
            return AsyncThematiqueAccessibilite;
        case 'consommation':
            return AsyncThematiqueConsommation;
        case 'agriculteurs':
            return AsyncThematiqueAgriculteurs;
        case 'ressources':
            return AsyncThematiqueRessources;
        case 'biodiversite':
            return AsyncThematiqueBiodiversite;
        case 'climat':
            return AsyncThematiqueClimat;
        default: {
            const casImpossible: never = idTheme;
            throw new Error(`Ne peut pas arriver`, casImpossible);
        }
    }
}
