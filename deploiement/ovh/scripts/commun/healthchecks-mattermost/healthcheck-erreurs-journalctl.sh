#!/bin/bash

# Vérifier la présence d'erreurs (priority >= 3) sur la dernière heure dans le journal
# Exclure (grep -v) les erreurs relatives à sshd (-e 'vps-e5fb237a sshd') qui sont très nombreuses
LOG_ERREURS=$(journalctl --priority 3 -xb --since "1 hour ago" | grep -e 'vps-e5fb237a sshd' -v)

if [[ -n "$LOG_ERREURS" ]]; then
    FICHIER_TMP_MESSAGE=$(mktemp)
    printf "ALERTE - ERREUR DANS JOURNALCTL :\n$LOG_ERREURS" > "$FICHIER_TMP_MESSAGE"

    ./post-mattermost.sh "$FICHIER_TMP_MESSAGE"

    rm -f "$FICHIER_TMP_MESSAGE"
fi

