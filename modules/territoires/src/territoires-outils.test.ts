import { describe, expect, it } from 'vitest';

import { calculerNomAvecDeterminant, calculerNomAvecPrepositionA, calculerNomAvecPrepositionDe } from './territoires-outils.js';

describe('Test fonctions', () => {
    it('test calculerNomAvecDeterminant', () => {
        expect(calculerNomAvecDeterminant('Bretagne', 'FEMININ_SINGULIER', 'en')).toEqual('la Bretagne');
        expect(calculerNomAvecDeterminant('Ardèche', 'FEMININ_SINGULIER', 'en')).toEqual("l'Ardèche");
        expect(calculerNomAvecDeterminant('Rhone', 'MASCULIN_SINGULIER', 'dans')).toEqual('le Rhone');
        expect(calculerNomAvecDeterminant('Ain', 'MASCULIN_SINGULIER', 'dans')).toEqual("l'Ain");
        expect(calculerNomAvecDeterminant('Ardennes', 'FEMININ_PLURIEL', 'dans')).toEqual('les Ardennes');
        expect(calculerNomAvecDeterminant('Hérault', 'MASCULIN_SINGULIER', 'dans')).toEqual("l'Hérault");
        expect(calculerNomAvecDeterminant('Hauts-de-France', 'MASCULIN_PLURIEL', 'dans')).toEqual('les Hauts-de-France');
        expect(calculerNomAvecDeterminant('Paris', 'FEMININ_SINGULIER', 'à')).toEqual('Paris');
        expect(calculerNomAvecDeterminant('Bordeaux Métropole', 'FEMININ_SINGULIER', 'à')).toEqual('Bordeaux Métropole');
    });
    it('test calculerNomAvecPrepositionA ', () => {
        expect(calculerNomAvecPrepositionA('Bretagne', 'FEMININ_SINGULIER', 'en')).toEqual('en Bretagne');
        expect(calculerNomAvecPrepositionA('Ardennes', 'FEMININ_PLURIEL', 'dans')).toEqual('dans les Ardennes');
        expect(calculerNomAvecPrepositionA('Hérault', 'MASCULIN_SINGULIER', 'dans')).toEqual("dans l'Hérault");
        expect(calculerNomAvecPrepositionA('Hauts-de-France', 'MASCULIN_PLURIEL', 'dans')).toEqual('dans les Hauts-de-France');
        expect(calculerNomAvecPrepositionA('Paris', 'FEMININ_PLURIEL', 'à')).toEqual('à Paris');
    });
    it('test calculerNomAvecPrepositionDe ', () => {
        expect(calculerNomAvecPrepositionDe('Bretagne', 'FEMININ_SINGULIER', 'en')).toEqual('de la Bretagne');
        expect(calculerNomAvecPrepositionDe('Rhone', 'MASCULIN_SINGULIER', 'dans')).toEqual('du Rhone');
        expect(calculerNomAvecPrepositionDe('Ain', 'MASCULIN_SINGULIER', 'dans')).toEqual("de l'Ain");
        expect(calculerNomAvecPrepositionDe('Ardennes', 'FEMININ_PLURIEL', 'dans')).toEqual('des Ardennes');
        expect(calculerNomAvecPrepositionDe('Hérault', 'MASCULIN_SINGULIER', 'dans')).toEqual("de l'Hérault");
        expect(calculerNomAvecPrepositionDe('Hauts-de-France', 'MASCULIN_PLURIEL', 'dans')).toEqual('des Hauts-de-France');
        expect(calculerNomAvecPrepositionDe('Paris', 'FEMININ_SINGULIER', 'à')).toEqual('de Paris');
    });
});
