export const PARAMETRE_URL_ACTIVER_FONCTIONS_BETA = 'beta';

// Permet d'implementer le feature toggling : si le paramètre "beta" est présent dans l'url, on active les fonctions expérimentales
export function estActifModeBeta() {
    // Location n'est pas défini hors du contexte d'exécution des browser (lors des tests par ex). Dans ce cas on active toutes les fonctions, yc fonctions expérimentales
    if (typeof location === 'undefined') return true;
    else return new URLSearchParams(location.search).has(PARAMETRE_URL_ACTIVER_FONCTIONS_BETA);
}
