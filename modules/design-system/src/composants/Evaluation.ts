import { css, html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { STYLES_DESIGN_SYSTEM } from '../styles/styles-design-system.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-evaluation': Evaluation;
    }
}

export type NoteEvaluation = 1 | 2 | 3;

@customElement('c-evaluation')
export class Evaluation extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        css`
            :host {
                display: block;
                height: fit-content;
            }
            main {
                display: flex;
                gap: 2px;
            }
            .rond {
                border: 1px solid var(--couleur-secondaire);
                background-color: transparent;
                border-radius: 100%;
                width: 1em;
                height: 1em;
            }
            .plein {
                background-color: var(--couleur-secondaire);
            }
        `
    ];

    @property({ type: Number })
    note: NoteEvaluation = 1;

    render() {
        return html`
            <main>
                <div class="rond ${this.note >= 1 ? 'plein' : ''}"></div>
                <div class="rond ${this.note >= 2 ? 'plein' : ''}"></div>
                <div class="rond ${this.note >= 3 ? 'plein' : ''}"></div>
            </main>
        `;
    }
}
