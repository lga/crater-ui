import { css, html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { ifDefined } from 'lit/directives/if-defined.js';
import { unsafeHTML } from 'lit/directives/unsafe-html.js';

import { ICONES_DESIGN_SYSTEM } from '../styles/icones-design-system';
import { STYLES_DESIGN_SYSTEM } from '../styles/styles-design-system.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-lien': Lien;
    }
}

@customElement('c-lien')
export class Lien extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        css`
            :host {
                display: inline;
            }

            a {
                cursor: pointer;
                text-decoration: underline;
                color: var(--couleur, var(--couleur-primaire));
            }

            svg {
                width: 0.8em;
                height: 0.8em;
                fill: var(--couleur, var(--couleur-primaire));
                padding-left: 0.1em;
            }

            a:hover {
                color: var(--couleur-hover, var(--couleur-accent));
            }

            a:hover svg {
                fill: var(--couleur-hover, var(--couleur-accent));
            }
        `
    ];

    @property()
    href?: string;

    render() {
        const estLienExterne = this.href?.startsWith('http');
        const estLienExterneCraterOuTef = estLienExterne && !/localhost|resiliencealimentaire|territoiresfertiles/.test(this.href ?? '');

        return html`<a href="${ifDefined(this.href)}" target="${estLienExterne ? '_blank' : '_self'}"
            >${estLienExterneCraterOuTef ? html`<slot></slot>${ICONES_DESIGN_SYSTEM.lienExterne}` : html`<slot></slot>`}</a
        >`;
    }
}

export const lien = (href: string, libelle: string) => html`<c-lien href="${href}">${unsafeHTML(libelle)}</c-lien>`;
