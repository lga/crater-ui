import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system';
import { css, html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-jauge-trois-classes': JaugeTroisClasses;
    }
}

type Classe = 1 | 2 | 3;

@customElement('c-jauge-trois-classes')
export class JaugeTroisClasses extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        css`
            :host {
                display: block;
                height: fit-content;
                width: fit-content;
                --taille-defaut: 2;
            }

            #jauge {
                display: grid;
                --taille-calculee: var(--taille, var(--taille-defaut));
                text-decoration: none;
            }

            #triangle {
                grid-row: 1;
                width: 0px;
                height: 0px;
                border-style: solid;
                border-width: calc(4px * var(--taille-calculee)) calc(3px * var(--taille-calculee)) 0px calc(3px * var(--taille-calculee));
                border-color: var(--couleur-neutre) transparent transparent transparent;
                margin: auto;
            }

            .position-1 {
                grid-column: 1;
            }

            .position-2 {
                grid-column: 2;
            }

            .position-3 {
                grid-column: 3;
            }

            .jauge-item {
                grid-row: 2;
                width: calc(10px * var(--taille-calculee));
                height: calc(6px * var(--taille-calculee));
            }

            #jauge-1 {
                background-color: var(--couleur-succes);
                grid-column: 1;
                border-radius: calc(2px * var(--taille-calculee)) 0 0 calc(2px * var(--taille-calculee));
            }

            #jauge-2 {
                background-color: var(--couleur-warning);
                grid-column: 2;
            }

            #jauge-3 {
                background-color: var(--couleur-danger);
                grid-column: 3;
                border-radius: 0 calc(2px * var(--taille-calculee)) calc(2px * var(--taille-calculee)) 0;
            }

            .legende-item {
                grid-row: 3;
                width: calc(10px * var(--taille-calculee));
                height: calc(6px * var(--taille-calculee) + 1px);
                line-height: calc(6px * var(--taille-calculee)+ 1px);
                text-align: center;
                color: var(--couleur-neutre);
            }

            #legende-1 {
                grid-column: 1;
            }
            #legende-2 {
                grid-column: 2;
            }
            #legende-3 {
                grid-column: 3;
            }
        `
    ];

    @property({ type: Number })
    classe: Classe = 1;

    get classeInversee(): Classe {
        return this.classe === 3 ? 1 : this.classe === 1 ? 3 : 2;
    }

    render() {
        return html`
            <abbr id="jauge" title="Cette jauge permet de donner une appréciation de la valeur de l'indicateur, de bon (A) à moins bon (C). ">
                <div id="triangle" class="position-${this.classeInversee}"></div>
                <div id="jauge-1" class="jauge-item"></div>
                <div id="jauge-2" class="jauge-item"></div>
                <div id="jauge-3" class="jauge-item"></div>
                <div id="legende-1" class="legende-item texte-petit">A</div>
                <div id="legende-2" class="legende-item texte-petit">B</div>
                <div id="legende-3" class="legende-item texte-petit">C</div>
            </abbr>
        `;
    }
}
