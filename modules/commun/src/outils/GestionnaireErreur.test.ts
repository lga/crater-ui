import { describe, expect, it } from 'vitest';

import { GestionnaireErreur } from './GestionnaireErreur';

describe('Test de GestionnaireErreur', () => {
    it("Gestion de l'état Erreur", () => {
        // when
        const erreurApp = new GestionnaireErreur();
        // then
        expect(erreurApp.estEnErreur()).toBeFalsy();
        // when
        erreurApp.mettreEnErreur('CODE', 'Message');
        // then
        expect(erreurApp.estEnErreur()).toBeTruthy();
        expect(erreurApp.erreur?.code).toEqual('CODE');
        expect(erreurApp.erreur?.message).toEqual('Message');
        // when
        erreurApp.annulerErreur();
        // then
        expect(erreurApp.estEnErreur()).toBeFalsy();
    });
});
