import type { OutilExterne } from './OutilExterne';
import type { VoieResilience } from './VoieResilience';

export interface Citation {
    texte: string;
    source: string;
}

export class Maillon {
    constructor(
        public readonly id: string,
        public readonly nom: string,
        public readonly icone: string,
        public readonly description: string,
        public readonly citations: Citation[],
        public readonly voiesResilience: VoieResilience[],
        public readonly outilsExternes: OutilExterne[],
        public readonly informationComplementaire?: string
    ) {}
}
