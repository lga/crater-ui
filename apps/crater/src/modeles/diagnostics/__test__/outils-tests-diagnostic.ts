import type { Territoire } from '@lga/territoires';

import { AgriculteursExploitations } from '../agriculteurs-exploitations/AgriculteursExploitations';
import { ClassesAges } from '../agriculteurs-exploitations/ClassesAges';
import { ClassesSuperficies } from '../agriculteurs-exploitations/ClassesSuperficies';
import { Cheptel, Cheptels } from '../Cheptels.js';
import { Consommation } from '../consommation/Consommation';
import { Diagnostic } from '../Diagnostic';
import { Eau } from '../eau/Eau';
import { Irrigation } from '../eau/Irrigation';
import { PratiquesIrrigation } from '../eau/PratiquesIrrigation';
import { Secheresse } from '../eau/Secheresse';
import { Energie } from '../energie/Energie.js';
import { IndicateursParAnnees } from '../IndicateursParAnnees';
import { IndicateursParAnneesMois } from '../IndicateursParAnneesMois';
import { Intrants } from '../intrants/Intrants';
import { Note } from '../Note';
import { OccupationSols } from '../OccupationSols';
import { Otex } from '../Otex';
import { IndicateursPesticidesParAnnees, Pesticides } from '../pesticides/Pesticides';
import { IndicateurHvn, Production } from '../production/Production';
import { SurfaceAgricoleUtile } from '../SurfaceAgricoleUtile';
import { PolitiqueAmenagement } from '../terres-agricoles/PolitiqueAmenagement';
import { TerresAgricoles } from '../terres-agricoles/TerresAgricoles';
import { CategorieCommerce } from '../transformation-distribution/CategorieCommerce';
import { IndicateursProximiteCommerces, TransformationDistribution } from '../transformation-distribution/TransformationDistribution';

export function creerDiagnosticVide(territoire: Territoire) {
    return new Diagnostic(
        territoire,
        null,
        null,
        null,
        1,
        new Otex(
            {
                grandesCultures: 1,
                maraichageHorticulture: 2,
                viticulture: 0,
                fruits: 0,
                bovinLait: 0,
                bovinViande: 0,
                bovinMixte: 0,
                ovinsCaprinsAutresHerbivores: 0,
                porcinsVolailles: 0,
                polyculturePolyelevage: 0,
                nonClassees: 0,
                sansExploitations: 0,
                nonRenseigne: 0
            },
            'GRANDES_CULTURES'
        ),
        new OccupationSols(0, 0, 0, 0, 0),
        new SurfaceAgricoleUtile(0, 0, 0, 0, []),
        new Cheptels([new Cheptel('VOLAILLES', 0, true, 0)]),
        0,
        new TerresAgricoles(territoire, new Note(0), 0, 0, 0, 0, 0, 0, 0, 0, PolitiqueAmenagement.DONNEES_NON_DISPONIBLES, 0, 0),
        new AgriculteursExploitations(
            territoire,
            new Note(0),
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            new ClassesAges(0, 0, 0, 0),
            new ClassesSuperficies(0, 0, 0, 0, 0),
            new ClassesSuperficies(0, 0, 0, 0, 0)
        ),
        creerIntrantsVide(territoire),
        new Production(territoire, new Note(0), 0, 0, 0, 0, 0, new IndicateurHvn(0, 0, 0, 0), 0, 0),
        new TransformationDistribution(territoire, new Note(0), 0, 0, [
            new IndicateursProximiteCommerces(CategorieCommerce.TroisTypesCommerces, 0, 0, 0)
        ]),
        new Consommation(territoire, new Note(0), 0, null)
    );
}

export function creerIntrantsVide(territoire: Territoire) {
    return new Intrants(territoire, new Note(0), creerEnergieVide(territoire), creerEauVide(territoire), creerPesticidesVide(territoire));
}

function creerEnergieVide(territoire: Territoire) {
    return new Energie(territoire, new Note(null), 'ALIMENTATION_ANIMALE', 0, 0, []);
}

function creerEauVide(territoire: Territoire) {
    return new Eau(
        territoire,
        new Note(null),
        new Irrigation(territoire, new Note(null), 0, 0, new IndicateursParAnnees([]), new PratiquesIrrigation([])),
        new Secheresse(territoire, new Note(null), 0, new IndicateursParAnneesMois([]))
    );
}

function creerPesticidesVide(territoire: Territoire) {
    return new Pesticides(territoire, new Note(null), new IndicateursPesticidesParAnnees([]));
}
