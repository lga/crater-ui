import { arrondirANDecimales, regressionLineaire } from '@lga/base';
import type {
    BesoinsParGroupeCultureApi,
    CodeGroupeCultureApi,
    DiagnosticApi,
    IrrigationIndicateursAnneeNApi,
    TerritoireApi
} from '@lga/specification-api';

export interface DonneesApi {
    diagnosticApi: DiagnosticApi;
    territoireApi: TerritoireApi;
}

export function estimerPopulation(
    populationInitale: number,
    populationFinale: number,
    anneeInitiale: number,
    anneeFinale: number,
    anneeFuture: number
): number {
    const tauxCroissance = Math.pow(populationFinale / populationInitale, 1 / (anneeFinale - anneeInitiale)) - 1;
    return arrondirANDecimales(populationFinale * Math.pow(1 + tauxCroissance, anneeFuture - anneeFinale), 0);
}

export function calculerTendanceEvolutionPrelevementEauPourcent(
    irrigationIndicateursParAnnees: IrrigationIndicateursAnneeNApi[] | null,
    anneeInitiale: number,
    anneeFinale: number
): number | null {
    let evolutionPrelevementEau = null;
    if (irrigationIndicateursParAnnees) {
        const rl = regressionLineaire(
            irrigationIndicateursParAnnees.map((i) => i.annee),
            irrigationIndicateursParAnnees.map((i) => i.irrigationM3)
        );
        const estimationAnneeFinale = rl.fn(anneeFinale);
        const estimationAnneeInitiale = rl.fn(anneeInitiale);
        if (estimationAnneeFinale !== null && estimationAnneeInitiale !== null) {
            evolutionPrelevementEau = Math.round((estimationAnneeFinale / estimationAnneeInitiale - 1) * 100);
        }
    }
    return evolutionPrelevementEau;
}

interface GroupeCulture {
    codeGroupeCulture: CodeGroupeCultureApi;
    nomGroupeCulture: string;
    tauxAdequationBrutPourcent: number;
}
const SEUIL_ADEQUATION_BRUTE_EXCEDENT_POURCENT = 150;
const SEUIL_ADEQUATION_BRUTE_MANQUE_POURCENT = 80;
export function classerTauxAdequationParGroupesCultures(groupesCultures: GroupeCulture[]) {
    return {
        excedents: groupesCultures
            .filter((i) => i.tauxAdequationBrutPourcent > SEUIL_ADEQUATION_BRUTE_EXCEDENT_POURCENT)
            .sort((a, b) => b.tauxAdequationBrutPourcent - a.tauxAdequationBrutPourcent),
        manques: groupesCultures
            .filter((i) => i.tauxAdequationBrutPourcent < SEUIL_ADEQUATION_BRUTE_MANQUE_POURCENT)
            .sort((a, b) => a.tauxAdequationBrutPourcent - b.tauxAdequationBrutPourcent)
    };
}

export function classerBesoinsAssiette(besoinsAssiette: BesoinsParGroupeCultureApi[]) {
    return classerTauxAdequationParGroupesCultures(
        besoinsAssiette.map((i) => {
            {
                return {
                    codeGroupeCulture: i.codeGroupeCulture,
                    nomGroupeCulture: i.nomGroupeCulture,
                    tauxAdequationBrutPourcent: i.tauxAdequationBrutPourcent ?? 0
                };
            }
        })
    );
}

export function calculerLibelleGroupeCultureAvecPreposition(groupeCulture: string) {
    return ('de ' + groupeCulture).toLowerCase().replace('de oléo', "d'oléo");
}

export function calculerLibellesPrincipauxGroupesCulturesManquants(besoinsParGroupeCulture: BesoinsParGroupeCultureApi[]) {
    return classerBesoinsAssiette(besoinsParGroupeCulture)
        .manques.filter((i) => i.codeGroupeCulture !== 'FOU' && i.codeGroupeCulture !== 'DVC')
        .slice(0, 2)
        .map((i) => calculerLibelleGroupeCultureAvecPreposition(i.nomGroupeCulture))
        .join(' et ');
}

export function calculerLibellesPrincipauxGroupesCulturesExcedentaires(besoinsParGroupeCulture: BesoinsParGroupeCultureApi[]) {
    return classerBesoinsAssiette(besoinsParGroupeCulture)
        .excedents.filter((i) => i.codeGroupeCulture !== 'DVC')
        .slice(0, 2)
        .map((i) => calculerLibelleGroupeCultureAvecPreposition(i.nomGroupeCulture))
        .join(' et ');
}

export type NiveauAutonomie = 'INCONNU' | 'TERRES_INSUFFISANTES' | 'TERRES_PARTIELLEMENT_SUFFISANTES' | 'TERRES_SUFFISANTES';

export function calculerNiveauAutonomie(
    tauxAdequationBrutAssietteActuellePourcent: number | null,
    tauxAdequationBrutAssietteDemitariennePourcent: number | null
): NiveauAutonomie {
    if (tauxAdequationBrutAssietteActuellePourcent === null || tauxAdequationBrutAssietteDemitariennePourcent === null) {
        return 'INCONNU';
    } else {
        if (tauxAdequationBrutAssietteActuellePourcent >= 85) {
            return 'TERRES_SUFFISANTES';
        } else if (tauxAdequationBrutAssietteDemitariennePourcent >= 85) {
            return 'TERRES_PARTIELLEMENT_SUFFISANTES';
        } else {
            return 'TERRES_INSUFFISANTES';
        }
    }
}
