import { configurerInterceptRequetesAPI } from '../config';

function verifierProductionEnHectares(nombreAttendu: string) {
    cy.get('[data-cy="synthese-valeur-sauProductiveHa"]').invoke('text').should('contains', nombreAttendu);
}

function verifierNoteEtMessage(cyId: string, note: string, message: string) {
    cy.get(`[data-cy="${cyId}"]`).find('c-jauge-note-sur-10').find('.texte-note').contains(note);
    cy.get(`[data-cy="${cyId}"]`).find('#message').contains(message);
}

describe('Tests e2e/isolation page diagnostic', () => {
    beforeEach(() => {
        configurerInterceptRequetesAPI();
    });

    it('Vérifier le contenu de la page de synthèse en mode desktop', () => {
        cy.viewport(1400, 800);

        cy.visit('/diagnostic/occitanie');
        cy.verifierAffichagePage('Diagnostic du système alimentaire');
        verifierProductionEnHectares('2,6 millions');
        cy.get('c-barre-onglets-item#PAYS').find('a').click();
        verifierProductionEnHectares('25 millions');
        verifierNoteEtMessage(
            'message-cle-terres-agricoles',
            '5',
            'La surface agricole par habitant peut convenir pour un régime alimentaire moins carné mais l’objectif Zéro Artificialisation n’a pas été atteint entre 2013 et 2018.'
        );
        verifierNoteEtMessage(
            'message-cle-agriculteurs-exploitations',
            '5',
            "Part d'actifs agricoles permanents proche de la moyenne française et en déclin."
        );

        verifierNoteEtMessage('message-cle-intrants', '6', "Dépendance très marquée à l'eau d'irrigation et aux pesticides et marquée à l'énergie.");

        verifierNoteEtMessage(
            'message-cle-production',
            '7',
            'Production élevée mais trop spécialisée pour couvrir la consommation et pratiques agricoles très préjudiciables à la biodiversité.'
        );
        verifierNoteEtMessage(
            'message-cle-transformation-distribution',
            '8',
            '24 % de la population est théoriquement dépendante de la voiture pour ses achats alimentaires.'
        );

        // Pas de vérification de la note pour le maillon consommation car pas de texte-note
        cy.get(`[data-cy="message-cle-consommation"]`)
            .find('#message')
            .contains(
                'Régime alimentaire riche et très carné qui induit un besoin important de surfaces agricoles. Précarité alimentaire importante et en hausse.'
            );
    });

    it('Vérifier les liens sortants en mode desktop', () => {
        cy.viewport(1400, 800);
        cy.visit('/diagnostic/occitanie');
        cy.get('[data-cy="message-cle-terres-agricoles"]').find('a').first().click({ force: true });
        cy.verifierAffichagePage('Terres agricoles');

        cy.visit('/diagnostic/occitanie');
        cy.get('[data-cy="message-cle-agriculteurs-exploitations"]').find('a').first().click({ force: true });
        cy.verifierAffichagePage('Agriculteurs & Exploitations');

        cy.visit('/diagnostic/occitanie');
        cy.get('[data-cy="message-cle-intrants"]').find('a').first().click({ force: true });
        cy.verifierAffichagePage('Intrants');

        cy.visit('/diagnostic/occitanie');
        cy.get('[data-cy="message-cle-production"]').find('a').first().click({ force: true });
        cy.verifierAffichagePage('Production');

        cy.visit('/diagnostic/occitanie');
        cy.get('[data-cy="message-cle-transformation-distribution"]').find('a').first().click({ force: true });
        cy.verifierAffichagePage('Transformation & Distribution');

        cy.visit('/diagnostic/occitanie');
        cy.get('[data-cy="message-cle-consommation"]').find('a').first().click({ force: true });
        cy.verifierAffichagePage('Consommation');
    });

    it.only('Vérifier une page Maillion puis Domaine', () => {
        cy.viewport(1400, 800);
        cy.visit('/diagnostic/occitanie');
        cy.get('[data-cy="message-cle-terres-agricoles"]').find('a').first().click({ force: true });
        cy.verifierAffichagePage('Terres agricoles');

        cy.get('[data-cy="encart-resume-domaine-sau-par-habitant"]').find('a').first().click({ force: true });
        cy.verifierAffichagePage('Surface agricole utile par habitant');

        cy.get('c-chart-barres[slot=chart-chapitre-domaine]').should('be.visible');
        cy.get('c-message-domaine').should('be.visible');
    });

    it('Vérifier le contenu de la page de synthèse en mode mobile', () => {
        cy.viewport(320, 500);
        cy.visit('/diagnostic/occitanie');
        cy.get('h1').contains('Occitanie');
        cy.get('c-menu-pied-page').should('exist');
        cy.get('c-panneau-glissant').find('a[title="Menu"]').click({ force: true });
        cy.get('c-bouton[libelle="Modifier"]').click({ force: true });
        cy.get('c-menu-mobile-diagnostic-modifier-territoire').find('input[type="text"]').eq(0).type('fra');
        cy.get('menu article').contains('France').click({ force: true });
        cy.verifierAffichagePage('Diagnostic du système alimentaire');
    });
});
