import '@lga/commun/build/composants/champ-recherche-territoire/ChampRechercheTerritoire.js';

import { getApiBaseUrl } from '@lga/commun/build/env/config-baseurl';
import { CSS_BREAKPOINT_MAX_WIDTH_DESKTOP_PETIT } from '@lga/design-system/build/styles/styles-breakpoints';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { css, html, LitElement, unsafeCSS } from 'lit';
import { customElement } from 'lit/decorators.js';

import { PAGES_PRINCIPALES } from '../../configuration/pages/declaration-pages';
import HERO from '../../ressources/illustrations/accueil/hero.png';
import { STYLES_CRATER } from '../commun/pages-styles';

declare global {
    interface HTMLElementTagNameMap {
        'c-rechercher-un-territoire': RechercherUnTerritoire;
    }
}

@customElement('c-rechercher-un-territoire')
export class RechercherUnTerritoire extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        STYLES_CRATER,
        css`
            :host {
                display: block;
                width: 100%;
            }

            main {
                display: flex;
                justify-content: center;
                min-height: calc(var(--dsem) * 60);
            }

            article {
                display: flex;
                flex-direction: column;
                max-width: 500px;
                padding: calc(var(--dsem) * 4);
            }

            figure {
                height: 100%;
                width: 100%;
                max-width: 800px;
                margin: initial;
                padding: calc(var(--dsem) * 4) 0 0 calc(var(--dsem) * 12);
                overflow: hidden;
                align-self: end;
            }

            c-champ-recherche-territoire {
                width: min(95vw, 28rem);
            }

            h1 {
                display: flex;
                flex-direction: column;
                gap: calc(2 * var(--dsem));
                color: var(--couleur-primaire);
                width: 700px;
            }

            h2 {
                margin: 0;
                padding: calc(5 * var(--dsem)) 0 var(--dsem);
                color: var(--couleur-primaire);
            }

            @media screen and (max-width: ${unsafeCSS(CSS_BREAKPOINT_MAX_WIDTH_DESKTOP_PETIT + 'px')}) {
                article {
                    margin: 0;
                    width: 100%;
                    align-items: center;
                    padding: 0;
                }

                h1 {
                    align-items: center;
                    text-align: center;
                    max-width: 100%;
                }

                h2 {
                    padding: calc(2 * var(--dsem)) var(--dsem);
                }

                figure {
                    display: none;
                }
            }
        `
    ];

    render() {
        return html`
            <main>
                <article>
                    <h1>Mon territoire peut-il garantir une alimentation saine et durable à ses habitants ?</h1>
                    <h2 class="texte-moyen">Découvrez le diagnostic alimentaire de votre territoire</h2>
                    <c-champ-recherche-territoire
                        apiBaseUrl=${getApiBaseUrl()}
                        data-cy="champ-recherche-territoire"
                        lienFormulaireDemandeAjoutTerritoire="${PAGES_PRINCIPALES.demandeAjoutTerritoire.getUrl()}"
                    ></c-champ-recherche-territoire>
                </article>
                <figure>
                    <img src="${HERO}" width="600" height="470" />
                </figure>
            </main>
        `;
    }
}
