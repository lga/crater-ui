export {};

declare global {
    namespace Cypress {
        interface Chainable {
            /**
             * Commande custom cypress pour rechercher un territoire
             * @param idChampRechercheTerritoire
             * @param texteSaisi
             * @param tailleListeSuggestion
             */
            rechercherTerritoire(idChampRechercheTerritoire: string, texteSaisi: string, tailleListeSuggestion: number): Chainable<JQuery>;
        }

        interface Chainable {
            /**
             * Commande custom cypress pour vérifier que la page est correctement affichée
             * Le contenuH1 passé en paramètre est présent dans le h1 de la page
             * et la page doit est scrollée en haut et à gauche
             * @param contenuH1
             */
            verifierAffichagePage(contenuH1: string): Chainable<JQuery>;
        }
    }
}
