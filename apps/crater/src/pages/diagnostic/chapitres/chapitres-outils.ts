import { getIdDomaineDepuisIdMaillon } from '@lga/indicateurs';
import { EchelleTerritoriale } from '@lga/territoires';

import { construireUrlPageCarteTerritoire, construireUrlPageMethodologie } from '../../../configuration/pages/pages-utils';
import type { ConsommationParGroupeCulture, IndicateurSynthese, Rapport, SauParGroupeCulture } from '../../../modeles/diagnostics';
import type { DonneesTreemapCultures, DonneesTreemapDetailCultures } from '../charts/ChartTreemapCultures';
import type { OptionsSyntheseMaillon } from './SyntheseMaillon';

export function creerDonneesTreemapProduction(sauParGroupeCultures: SauParGroupeCulture[]) {
    return sauParGroupeCultures.map(
        (p) =>
            ({
                code: p.codeGroupeCulture,
                labelCourt: p.groupeCulture.nomCourt,
                labelLong: p.nomGroupeCulture,
                couleur: p.groupeCulture.couleur,
                sauHa: p.sauHa,
                detailCultures: p.sauParCulture.map(
                    (s) =>
                        ({
                            codeCulture: s.codeCulture,
                            nomCulture: s.nomCulture,
                            sauHa: s.sauHa
                        }) as DonneesTreemapDetailCultures
                )
            }) as DonneesTreemapCultures
    );
}

export function creerDonneesTreemapConsommation(consommationParGroupeCultures: ConsommationParGroupeCulture[]) {
    return consommationParGroupeCultures.map(
        (b) =>
            ({
                code: b.groupeCulture.code,
                labelCourt: b.groupeCulture.nomCourt,
                labelLong: b.nomGroupeCulture,
                couleur: b.groupeCulture.couleur,
                sauHa: b.consommationHa,
                detailCultures: b.consommationParCulture.map(
                    (s) =>
                        ({
                            codeCulture: s.codeCulture,
                            nomCulture: s.nomCulture,
                            sauHa: s.consommationHa
                        }) as DonneesTreemapDetailCultures
                )
            }) as DonneesTreemapCultures
    );
}

export function creerOptionsSyntheseMaillon(
    rapport: Rapport,
    indicateurSynthese: IndicateurSynthese,
    idMaillon: string,
    idIndicateurCarte?: string
): OptionsSyntheseMaillon {
    const lienCarte =
        EchelleTerritoriale.listerTousLesCodeCategories.includes(rapport.getTerritoireActif()!.categorie.codeCategorie) && idIndicateurCarte
            ? construireUrlPageCarteTerritoire(idIndicateurCarte, rapport.getTerritoireActif()?.id ?? '')
            : undefined;
    const lienMethodologie = construireUrlPageMethodologie(idMaillon, getIdDomaineDepuisIdMaillon(idMaillon));
    return {
        note: indicateurSynthese.note,
        messageSynthese: indicateurSynthese.messageSynthese,
        lienMethodologie: lienMethodologie,
        lienCarte: lienCarte
    };
}
