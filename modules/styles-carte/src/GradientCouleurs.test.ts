import { describe, expect, it } from 'vitest';

import { COULEUR_VALEUR_NULL } from './couleurs-carte.js';
import { calculerCodeCouleurInt256, calculerCouleurHex, GradientCouleurs } from './GradientCouleurs';

describe('Test de la classe GradientCouleurs', () => {
    const couleurNote10 = '#219653';
    const couleurNote5 = '#f2994a';
    const couleurNote0 = '#9b0000';
    const seuilsCouleursNotes = [
        { valeur: 0, couleur: couleurNote0 },
        { valeur: 5, couleur: couleurNote5 },
        { valeur: 10, couleur: couleurNote10 }
    ];
    const couleurNote2 = '#be3d1e';
    const couleurNote5_5 = '#dd994b';

    const gradientCouleurs = new GradientCouleurs(seuilsCouleursNotes);

    it('Test fonction calculerCodeCouleurInt', () => {
        expect(calculerCodeCouleurInt256('ff', 'ff', 0)).toEqual(255);
        expect(calculerCodeCouleurInt256('ff', 'ff', 1)).toEqual(255);
        expect(calculerCodeCouleurInt256('ff', 'ff', 0.00864)).toEqual(255); //renvoie 255.00000000000003 (interdit car > 255) avant arrondi
        expect(calculerCodeCouleurInt256('00', 'ff', 0)).toEqual(255);
        expect(calculerCodeCouleurInt256('00', 'ff', 0.000001)).toEqual(255);
        expect(calculerCodeCouleurInt256('00', 'ff', 0.5)).toEqual(128);
        expect(calculerCodeCouleurInt256('00', 'ff', 0.999999)).toEqual(0);
        expect(calculerCodeCouleurInt256('00', 'ff', 1)).toEqual(0);
    });

    it('Test fonction calculerCouleurHex', () => {
        expect(calculerCouleurHex(0)).toEqual('00');
        expect(calculerCouleurHex(10)).toEqual('0a');
        expect(calculerCouleurHex(128)).toEqual('80');
        expect(calculerCouleurHex(255)).toEqual('ff');
        expect(calculerCouleurHex(256)).toEqual('100');
    });

    it('Calculer couleur pour une valeur entre bornes inf et sup', () => {
        expect(gradientCouleurs.calculerCouleur(0)).toEqual(couleurNote0);
        expect(gradientCouleurs.calculerCouleur(5)).toEqual(couleurNote5);
        expect(gradientCouleurs.calculerCouleur(5.5)).toEqual(couleurNote5_5);
        expect(gradientCouleurs.calculerCouleur(10)).toEqual(couleurNote10);
        expect(gradientCouleurs.calculerCouleur(2)).toEqual(couleurNote2);
    });

    it('Calculer couleur pour cas null ou hors bornes', () => {
        expect(gradientCouleurs.calculerCouleur(null)).toEqual(COULEUR_VALEUR_NULL);
        expect(gradientCouleurs.calculerCouleur(-1)).toEqual(couleurNote0);
        expect(gradientCouleurs.calculerCouleur(11)).toEqual(couleurNote10);
    });

    it('Calculer couleur basse et haute', () => {
        expect(gradientCouleurs.couleurBasse).toEqual(couleurNote0);
        expect(gradientCouleurs.couleurHaute).toEqual(couleurNote10);
    });
});
