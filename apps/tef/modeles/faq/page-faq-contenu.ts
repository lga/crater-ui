import { type HtmlString, htmlstring } from '@lga/base';

import { creerLienExterneHtmlBrut } from '../liens-utils.js';

export interface SectionPageFAQ {
    id: string;
    libelle: string;
    definition: HtmlString;
}

export const PAGE_FAQ_CONTENU: SectionPageFAQ[] = [
    {
        id: 'pourquoi-les-drom',
        libelle: `Pourquoi les territoires d'outre-mer (DROM-COM) ne sont-il pas présents ? Avez-vous prévu de les intégrer ?`,
        definition: htmlstring`Ces territoires ne sont pas présents parce que d'une part certaines données comme les besoins en surfaces agricoles ne sont pas disponibles, et d'autre part du fait de leurs spécifités (climat, insularité, etc.) qui remettent en question la pertinence de certains indicateurs. Faute de moyens suffisants pour le moment pour adresser ces problématiques, nous ne pouvons donc malheureusement les inclure.
        Nous vous invitons cela étant à lire sur le site de l'association des Greniers d'Abondance ${creerLienExterneHtmlBrut('https://resiliencealimentaire.org/securite-alimentaire-france-outre-mer/', 'un article sur la sécurité alimentaire de ces territoires')}.`
    },
    {
        id: 'certains-indicateurs-biais',
        libelle: 'Certains indicateurs ont des biais ou ne s’appliquent pas à tous les contextes, comment traitez-vous ce problème ?',
        definition: htmlstring`Nous savons que les indicateurs calculés ne sont pas adaptés à tous les territoires. Cependant, nous
        avons fait le choix d’appliquer les mêmes calculs à tous les territoires par souci d’homogénéité et de cohérence. Ainsi, comme expliqué dans
        notre méthodologie, le diagnostic proposé est à considérer comme un pré-diagnostic qui ne se substitue pas à un diagnostic terrain plus
        poussé. Cela étant, nous sommes preneurs de vos remarques pour améliorer notre méthodologie afin de la rendre toujours plus robuste !`
    },
    {
        id: 'pourquoi-des-notes',
        libelle: 'Pourquoi avoir utilisé des notes pour évaluer les composantes du système alimentaire dans le diagnostic détaillé ?',
        definition: htmlstring`Nous avons longuement hésité avant de décider d’utiliser des notes pour évaluer les composantes du système alimentaire.
        Conscients des limites d’utiliser un système de notation, nous avons cependant trouvé que c’était le moyen le plus simple et le plus
        intelligible pour présenter de manière synthétique les éléments apportés par les différents indicateurs. Nous nous sommes en revanche interdit
        de présenter une note globale – moyenne des différentes notes – car la résilience alimentaire d’un territoire ne peut se résumer en une seule
        phrase mais est au contraire à aborder sur toutes ses facettes.`
    },
    {
        id: 'certaines-donnees-2010',
        libelle: 'Certaines données datent de 2010 : pourquoi ne sont-elles pas plus récentes ?',
        definition: htmlstring`Dans la majorité des cas, pour chaque source de données, le jeu le plus récent a été utilisé. Une mise à jour régulière est prévue
        au fur et à mesure. Cela étant, il est possible que nous n’ayons pas eu le temps, ou bien que nous n’ayons pas encore repéré une mise à jour.
        Dans ce cas-là, n’hésitez pas à nous le signaler en nous contactant !`
    },
    {
        id: 'donnees-calcul-actualiser',
        libelle: 'Les données ou modes de calcul sont-ils faciles à actualiser ?',
        definition: htmlstring`Pour les données, le travail est relativement simple si le format des données n’a pas changé entre deux publications et ne
        demande donc pas d’ajustement du code informatique. Pour les modes de calcul, cela dépend de l’actualisation à faire : s’il s’agit d’un
        changement à la marge (changement de la valeur d’un paramètre, changement de la formule de calcul en se basant sur les mêmes données...) cela
        est relativement facile ; si le changement est plus profond (prise en compte d’une nouvelle donnée, changement radical du mode de calcul...),
        cela peut demander des efforts plus importants.`
    }
];
