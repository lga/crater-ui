import '../../../../modules/design-system/public/theme-defaut/theme-defaut.css';
import '../../src/composants/element-scrollable/ElementScrollable';

import { css, html, LitElement } from 'lit';
import { customElement, query } from 'lit/decorators.js';

import type { ElementScrollable } from '../../src/composants/element-scrollable/ElementScrollable';
import type { EvenementEntrerZoneScroll } from '../../src/composants/element-scrollable/EvenementEntrerZoneScroll';
import type { EvenementSortirZoneScroll } from '../../src/composants/element-scrollable/EvenementSortirZoneScroll';

@customElement('c-page-scrollable')
// eslint-disable-next-line @typescript-eslint/no-unused-vars
class PageScrollable extends LitElement {
    static styles = [
        css`
            #conteneur-scroll {
                background-color: white;
                display: block;
                height: 800px;
                position: relative;
            }

            #zone-scrollable {
                height: 100%;
                overflow: scroll;
                padding-top: 700px;
            }

            #zone-masque {
                width: 100%;
                height: 100%;
                position: absolute;
                border: 1px solid red;
            }

            c-element-scrollable {
                margin: 50px auto;
                width: 400px;
                border: 1px solid black;
                height: 300px;
                display: flex;
                align-items: center;
                justify-content: center;
            }

            #sommaire {
                position: fixed;
                top: 0;
                right: 0;
                width: 200px;
                height: 100vh;
            }

            .est-actif {
                background-color: green;
            }
        `
    ];

    render() {
        const seuilBasPourcent = 20;
        const seuilHautPourcent = 35;

        return html` <main id="conteneur-scroll">
            <div id="zone-scrollable">
                <section id="intro">
                    <h1>Test intersection observer</h1>
                    <p>Introduction, commencez a scroller pour voir le résultat</p>
                </section>
                <section id="sommaire">
                    <h3>Dernière étape activée</h3>
                    <p id="sommaire-nom-etape-activee">Aucune</p>
                </section>
                <section id="elements-scroll">
                    <c-element-scrollable
                        id="A"
                        querySelectorConteneurScroll="#conteneur-scroll"
                        seuilBasPourcent="${seuilBasPourcent}"
                        seuilHautPourcent="${seuilHautPourcent}"
                        @entrerZoneScroll="${this.entrerScrollZone}"
                        @sortirZoneScroll="${this.sortirScrollZone}"
                    >
                        <div slot="contenu">A</div>
                    </c-element-scrollable>
                    <c-element-scrollable
                        id="B"
                        querySelectorConteneurScroll="#conteneur-scroll"
                        seuilBasPourcent="${seuilBasPourcent}"
                        seuilHautPourcent="${seuilHautPourcent}"
                        @entrerZoneScroll="${this.entrerScrollZone}"
                        @sortirZoneScroll="${this.sortirScrollZone}"
                    >
                        <div slot="contenu">B</div>
                    </c-element-scrollable>
                    <c-element-scrollable
                        id="C"
                        querySelectorConteneurScroll="#conteneur-scroll"
                        seuilBasPourcent="${seuilBasPourcent}"
                        seuilHautPourcent="${seuilHautPourcent}"
                        @entrerZoneScroll="${this.entrerScrollZone}"
                        @sortirZoneScroll="${this.sortirScrollZone}"
                    >
                        <div slot="contenu">C</div>
                    </c-element-scrollable>
                    <c-element-scrollable
                        id="D"
                        querySelectorConteneurScroll="#conteneur-scroll"
                        seuilBasPourcent="${seuilBasPourcent}"
                        seuilHautPourcent="${seuilHautPourcent}"
                        @entrerZoneScroll="${this.entrerScrollZone}"
                        @sortirZoneScroll="${this.sortirScrollZone}"
                    >
                        <div slot="contenu">D</div>
                    </c-element-scrollable>
                    <c-element-scrollable
                        id="E"
                        querySelectorConteneurScroll="#conteneur-scroll"
                        seuilBasPourcent="${seuilBasPourcent}"
                        seuilHautPourcent="${seuilHautPourcent}"
                        @entrerZoneScroll="${this.entrerScrollZone}"
                        @sortirZoneScroll="${this.sortirScrollZone}"
                    >
                        <div slot="contenu">E</div>
                    </c-element-scrollable>
                    <c-element-scrollable
                        id="F"
                        querySelectorConteneurScroll="#conteneur-scroll"
                        seuilBasPourcent="${seuilBasPourcent}"
                        seuilHautPourcent="${seuilHautPourcent}"
                        @entrerZoneScroll="${this.entrerScrollZone}"
                        @sortirZoneScroll="${this.sortirScrollZone}"
                    >
                        <div slot="contenu">F</div>
                    </c-element-scrollable>
                </section>
            </div>
            <div
                id="zone-masque"
                style="top: ${seuilHautPourcent}%;bottom: ${seuilBasPourcent}%;height: ${100 - seuilHautPourcent - seuilBasPourcent}%;"
            ></div>
        </main>`;
    }

    @query('#sommaire-nom-etape-activee')
    private sommaireNomEtape!: HTMLElement;

    private entrerScrollZone(event: EvenementEntrerZoneScroll) {
        const elementScrollable = event.target as ElementScrollable;
        this.sommaireNomEtape.innerHTML = elementScrollable.id;
        elementScrollable.classList.add('est-actif');
    }

    private sortirScrollZone(event: EvenementSortirZoneScroll) {
        const elementScrollable = event.target as ElementScrollable;
        elementScrollable.classList.remove('est-actif');
    }
}

describe('Page scrollable', () => {
    it(' Test affichage desktop', () => {
        cy.viewport(1200, 1000);
        cy.document()
            .then((doc) => {
                doc.addEventListener('entrerZoneScroll', cy.stub().as('entrerZoneScroll'));
                doc.addEventListener('sortirZoneScroll', cy.stub().as('sortirZoneScroll'));
            })
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-expect-error
            .mount<'c-page-scrollable'>(html` <c-page-scrollable></c-page-scrollable>`);

        // then
        cy.get('@entrerZoneScroll').should('have.not.been.called');
        cy.get('@sortirZoneScroll').should('have.been.called'); // sortirZoneScroll est appelé pour chaque élément scrollable, car ils sont tous en dehors
        cy.get('#zone-scrollable').scrollTo(0, 250, { duration: 1000 });
        cy.get('@entrerZoneScroll').should('have.been.calledOnce');
        cy.get('#zone-scrollable').scrollTo(0, 900, { duration: 1000 });
    });
});
