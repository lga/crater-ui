import '../commun/template/TemplatePageSansSommaire.js';

import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { css, html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { PAGES_PRINCIPALES } from '../../configuration/pages/declaration-pages.js';
import { STYLES_CRATER } from '../commun/pages-styles';

declare global {
    interface HTMLElementTagNameMap {
        'c-template-page-formulaire': TemplatePageFormulaire;
    }
}

@customElement('c-template-page-formulaire')
export class TemplatePageFormulaire extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        STYLES_CRATER,
        css`
            iframe {
                display: block;
                min-width: 100%;
                margin: auto;
                border: none;
            }
        `
    ];

    render() {
        return html`
            <c-template-page-sans-sommaire idItemActifMenuPrincipal="${PAGES_PRINCIPALES.contact.getId()}">
                >${this.renderCoeurPage()}
            </c-template-page-sans-sommaire>
        `;
    }

    @property()
    titre = '';

    @property()
    lienFormulaire = '';

    @property()
    hauteur = '600px';

    private renderCoeurPage() {
        return html`
            <section slot="contenu">
                <h1>${this.titre}</h1>
                <iframe title=${this.titre} height=${this.hauteur} allowfullscreen src="${this.lienFormulaire}"> </iframe>
            </section>
        `;
    }
}
