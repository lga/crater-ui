import ADEME from '../../ressources/images/logo-ademe.svg';
import AGENCE_BIO from '../../ressources/images/logo-agence-bio.png';
import AGRESTE from '../../ressources/images/logo-agreste.png';
import BANATIC from '../../ressources/images/logo-banatic.jpg';
import CEREMA from '../../ressources/images/logo-cerema.svg';
import CTIFL from '../../ressources/images/logo-ctifl.jpg';
import EAU_FRANCE from '../../ressources/images/logo-eaufrance.png';
import FEDERATION_SCOT from '../../ressources/images/logo-federation-scot.png';
import FRANCE_PAT from '../../ressources/images/logo-france-pat.png';
import FRANCE_AGRI_MER from '../../ressources/images/logo-franceagrimer.png';
import IGN from '../../ressources/images/logo-ign.png';
import INSEE from '../../ressources/images/logo-insee.png';
import LA_POSTE from '../../ressources/images/logo-laposte.png';
import MINISTERE_AGRICULTURE from '../../ressources/images/logo-maa.png';
import MINISTERE_TRANSITION_ECOLOGIQUE from '../../ressources/images/logo-mtect.png';
import OFFICE_FRANCAIS_BIODIVERSITE from '../../ressources/images/logo-ofb.png';
import OPEN_STREET_MAP from '../../ressources/images/logo-openstreetmap.png';
import OBSERVATOIRE_DES_TERRITOIRES from '../../ressources/images/logo-ot.png';
import PARCEL from '../../ressources/images/logo-parcel.png';
import PROPLUVIA from '../../ressources/images/logo-propluvia.png';
import SOLAGRO from '../../ressources/images/logo-solagro.png';
import UNIFA from '../../ressources/images/logo-unifa.png';

export interface FournisseurDonnee {
    nom: string;
    lienLogo: string;
    largeurLogo: number;
}

export const fournisseursDonnees: {
    ademe: FournisseurDonnee;
    agreste: FournisseurDonnee;
    agenceBio: FournisseurDonnee;
    banatic: FournisseurDonnee;
    eauFrance: FournisseurDonnee;
    cerema: FournisseurDonnee;
    ctifl: FournisseurDonnee;
    federationScot: FournisseurDonnee;
    franceagrimer: FournisseurDonnee;
    francePat: FournisseurDonnee;
    ign: FournisseurDonnee;
    insee: FournisseurDonnee;
    laPoste: FournisseurDonnee;
    ministereAgriculture: FournisseurDonnee;
    ministereTransitionEcologique: FournisseurDonnee;
    officeFrancaisBiodiversite: FournisseurDonnee;
    openStreetMap: FournisseurDonnee;
    observatoireDesTerritoires: FournisseurDonnee;
    parcel: FournisseurDonnee;
    propluvia: FournisseurDonnee;
    solagro: FournisseurDonnee;
    unifa: FournisseurDonnee;
} = {
    ademe: {
        nom: 'ADEME',
        lienLogo: ADEME,
        largeurLogo: 60
    },
    agreste: {
        nom: 'Agreste',
        lienLogo: AGRESTE,
        largeurLogo: 200
    },
    agenceBio: {
        nom: 'Agence Bio',
        lienLogo: AGENCE_BIO,
        largeurLogo: 80
    },
    banatic: {
        nom: "BANATIC, la base nationale sur l'intercommunalité",
        lienLogo: BANATIC,
        largeurLogo: 100
    },
    eauFrance: {
        nom: 'Eau France',
        lienLogo: EAU_FRANCE,
        largeurLogo: 150
    },
    cerema: {
        nom: 'CEREMA',
        lienLogo: CEREMA,
        largeurLogo: 150
    },
    ctifl: {
        nom: 'CTIFL',
        lienLogo: CTIFL,
        largeurLogo: 60
    },
    federationScot: {
        nom: 'Fédération des SCoT',
        lienLogo: FEDERATION_SCOT,
        largeurLogo: 60
    },
    franceagrimer: {
        nom: 'FranceAgriMer',
        lienLogo: FRANCE_AGRI_MER,
        largeurLogo: 120
    },
    francePat: {
        nom: 'France PAT',
        lienLogo: FRANCE_PAT,
        largeurLogo: 100
    },
    ign: {
        nom: 'IGN',
        lienLogo: IGN,
        largeurLogo: 80
    },
    insee: {
        nom: 'INSEE',
        lienLogo: INSEE,
        largeurLogo: 60
    },
    laPoste: {
        nom: 'La Poste',
        lienLogo: LA_POSTE,
        largeurLogo: 120
    },
    ministereAgriculture: {
        nom: "Ministère de l'Agriculture",
        lienLogo: MINISTERE_AGRICULTURE,
        largeurLogo: 120
    },
    ministereTransitionEcologique: {
        nom: 'Ministère de la Transition Écologique et de la Cohésion des Territoires',
        lienLogo: MINISTERE_TRANSITION_ECOLOGIQUE,
        largeurLogo: 120
    },
    officeFrancaisBiodiversite: {
        nom: 'Office Français de la Biodiversité',
        lienLogo: OFFICE_FRANCAIS_BIODIVERSITE,
        largeurLogo: 150
    },
    openStreetMap: {
        nom: 'OpenStreetMap France',
        lienLogo: OPEN_STREET_MAP,
        largeurLogo: 80
    },
    observatoireDesTerritoires: {
        nom: 'Observatoire des territoires',
        lienLogo: OBSERVATOIRE_DES_TERRITOIRES,
        largeurLogo: 200
    },
    parcel: {
        nom: 'PARCEL',
        lienLogo: PARCEL,
        largeurLogo: 200
    },
    propluvia: {
        nom: 'Propluvia',
        lienLogo: PROPLUVIA,
        largeurLogo: 100
    },
    solagro: {
        nom: 'Solagro',
        lienLogo: SOLAGRO,
        largeurLogo: 150
    },
    unifa: {
        nom: 'UNIFA',
        lienLogo: UNIFA,
        largeurLogo: 120
    }
};
