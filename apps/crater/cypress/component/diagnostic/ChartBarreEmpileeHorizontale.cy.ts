import '@lga/design-system/public/theme-defaut/theme-defaut.css';
import '../../../src/pages/diagnostic/charts/ChartBarreEmpileeHorizontale.js';

import { html } from 'lit';

describe('Test ChartBarreEmpileeHorizontale', () => {
    const items = [
        {
            valeur: 0,
            libelle: 'categorie a',
            couleur: 'red'
        },
        {
            valeur: 4,
            libelle: 'categorie b',
            couleur: 'green'
        },
        {
            valeur: 5,
            libelle: 'categorie c',
            couleur: 'blue'
        },
        {
            valeur: 1,
            libelle: 'categorie d',
            couleur: 'black'
        }
    ];

    it('Test affichage Desktop', () => {
        // given
        cy.viewport(1000, 500);
        cy.mount<'c-chart-barre-empilee-horizontale'>(html`
            <c-chart-barre-empilee-horizontale .items=${items} unite="ha"></c-chart-barre-empilee-horizontale>
        `);
        // then
        cy.get('c-chart-barre-empilee-horizontale').invoke('width').should('be.approximately', 784, 10);
        cy.get('c-chart-barre-empilee-horizontale').get('.barre-item').should('have.length', 4);
        cy.get('c-chart-barre-empilee-horizontale').get('.legende-item').should('have.length', 4);
        cy.get('c-chart-barre-empilee-horizontale').get('.texte-titre').eq(0).should('have.text', '0%');
        cy.get('c-chart-barre-empilee-horizontale').get('.texte-titre').eq(1).should('have.text', '40%');
        cy.get('c-chart-barre-empilee-horizontale').get('.texte-titre').eq(2).should('have.text', '50%');
        cy.get('c-chart-barre-empilee-horizontale').get('.texte-titre').eq(3).should('have.text', '10%');
        cy.get('c-chart-barre-empilee-horizontale').get('.barre-item').eq(3).invoke('width').should('be.approximately', 78, 2);
        cy.get('c-chart-barre-empilee-horizontale').get('.barre-item').eq(3).should('have.css', 'background-color', 'rgb(0, 0, 0)');
    });
});
