import { arrondirANDecimales } from '@lga/base';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system';
import { css, html, LitElement, svg } from 'lit';
import { customElement, property, state } from 'lit/decorators.js';

import { CategorieCommerce, type IndicateursProximiteCommerces } from '../../../modeles/diagnostics';

const ID_CARACTERE_PIETON = String.fromCodePoint(128694);
const ID_CARACTERE_VELO = String.fromCodePoint(128690);
const ID_CARACTERE_MAISON = String.fromCodePoint(127968);

const DISTANCE_MAX_VOL_OISEAU_ACCESSIBLE_A_PIED_METRES = 1000;
const DISTANCE_MAX_VOL_OISEAU_ACCESSIBLE_A_VELO_METRES = 2000;
const DUREE_ACCESSIBLE_A_PIED_MIN = 15;
const DUREE_ACCESSIBLE_A_VELO_MIN = 10;

interface Commerces {
    libelle: string;
    couleur: string;
    distance: number;
}

declare global {
    interface HTMLElementTagNameMap {
        'c-chart-distances-plus-proches-commerces': ChartDistancesAuxCommerces;
    }
}

@customElement('c-chart-distances-plus-proches-commerces')
export class ChartDistancesAuxCommerces extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        css`
            div {
                display: grid;
                justify-content: center;
            }
            #figure {
                height: 400px;
                width: 100%;
            }

            #legende {
                display: flex;
                flex-wrap: wrap;
                margin: auto;
                justify-content: space-around;
            }

            #note {
                color: var(--couleur-neutre);
                margin: auto;
                padding-bottom: var(--dsem);
            }
        `
    ];

    private calculerPositionsXY(distance: number, cadran: number, nbCadrans: number): { x: number; y: number } {
        const angleCadran = (2 * Math.PI) / nbCadrans;
        const x = arrondirANDecimales(distance * Math.cos(angleCadran / 2 + (cadran - 1) * angleCadran), 1);
        const y = -arrondirANDecimales(distance * Math.sin(angleCadran / 2 + (cadran - 1) * angleCadran), 1);
        return {
            x: x,
            y: y
        };
    }

    private creerCerclesCommercesSVG() {
        let cerclesSvg = svg``;
        for (let i = 0; i < this.commerces.length; i++) {
            if (this.commerces[i].distance !== null) {
                cerclesSvg = svg`
                ${cerclesSvg}
                <circle
                cx="${this.calculerPositionsXY(this.commerces[i].distance, i + 1, this.commerces.length).x}"
                cy="${this.calculerPositionsXY(this.commerces[i].distance, i + 1, this.commerces.length).y}"
                r="150"
                fill-opacity="0.7"
                style="fill:${this.commerces[i].couleur}"
                />`;
            }
        }
        return cerclesSvg;
    }

    private creerLegendeHTML() {
        let legendeHtml = html``;
        for (const item of this.commerces) {
            legendeHtml = html` ${legendeHtml}
                <svg width="220px" height="50px" xmlns="http://www.w3.org/2000/svg">
                    <circle cx="20" cy="50%" r="10" fill-opacity="0.7" style="fill:${item.couleur}" />
                    <text x="35" y="40%">${item.libelle}</text>
                    <text x="35" y="70%">${
                        item.distance === null ? `Donnée indisponible` : html`${arrondirANDecimales(item.distance / 1000, 1)} km</text>`
                    }
                </svg>`;
        }
        return legendeHtml;
    }

    @property({ attribute: false })
    indicateurs?: IndicateursProximiteCommerces[];

    @state()
    commerces: Commerces[] = [];

    render() {
        return html`
            <div>
                <svg id="figure" viewBox="-3500 -3500 7000 7000" xmlns="http://www.w3.org/2000/svg">
                    <text x="-160" y="110" font-size="30rem">${ID_CARACTERE_MAISON}</text>

                    <circle
                        cx="0"
                        cy="0"
                        r="${DISTANCE_MAX_VOL_OISEAU_ACCESSIBLE_A_PIED_METRES}"
                        style="stroke:#444242; fill:transparent; stroke-width:50; stroke-dasharray:20"
                    />
                    <text x="${0.65 * DISTANCE_MAX_VOL_OISEAU_ACCESSIBLE_A_PIED_METRES}" y="200" font-size="40rem">${ID_CARACTERE_PIETON}</text>
                    <text x="${0.65 * DISTANCE_MAX_VOL_OISEAU_ACCESSIBLE_A_PIED_METRES}" y="600" font-size="16rem" font-family="font-family-texte">
                        ${DISTANCE_MAX_VOL_OISEAU_ACCESSIBLE_A_PIED_METRES / 1000} km
                    </text>

                    <circle
                        cx="0"
                        cy="0"
                        r="${DISTANCE_MAX_VOL_OISEAU_ACCESSIBLE_A_VELO_METRES}"
                        style="stroke:#444242; fill:transparent; stroke-width:50; stroke-dasharray:20"
                    />
                    <text x="${0.9 * DISTANCE_MAX_VOL_OISEAU_ACCESSIBLE_A_VELO_METRES}" y="200" font-size="40rem">${ID_CARACTERE_VELO}</text>
                    <text x="${0.9 * DISTANCE_MAX_VOL_OISEAU_ACCESSIBLE_A_VELO_METRES}" y="600" font-size="16rem" font-family="font-family-texte">
                        ${DISTANCE_MAX_VOL_OISEAU_ACCESSIBLE_A_VELO_METRES / 1000} km
                    </text>

                    ${this.creerCerclesCommercesSVG()}
                </svg>
                <div id="legende" class="texte-petit">${this.creerLegendeHTML()}</div>
                <p id="note" class="texte-petit">
                    Note: toutes les distances sont à vol d'oiseau, ${DISTANCE_MAX_VOL_OISEAU_ACCESSIBLE_A_PIED_METRES / 1000} km correspondant à une
                    zone accessible en ${DUREE_ACCESSIBLE_A_PIED_MIN} min à pied et ${DISTANCE_MAX_VOL_OISEAU_ACCESSIBLE_A_VELO_METRES / 1000} km à
                    ${DUREE_ACCESSIBLE_A_VELO_MIN} min à vélo.
                </p>
            </div>
        `;
    }

    willUpdate() {
        if (this.indicateurs) this.commerces = this.obtenirCommerces(this.indicateurs);
    }

    private obtenirCommerces(indicateurs: IndicateursProximiteCommerces[]): Commerces[] {
        return indicateurs
            .filter((ipc) => ipc.distancePlusProcheCommerceMetres !== null)
            .map((ipc) => {
                return {
                    libelle: ipc.categorieCommerce.libelle,
                    couleur: CategorieCommerce.fromString(ipc.categorieCommerce.code).couleur!,
                    distance: ipc.distancePlusProcheCommerceMetres!
                };
            });
    }
}
