import { arrondirANDecimales, calculerIndiceClasse, formaterNombreSelonValeurString } from '@lga/base';
import {
    type ClasseCarteChorochromatique,
    type FonctionCalculerLibelleTooltip,
    StyleCarteCerclesProportionnelsColores,
    StyleCarteChorochromatique
} from '@lga/styles-carte';

export const CLASSES_HVN: ClasseCarteChorochromatique[] = [
    {
        code: 'NÉFASTE',
        libelle: 'Néfaste',
        couleur: '#DB9C74'
    },
    {
        code: 'MAUVAIS',
        libelle: 'Mauvais',
        couleur: '#E6CFAE'
    },
    {
        code: 'MOYEN',
        libelle: 'Moyen',
        couleur: '#BFC0A5'
    },
    {
        code: 'BON',
        libelle: 'Bon',
        couleur: '#98B19C'
    },
    {
        code: 'TRES_BON',
        libelle: 'Très bon',
        couleur: '#64857C'
    }
];

const calculerClasseHVN = (valeurHVN: number | null): string | null => {
    const indiceClasse = calculerIndiceClasse(valeurHVN, 30, CLASSES_HVN.length);
    return indiceClasse === null ? null : CLASSES_HVN[indiceClasse].code;
};

const calculerLibelleTooltipHVN: FonctionCalculerLibelleTooltip = (valeur: number | string | null): string => {
    return typeof valeur !== 'number'
        ? 'Score HVN non disponible'
        : 'Score HVN = ' +
              arrondirANDecimales(valeur, 0) +
              ' : ' +
              CLASSES_HVN.find((classe) => classe.code === calculerClasseHVN(valeur))?.libelle +
              ' pour la biodiversité';
};

const calculerLibelleSau: FonctionCalculerLibelleTooltip = (valeur: number | string | null) => {
    return 'SAU : ' + formaterNombreSelonValeurString(valeur as number | null) + ' ha';
};

export const styleCarteHVNCerclesProportionnelsColores = new StyleCarteCerclesProportionnelsColores({
    couleurContours: 'grey',
    classes: CLASSES_HVN,
    opacite: 0.9,
    facteurMultiplicatifTailleCercles: 0.05,
    fonctionNombreVersCodeClasse: calculerClasseHVN,
    calculerLibelleTooltipPersonnalise: calculerLibelleTooltipHVN,
    calculerLibelleTooltipRayonCercle: calculerLibelleSau
});

export const styleCarteHVNChorochromatique = new StyleCarteChorochromatique({
    couleurContours: 'grey',
    classes: CLASSES_HVN,
    fonctionNombreVersCodeClasse: calculerClasseHVN,
    calculerLibelleTooltipPersonnalise: calculerLibelleTooltipHVN
});
