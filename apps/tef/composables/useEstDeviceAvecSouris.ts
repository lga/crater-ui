export function useEstDeviceAvecSouris(): boolean {
    if (typeof window === 'undefined') return false; // Cas d'un render coté serveur
    return window.matchMedia('(any-pointer: fine)').matches;
}
