import { CategorieTerritoire } from '@lga/territoires';
import { describe, expect, it } from 'vitest';

import { Note } from '../Note';
import { PolitiqueAmenagement } from './PolitiqueAmenagement';
import { TerresAgricoles } from './TerresAgricoles';

describe('Tests de la classe TerresAgricoles', () => {
    it('Les champs calculés retournent null quand les valeurs utilisées dans les calculs sont null', () => {
        const terresAgricoles = new TerresAgricoles(
            { id: '', nom: '', categorie: CategorieTerritoire.Commune },
            new Note(null),
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            PolitiqueAmenagement.DONNEES_NON_DISPONIBLES,
            null,
            null
        );
        expect(terresAgricoles.ratioArtificialisation5ansSurSuperficieTerritoirePourcent).toEqual(null);
        expect(terresAgricoles.chiffreRythmeArtificialisation).toEqual({ nombre: null, periode: null });
        expect(terresAgricoles.evolutionPartLogementsVacants2013a2018Points).toEqual(null);
    });
    it('Les champs calculés retournent une valeur', () => {
        const terresAgricoles = new TerresAgricoles(
            { id: '', nom: '', categorie: CategorieTerritoire.Commune },
            new Note(null),
            100,
            7,
            null,
            null,
            7,
            5,
            20,
            null,
            PolitiqueAmenagement.DONNEES_NON_DISPONIBLES,
            5,
            8
        );
        expect(terresAgricoles.besoinsParHabitantAssietteActuelleM2).toEqual(3500);
        expect(terresAgricoles.besoinsParHabitantAssietteDemitarienneM2).toEqual(2500);
        expect(terresAgricoles.ratioArtificialisation5ansSurSuperficieTerritoirePourcent).toEqual(7);
        expect(terresAgricoles.chiffreRythmeArtificialisation).toEqual({ nombre: 10, periode: 'tous les cinq ans' });
        expect(terresAgricoles.evolutionPartLogementsVacants2013a2018Points).toEqual(3);
    });
});
