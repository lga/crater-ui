import type { IdTheme } from '@lga/indicateurs';
import { type RouteLocationRaw, type RouteParams, useRoute } from 'vue-router/auto';

export function useRouteThematiques(
    idTheme: IdTheme,
    idTerritoire?: string
): RouteLocationRaw<'thematiques-risques-securite-alimentaire' | 'thematiques-idTheme-idTerritoire'> {
    if (idTheme === 'risques-securite-alimentaire') {
        const route: RouteLocationRaw<'thematiques-risques-securite-alimentaire'> = {
            name: 'thematiques-risques-securite-alimentaire'
        };
        return route;
    }
    const route: RouteLocationRaw<'thematiques-idTheme-idTerritoire'> = {
        name: 'thematiques-idTheme-idTerritoire',
        // eslint-disable-next-line @typescript-eslint/prefer-nullish-coalescing
        params: { idTheme: idTheme, idTerritoire: idTerritoire }
    };
    return route;
}

export function useRouteParamsThematiques(): RouteParams<'thematiques-idTheme-idTerritoire'> {
    return useRoute().params as RouteParams<'thematiques-idTheme-idTerritoire'>;
}
