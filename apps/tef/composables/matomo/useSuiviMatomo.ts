// Voir https://developer.matomo.org/guides/tracking-javascript-guide#optional-creating-a-custom-opt-out-form

import type { RouteLocationRaw } from 'vue-router/auto';

export type CategorieEvenementMatomo = 'Scroll' | 'Navigation' | 'Clic' | 'Partage RS' | 'Recherche territoire' | 'Swipe';

export const useTracerActionMatomo = (categorie: CategorieEvenementMatomo, action: string, nom?: string, valeur?: number) => {
    window._paq?.push(['trackEvent', categorie, action, nom, valeur]);
};

export const useTracerNavigationMatomo = (nomRouteCourante: string, nomComposantCourant: string, routeCible: RouteLocationRaw) => {
    const pageDestination: string | undefined = typeof routeCible === 'string' ? routeCible : 'name' in routeCible ? routeCible.name : undefined;
    if (pageDestination) {
        useTracerActionMatomo(
            'Navigation',
            `Navigation DE:page=${nomRouteCourante} VERS:page=${pageDestination}`,
            `Navigation DE:page=${nomRouteCourante},comp=${nomComposantCourant} VERS:page=${pageDestination}`
        );
    }
};
