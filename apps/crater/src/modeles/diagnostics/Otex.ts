import type { CodeOtexMajoritaire5PostesApi } from '@lga/specification-api';
interface OtexMajoritaire5Postes {
    code: CodeOtexMajoritaire5PostesApi;
    libelle: string;
}

interface OtexEntite {
    libelle: string;
    couleur?: string;
}

export interface NbCommunesOtex {
    otex: OtexEntite;
    valeur: number;
}

interface NbCommunesParOtex {
    grandesCultures: number;
    maraichageHorticulture: number;
    viticulture: number;
    fruits: number;
    bovinLait: number;
    bovinViande: number;
    bovinMixte: number;
    ovinsCaprinsAutresHerbivores: number;
    porcinsVolailles: number;
    polyculturePolyelevage: number;
    nonClassees: number;
    sansExploitations: number;
    nonRenseigne: number;
}

export const LIBELLE_OTEX_NON_DEFINIE_OU_NON_MAJORITAIRE = 'Non définie ou non majoritaire';

const OTEX_5_POSTES: OtexMajoritaire5Postes[] = [
    {
        code: 'GRANDES_CULTURES',
        libelle: 'Grandes Cultures'
    },
    {
        code: 'VITICULTURE',
        libelle: 'Viticulture'
    },
    {
        code: 'FRUITS_ET_LEGUMES',
        libelle: 'Maraîchage Horticulture ou Fruits'
    },
    {
        code: 'ELEVAGE',
        libelle: 'Élevage'
    },
    {
        code: 'POLYCULTURE_POLYELEVAGE',
        libelle: 'Polyculture Polyélevage'
    },
    {
        code: 'NON_DEFINIE',
        libelle: LIBELLE_OTEX_NON_DEFINIE_OU_NON_MAJORITAIRE
    },
    {
        code: 'SANS_OTEX_MAJORITAIRE',
        libelle: LIBELLE_OTEX_NON_DEFINIE_OU_NON_MAJORITAIRE
    }
];

const Otex12Postes = {
    grandesCultures: {
        libelle: 'Grandes Cultures',
        couleur: 'rgb(219, 246, 92)'
    },
    maraichageHorticulture: {
        libelle: 'Maraîchage Horticulture',
        couleur: 'rgb(91, 126, 113)'
    },
    viticulture: {
        libelle: 'Viticulture',
        couleur: 'rgb(127, 74, 91)'
    },
    fruits: {
        libelle: 'Fruits',
        couleur: 'rgb(80, 168, 92)'
    },
    bovinLait: {
        libelle: 'Bovin lait',
        couleur: 'rgb(121, 69, 27)'
    },
    bovinViande: {
        libelle: 'Bovin viande',
        couleur: 'rgb(165, 33, 33)'
    },
    bovinMixte: {
        libelle: 'Bovin mixte',
        couleur: 'rgb(65, 44, 38)'
    },
    ovinsCaprinsAutresHerbivores: {
        libelle: 'Ovins Caprins et autres Herbivores',
        couleur: 'rgb(188, 138, 134)'
    },
    porcinsVolailles: {
        libelle: 'Porcins Volailles',
        couleur: 'rgb(236, 95, 71)'
    },
    polyculturePolyelevage: {
        libelle: 'Polyculture Polyélevage',
        couleur: 'rgb(168, 106, 66)'
    },
    nonClassees: {
        libelle: 'Non Classées',
        couleur: 'rgb(213, 199, 176)'
    },
    sansExploitations: {
        libelle: 'Sans Exploitations',
        couleur: 'rgb(255, 255, 255)'
    },
    nonRenseigne: {
        libelle: 'Non renseignées',
        couleur: 'rgb(100, 100, 100)'
    }
};

export class Otex {
    public readonly nbCommunesOtex12Postes: NbCommunesOtex[] = [];
    public readonly libelleOtexMajoritaire5Postes: string;

    constructor(nbCommunesParOtex: NbCommunesParOtex, codeOtexMajoritaire5Postes: CodeOtexMajoritaire5PostesApi) {
        if (nbCommunesParOtex) {
            this.nbCommunesOtex12Postes = [
                {
                    otex: Otex12Postes.grandesCultures,
                    valeur: nbCommunesParOtex.grandesCultures
                },
                {
                    otex: Otex12Postes.maraichageHorticulture,
                    valeur: nbCommunesParOtex.maraichageHorticulture
                },
                {
                    otex: Otex12Postes.viticulture,
                    valeur: nbCommunesParOtex.viticulture
                },
                {
                    otex: Otex12Postes.fruits,
                    valeur: nbCommunesParOtex.fruits
                },
                {
                    otex: Otex12Postes.bovinLait,
                    valeur: nbCommunesParOtex.bovinLait
                },
                {
                    otex: Otex12Postes.bovinViande,
                    valeur: nbCommunesParOtex.bovinViande
                },
                {
                    otex: Otex12Postes.bovinMixte,
                    valeur: nbCommunesParOtex.bovinMixte
                },
                {
                    otex: Otex12Postes.ovinsCaprinsAutresHerbivores,
                    valeur: nbCommunesParOtex.ovinsCaprinsAutresHerbivores
                },
                {
                    otex: Otex12Postes.porcinsVolailles,
                    valeur: nbCommunesParOtex.porcinsVolailles
                },
                {
                    otex: Otex12Postes.polyculturePolyelevage,
                    valeur: nbCommunesParOtex.polyculturePolyelevage
                },
                {
                    otex: Otex12Postes.nonClassees,
                    valeur: nbCommunesParOtex.nonClassees
                },
                {
                    otex: Otex12Postes.sansExploitations,
                    valeur: nbCommunesParOtex.sansExploitations
                },
                {
                    otex: Otex12Postes.nonRenseigne,
                    valeur: nbCommunesParOtex.nonRenseigne
                }
            ];
        }
        const otexMajoritaire5Postes = OTEX_5_POSTES.find((i) => i.code === codeOtexMajoritaire5Postes);
        if (otexMajoritaire5Postes) {
            this.libelleOtexMajoritaire5Postes = otexMajoritaire5Postes.libelle;
        } else {
            console.error(`codeOtexMajoritaire5Postes ${codeOtexMajoritaire5Postes} inconnu !`);
            throw new Error(`codeOtexMajoritaire5Postes ${codeOtexMajoritaire5Postes} inconnu !`);
        }
    }
}
