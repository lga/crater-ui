import { SystemeAlimentaire } from '../../modeles/systeme-alimentaire/SystemeAlimentaire';
import { DONNEES_DOMAINES } from '../domaines/domaines.js';
import { DONNEES_MAILLONS } from './maillons';
import { DONNEES_OUTILS_EXTERNES } from './outils-externes';
import { DONNEES_VOIES_DE_RESILIENCE } from './voies-resilience';

export const SA = new SystemeAlimentaire(DONNEES_VOIES_DE_RESILIENCE, DONNEES_OUTILS_EXTERNES, DONNEES_MAILLONS, DONNEES_DOMAINES);
