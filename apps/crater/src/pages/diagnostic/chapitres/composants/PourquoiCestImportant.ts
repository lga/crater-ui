import { CSS_BREAKPOINT_MAX_WIDTH_DESKTOP_PETIT } from '@lga/design-system/build/styles/styles-breakpoints';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system';
import type { Maillon } from '@lga/indicateurs';
import { ICONES_SVG } from '@lga/indicateurs';
import { css, html, LitElement, unsafeCSS } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { unsafeHTML } from 'lit/directives/unsafe-html.js';

import { STYLES_CRATER } from '../../../commun/pages-styles';

declare global {
    interface HTMLElementTagNameMap {
        'c-pourquoi-cest-important': PourquoiCestImportant;
    }
}

@customElement('c-pourquoi-cest-important')
export class PourquoiCestImportant extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        STYLES_CRATER,
        css`
            div {
                margin: 0;
            }

            #encart {
                margin: 1rem auto 2rem auto;
            }

            #idees {
                padding-left: 1rem;
                display: flex;
            }

            #idees ul {
                padding: 0;
            }

            #idees li {
                list-style: none;
            }

            #icone {
                padding-left: 5px;
                padding-right: 10px;
                height: 25px;
                margin: auto 0;
            }

            #icone svg {
                height: 100%;
                stroke: 3px;
            }

            #icone path {
                fill: var(--couleur-primaire);
            }

            blockquote {
                border-left: 4px solid var(--couleur-primaire);
                padding-left: 5px;
                padding-top: 5px;
                padding-bottom: 5px;
                margin: 0;
                width: fit-content;
            }

            .idee-texte {
                padding-left: 10px;
            }

            .idee-source {
                text-align: right;
                padding-top: 10px;
            }

            @media screen and (max-width: ${unsafeCSS(CSS_BREAKPOINT_MAX_WIDTH_DESKTOP_PETIT + 'px')}) {
                #idees {
                    padding-left: 0;
                }
            }
        `
    ];

    @property({ attribute: false })
    maillon?: Maillon;

    render() {
        return html` <div id="encart">
            <p>${unsafeHTML(this.maillon?.description)}</p>
            <div id="idees">
                <div id="icone">${unsafeHTML(ICONES_SVG.ampoule)}</div>
                <ul>
                    ${this.maillon?.citations.map((idee) => {
                        return html` <li>
                            <blockquote>
                                <div class="idee-texte texte-moyen">${unsafeHTML(idee.texte)}</div>
                                <div class="idee-source texte-petit">${unsafeHTML(idee.source)}</div>
                            </blockquote>
                        </li>`;
                    })}
                </ul>
            </div>
        </div>`;
    }
}
