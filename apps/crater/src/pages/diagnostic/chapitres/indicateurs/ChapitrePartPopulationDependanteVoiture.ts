import '../../charts/ChartBarres.js';
import '../composants/ChapitreDomaine.js';
import '../../../commun/PageSablierCrater.js';

import { IDS_DOMAINES } from '@lga/indicateurs';
import { SA } from '@lga/indicateurs';
import { html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

import { EtatRapport, type Rapport } from '../../../../modeles/diagnostics/Rapport.js';
import { Serie, SerieMultiple } from '../../charts/chart-outils.js';
import type { OptionsChartBarres } from '../../charts/ChartBarres';
import type { DonneesPageDiagnostic } from '../../PageDiagnostic.js';
import type { OptionsDomaineDetaille } from '../composants/ChapitreDomaine';

declare global {
    interface HTMLElementTagNameMap {
        'c-chapitre-part-population-dependante-voiture': ChapitrePartPopulationDependanteVoiture;
    }
}

@customElement('c-chapitre-part-population-dependante-voiture')
export class ChapitrePartPopulationDependanteVoiture extends LitElement {
    @property({ attribute: false })
    donneesPage?: DonneesPageDiagnostic;

    @property({ attribute: false })
    rapport?: Rapport;

    render() {
        if (this.rapport?.getEtat() === EtatRapport.PRET) {
            return html`<c-chapitre-domaine
                .options=${this.calculerOptionsChapitreDomainePartPopulationDependanteVoiture(this.rapport, this.donneesPage)}
                nbLignesMessageMinimum="6"
            >
                <c-chart-barres
                    .options=${this.calculerOptionsChartPartPopulationDependanteVoiture(this.rapport)}
                    slot="chart-chapitre-domaine"
                ></c-chart-barres>
            </c-chapitre-domaine>`;
        } else {
            return html`<c-page-sablier-crater></c-page-sablier-crater>`;
        }
    }

    private calculerOptionsChapitreDomainePartPopulationDependanteVoiture(
        rapport: Rapport,
        donneesPage: DonneesPageDiagnostic | undefined
    ): OptionsDomaineDetaille {
        return {
            domaine: SA.getDomaine(IDS_DOMAINES.dependancePopulationVoiture)!,
            message:
                rapport.getDiagnosticActif()?.transformationDistribution.messagePartPopulationDependanteVoiture +
                `<br><br>Le graphique suivant présente la part de la population théoriquement dépendante de la voiture pour ses achats alimentaires pour chaque type de commerces :`,
            territoire: rapport.getTerritoireActif(),
            estPagePourTef: donneesPage?.estPagePourTef
        };
    }

    private calculerOptionsChartPartPopulationDependanteVoiture(rapport: Rapport): OptionsChartBarres {
        return {
            series: new SerieMultiple(
                [
                    new Serie(
                        rapport
                            .getDiagnosticActif()
                            ?.transformationDistribution.indicateursProximiteCommercesParTypesCommerces.map(
                                (e) => e.partPopulationDependanteVoiturePourcent
                            ),
                        rapport.getDiagnosticActif()?.territoire.nom
                    ),
                    new Serie(
                        rapport
                            .getDiagnosticActif()
                            ?.transformationDistribution.indicateursProximiteCommercesParTypesCommerces.map(
                                (e) => e.partPopulationDependanteVoiturePourcent
                            ),
                        rapport.getDiagnosticPays()?.territoire.nom
                    )
                ],
                rapport
                    .getDiagnosticActif()
                    ?.transformationDistribution.indicateursProximiteCommercesParTypesCommerces.map((e) => e.categorieCommerce.libelle)
            ),
            nomAxeOrdonnees: 'Part de la population [%]',
            maxY: 100
        };
    }
}
