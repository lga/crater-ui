import { stringifyAvecTri } from '@lga/base';
import type express from 'express';

import { ErreurTechnique, ErreurTemporairementIndisponible } from './erreurs';

export enum Etat {
    INIT_EN_COURS,
    PRET,
    ERREUR
}

export function trierObjet(objet: object): string {
    return JSON.parse(stringifyAvecTri(objet));
}
export function ajouterBodyJson(reponse: express.Response, jsonBody: object): express.Response {
    reponse.setHeader('Content-Type', 'application/json');
    const stringBody = stringifyAvecTri(jsonBody);
    reponse.end(stringBody);
    return reponse;
}

export function verifierEtatApi(etat: Etat) {
    if (etat === Etat.INIT_EN_COURS) throw new ErreurTemporairementIndisponible('Initialisation en cours, réessayer dans quelques instants.');
    if (etat === Etat.ERREUR) throw new ErreurTechnique('Erreur technique inconnue.');
}

export const SAUT_LIGNE = '\n';

// TODO : refactor pour supprimer les eslint-disable
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function jsonToCsv(objetJson: any, prefixeNomChamp = ''): string[] {
    const csv: string[] = [];

    for (const nomChampCourant in objetJson) {
        const nomCompletChamp = calculerNomCompletChamp(objetJson, prefixeNomChamp, nomChampCourant);
        const valeurChampCourant = objetJson[nomChampCourant];

        if (estTableauDeValeurs(valeurChampCourant)) {
            csv.push(nomCompletChamp + ';' + valeurChampCourant.join(';'));
        } else if (estObjetOuTableauDObjets(valeurChampCourant)) {
            csv.push(...jsonToCsv(valeurChampCourant, nomCompletChamp));
        } else {
            // cas champ avec une valeur simple : string/number/boolean
            csv.push(nomCompletChamp + ';' + valeurChampCourant);
        }
    }
    return csv;

    // TODO : refactor pour supprimer les eslint-disable
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    function calculerNomCompletChamp(objetJson: any, prefixeNomChamp: string, nomChampCourant: string) {
        if (Array.isArray(objetJson)) {
            return prefixeNomChamp;
        } else {
            return (prefixeNomChamp ? prefixeNomChamp + '.' : '') + nomChampCourant;
        }
    }

    // TODO : refactor pour supprimer les eslint-disable
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    function estObjetOuTableauDObjets(valeurChamp: any) {
        return !estTableauDeValeurs(valeurChamp) && typeof valeurChamp === 'object';
    }

    // TODO : refactor pour supprimer les eslint-disable
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    function estTableauDeValeurs(valeurChamp: any) {
        return (
            typeof valeurChamp === 'object' &&
            Array.isArray(valeurChamp) &&
            valeurChamp.length > 0 &&
            (!valeurChamp[0] || typeof valeurChamp[0] === 'number' || typeof valeurChamp[0] === 'string' || typeof valeurChamp[0] === 'boolean')
        );
    }
}

// TODO : refactor pour supprimer les eslint-disable
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function extraireValeurDepuisSousObjet(objet: object, cheminChampAExtraire: string, anneeAConserver = ''): any {
    const cheminObjetParent = cheminChampAExtraire.split('.').slice(0, -1).join('.');
    const objetParent = extraireSousObjet(objet, cheminObjetParent);

    if (!Array.isArray(objetParent)) {
        return extraireSousObjet(objet, cheminChampAExtraire);
    } else {
        let nomChampFiltre = '';
        let valeurChampFiltre = '';
        if (anneeAConserver != '') {
            nomChampFiltre = 'annee';
            valeurChampFiltre = anneeAConserver;
        }
        return extrairePremiereValeurSelonFiltre(objet, cheminChampAExtraire, nomChampFiltre, valeurChampFiltre);
    }
}

// TODO : refactor pour supprimer les eslint-disable
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function extraireSousObjet(objet: any, cheminSousObjet: string): any {
    const listeCles = cheminSousObjet.split('.');
    let sousObjet = objet;
    for (const cle of listeCles) {
        // TODO : refactor pour supprimer les eslint-disable
        // eslint-disable-next-line no-prototype-builtins
        if (sousObjet.hasOwnProperty(cle)) {
            sousObjet = sousObjet[cle];
        } else {
            return {};
        }
    }
    return sousObjet;
}

export function extrairePremiereValeurSelonFiltre(
    // TODO : refactor pour supprimer les eslint-disable
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    objet: any,
    cheminChampAExtraire: string,
    nomChampFiltre: string,
    valeurFiltre: string
): (string | number) | (string | number)[] | null {
    const cheminObjetParent = cheminChampAExtraire.split('.').slice(0, -1).join('.');
    const nomChampAExtraire = cheminChampAExtraire.split('.').slice(-1)[0];
    const tableauParent = extraireSousObjet(objet, cheminObjetParent);

    if (!Array.isArray(tableauParent)) return null;

    const premierObjetSelonFiltre = tableauParent.find((o) => {
        // TODO : refactor pour supprimer les eslint-disable
        // eslint-disable-next-line no-prototype-builtins
        return !o.hasOwnProperty(nomChampFiltre) || o[nomChampFiltre].toString() === valeurFiltre;
    });
    // TODO : refactor pour supprimer les eslint-disable
    // eslint-disable-next-line no-prototype-builtins
    if (premierObjetSelonFiltre.hasOwnProperty(nomChampAExtraire)) {
        if (nomChampFiltre == '') return tableauParent.map((o) => o[nomChampAExtraire]);
        else return premierObjetSelonFiltre[nomChampAExtraire];
    }
    return null;
}

export function convertirDateEnString(date: Date): string {
    return date.toISOString().split('T')[0];
}
