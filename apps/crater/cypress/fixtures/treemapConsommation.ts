export const occitanie = {
    consommationAnimauxHa: 1930121.85,
    consommationHumainsHa: 278642.57999999996,
    donneesTreemapConsommationAnimaux: [
        {
            code: 'CER',
            labelCourt: 'CE',
            labelLong: 'Céréales',
            couleur: '#ffba66',
            sauHa: 282312.17,
            detailCultures: [
                {
                    codeCulture: 'BA-AAC',
                    nomCulture: 'Alim animale',
                    sauHa: 282312.17
                }
            ]
        },
        {
            code: 'FOU',
            labelCourt: 'FO',
            labelLong: 'Fourrages',
            couleur: '#ce7676',
            sauHa: 1490335.58,
            detailCultures: [
                {
                    codeCulture: 'BA-AFO',
                    nomCulture: 'Fourrages annuels',
                    sauHa: 107315.3
                },
                {
                    codeCulture: 'BA-PRA',
                    nomCulture: 'Prairies',
                    sauHa: 1383020.28
                }
            ]
        },
        {
            code: 'OLP',
            labelCourt: 'OL',
            labelLong: 'Oléoprotéagineux',
            couleur: '#a38566',
            sauHa: 157474.1,
            detailCultures: [
                {
                    codeCulture: 'BA-AAO',
                    nomCulture: 'Tourteaux',
                    sauHa: 145558.6
                },
                {
                    codeCulture: 'BA-PRO',
                    nomCulture: 'Légumin graines',
                    sauHa: 11915.5
                }
            ]
        }
    ],
    donneesTreemapConsommationHumains: [
        {
            code: 'CER',
            labelCourt: 'CE',
            labelLong: 'Céréales',
            couleur: '#ffba66',
            sauHa: 124033.18,
            detailCultures: [
                {
                    codeCulture: 'BH-AHC',
                    nomCulture: 'Alim humaine',
                    sauHa: 124033.18
                }
            ]
        },
        {
            code: 'DVC',
            labelCourt: 'DV',
            labelLong: 'Autres cultures',
            couleur: '#9195d4',
            sauHa: 39679.119999999995,
            detailCultures: [
                {
                    codeCulture: 'BH-BET',
                    nomCulture: 'Betterave sucrière',
                    sauHa: 23546.94
                },
                {
                    codeCulture: 'BH-OLV',
                    nomCulture: 'Oliviers',
                    sauHa: 1455.53
                },
                {
                    codeCulture: 'BH-PDT',
                    nomCulture: 'Pommes de terre',
                    sauHa: 14676.65
                }
            ]
        },
        {
            code: 'FLC',
            labelCourt: 'FL',
            labelLong: 'Fruits et légumes',
            couleur: '#74a48b',
            sauHa: 46134.17,
            detailCultures: [
                {
                    codeCulture: 'BH-FCO',
                    nomCulture: 'Fruits à coque',
                    sauHa: 10297.76
                },
                {
                    codeCulture: 'BH-FRU',
                    nomCulture: 'Fruits',
                    sauHa: 16161.57
                },
                {
                    codeCulture: 'BH-LEG',
                    nomCulture: 'Légumes',
                    sauHa: 19674.84
                }
            ]
        },
        {
            code: 'OLP',
            labelCourt: 'OL',
            labelLong: 'Oléoprotéagineux',
            couleur: '#a38566',
            sauHa: 68796.11,
            detailCultures: [
                {
                    codeCulture: 'BH-AHO',
                    nomCulture: 'Alim humaine',
                    sauHa: 68796.11
                }
            ]
        }
    ]
};
