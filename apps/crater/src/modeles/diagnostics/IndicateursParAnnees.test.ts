import { describe, expect, it } from 'vitest';

import { IndicateursParAnnees } from './IndicateursParAnnees';

describe('Tests de la classe IndicateursParAnnees', () => {
    interface Indicateur {
        annee: number;
        valeur: number | null;
    }
    const indicateursParAnnees = new IndicateursParAnnees<Indicateur>([
        { annee: 2019, valeur: 2 },
        { annee: 2018, valeur: 1 }
    ]);

    it('Verifier annees et valeurs', () => {
        expect(indicateursParAnnees.annees).toEqual([2018, 2019]);
        expect(indicateursParAnnees.valeurs((indicateur) => indicateur.valeur)).toEqual([1, 2]);
    });

    it('Verifier tendance', () => {
        expect(indicateursParAnnees.tendance((i) => i.valeur)).toEqual(1);
    });
});
