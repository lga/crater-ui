export interface Erreur {
    code: string;
    message: string;
}
export class GestionnaireErreur {
    private _erreur?: Erreur;

    get erreur(): Erreur | undefined {
        return this._erreur;
    }

    mettreEnErreur(codeErreur: string, messageErreur: string) {
        console.log('mettreEnErreur', codeErreur, messageErreur);
        this._erreur = { code: codeErreur, message: messageErreur };
    }

    annulerErreur() {
        this._erreur = undefined;
    }

    public estEnErreur(): boolean {
        return this._erreur !== undefined;
    }
}
