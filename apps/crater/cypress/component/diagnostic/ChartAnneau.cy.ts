import '@lga/design-system/public/theme-defaut/theme-defaut.css';
import '../../../src/pages/diagnostic/charts/ChartAnneau.js';

import { html } from 'lit';

describe('Test ChartAnneau', () => {
    const items = [
        {
            valeur: 0,
            libelle: 'categorie a',
            tooltip: 'tooltip a',
            couleur: 'red'
        },
        {
            valeur: 4,
            libelle: 'categorie b',
            tooltip: 'tooltip b',
            couleur: 'green'
        },
        {
            valeur: 5,
            libelle: 'categorie c',
            tooltip: 'tooltip c',
            couleur: 'blue'
        },
        {
            valeur: 1,
            libelle: 'categorie d',
            tooltip: 'tooltip d',
            couleur: 'black'
        }
    ];

    it('Test affichage mobile', () => {
        // given
        cy.viewport(500, 500);
        cy.mount<'c-chart-anneau'>(html` <c-chart-anneau .items=${items} unite="ha"></c-chart-anneau> `);
        // then
        cy.get('c-chart-anneau').invoke('width').should('be.approximately', 484, 12);
        cy.get('c-chart-anneau').get('path').should('have.length', 4);
        cy.get('c-chart-anneau').get('#legende-discrete').get('li').should('have.length', 4);
        cy.get('c-chart-anneau').get('path').eq(3).invoke('width').should('be.approximately', 298, 5);
        cy.get('c-chart-anneau').get('path').eq(3).invoke('height').should('be.approximately', 236, 5);
        cy.get('c-chart-anneau').get('path').eq(3).should('have.css', 'background-color', 'rgba(0, 0, 0, 0)');
    });
});
