import { formaterNombreSelonValeurString, htmlstring, type ImageModele } from '@lga/base';
import { recupererListe3InitiativesParConstats } from '@lga/initiatives';

import { calculerNiveauAutonomie } from '../fonctions-utils';
import type { DonneesApi } from '../fonctions-utils.js';
import { creerLienExterneHtmlBrut } from '../liens-utils';
import type { ConstatModele } from './ConstatModele';
import { creerConstatAbsenceDonnees } from './constats-utils';
import type { InfographieBaseModele, InfographieImageMessageModele } from './InfographieModele.js';

const LIBELLE_TITRE_THEME = 'Changer de régime ?';

export const IMAGE_SELF_SERVICE: ImageModele = {
    url: '/images/constats/titres/restaurant-self-service-800x861.jpg',
    descriptionAlt: 'Restaurant self-service',
    resolutionX: 800,
    resolutionY: 861
};

export function creerConstatRegimeAlimentaireTropCarne(donneesApi: DonneesApi): ConstatModele<InfographieImageMessageModele> {
    const niveauAutonomie = calculerNiveauAutonomie(
        donneesApi.diagnosticApi.productionsBesoins.besoinsAssietteActuelle.tauxAdequationBrutPourcent,
        donneesApi.diagnosticApi.productionsBesoins.besoinsAssietteDemitarienne.tauxAdequationBrutPourcent
    );

    let message;
    const initiatives = recupererListe3InitiativesParConstats(donneesApi.diagnosticApi.profils, ['regime-alimentaire-trop-carne']);
    switch (niveauAutonomie) {
        case 'INCONNU':
            return creerConstatAbsenceDonnees('consommation', LIBELLE_TITRE_THEME, IMAGE_SELF_SERVICE);
        case 'TERRES_INSUFFISANTES':
            message = {
                titre: 'Notre consommation de viande menace notre souveraineté alimentaire.',
                texte: htmlstring`Consommer moins de viande permet de nettement diminuer la surface agricole nécessaire pour nous nourrir. Or, d’importantes baisses de production sont prévisibles avec le changement climatique. C’est donc un levier majeur pour construire la sécurité alimentaire de demain.`,
                question: 'Et si on végétalisait un peu nos assiettes ?',
                initiatives: initiatives
            };
            break;
        case 'TERRES_PARTIELLEMENT_SUFFISANTES':
            message = {
                titre: 'Bonne nouvelle ! Le territoire pourrait nourrir tous les habitants si on réduisait la consommation de viande.',
                texte: htmlstring`Consommer moins de viande permet quasiment de doubler votre capacité nourricière ! Et en bonus, de supprimer les importations de viande et de nettement baisser votre impact environnemental.`,
                question: 'Et si on végétalisait un peu nos assiettes ?',
                initiatives: initiatives
            };
            break;
        case 'TERRES_SUFFISANTES':
            message = {
                titre: 'Notre consommation de viande menace notre souveraineté alimentaire.',
                texte: htmlstring`D’importantes baisses de production sont prévisibles avec le changement climatique. Or, consommer moins de viande permet de doubler votre capacité nourricière ET de diminuer votre impact sur le climat ! C'est un levier majeur de la sécurité alimentaire de demain.`,
                question: 'Et si on végétalisait un peu nos assiettes ?',
                initiatives: initiatives
            };
            break;
    }

    return {
        id: 'regime-alimentaire-trop-carne',
        idTheme: 'consommation',
        titre: {
            libelle: LIBELLE_TITRE_THEME,
            image: IMAGE_SELF_SERVICE
        },
        message: message,
        infographie: {
            nomComposant: 'InfographieImageMessage',
            informationsMethodologie: {
                libelleBouton: 'Chiffres',
                titre: "Surfaces agricoles nécessaires pour couvrir la consommation de la population, et part dédiée à l'alimentation des animaux d'élevage",
                descriptif: htmlstring`
                <p>L'évaluation des surfaces nécessaires pour couvrir la consommation de la population est fournie par <a href='https://parcel-app.org/'>PARCEL</a></p>
                <p>La part de ces surfaces dédiée à l'alimentation animale provient de l’étude Le revers de notre assiette, publiée par Solagro en 2019 (Figure 2 “Comparaison des empreintes surface de différents régimes” p.31). Pour simplifier, nous parlons de viande dans le diagnostic, mais la catégorie prise en compte englobe en fait tous les produits d’origine animale : tous types de viande (majoritaire), lait et produits laitiers, œufs, produits de la mer (minoritaire).</p>
            `
            },
            image: {
                url: '/images/constats/infographies/surfaces-alimentation-elevage-854x515.png',
                descriptionAlt: "Histogramme : 85% des surfaces cultivées sont dédiées à l'alimentation des animaux d'élevage",
                resolutionX: 854,
                resolutionY: 515,
                espacerImageEtMessage: true,
                largeurPrefereePx: 400
            },
            message: htmlstring`${formaterNombreSelonValeurString(donneesApi.diagnosticApi.productionsBesoins.besoinsAssietteActuelle.besoinsHa ?? 0)} hectares nécessaires pour nourrir les habitants, dont 85% pour l’élevage`,
            accroche: "Les vaches et les poulets d'abord !"
        }
    };
}

export function creerInfographieAssiette(): InfographieBaseModele {
    return {
        nomComposant: 'InfographieAssiette',
        informationsMethodologie: {
            titre: 'Assiette selon le régime alimentaire',
            descriptif: htmlstring`Nous avons repris les chiffres issus de l’étude ${creerLienExterneHtmlBrut('https://reseauactionclimat.org/wp-content/uploads/2024/02/rac_alimentation-synthese-08-webpage.pdf', 'Comment concilier nutrition et climat ?')} du Réseau Action Climat et de la Société Française de Nutrition. L’assiette proposée (« diète 1 » dans l’étude) présente notamment une baisse de 50 % pour la viande et la charcuterie. Elle répond aux recommandations nutritionnelles de la FAO et de l’OMS et permet d’améliorer notre santé tout en réduisant l’impact environnemental de notre alimentation.`,
            estPerimetreNational: true,
            libelleBouton: 'Chiffres'
        }
    };
}
