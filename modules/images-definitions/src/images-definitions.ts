// Fichier généré automatiquement par le script generer-definitions-images.ts
import type { ImageDefinition } from './ImageDefinition';

export const IMG0001: ImageDefinition = {
    id: 'img0001',
    url: '/images/galerie/img0001_panormama-ville-poitiers.jpg',
    descriptionAlt: `Vue du centre historique de Poitiers prise depuis le quartier des Dunes : église Sainte-Radegonde, cathédrale Saint-Pierre, palais de Justice.`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `Luca Aless, CC BY-SA 4.0 <https://creativecommons.org/licenses/by-sa/4.0>, via Wikimedia Commons`,
    source: `https://commons.wikimedia.org/wiki/File:Poitiers_panorama_01.jpg`
};

export const IMG0002: ImageDefinition = {
    id: 'img0002',
    url: '/images/galerie/img0002_place-mesirard-a-dreux.jpg',
    descriptionAlt: `Vue de la place Mésirard et de la Chapelle Royale de Dreux`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `Adrien Goussard, CC BY-SA 4.0 <https://creativecommons.org/licenses/by-sa/4.0>, via Wikimedia Commons`,
    source: `https://commons.wikimedia.org/wiki/File:Place_Mesirard_Dreux.jpg`
};

export const IMG0003: ImageDefinition = {
    id: 'img0003',
    url: '/images/galerie/img0003_vignoble-champagne.jpg',
    descriptionAlt: ` Vignoble de Champagne`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `FrDr, CC BY-SA 4.0 <https://creativecommons.org/licenses/by-sa/4.0>, via Wikimedia Commons`,
    source: `https://commons.wikimedia.org/wiki/File:Vignoble_de_Champagne_18.jpg`
};

export const IMG0004: ImageDefinition = {
    id: 'img0004',
    url: '/images/galerie/img0004_vue-artificialisation.jpg',
    descriptionAlt: `Vue d'ensemble du village`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `Budotradan, Public domain, via Wikimedia Commons`,
    source: `https://commons.wikimedia.org/wiki/File:VueEnsembleAssevent091106_(4).JPG`
};

export const IMG0005: ImageDefinition = {
    id: 'img0005',
    url: '/images/galerie/img0005_gaillon-vue-chateau.jpg',
    descriptionAlt: `La ville de Gaillon vue du château`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `I, Nitot, CC BY-SA 3.0 <http://creativecommons.org/licenses/by-sa/3.0/>, via Wikimedia Commons`,
    source: `https://commons.wikimedia.org/wiki/File:Gaillon-vu-du-chateau.jpg`
};

export const IMG0006: ImageDefinition = {
    id: 'img0006',
    url: '/images/galerie/img0006_sete-mont-st-clair.jpg',
    descriptionAlt: `Vue depuis Saint-Clair`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `Clemens Franz, CC BY-SA 3.0 <http://creativecommons.org/licenses/by-sa/3.0/>, via Wikimedia Commons`,
    source: `https://commons.wikimedia.org/wiki/File:Sete_von_Mont_St_Clair.jpg`
};

export const IMG0007: ImageDefinition = {
    id: 'img0007',
    url: '/images/galerie/img0007_panorama-ventabren-village.jpg',
    descriptionAlt: `Vue générale du village de Ventabren (Bouches-du-Rhône)`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `Allie Caulfield, CC BY 2.0 <https://creativecommons.org/licenses/by/2.0>, via Wikimedia Commons`,
    source: `https://commons.wikimedia.org/wiki/File:Ventabren_village.jpg`
};

export const IMG0008: ImageDefinition = {
    id: 'img0008',
    url: '/images/galerie/img0008_village-de-flayosc.jpg',
    descriptionAlt: `Vue du village de Flayosc depuis le chemin de la Colle`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `Jpchevreau, CC BY-SA 3.0 <https://creativecommons.org/licenses/by-sa/3.0>, via Wikimedia Commons`,
    source: `https://commons.wikimedia.org/wiki/File:Vue_du_village_de_Flayosc_depuis_le_chemin_de_la_Colle_(2).JPG`
};

export const IMG0009: ImageDefinition = {
    id: 'img0009',
    url: '/images/galerie/img0009_agriculture-claret.jpg',
    descriptionAlt: ` Agriculture à Claret`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `Alpes de Haute Provence, CC BY 2.0 <https://creativecommons.org/licenses/by/2.0>, via Wikimedia Commons`,
    source: `https://commons.wikimedia.org/wiki/File:Agriculture_%C3%A0_Claret.jpg`
};

export const IMG0010: ImageDefinition = {
    id: 'img0010',
    url: '/images/galerie/img0010_bocage-le-pin.jpg',
    descriptionAlt: `Paysage de bocage au Pin, Loire-Atlantique, France`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `Oie blanche, CC BY-SA 4.0 <https://creativecommons.org/licenses/by-sa/4.0>, via Wikimedia Commons`,
    source: `https://commons.wikimedia.org/wiki/File:Bocage_Le_Pin.jpg`
};

export const IMG0011: ImageDefinition = {
    id: 'img0011',
    url: '/images/galerie/img0011_manger-bio.jpg',
    descriptionAlt: `Panier fruits et légumes`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `Photo de <a href="https://unsplash.com/fr/@idelamaza?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Iñigo De la Maza</a> sur <a href="https://unsplash.com/fr/photos/legumes-et-fruits-s285sDw5Ikc?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Unsplash</a>`,
    source: `https://unsplash.com/fr/photos/legumes-et-fruits-s285sDw5Ikc`
};

export const IMG0012: ImageDefinition = {
    id: 'img0012',
    url: '/images/galerie/img0012_ferme-binage.jpg',
    descriptionAlt: `Tracteur qui bine un champ`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `Les fermes de Chassagne`,
    source: `https://www.lesfermesdechassagne.fr/`
};

export const IMG0013: ImageDefinition = {
    id: 'img0013',
    url: '/images/galerie/img0013_herriko-filiere-viande.jpg',
    descriptionAlt: `Vue de Herriko`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `https://herriko.fr/eu/`,
    source: `https://herriko.fr/eu/`
};

export const IMG0014: ImageDefinition = {
    id: 'img0014',
    url: '/images/galerie/img0014_transformation-lait.jpg',
    descriptionAlt: `Personnes qui discutent dans un atelier de traitement de lait`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `Agri71`,
    source: `https://www.agri71.fr/articles/08/04/2022/Clunisois-deux-outils-au-service-du-projet-alimentaire-de-territoire-70423/`
};

export const IMG0015: ImageDefinition = {
    id: 'img0015',
    url: '/images/galerie/img0015_champ-legumineuse.jpg',
    descriptionAlt: `Chemin bordant un champ de légumineuses`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `INRAE`,
    source: `https://revue-sesame-inrae.fr/`
};

export const IMG0016: ImageDefinition = {
    id: 'img0016',
    url: '/images/galerie/img0016_panier-legumes.jpg',
    descriptionAlt: `Paniers de légumes`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `PNR Perche`,
    source: `https://www.parc-naturel-perche.fr/actualites/le-projet-alimentaire-de-territoire-du-perche-officiellement-reconnu`
};

export const IMG0017: ImageDefinition = {
    id: 'img0017',
    url: '/images/galerie/img0017_illustration-grande-epicerie-generale.jpg',
    descriptionAlt: `Illustration fruits et légumes `,
    resolutionX: 800,
    resolutionY: 516,
    credit: `La Grande Epicerie Générale`,
    source: `https://www.grandeepiceriegenerale.fr/wp-content/themes/grandeepiceriegenerale/assets/images/bandeausuperieurlegumes-repeat.jpg`
};

export const IMG0018: ImageDefinition = {
    id: 'img0018',
    url: '/images/galerie/img0018_femme-camion-legumes.jpg',
    descriptionAlt: `Femme déchargeant un camion de légumes`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `Régie rurale - Banque des territoires`,
    source: `https://www.banquedesterritoires.fr/experience/le-pays-de-langres-livre-la-restauration-collective-et-developpe-lagriculture-de`
};

export const IMG0019: ImageDefinition = {
    id: 'img0019',
    url: '/images/galerie/img0019_panorama-feline-minervois.jpg',
    descriptionAlt: `Vue aérienne d'un village`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `Par Jcb-caz-11 — Travail personnel, CC BY-SA 4.0, https://commons.wikimedia.org/w/index.php?curid=87446614`,
    source: `https://fr.wikipedia.org/wiki/F%C3%A9lines-Minervois#/media/Fichier:F%C3%A9lines_1.jpg`
};

export const IMG0020: ImageDefinition = {
    id: 'img0020',
    url: '/images/galerie/img0020_agriculteur-champ-ble.jpg',
    descriptionAlt: `Dans un champs de blé, plan moyen d'un homme coiffé d'un chapeau de paille, penché sur les blés`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `Odyssée d’engrain`,
    source: `https://www.banquedesterritoires.fr/experience/du-ble-aux-pates-une-aventure-soutenue-par-la-communaute-de-communes-pays-de-trie-et-du`
};

export const IMG0021: ImageDefinition = {
    id: 'img0021',
    url: '/images/galerie/img0021_sac-ble-filiere-legumes-secs.jpg',
    descriptionAlt: `vue aérienne d'un village`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `Photo de <a href="https://unsplash.com/fr/@enginakyurt?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">engin akyurt</a> sur <a href="https://unsplash.com/fr/photos/un-sac-rempli-de-haricots-pose-sur-une-table-fLUwM3AtILk?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Unsplash</a>   `,
    source: `https://unsplash.com/fr/photos/un-sac-rempli-de-haricots-pose-sur-une-table-fLUwM3AtILk`
};

export const IMG0022: ImageDefinition = {
    id: 'img0022',
    url: '/images/galerie/img0022_pain-rustique.jpg',
    descriptionAlt: `Miche de pain posée sur un linge`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `Photo de <a href="https://unsplash.com/fr/@maverick2001?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Mathieu LESNIAK</a> sur <a href="https://unsplash.com/fr/photos/une-miche-de-pain-posee-sur-un-linge-rf92VaW_UqM?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Unsplash</a>   `,
    source: `https://unsplash.com/fr/photos/une-miche-de-pain-posee-sur-un-linge-rf92VaW_UqM`
};

export const IMG0023: ImageDefinition = {
    id: 'img0023',
    url: '/images/galerie/img0023_porte-monaie.jpg',
    descriptionAlt: `Personne qui paye au marché`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `INPACTPC`,
    source: `https://www.inpactpc.org/toutes-les-actualites/831-la-ssa-vous-connaissez`
};

export const IMG0024: ImageDefinition = {
    id: 'img0024',
    url: '/images/galerie/img0024_carte-securite-sociale-alimentation-montpellier.jpg',
    descriptionAlt: `Carte Sécurité Sociale Alimentation`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `SSA`,
    source: `https://securite-sociale-alimentation.org/`
};

export const IMG0025: ImageDefinition = {
    id: 'img0025',
    url: '/images/galerie/img0025_deux-mains-tenant-un-bol_de-tomates.jpg',
    descriptionAlt: `Echange de légumes`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `Ecasap - Istockphoto`,
    source: `https://www.istockphoto.com/fr/photo/%C3%A9change-de-l%C3%A9gumes-gm526720720-92639137`
};

export const IMG0026: ImageDefinition = {
    id: 'img0026',
    url: '/images/galerie/img0026_logo-securite-sociale-alimentation-au-maquis.jpg',
    descriptionAlt: `Logo Au Maquis`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `Logo Au Maquis`,
    source: `https://securite-sociale-alimentation.org/initiative/au-maquis/`
};

export const IMG0027: ImageDefinition = {
    id: 'img0027',
    url: '/images/galerie/img0027_logo-vrac.jpg',
    descriptionAlt: `Logo VRAC`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `VRAC`,
    source: `https://vrac-asso.org/espace-presse/`
};

export const IMG0028: ImageDefinition = {
    id: 'img0028',
    url: '/images/galerie/img0028_legumes-sur-table.jpg',
    descriptionAlt: `Légumes verts sur table en bois`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `Photo de <a href="https://unsplash.com/fr/@gadgapho?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Gareth Hubbard</a> sur <a href="https://unsplash.com/fr/photos/legumes-verts-sur-table-en-bois-marron-qPcSUERqBAc?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Unsplash</a>   `,
    source: `https://unsplash.com/fr/photos/legumes-verts-sur-table-en-bois-marron-qPcSUERqBAc`
};

export const IMG0029: ImageDefinition = {
    id: 'img0029',
    url: '/images/galerie/img0029_cantine-scolaire-vegetalisation-alimentation.jpg',
    descriptionAlt: `Assiette de carottes rapées`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `Foto de <a href="https://unsplash.com/es/@umar3961?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Muhammad umar hayat Yahya</a> en <a href="https://unsplash.com/es/fotos/un-primer-plano-de-un-plato-de-comida-en-una-mesa-CFeZHVB5Yuc?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Unsplash</a>   `,
    source: `https://unsplash.com/es/fotos/un-primer-plano-de-un-plato-de-comida-en-una-mesa-CFeZHVB5Yuc`
};

export const IMG0030: ImageDefinition = {
    id: 'img0030',
    url: '/images/galerie/img0030_plateaux-repas-restauration-scolaire.jpg',
    descriptionAlt: `Plateau de cantine scolaire`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `Département Bouches du Rhône`,
    source: `https://www.departement13.fr/manger-autrement/`
};

export const IMG0031: ImageDefinition = {
    id: 'img0031',
    url: '/images/galerie/img0031_cantine-college-enfants.jpg',
    descriptionAlt: `Enfant au slef avec son plateau`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `Ville de Lyon`,
    source: `https://mairie1.lyon.fr/enfance-et-education/education/la-restauration-scolaire`
};

export const IMG0032: ImageDefinition = {
    id: 'img0032',
    url: '/images/galerie/img0032_assiette-repas-vegetarien.jpg',
    descriptionAlt: `Plat végétarien`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `Gretchens Koblenz`,
    source: `https://gretchens-koblenz.de/`
};

export const IMG0033: ImageDefinition = {
    id: 'img0033',
    url: '/images/galerie/img0033_enfants-cantine-scolaire.jpg',
    descriptionAlt: `Plateau de cantine scolaire`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `Erwan Floch`,
    source: `https://saintehelenesurmer.bzh/questionnaire-sur-la-restauration-scolaire/`
};

export const IMG0034: ImageDefinition = {
    id: 'img0034',
    url: '/images/galerie/img0034_caisses-legumes-potager.jpg',
    descriptionAlt: `Caisse de légumes au pied d'un arbre`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `Saint Junien`,
    source: `https://www.saint-junien.fr/wp-content/uploads/2023/03/BONJOUR-38.pdf`
};

export const IMG0035: ImageDefinition = {
    id: 'img0035',
    url: '/images/galerie/img0035_preparation-cuisine-mouans-sartoux.jpg',
    descriptionAlt: `Cantinières à Mouans Sartoux`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `Mouans Sartoux`,
    source: `https://mead-mouans-sartoux.fr/wp-content/uploads/2017/08/Manger-bio-cuisine.jpg`
};

export const IMG0036: ImageDefinition = {
    id: 'img0036',
    url: '/images/galerie/img0036_plateaux-cantine-scolaire.jpg',
    descriptionAlt: `Cantine scolaire avec enfants qui mangent`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `Istockphoto - Lordn`,
    source: `https://www.istockphoto.com/fr/photo/enfants-mangeant-une-collation-aux-fruits-dans-un-jardin-denfants-gm1463491901-496488664`
};

export const IMG0037: ImageDefinition = {
    id: 'img0037',
    url: '/images/galerie/img0037_village-alloue.jpg',
    descriptionAlt: `Vue du Village Alloué`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `Par Jack ma — Travail personnel, CC BY-SA 3.0, https://commons.wikimedia.org/w/index.php?curid=11731449`,
    source: `https://fr.wikipedia.org/wiki/Alloue#/media/Fichier:Alloue_eg1.JPG`
};

export const IMG0038: ImageDefinition = {
    id: 'img0038',
    url: '/images/galerie/img0038_haricots_verts.jpg',
    descriptionAlt: `Haricots verts`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `CEDD`,
    source: `https://www.cerdd.org/Parcours-thematiques/Alimentation-durable/Ressources-Alimentation-Durable/Dossier-l-agriculture-un-projet-de-territoire`
};

export const IMG0039: ImageDefinition = {
    id: 'img0039',
    url: '/images/galerie/img0039_vue-aerienne-champs.jpg',
    descriptionAlt: `Vue aérienne de champs cultivés`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `Terre de Liens`,
    source: `https://ressources.terredeliens.org/les-ressources/le-fonds-d-intervention-fonciere-du-val-de-drome-26`
};

export const IMG0040: ImageDefinition = {
    id: 'img0040',
    url: '/images/galerie/img0040_collectif-agriculteurs.jpg',
    descriptionAlt: `Collectif d'agriculteurs`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `Terre de Liens`,
    source: `https://ressources.terredeliens.org/images/1009/collectif-ferme5sensresized.jpg`
};

export const IMG0041: ImageDefinition = {
    id: 'img0041',
    url: '/images/galerie/img0041_semis-en-gros-plan.jpg',
    descriptionAlt: `Main qui fait des semis`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `Photo de <a href="https://unsplash.com/fr/@dirtjoy?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Zoe Schaeffer</a> sur <a href="https://unsplash.com/fr/photos/plante-verte-sur-la-main-des-personnes-tbiV-yc903g?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Unsplash</a>   `,
    source: `https://unsplash.com/fr/photos/plante-verte-sur-la-main-des-personnes-tbiV-yc903g`
};

export const IMG0042: ImageDefinition = {
    id: 'img0042',
    url: '/images/galerie/img0042_plairie-champ-romorantin.jpg',
    descriptionAlt: `Chemin au milieu d'un champ avec haie`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `Terre de Liens`,
    source: `https://ressources.terredeliens.org/images/1197/romorantin.jpg`
};

export const IMG0043: ImageDefinition = {
    id: 'img0043',
    url: '/images/galerie/img0043_tournesol.jpg',
    descriptionAlt: `Tournesol en gros plan `,
    resolutionX: 800,
    resolutionY: 516,
    credit: `Terre de Liens`,
    source: `https://ressources.terredeliens.org/images/1660/9f425e74-11db-485a-9ea2-814b5bc7696f.JPG`
};

export const IMG0044: ImageDefinition = {
    id: 'img0044',
    url: '/images/galerie/img0044_logo-fonciere-elementaire.jpg',
    descriptionAlt: `Logo La foncière Elementaire`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `La Foncière Elementaire`,
    source: `https://fonciere-elementaire.fr/`
};

export const IMG0045: ImageDefinition = {
    id: 'img0045',
    url: '/images/galerie/img0045_vue-aerienne-village-avec-riviere.jpg',
    descriptionAlt: `Vue aérienne d'un village avec rivière`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `adaseadoc - Terre de Liens`,
    source: `https://ressources.terredeliens.org/recolte/douelle`
};

export const IMG0046: ImageDefinition = {
    id: 'img0046',
    url: '/images/galerie/img0046_deux-mains-qui-plantent-des-semis.jpg',
    descriptionAlt: `Mains qui font des semis`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `Photo de <a href="https://unsplash.com/fr/@honeypoppet?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Sandie Clarke</a> sur <a href="https://unsplash.com/fr/photos/personne-tenant-une-grenouille-brune-et-noire-q13Zq1Jufks?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Unsplash</a>   `,
    source: `https://unsplash.com/fr/photos/personne-tenant-une-grenouille-brune-et-noire-q13Zq1Jufks`
};

export const IMG0047: ImageDefinition = {
    id: 'img0047',
    url: '/images/galerie/img0047_femme-semis-dans-une-serre.jpg',
    descriptionAlt: `Femme qui fait des semis`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `Photo de <a href="https://unsplash.com/fr/@dirtjoy?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Zoe Schaeffer</a> sur <a href="https://unsplash.com/fr/photos/une-femme-travaillant-dans-une-serre-m1eKjNDaf-4?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Unsplash</a>   `,
    source: `https://unsplash.com/fr/photos/une-femme-travaillant-dans-une-serre-m1eKjNDaf-4`
};

export const IMG0048: ImageDefinition = {
    id: 'img0048',
    url: '/images/galerie/img0048_femmes-caisse-legumes.jpg',
    descriptionAlt: `Femmes qui s'échangent une caisse d'aubergines`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `Pexels`,
    source: `https://www.pexels.com`
};

export const IMG0049: ImageDefinition = {
    id: 'img0049',
    url: '/images/galerie/img0049_femme-dans-une-serre.jpg',
    descriptionAlt: `Femme dans une serre`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `Photo de <a href="https://unsplash.com/fr/@fraenkly?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Frank Holleman</a> sur <a href="https://unsplash.com/fr/photos/homme-en-veste-bleue-debout-dans-des-plantes-vertes-coK192IU868?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Unsplash</a>   `,
    source: `https://unsplash.com/fr/photos/homme-en-veste-bleue-debout-dans-des-plantes-vertes-coK192IU868`
};

export const IMG0050: ImageDefinition = {
    id: 'img0050',
    url: '/images/galerie/img0050_champ-herbe.jpg',
    descriptionAlt: `Champ ensoleillé`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `Photo de <a href="https://unsplash.com/fr/@da_shika_photo?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Da-shika</a> sur <a href="https://unsplash.com/fr/photos/un-champ-dherbe-avec-des-arbres-en-arriere-plan-DlAy59VlqKs?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Unsplash</a>   `,
    source: `https://unsplash.com/fr/photos/un-champ-dherbe-avec-des-arbres-en-arriere-plan-DlAy59VlqKs`
};

export const IMG0051: ImageDefinition = {
    id: 'img0051',
    url: '/images/galerie/img0051_prairie-trefles.jpg',
    descriptionAlt: `Champ de trèfles`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `Territoires Bio`,
    source: `https://territoiresbio.fr/wp-content/uploads/2020/02/prairies-PPR1-2.jpg`
};

export const IMG0052: ImageDefinition = {
    id: 'img0052',
    url: '/images/galerie/img0052_ruisseau-sauvage.jpg',
    descriptionAlt: `Ruisseau verdoyant`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `Photo de <a href="https://unsplash.com/fr/@archange1michael?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Mihail Ilchov</a> sur <a href="https://unsplash.com/fr/photos/un-ruisseau-qui-traverse-une-foret-verdoyante-LVkMJaSz0t8?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Unsplash</a>   `,
    source: `https://unsplash.com/fr/photos/un-ruisseau-qui-traverse-une-foret-verdoyante-LVkMJaSz0t8`
};

export const IMG0053: ImageDefinition = {
    id: 'img0053',
    url: '/images/galerie/img0053_champ-chemin-foret-arriere-plan.jpg',
    descriptionAlt: `Champ avec forêt en arrière plan`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `Banque des territoires`,
    source: `https://www.banquedesterritoires.fr/sites/default/files/ra/19.jpg`
};

export const IMG0054: ImageDefinition = {
    id: 'img0054',
    url: '/images/galerie/img0054_champ-de-ble-botte-de-paille.jpg',
    descriptionAlt: `Champ de blé avec des bottes de paille`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `Photo de <a href="https://unsplash.com/fr/@martin_sepion?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Martin Sepion</a> sur <a href="https://unsplash.com/fr/photos/un-champ-de-foin-avec-des-balles-de-foin-au-premier-plan-LE84KaKYGUk?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Unsplash</a>   `,
    source: `https://unsplash.com/fr/photos/un-champ-de-foin-avec-des-balles-de-foin-au-premier-plan-LE84KaKYGUk`
};

export const IMG0055: ImageDefinition = {
    id: 'img0055',
    url: '/images/galerie/img0055_champ-haie-avec-chevre-arriere-plan.jpg',
    descriptionAlt: `Champ au milieu d'une forêt avec une chèvre en arrière plan`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `Territoires Bio`,
    source: `https://territoiresbio.fr/wp-content/uploads/2020/02/DSC05540.jpg`
};

export const IMG0056: ImageDefinition = {
    id: 'img0056',
    url: '/images/galerie/img0056_champ-et-foret.jpg',
    descriptionAlt: `Champ au milieu des forêts`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `Territoires Bio`,
    source: `https://territoiresbio.fr/wp-content/uploads/2020/02/Eau-de-Paris-_068.jpg`
};

export const IMG0057: ImageDefinition = {
    id: 'img0057',
    url: '/images/galerie/img0057_champ-ensoleille-avec-des-vaches.jpg',
    descriptionAlt: `Champ ensoleillé avec des vaches`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `Territoires Bio`,
    source: `https://territoiresbio.fr/wp-content/uploads/2022/02/Image4.jpg`
};

export const IMG0058: ImageDefinition = {
    id: 'img0058',
    url: '/images/galerie/img0058_champ-brumeux-avec-des-vaches.jpg',
    descriptionAlt: `Champ brumeux avec des vaches`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `Photo de <a href="https://unsplash.com/fr/@lucasgallone?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Lucas Gallone</a> sur <a href="https://unsplash.com/fr/photos/troupeau-de-vaches-sur-lherbe-Q4QjAPMpJRQ?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Unsplash</a>   `,
    source: `https://unsplash.com/fr/photos/troupeau-de-vaches-sur-lherbe-Q4QjAPMpJRQ`
};

export const IMG0059: ImageDefinition = {
    id: 'img0059',
    url: '/images/galerie/img0059_haie-champ.jpg',
    descriptionAlt: `Haie au milieu d'un champ verdoyant`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `Photo de <a href="https://unsplash.com/fr/@anniespratt?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Annie Spratt</a> sur <a href="https://unsplash.com/fr/photos/un-chemin-etroit-au-milieu-dun-champ-verdoyant-u5J2dy1uhY8?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Unsplash</a>   `,
    source: `https://unsplash.com/fr/photos/un-chemin-etroit-au-milieu-dun-champ-verdoyant-u5J2dy1uhY8`
};

export const IMG0060: ImageDefinition = {
    id: 'img0060',
    url: '/images/galerie/img0060_ruisseau-foret.jpg',
    descriptionAlt: `Ruisseau dans une forêt`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `Photo de <a href="https://unsplash.com/fr/@lucabravo?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Luca Bravo</a> sur <a href="https://unsplash.com/fr/photos/photographie-timelapse-dune-riviere-entouree-darbres-IfsvDZBSeWc?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Unsplash</a>   `,
    source: `https://unsplash.com/fr/photos/photographie-timelapse-dune-riviere-entouree-darbres-IfsvDZBSeWc`
};

export const IMG0061: ImageDefinition = {
    id: 'img0061',
    url: '/images/galerie/img0061_logo-villages-vivants.jpg',
    descriptionAlt: `Logo Village Vivants`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `Village Vivants`,
    source: `https://villagesvivants.com/`
};

export const IMG0062: ImageDefinition = {
    id: 'img0062',
    url: '/images/galerie/img0062_camion-epicerie-ambulante.jpg',
    descriptionAlt: `Camion de l'épicerie de Sandrine`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `Epicerie d'Sandrine`,
    source: `https://www.facebook.com/profile.php?id=61551718027733`
};

export const IMG0063: ImageDefinition = {
    id: 'img0063',
    url: '/images/galerie/img0063_logo-bouge-ton-coq.jpg',
    descriptionAlt: `Logo Bouge ton coq`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `Bouge ton coq`,
    source: `https://bougetoncoq.fr/`
};

export const IMG0065: ImageDefinition = {
    id: 'img0065',
    url: '/images/galerie/img0065_illustration-maraichage-communal-cugnaux.jpg',
    descriptionAlt: `Illustration Maraichage communal`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `Ville de Cugnaux`,
    source: `https://ville-cugnaux.fr/grands-projets/grands-projets-maraichage/`
};

export const IMG0070: ImageDefinition = {
    id: 'img0070',
    url: '/images/galerie/img0070_semis-trefle-agroecologie.jpg',
    descriptionAlt: `Champ de trèfles`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `Par Arn — Travail personnel, CC BY-SA 4.0, https://commons.wikimedia.org/w/index.php?curid=55536266`,
    source: `https://fr.wikipedia.org/wiki/Trifolium_resupinatum#/media/Fichier:Trifolium_resupinatum_4.jpg`
};

export const IMG0075: ImageDefinition = {
    id: 'img0075',
    url: '/images/galerie/img0075_champ-paillage.jpg',
    descriptionAlt: `Champ avec du paillage`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `Osez Agroécologie`,
    source: `https://osez-agroecologie.org/joubert-systeme`
};

export const IMG0076: ImageDefinition = {
    id: 'img0076',
    url: '/images/galerie/img0076_point-de-collecte-urine-ocapi.jpg',
    descriptionAlt: `Femme qui transporte un bidon d'urine`,
    resolutionX: 800,
    resolutionY: 516,
    credit: `Ocapi`,
    source: `https://usbeketrica.com/fr/article/on-a-visite-le-tout-premier-point-d-apport-d-urine-de-france#lg=1&slide=1`
};

export const IMG0077: ImageDefinition = {
    id: 'img0077',
    url: '/images/galerie/img0077_capture-ecran-accueil-territoires-fertiles.png',
    descriptionAlt: `Capture écran page d'accueil TerritoiresFertiles.fr`,
    resolutionX: 2248,
    resolutionY: 1292,
    credit: `territoiresfertiles.fr`,
    source: `territoiresfertiles.fr`
};

export const IMG0078: ImageDefinition = {
    id: 'img0078',
    url: '/images/galerie/img0078_apercu-diagnostic-flash.png',
    descriptionAlt: `Aperçu diagnostic flash`,
    resolutionX: 1158,
    resolutionY: 692,
    credit: `undefined`,
    source: `undefined`
};

export const IMG0079: ImageDefinition = {
    id: 'img0079',
    url: '/images/galerie/img0079_apercu-livret4pages.png',
    descriptionAlt: `Aperçu livret 4 pages`,
    resolutionX: 894,
    resolutionY: 1252,
    credit: `undefined`,
    source: `undefined`
};

export const IMG0080: ImageDefinition = {
    id: 'img0080',
    url: '/images/galerie/img0080_couverture-rapport-terre-de-liens.jpg',
    descriptionAlt: `Couverture rapport Terre de Liens`,
    resolutionX: 1000,
    resolutionY: 1334,
    credit: `Terre de Liens`,
    source: `undefined`
};
