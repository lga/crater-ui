import { arrondirANDecimales, formaterNombreEnNDecimalesString } from '@lga/base';
import type { Territoire } from '@lga/territoires';

import type { Note } from '../Note';
import { MessageEvolutionPesticides } from './MessageEvolutionPesticides';
import { calculerMessageSynthesePesticides } from './messages-pesticides';

export class Pesticides {
    constructor(
        private territoire: Territoire,
        public readonly note: Note,
        public readonly indicateursParAnnees: IndicateursPesticidesParAnnees
    ) {}

    get messageSynthese() {
        return calculerMessageSynthesePesticides(this.indicateursParAnnees.anneeFinale.noduNormalise);
    }

    get messageQSANodu() {
        const indicateursAnneeInitiale = this.indicateursParAnnees.anneeInitiale;
        const indicateursAnneeFinale = this.indicateursParAnnees.anneeFinale;
        return MessageEvolutionPesticides.construireMessage(
            this.territoire.nom,
            indicateursAnneeInitiale.annee,
            indicateursAnneeFinale.annee,
            indicateursAnneeInitiale.quantiteSubstanceTotaleKg,
            indicateursAnneeFinale.quantiteSubstanceTotaleKg,
            indicateursAnneeInitiale.noduHa,
            indicateursAnneeFinale.noduHa
        );
    }

    get qsaTotalAnneeFinale() {
        const q = this.indicateursParAnnees.anneeFinale.quantiteSubstanceTotaleKg;
        if (q === null) {
            return '- kg';
        } else if (q > 1e6) {
            return formaterNombreEnNDecimalesString(q / 1000, 0) + ' t';
        } else {
            return formaterNombreEnNDecimalesString(q, 0) + ' kg';
        }
    }

    get evolutionNodu() {
        if (this.indicateursParAnnees.anneeFinale.noduHa === null || this.indicateursParAnnees.anneeInitiale.noduHa === null) return null;

        return (this.indicateursParAnnees.anneeFinale.noduHa / this.indicateursParAnnees.anneeInitiale.noduHa - 1) * 100;
    }
}

export class IndicateursPesticidesParAnnees {
    private listeIndicateursParAnnees: IndicateursPesticidesAnneeN[] = [];

    constructor(indicateursPesticidesParAnnees: IndicateursPesticidesAnneeN[]) {
        this.listeIndicateursParAnnees = indicateursPesticidesParAnnees;
        this.listeIndicateursParAnnees.sort((ia1, ia2) => {
            return ia1.annee - ia2.annee;
        });
    }

    get annees(): number[] {
        return this.listeIndicateursParAnnees.map((ia) => ia.annee);
    }

    get quantitesSubstancesSansDUKg(): (number | null)[] {
        return this.listeIndicateursParAnnees.map((ia) => ia.quantiteSubstanceSansDUKg);
    }

    get quantitesSubstancesAvecDUKg(): (number | null)[] {
        return this.listeIndicateursParAnnees.map((ia) => ia.quantiteSubstanceAvecDUKg);
    }

    get nodusHa(): (number | null)[] {
        return this.listeIndicateursParAnnees.map((ia) => ia.noduHa);
    }

    get nodusNormalises(): (number | null)[] {
        return this.listeIndicateursParAnnees.map((ia) => ia.noduNormalise);
    }

    get anneeInitiale(): IndicateursPesticidesAnneeN {
        return this.listeIndicateursParAnnees.slice(0)[0];
    }

    get anneeFinale(): IndicateursPesticidesAnneeN {
        return this.listeIndicateursParAnnees.slice(-1)[0];
    }

    get quantitesSubstancesTotalesKg(): (number | null)[] {
        return this.listeIndicateursParAnnees.map((ia) => ia.quantiteSubstanceTotaleKg);
    }
}

export class IndicateursPesticidesAnneeN {
    constructor(
        public annee: number,
        public quantiteSubstanceSansDUKg: number | null,
        public quantiteSubstanceAvecDUKg: number | null,
        public noduHa: number | null,
        public noduNormalise: number | null
    ) {}

    get quantiteSubstanceTotaleKg(): number | null {
        if (this.quantiteSubstanceSansDUKg && this.quantiteSubstanceAvecDUKg) {
            return arrondirANDecimales(this.quantiteSubstanceSansDUKg + this.quantiteSubstanceAvecDUKg, 2);
        } else {
            return null;
        }
    }
}
