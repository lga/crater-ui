import './TemplatePageFormulaire.js';

import { html, LitElement } from 'lit';
import { customElement } from 'lit/decorators.js';

declare global {
    interface HTMLElementTagNameMap {
        'c-page-contact': PageContact;
    }
}

@customElement('c-page-contact')
export class PageContact extends LitElement {
    render() {
        return html`
            <c-template-page-formulaire
                titre="Formulaire de contact"
                hauteur="700px"
                lienFormulaire="https://resiliencealimentaire.org/formulaire-crater/"
            >
            </c-template-page-formulaire>
        `;
    }
}
