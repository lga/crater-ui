import type { CodeProfilDiagnosticApi } from '@lga/specification-api';
import { StyleCarteChorochromatique } from '@lga/styles-carte';

interface ClasseProfilDiagnostic {
    code: CodeProfilDiagnosticApi;
    libelle: string;
    couleur: string;
}

const CLASSES_PROFILS: ClasseProfilDiagnostic[] = [
    {
        code: 'URBAIN',
        libelle: 'Urbain',
        couleur: '#3047db'
    },
    {
        code: 'VITICULTURE',
        libelle: 'Viticulture',
        couleur: '#902ee6'
    },
    {
        code: 'GRANDES_CULTURES',
        libelle: 'Grandes cultures',
        couleur: '#e6ae2e'
    },
    {
        code: 'FRUITS_ET_LEGUMES',
        libelle: 'Fruits et légumes',
        couleur: '#179344'
    },
    {
        code: 'ELEVAGE_INTENSIF',
        libelle: 'Élevage intensif',
        couleur: '#6b3615'
    },
    {
        code: 'ELEVAGE_EXTENSIF',
        libelle: 'Élevage extensif',
        couleur: '#e6762e'
    },
    {
        code: 'DEFAUT',
        libelle: 'Défaut',
        couleur: '#a39b9b'
    }
];

export const styleCarteProfilsDiagnostics = new StyleCarteChorochromatique({
    couleurContours: 'grey',
    classes: CLASSES_PROFILS
});
