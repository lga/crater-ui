import { fetchRetry } from '@lga/base';
import type {
    ArretesSecheresseAnneeMoisNApi,
    BesoinsParCultureApi,
    BesoinsParGroupeCultureApi,
    CheptelApi,
    DiagnosticApi,
    IndicateursParTypeCommerceApi,
    IrrigationIndicateursAnneeNApi,
    PesticidesIndicateursAnneeNApi,
    PosteEnergieApi,
    PratiquesIrrigationCultureApi,
    SauParCultureApi,
    SauParGroupeCultureApi,
    SourceEnergieApi
} from '@lga/specification-api';
import type { HierarchieTerritoires, Territoire } from '@lga/territoires';

import {
    AgriculteursExploitations,
    CategorieCommerce,
    ClassesAges,
    ClassesSuperficies,
    Consommation,
    ConsommationParCulture,
    ConsommationParGroupeCulture,
    Diagnostic,
    IndicateurConsommation,
    IndicateurHvn,
    IndicateursProximiteCommerces,
    PolitiqueAmenagement,
    Production,
    SauParCulture,
    SauParGroupeCulture,
    SurfaceAgricoleUtile,
    TerresAgricoles,
    TransformationDistribution
} from '../modeles/diagnostics';
import { Intrants } from '../modeles/diagnostics';
import { Cheptel, Cheptels } from '../modeles/diagnostics/Cheptels.js';
import { Eau } from '../modeles/diagnostics/eau/Eau';
import { Irrigation, type IrrigationIndicateursAnneeN } from '../modeles/diagnostics/eau/Irrigation';
import { PratiquesIrrigation, type PratiquesIrrigationCultureParametres } from '../modeles/diagnostics/eau/PratiquesIrrigation';
import { Secheresse, type SecheresseAnneeMoisN } from '../modeles/diagnostics/eau/Secheresse';
import { Energie, PosteEnergie, SourceEnergie } from '../modeles/diagnostics/energie/Energie.js';
import { IndicateursParAnnees } from '../modeles/diagnostics/IndicateursParAnnees';
import { IndicateursParAnneesMois } from '../modeles/diagnostics/IndicateursParAnneesMois';
import { Note, NOTE_VALEUR_WARNING } from '../modeles/diagnostics/Note';
import { OccupationSols } from '../modeles/diagnostics/OccupationSols.js';
import { Otex } from '../modeles/diagnostics/Otex';
import { IndicateursPesticidesAnneeN, IndicateursPesticidesParAnnees, Pesticides } from '../modeles/diagnostics/pesticides';

export function chargerDiagnostics(apiBaseUrl: string, hierarchieTerritoires: HierarchieTerritoires): Promise<unknown[]> {
    return Promise.all(hierarchieTerritoires.getListeTerritoires().map((t) => chargerDiagnostic(apiBaseUrl, t.id)));
}

export function chargerDiagnostic(apiBaseUrl: string, idTerritoire: string) {
    return fetchRetry(apiBaseUrl + '/crater/api/diagnostics/' + idTerritoire).then((response) => response.json());
}

export function chargerDiagnosticCsv(apiBaseUrl: string, idTerritoire: string) {
    window.location.href = apiBaseUrl + '/crater/api/diagnostics/csv/' + idTerritoire;
}

export function traduireReponseApiEnDiagnostic(territoire: Territoire, diagnosticApi: DiagnosticApi): Diagnostic {
    const occupationSols = new OccupationSols(
        diagnosticApi.occupationSols.superficieTotaleHa,
        diagnosticApi.occupationSols.superficieArtificialiseeClcHa,
        diagnosticApi.occupationSols.superficieAgricoleClcHa,
        diagnosticApi.occupationSols.superficieNaturelleOuForestiereClcHa,
        diagnosticApi.occupationSols.superficieAgricoleRA2020Ha
    );
    const surfaceAgricoleUtile = new SurfaceAgricoleUtile(
        diagnosticApi.surfaceAgricoleUtile.sauTotaleHa,
        diagnosticApi.surfaceAgricoleUtile.sauPeuProductiveHa,
        diagnosticApi.surfaceAgricoleUtile.sauProductiveHa,
        diagnosticApi.surfaceAgricoleUtile.sauBioHa,
        creerListeSauParGroupeCulture(diagnosticApi.surfaceAgricoleUtile.sauParGroupeCulture)
    );

    return new Diagnostic(
        territoire,
        diagnosticApi.population,
        diagnosticApi.densitePopulationHabParKm2,
        diagnosticApi.idTerritoireParcel,
        diagnosticApi.nbCommunes,
        new Otex(diagnosticApi.nbCommunesParOtex, diagnosticApi.codeOtexMajoritaire5Postes),
        occupationSols,
        surfaceAgricoleUtile,
        new Cheptels(creerListeCheptels(diagnosticApi.cheptels)),
        diagnosticApi.ugbParHa,
        new TerresAgricoles(
            territoire,
            new Note(diagnosticApi.politiqueFonciere.note),
            diagnosticApi.occupationSols.superficieTotaleHa,
            diagnosticApi.politiqueFonciere.artificialisation5ansHa,
            diagnosticApi.politiqueFonciere.evolutionMenagesEmplois5ans,
            diagnosticApi.politiqueFonciere.sauParHabitantM2,
            diagnosticApi.productionsBesoins.besoinsAssietteActuelle.besoinsHa,
            diagnosticApi.productionsBesoins.besoinsAssietteDemitarienne.besoinsHa,
            diagnosticApi.population,
            diagnosticApi.politiqueFonciere.rythmeArtificialisationSauPourcent,
            PolitiqueAmenagement.fromString(diagnosticApi.politiqueFonciere.evaluationPolitiqueAmenagement),
            diagnosticApi.politiqueFonciere.partLogementsVacants2013Pourcent,
            diagnosticApi.politiqueFonciere.partLogementsVacants2018Pourcent
        ),
        new AgriculteursExploitations(
            territoire,
            new Note(diagnosticApi.populationAgricole.note),
            diagnosticApi.populationAgricole.populationAgricole1988,
            diagnosticApi.populationAgricole.populationAgricole2010,
            diagnosticApi.populationAgricole.partPopulationAgricole1988Pourcent,
            diagnosticApi.populationAgricole.partPopulationAgricole2010Pourcent,
            diagnosticApi.populationAgricole.nbExploitations1988,
            diagnosticApi.populationAgricole.nbExploitations2010,
            diagnosticApi.populationAgricole.sauHa1988,
            diagnosticApi.populationAgricole.sauHa2010,
            diagnosticApi.populationAgricole.sauMoyenneParExploitationHa1988,
            diagnosticApi.populationAgricole.sauMoyenneParExploitationHa2010,
            new ClassesAges(
                diagnosticApi.populationAgricole.nbExploitationsParClassesAgesChefExploitation.moins_40_ans,
                diagnosticApi.populationAgricole.nbExploitationsParClassesAgesChefExploitation.de_40_a_49_ans,
                diagnosticApi.populationAgricole.nbExploitationsParClassesAgesChefExploitation.de_50_a_59_ans,
                diagnosticApi.populationAgricole.nbExploitationsParClassesAgesChefExploitation.plus_60_ans
            ),
            new ClassesSuperficies(
                diagnosticApi.populationAgricole.nbExploitationsParClassesSuperficies.moins_20_ha,
                diagnosticApi.populationAgricole.nbExploitationsParClassesSuperficies.de_20_a_50_ha,
                diagnosticApi.populationAgricole.nbExploitationsParClassesSuperficies.de_50_a_100_ha,
                diagnosticApi.populationAgricole.nbExploitationsParClassesSuperficies.de_100_a_200_ha,
                diagnosticApi.populationAgricole.nbExploitationsParClassesSuperficies.plus_200_ha
            ),
            new ClassesSuperficies(
                diagnosticApi.populationAgricole.sauHaParClassesSuperficies.moins_20_ha,
                diagnosticApi.populationAgricole.sauHaParClassesSuperficies.de_20_a_50_ha,
                diagnosticApi.populationAgricole.sauHaParClassesSuperficies.de_50_a_100_ha,
                diagnosticApi.populationAgricole.sauHaParClassesSuperficies.de_100_a_200_ha,
                diagnosticApi.populationAgricole.sauHaParClassesSuperficies.plus_200_ha
            )
        ),
        new Intrants(
            territoire,
            new Note(diagnosticApi.intrants.note),
            new Energie(
                territoire,
                new Note(diagnosticApi.energie.note),
                diagnosticApi.energie.codePostePrincipal,
                diagnosticApi.energie.energiePrimaireGJ,
                diagnosticApi.energie.energiePrimaireGJParHa,
                creerListePostesEnergie(diagnosticApi.energie.postes)
            ),
            new Eau(
                territoire,
                new Note(diagnosticApi.eau.note),
                new Irrigation(
                    territoire,
                    new Note(diagnosticApi.eau.irrigation.note),
                    diagnosticApi.eau.irrigation.irrigationM3,
                    diagnosticApi.eau.irrigation.irrigationMM,
                    creerListeIrrigationIndicateursAnneeN(diagnosticApi.eau.irrigation.indicateursParAnnees),
                    new PratiquesIrrigation(creerListePratiquesIrrigationParCulture(diagnosticApi.eau.irrigation.pratiquesIrrigationParCultures))
                ),
                new Secheresse(
                    territoire,
                    new Note(diagnosticApi.eau.arretesSecheresse.note),
                    diagnosticApi.eau.arretesSecheresse.tauxImpactArretesSecheressePourcent,
                    creerListeSecheresseIndicateursAnneeMoisN(diagnosticApi.eau.arretesSecheresse.indicateursParAnneesMois)
                )
            ),
            new Pesticides(
                territoire,
                new Note(diagnosticApi.pesticides.note),
                new IndicateursPesticidesParAnnees(creerListeIndicateursPesticidesAnneeN(diagnosticApi.pesticides.indicateursParAnnees))
            )
        ),
        new Production(
            territoire,
            new Note(diagnosticApi.production.note),
            diagnosticApi.productionsBesoins.besoinsAssietteActuelle.tauxAdequationBrutPourcent,
            diagnosticApi.productionsBesoins.besoinsAssietteActuelle.tauxAdequationMoyenPonderePourcent,
            diagnosticApi.production.noteTauxAdequationMoyenPondere,
            diagnosticApi.production.notePartSauBio,
            diagnosticApi.production.noteHvn,
            new IndicateurHvn(
                diagnosticApi.pratiquesAgricoles.indicateurHvn.indice1,
                diagnosticApi.pratiquesAgricoles.indicateurHvn.indice2,
                diagnosticApi.pratiquesAgricoles.indicateurHvn.indice3,
                diagnosticApi.pratiquesAgricoles.indicateurHvn.indiceTotal
            ),
            diagnosticApi.surfaceAgricoleUtile.sauBioHa,
            diagnosticApi.pratiquesAgricoles.partSauBioPourcent
        ),
        new TransformationDistribution(
            territoire,
            new Note(diagnosticApi.proximiteCommerces.note),
            diagnosticApi.proximiteCommerces.partPopulationDependanteVoiturePourcent,
            diagnosticApi.proximiteCommerces.partTerritoireDependantVoiturePourcent,
            creerListeIndicateursProximitesCommerces(diagnosticApi.proximiteCommerces.indicateursParTypesCommerces)
        ),
        new Consommation(
            territoire,
            new Note(NOTE_VALEUR_WARNING),
            diagnosticApi.consommation.tauxPauvrete60Pourcent,
            new IndicateurConsommation(
                diagnosticApi.productionsBesoins.besoinsAssietteActuelle.besoinsHa,
                creerListeConsommationParGroupeCulture(
                    diagnosticApi.productionsBesoins.besoinsAssietteActuelle.besoinsParGroupeCulture,
                    diagnosticApi.productionsBesoins.besoinsAssietteActuelle.besoinsHa
                )
            )
        )
    );
}

function creerListeSauParGroupeCulture(listeSauParGroupeCultureApi: SauParGroupeCultureApi[]) {
    return listeSauParGroupeCultureApi.map(
        (sauParGroupeCultureApi) =>
            new SauParGroupeCulture(
                sauParGroupeCultureApi.codeGroupeCulture,
                sauParGroupeCultureApi.nomGroupeCulture,
                sauParGroupeCultureApi.sauHa,
                creerListeSauParCulture(sauParGroupeCultureApi.sauParCulture)
            )
    );
}

function creerListeSauParCulture(listeSauParCultureApi: SauParCultureApi[]) {
    return listeSauParCultureApi.map((sauParCulture) => new SauParCulture(sauParCulture.codeCulture, sauParCulture.nomCulture, sauParCulture.sauHa));
}

function creerListeConsommationParGroupeCulture(
    listeConsommationParGroupeCultureApi: BesoinsParGroupeCultureApi[],
    consommationTotaleTousGroupesCultures: number | null
) {
    return listeConsommationParGroupeCultureApi.map(
        (consommationParGroupeCultureApi) =>
            new ConsommationParGroupeCulture(
                consommationParGroupeCultureApi.codeGroupeCulture,
                consommationParGroupeCultureApi.nomGroupeCulture,
                consommationParGroupeCultureApi.besoinsHa,
                consommationParGroupeCultureApi.tauxAdequationBrutPourcent,
                consommationTotaleTousGroupesCultures,
                creerListeConsommationParCulture(consommationParGroupeCultureApi.besoinsParCulture)
            )
    );
}

function creerListeConsommationParCulture(listeConsommationParCultureApi: BesoinsParCultureApi[]) {
    return listeConsommationParCultureApi.map(
        (consommationParCultureApi) =>
            new ConsommationParCulture(
                consommationParCultureApi.codeCulture,
                consommationParCultureApi.nomCulture,
                consommationParCultureApi.alimentationAnimale,
                consommationParCultureApi.besoinsHa
            )
    );
}

function creerListeCheptels(listeCheptelsApi: CheptelApi[]): Cheptel[] {
    return listeCheptelsApi.map((i) => new Cheptel(i.code, i.tetes, i.estEstime, i.ugb));
}

function creerListePostesEnergie(listePostesEnergieApi: PosteEnergieApi[]): PosteEnergie[] {
    return listePostesEnergieApi.map((i) => {
        return new PosteEnergie(i.code, i.energiePrimaireGJ, i.estEnergieDirecte, i.sources ? creerListeSourcesEnergie(i.sources) : undefined);
    });
}

function creerListeSourcesEnergie(listeSourcesEnergieApi: SourceEnergieApi[]): SourceEnergie[] {
    return listeSourcesEnergieApi.map((i) => {
        return new SourceEnergie(i.code, i.energiePrimaireGJ);
    });
}

function creerListeIrrigationIndicateursAnneeN(
    listeIrrigationIndicateursAnneeNApi: IrrigationIndicateursAnneeNApi[]
): IndicateursParAnnees<IrrigationIndicateursAnneeN> {
    return new IndicateursParAnnees<IrrigationIndicateursAnneeN>(
        listeIrrigationIndicateursAnneeNApi.map((i) => {
            return { annee: i.annee, irrigationM3: i.irrigationM3, irrigationMM: i.irrigationMM };
        })
    );
}

function creerListePratiquesIrrigationParCulture(
    listePratiquesIrrigationParCulturesApi: PratiquesIrrigationCultureApi[]
): PratiquesIrrigationCultureParametres[] {
    return listePratiquesIrrigationParCulturesApi.map((pratiquesIrrigationCultureApi) => {
        return {
            categorieCulture: pratiquesIrrigationCultureApi.categorieCulture,
            culture: pratiquesIrrigationCultureApi.culture,
            sauHa: pratiquesIrrigationCultureApi.sauHa,
            sauIrrigueeHa: pratiquesIrrigationCultureApi.sauIrrigueeHa
        };
    });
}

function creerListeSecheresseIndicateursAnneeMoisN(
    listeSecheresseIndicateursAnneeMoisNApi: ArretesSecheresseAnneeMoisNApi[]
): IndicateursParAnneesMois<SecheresseAnneeMoisN> {
    return new IndicateursParAnneesMois<SecheresseAnneeMoisN>(
        listeSecheresseIndicateursAnneeMoisNApi.map((i) => {
            return {
                anneeMois: new Date(i.anneeMois),
                partTerritoireEnArretePourcent: i.partTerritoireEnArretePourcent
            };
        })
    );
}

function creerListeIndicateursPesticidesAnneeN(listeIndicateursPesticidesAnneeNApi: PesticidesIndicateursAnneeNApi[]): IndicateursPesticidesAnneeN[] {
    return listeIndicateursPesticidesAnneeNApi.map(
        (i) => new IndicateursPesticidesAnneeN(i.annee, i.quantiteSubstanceSansDUKg, i.quantiteSubstanceAvecDUKg, i.noduHa, i.noduNormalise)
    );
}

function creerListeIndicateursProximitesCommerces(
    listeIndicateursParTypeCommerceApi: IndicateursParTypeCommerceApi[]
): IndicateursProximiteCommerces[] {
    return listeIndicateursParTypeCommerceApi
        .filter((i) => CategorieCommerce.listerToutes.find((c) => c.code === i.typeCommerce))
        .map(
            (i) =>
                new IndicateursProximiteCommerces(
                    CategorieCommerce.fromString(i.typeCommerce),
                    i.distancePlusProcheCommerceMetres,
                    i.partPopulationAccesAVeloPourcent,
                    i.partPopulationDependanteVoiturePourcent
                )
        );
}
