import type { TerritoireApi } from '@lga/specification-api';
import { creerDiagnosticApiVide } from '@lga/specification-api/build/outils/modelesDiagnosticsUtils';

import type { DonneesApi } from '../fonctions-utils.js';

export function creerDonneesApiVide(): DonneesApi {
    return {
        diagnosticApi: creerDiagnosticApiVide(),
        territoireApi: creerTerritoire()
    };
}

function creerTerritoire(): TerritoireApi {
    return {
        categorie: 'COMMUNE',
        id: 'mende',
        idCode: 'C-48095',
        idNom: 'mende',
        idTerritoireParcel: '34832',
        nom: 'Mende',
        boundingBox: {
            latitudeMax: 44.58,
            latitudeMin: 44.49,
            longitudeMax: 3.55,
            longitudeMin: 3.43
        },
        genreNombre: 'FEMININ_SINGULIER',
        preposition: 'à',
        departement: {
            categorie: 'DEPARTEMENT',
            id: 'lozere',
            idCode: 'D-48',
            idNom: 'lozere',
            idTerritoireParcel: '110',
            nom: 'Lozère',
            boundingBox: {
                latitudeMax: 44.98,
                latitudeMin: 44.11,
                longitudeMax: 4,
                longitudeMin: 2.98
            },
            genreNombre: 'FEMININ_SINGULIER',
            preposition: 'en'
        },
        epci: {
            categorie: 'EPCI',
            id: 'communaute-de-communes-coeur-de-lozere',
            idCode: 'E-200030435',
            idNom: 'communaute-de-communes-coeur-de-lozere',
            idTerritoireParcel: null,
            nom: 'Communauté de communes Cœur de Lozère',
            sousCategorie: 'COMMUNAUTE_COMMUNES',
            boundingBox: {
                latitudeMax: 44.63,
                latitudeMin: 44.43,
                longitudeMax: 3.64,
                longitudeMin: 3.37
            },
            genreNombre: 'FEMININ_SINGULIER',
            preposition: 'dans'
        },
        pays: {
            categorie: 'PAYS',
            id: 'france',
            idCode: 'P-FR',
            idNom: 'france',
            idTerritoireParcel: '1',
            nom: 'France',
            boundingBox: {
                latitudeMax: 51.09,
                latitudeMin: 41.33,
                longitudeMax: 9.56,
                longitudeMin: -5.14
            },
            genreNombre: 'FEMININ_SINGULIER',
            preposition: 'en'
        },
        region: {
            categorie: 'REGION',
            id: 'occitanie',
            idCode: 'R-76',
            idNom: 'occitanie',
            idTerritoireParcel: '11',
            nom: 'Occitanie',
            boundingBox: {
                latitudeMax: 45.05,
                latitudeMin: 42.33,
                longitudeMax: 4.85,
                longitudeMin: -0.33
            },
            genreNombre: 'FEMININ_SINGULIER',
            preposition: 'en'
        }
    };
}
