export interface Entite {
    id: string;
}

export function differenceListeAMoinsListeB<T extends Entite>(a: T[], b: T[]): T[] {
    return a.filter((e) => !b.map((i) => i.id).includes(e.id));
}

export function deplacerElementEnPositionN<T extends Entite>(liste: T[], id: string, position: number): T[] {
    const listeResultat = liste.slice();
    const index = listeResultat.findIndex((i) => i.id === id);

    if (index !== -1) {
        const [element] = listeResultat.splice(index, 1);
        listeResultat.splice(position - 1, 0, element);
    }

    return listeResultat;
}

export function verifierListeAIncluseDansListeB(listeA: string[], listeB: string[]) {
    return listeA.every((i) => listeB.includes(i));
}
