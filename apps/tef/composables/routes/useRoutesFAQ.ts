import type { RouteLocationRaw } from 'vue-router/auto';

export function useRouteFAQ(): RouteLocationRaw<'faq'> {
    const route: RouteLocationRaw<'faq'> = {
        name: 'faq'
    };
    return route;
}
