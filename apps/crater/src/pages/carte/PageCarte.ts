import './composants/widget-carte/WidgetCarte.js';
import './composants/ListeChoixIndicateursCarte.js';
import './composants/ContenuPanneauSommaireCarte.js';
import '../commun/template/TemplatePageAvecMenuTerritoire.js';
import '@lga/design-system/build/composants/Lien.js';
import '@lga/design-system/build/composants/Legende.js';
import './composants/MenuGlissantMobileDescriptionIndicateurActifCarte';
import './composants/MenuGlissantDesktopChangerIndicateurCarte';
import '../commun/SablierCrater.js';

import type { EvenementSelectionnerTerritoire } from '@lga/commun/build/composants/champ-recherche-territoire/EvenementSelectionnerTerritoire';
import { EvenementErreur } from '@lga/commun/build/evenements/EvenementErreur.js';
import { type DonneesPageSimple, majDonneesPage } from '@lga/commun/build/modeles/pages/donnees-pages.js';
import { EvenementMajDonneesPage } from '@lga/commun/build/modeles/pages/EvenementMajDonneesPage.js';
import { type ItemLegende, Legende } from '@lga/design-system/build/composants/Legende';
import type { OptionsRadioBouton } from '@lga/design-system/build/composants/RadioBouton';
import type { EvenementSelectionner } from '@lga/design-system/build/evenements/EvenementSelectionner';
import { CSS_BREAKPOINT_MAX_WIDTH_DESKTOP_PETIT } from '@lga/design-system/build/styles/styles-breakpoints.js';
import { STYLES_DESIGN_SYSTEM } from '@lga/design-system/build/styles/styles-design-system.js';
import { SA } from '@lga/indicateurs';
import { EchelleTerritoriale } from '@lga/territoires';
import { css, html, LitElement, unsafeCSS } from 'lit';
import { customElement, property, query } from 'lit/decorators.js';
import { unsafeHTML } from 'lit/directives/unsafe-html.js';

import { PAGES_PRINCIPALES } from '../../configuration/pages/declaration-pages.js';
import { construireUrlPageMethodologie } from '../../configuration/pages/pages-utils.js';
import type { EvenementSelectionnerCategorieTerritoire } from '../../evenements/EvenementSelectionnerCategorieTerritoire';
import { EtatCarte, ModeleCarte } from '../../modeles/carte/ModeleCarte.js';
import type { OptionsBarreMenuTerritoires } from '../commun/BarreMenuTerritoires.js';
import { construireLienVersSourcesIndicateur } from '../commun/liens-utils.js';
import { STYLES_CRATER } from '../commun/pages-styles';
import type { EvenementDeplacerCarte } from './composants/widget-carte/EvenementDeplacerCarte';
import type { WidgetCarte } from './composants/widget-carte/WidgetCarte';
import { ControleurCarte } from './ControleurCarte';

declare global {
    interface HTMLElementTagNameMap {
        'c-page-carte': PageCarte;
    }
}

export type DonneesPageCarte = DonneesPageSimple & {
    idIndicateur: string;
    nomIndicateur?: string;
    idEchelleTerritoriale?: string;
    libelleEchelleTerritoriale?: string;
    idTerritoirePrincipal?: string;
};

@customElement('c-page-carte')
export class PageCarte extends LitElement {
    static styles = [
        STYLES_DESIGN_SYSTEM,
        STYLES_CRATER,
        css`
            :host {
                --largeur-legende: 300px;
            }

            main {
                background-color: var(--couleur-blanc);
                display: grid;
                grid-template-columns: 40% 60%;
                height: calc(100vh - var(--hauteur-totale-entete));
            }

            #section-choix-indicateur {
                background-color: var(--couleur-blanc);
                border-right: 1px solid var(--couleur-neutre-clair);
            }

            #description-indicateur-actif {
                padding: calc(2 * var(--dsem));
                background-color: var(--couleur-blanc);
            }

            #description-indicateur-actif h3 {
                margin-top: 0;
            }

            #titre-indicateur-actif {
                display: flex;
                align-items: center;
                justify-content: space-between;
            }

            h2 {
                margin: 0;
            }

            #section-carte {
                position: relative;
                height: 100%;
                z-index: 0;
            }

            #bandeau-haut-carte {
                position: absolute;
                top: 0;
                z-index: 501;
                width: 100%;
                margin: auto;
                padding: calc(2 * var(--dsem));
                text-align: center;
                background-color: rgba(243, 245, 246, 0.9);
            }

            #titre-carte h1 {
                margin: 0;
            }

            #indication-titre-carte {
                margin-bottom: 0;
            }

            #texte-indicateur-actif {
                padding-top: calc(2 * var(--dsem));
            }

            #nom-indicateur-actif {
                padding-right: var(--dsem);
            }

            c-menu-glissant-desktop-changer-indicateur-carte {
                align-self: start;
            }

            c-menu-glissant-mobile-description-indicateur-actif-carte {
                display: none;
            }

            c-legende {
                position: absolute;
                bottom: 0;
                left: 0;
                width: var(--largeur-legende);
                padding: var(--dsem);
                --direction-flex-legende-discrete: column;
                text-shadow:
                    1px 0 var(--couleur-fond),
                    -1px 0 var(--couleur-fond),
                    0 1px var(--couleur-fond),
                    0 -1px var(--couleur-fond);
                z-index: 500;
            }

            #source {
                width: calc(100% - var(--largeur-legende));
                padding: var(--dsem);
                position: absolute;
                bottom: 0;
                right: 0;
                text-align: center;
                z-index: 500;
                -webkit-text-stroke: 0.2em;
                -webkit-text-stroke-color: var(--couleur-fond);
                paint-order: stroke fill;
            }

            @media screen and (max-width: ${unsafeCSS(CSS_BREAKPOINT_MAX_WIDTH_DESKTOP_PETIT + 'px')}) {
                main {
                    display: block;
                    height: calc(100vh - var(--hauteur-totale-entete) - var(--hauteur-menu-pied-page));
                }

                #section-choix-indicateur {
                    display: none;
                }

                c-legende,
                #source {
                    display: none;
                }

                #titre-carte {
                    display: flex;
                    align-items: center;
                    justify-content: center;
                    gap: var(--dsem);
                }

                #indication-titre-carte {
                    display: none;
                }

                c-menu-glissant-mobile-description-indicateur-actif-carte {
                    display: block;
                }
            }
        `
    ];

    private controleurCarte = new ControleurCarte();

    private modeleCarte = new ModeleCarte();

    @property({ attribute: false })
    donneesPage: DonneesPageCarte = {
        idIndicateur: SA.indicateursCarte[0].id
    };

    @query('c-carte') private carte?: WidgetCarte;

    willUpdate() {
        this.verifierDonneesPage();
        if (this.estNecessaireMajCarte(this.modeleCarte, this.donneesPage)) {
            this.modeleCarte.setEtatVide();
            this.modeleCarte.majIndicateurActif(this.donneesPage.idIndicateur);
            if (this.donneesPage.idTerritoirePrincipal) {
                this.controleurCarte
                    .chargerTerritoirePrincipal(this.donneesPage.idTerritoirePrincipal, this.modeleCarte)
                    .then((domaineCarte) => {
                        this.modeleCarte = domaineCarte;
                        this.positionnerEtatCartePrete(
                            this.modeleCarte.getIndicateurActif().libelle,
                            undefined,
                            this.modeleCarte.getEchelleTerritorialeActive().libelle
                        );
                    })
                    .catch((e: Error) => {
                        this.actionErreur('CARTE-01', 'ERREUR lors du chargement de la carte', e);
                    });
            } else {
                // param echelleTerritoriale pris en compte uniquement si pas de territoire dans l'url
                this.modeleCarte.majEchelleTerritorialeActive(this.donneesPage.idEchelleTerritoriale!);
                this.positionnerEtatCartePrete(
                    this.modeleCarte.getIndicateurActif().libelle,
                    this.modeleCarte.getEchelleTerritorialeActive().id,
                    this.modeleCarte.getEchelleTerritorialeActive().libelle
                );
            }
        }
    }

    private positionnerEtatCartePrete(nomIndicateur: string, idEchelleTerritoriale: string | undefined, libelleEchelleTerritoriale: string) {
        this.modeleCarte.setEtatPret();
        this.donneesPage = majDonneesPage(this.donneesPage, {
            nomIndicateur: nomIndicateur,
            idEchelleTerritoriale: idEchelleTerritoriale,
            libelleEchelleTerritoriale: libelleEchelleTerritoriale
        });
        this.dispatchEvent(new EvenementMajDonneesPage(this.donneesPage));
    }

    render() {
        if (this.modeleCarte.getEtat() === EtatCarte.PRET) {
            const indicateurActif = this.modeleCarte.getIndicateurActif();

            return html`
                <c-template-page-avec-menu-territoire
                    idItemActifMenuPrincipal=${PAGES_PRINCIPALES.carte.getId()}
                    .optionsBarreMenuTerritoires=${this.calculerOptionsBarreMenuTerritoires()}
                    @selectionnerCategorieTerritoire=${this.actionModifierEchelleTerritorialeCarte}
                    @selectionnerTerritoire=${this.actionNouveauTerritoire}
                >
                    <main slot="contenu">
                        <section id="section-choix-indicateur">
                            <div id="description-indicateur-actif">
                                <div id="titre-indicateur-actif">
                                    <h2 id="nom-indicateur-actif" class="texte-moyen gras">
                                        Carte des ${this.modeleCarte.getEchelleTerritorialeActive().libelle}<br />${indicateurActif.libelle}
                                    </h2>
                                    <c-menu-glissant-desktop-changer-indicateur-carte
                                        .idIndicateurActif=${this.modeleCarte.getIndicateurActif().id}
                                        @selectionner=${this.actionModifierIndicateurActif}
                                    ></c-menu-glissant-desktop-changer-indicateur-carte>
                                </div>
                                <div id="texte-indicateur-actif">
                                    <p class="texte-moyen">${unsafeHTML(indicateurActif.description)}</p>
                                    <c-lien
                                        class="texte-moyen"
                                        href=${construireUrlPageMethodologie(indicateurActif.idMaillon, indicateurActif.idDomaine)}
                                        >→ voir la méthodologie</c-lien
                                    >
                                </div>
                            </div>
                        </section>
                        <section id="section-carte">
                            <div id="bandeau-haut-carte">
                                <div id="titre-carte">
                                    <h1 class="texte-titre">
                                        ${this.modeleCarte.getIndicateurActif().libelle +
                                        (this.modeleCarte.getIndicateurActif().unite ? ' [' + this.modeleCarte.getIndicateurActif().unite + ']' : '')}
                                    </h1>
                                    <c-menu-glissant-mobile-description-indicateur-actif-carte
                                        .indicateurCarteActif=${this.modeleCarte.getIndicateurActif()}
                                    ></c-menu-glissant-mobile-description-indicateur-actif-carte>
                                </div>
                                <p id="indication-titre-carte" class="texte-petit">Cliquez sur un territoire pour aller sur son diagnostic</p>
                            </div>
                            <c-carte
                                autoriserZoom
                                autoriserMouvements
                                @deplacerCarte=${(e: Event) => {
                                    this.actionMouvementCarte((e as EvenementDeplacerCarte).detail.zoom);
                                }}
                            ></c-carte>
                            <c-legende
                                .items=${this.modeleCarte.getIndicateurActif().styleCarte?.seuilsLegende?.map(
                                    (seuil) =>
                                        ({
                                            couleur: seuil.couleur,
                                            libelle: seuil.libelle
                                        }) as ItemLegende
                                )}
                                type=${this.modeleCarte.getIndicateurActif().styleCarte?.estIndicateurContinu
                                    ? Legende.TYPE_LEGENDE.continue
                                    : Legende.TYPE_LEGENDE.discrete}
                            ></c-legende>
                            <p id="source" class="texte-petit">
                                ${this.modeleCarte.getIndicateurActif()
                                    ? construireLienVersSourcesIndicateur(this.modeleCarte.getIndicateurActif())
                                    : html`Source : <c-lien href="https://resiliencealimentaire.org">Les Greniers d'Abondance</c-lien>`}
                            </p>
                        </section>
                    </main>
                    <c-contenu-panneau-sommaire-carte
                        slot="contenu-sommaire"
                        .idIndicateurActif=${this.modeleCarte.getIndicateurActif().id}
                        .optionsBoutonsTerritoires=${{
                            boutons: EchelleTerritoriale.listerToutes.map((echelle: EchelleTerritoriale) => ({
                                id: echelle.id,
                                libelle: echelle.libelle
                            })),
                            idBoutonActif: this.modeleCarte.getEchelleTerritorialeActive().id
                        } as OptionsRadioBouton}
                        @selectionner=${this.actionModifierIndicateurActif}
                        @selectionnerCategorieTerritoire=${this.actionModifierEchelleTerritorialeCarte}
                        @selectionnerTerritoire=${this.actionNouveauTerritoire}
                    ></c-contenu-panneau-sommaire-carte>
                </c-template-page-avec-menu-territoire>
            `;
        } else {
            return html`<c-sablier-crater></c-sablier-crater>`;
        }
    }

    private calculerOptionsBarreMenuTerritoires() {
        return {
            onglets: EchelleTerritoriale.listerToutes.map((echelle: EchelleTerritoriale) => ({
                id: echelle.id,
                libelle: echelle.libelle
            })),
            idOngletSelectionne: this.modeleCarte.getEchelleTerritorialeActive().id,
            filtrerRecherchePourCarte: true
        } as OptionsBarreMenuTerritoires;
    }

    updated() {
        if (this.modeleCarte.getEtat() === EtatCarte.PRET) {
            this.carte?.maj(
                this.modeleCarte.getEchelleTerritorialeActive(),
                this.modeleCarte.getIndicateurActif(),
                this.modeleCarte.getIdTerritoireZoom(),
                this.modeleCarte.getIdDepartementInitialCommune()
            );
            this.modeleCarte.desactiverZoomAutoTerritoire();
        }
    }

    private actionModifierIndicateurActif = (e: Event) => {
        const idIndicateur = (e as EvenementSelectionner).detail.idItemSelectionne;
        this.donneesPage = majDonneesPage(this.donneesPage, {
            idIndicateur: idIndicateur,
            nomIndicateur: this.modeleCarte.getIndicateurActif().libelle,
            idEchelleTerritoriale: this.modeleCarte.getEchelleTerritorialeActive().id,
            libelleEchelleTerritoriale: this.modeleCarte.getEchelleTerritorialeActive().libelle,
            idTerritoirePrincipal: undefined
        });
        this.dispatchEvent(new EvenementMajDonneesPage(this.donneesPage));
    };

    private actionModifierEchelleTerritorialeCarte = (e: Event) => {
        const idEchelleTerritoriale = (e as EvenementSelectionnerCategorieTerritoire).detail.codeCategorieTerritoire;
        this.modifierEchelleTerritoriale(idEchelleTerritoriale);
    };

    private modifierEchelleTerritoriale(idEchelleTerritoriale: string) {
        this.donneesPage = majDonneesPage(this.donneesPage, {
            idEchelleTerritoriale: idEchelleTerritoriale,
            libelleEchelleTerritoriale: this.modeleCarte.getEchelleTerritorialeActive().libelle,
            idTerritoirePrincipal: undefined
        });
        this.dispatchEvent(new EvenementMajDonneesPage(this.donneesPage));
    }

    private actionMouvementCarte(zoom: number) {
        if (this.modeleCarte.getEchelleTerritorialeActive().id === EchelleTerritoriale.Communes.id) {
            if (zoom < EchelleTerritoriale.Communes.minZoom) {
                this.modifierEchelleTerritoriale(EchelleTerritoriale.Epcis.id);
            }
        }
    }

    private actionNouveauTerritoire = (e: Event) => {
        this.donneesPage = majDonneesPage(this.donneesPage, {
            idTerritoirePrincipal: (e as EvenementSelectionnerTerritoire).detail.idTerritoireCrater
        });
        // Pas de dispatchEvent car il est fait dans willUpdate
    };

    private actionErreur = (codeErreur: string, messageErreur: string, erreur?: Error) => {
        console.warn(`Erreur : ${codeErreur} ${messageErreur}`, erreur);
        this.dispatchEvent(new EvenementErreur(codeErreur, messageErreur));
    };

    private estNecessaireMajCarte(domaineCarte: ModeleCarte, donneesPage: DonneesPageCarte): boolean {
        const estModifieIdIndicateur = donneesPage.idIndicateur !== domaineCarte.getIndicateurActif().id;

        const estModifieIdTerritoirePrincipal =
            donneesPage.idTerritoirePrincipal !== undefined && donneesPage.idTerritoirePrincipal !== domaineCarte.getIdTerritoirePrincipal();

        const estModifieeEchelleTerritoriale =
            donneesPage.idTerritoirePrincipal === undefined && donneesPage.idEchelleTerritoriale !== domaineCarte.getEchelleTerritorialeActive().id;
        return (
            domaineCarte.getEtat() === EtatCarte.VIDE ||
            (domaineCarte.getEtat() === EtatCarte.PRET &&
                (estModifieIdIndicateur || estModifieIdTerritoirePrincipal || estModifieeEchelleTerritoriale))
        );
    }

    private verifierDonneesPage() {
        if (!SA.getIndicateurCarte(this.donneesPage.idIndicateur)) {
            this.dispatchEvent(
                new EvenementErreur('CARTE-02', `Erreur dans l'adresse saisie, le nom de l'indicateur est absent ou inconnu: ${location.toString()}`)
            );
        } else if (
            this.donneesPage.idEchelleTerritoriale !== undefined &&
            EchelleTerritoriale.fromId(this.donneesPage.idEchelleTerritoriale) === undefined
        ) {
            this.dispatchEvent(
                new EvenementErreur(
                    'CARTE-03',
                    `Erreur dans l'adresse saisie, la valeur du paramètre echelleTerritoriale est incorrecte : ${location.toString()}`
                )
            );
        }
    }
}
