import { onMounted, onUnmounted, type Ref, ref } from 'vue';

export function useLargeurDevice(): Ref<number> | undefined {
    if (typeof window === 'undefined') return undefined; // Cas d'un render coté serveur

    const largeur = ref(window.innerWidth);

    const majLargeur = () => {
        largeur.value = window.innerWidth;
    };

    onMounted(() => {
        window.addEventListener('resize', majLargeur);
    });

    onUnmounted(() => {
        window.removeEventListener('resize', majLargeur);
    });

    return largeur;
}
