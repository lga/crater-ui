import { LitElement } from 'lit';
import { html } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { querySelectorDeep } from 'query-selector-shadow-dom';

import { EvenementEntrerZoneScroll } from './EvenementEntrerZoneScroll';
import { EvenementSortirZoneScroll } from './EvenementSortirZoneScroll';

declare global {
    interface HTMLElementTagNameMap {
        'c-element-scrollable': ElementScrollable;
    }
}

@customElement('c-element-scrollable')
export class ElementScrollable extends LitElement {
    @property({ type: Number })
    seuilBasPourcent = 50;

    @property({ type: Number })
    seuilHautPourcent = 50;

    @property()
    querySelectorConteneurScroll = '';

    render() {
        return html`<slot name="contenu"></slot>`;
    }

    firstUpdated() {
        const intersectionObserverRootMargin = '-' + this.seuilHautPourcent + '% 0px -' + this.seuilBasPourcent + '% 0px';

        const callbackEntrerZoneScroll = (): void => {
            this.dispatchEvent(new EvenementEntrerZoneScroll(this.id));
        };
        const callbackSortirZoneScroll = () => {
            this.dispatchEvent(new EvenementSortirZoneScroll(this.id));
        };

        new IntersectionObserver(
            (entries) => {
                entries.forEach((entry) => {
                    if (entry.isIntersecting) {
                        callbackEntrerZoneScroll();
                    } else {
                        callbackSortirZoneScroll();
                    }
                });
            },
            {
                root: this.querySelectorConteneurScroll ? querySelectorDeep(this.querySelectorConteneurScroll) : null,
                rootMargin: intersectionObserverRootMargin,
                threshold: 0
            }
        ).observe(this);
    }
}
