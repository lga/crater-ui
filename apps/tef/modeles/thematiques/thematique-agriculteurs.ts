import { calculerDonneesEvolutionPopulationAgricole, type EvolutionAgriculteurs } from '../constats/constats-agriculteurs.js';
import type { DonneesApi } from '../fonctions-utils.js';
import type { ThematiqueModele } from './ThematiqueModele.js';

export interface ThematiqueAgriculteursModele extends ThematiqueModele {
    evolutionAgriculteurs: EvolutionAgriculteurs;
}

export function creerThematiqueAgriculteurs(donneesApi: DonneesApi): ThematiqueAgriculteursModele {
    const donneesPopulationAgricole = calculerDonneesEvolutionPopulationAgricole(
        donneesApi.diagnosticApi.populationAgricole.populationAgricole1988 ?? 0,
        donneesApi.diagnosticApi.populationAgricole.populationAgricole2010 ?? 0
    );

    return {
        idTheme: 'agriculteurs',
        evolutionAgriculteurs: {
            annees: donneesPopulationAgricole.annees,
            nombreAgriculteurs: donneesPopulationAgricole.nombreAgriculteurs
        }
    };
}
