import { describe, expect, it } from 'vitest';

import type { CodeProfil } from './Codes';
import type { TagProfil } from './Tags';
import { convertirTagsEnCodes } from './tags-utils';

describe('Test des fonctions de manipulation des Tags', () => {
    it('Test convertirTagsEnCodes', () => {
        const tags: TagProfil[] = ['COMMUNE_TOUS', 'ELEVAGE_EXTENSIF'];
        const codesAttendus: CodeProfil[] = [
            'GRAND_CENTRE_URBAIN',
            'CENTRE_URBAIN_INTERMEDIAIRE',
            'PETITE_VILLE',
            'CEINTURE_URBAINE',
            'BOURG_RURAL',
            'COMMUNE_A_HABITAT_DISPERSE',
            'COMMUNE_A_HABITAT_TRES_DISPERSE',
            'ELEVAGE_EXTENSIF'
        ];
        expect(convertirTagsEnCodes(tags)).toEqual(codesAttendus);
    });
});
