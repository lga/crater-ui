import { type HtmlString, htmlstring, rendreInitialeMajuscule } from '@lga/base';
import { IDS_MAILLONS } from '@lga/indicateurs';
import { SA } from '@lga/indicateurs';
import { CategorieTerritoire, type Territoire } from '@lga/territoires';

import { PAGE_METHODOLOGIE_HASH_OTEX, PAGES_METHODOLOGIE } from '../../configuration/pages/declaration-pages';
import type { Note } from './Note';
import { LIBELLE_OTEX_NON_DEFINIE_OU_NON_MAJORITAIRE } from './Otex.js';

export function calculerMessageTerritoire(
    categorieTerritoire: CategorieTerritoire,
    densitePopulationHabParKM2: number | null,
    pourcentageSuperficieAgricole: number | null,
    libelleOtexMajoritaire5Postes: string
): string {
    if (densitePopulationHabParKM2 === null || pourcentageSuperficieAgricole === null) {
        return '';
    }
    let messageDensite = '';
    if (densitePopulationHabParKM2 < 100) {
        messageDensite = ' peu dense';
    } else if (densitePopulationHabParKM2 < 200) {
        messageDensite = ' de densité moyenne';
    } else {
        if ([CategorieTerritoire.Commune, CategorieTerritoire.Epci, CategorieTerritoire.Region].includes(categorieTerritoire)) {
            messageDensite = ' densément peuplée';
        } else {
            messageDensite = ' densément peuplé';
        }
    }

    let messageSau = '';
    if (pourcentageSuperficieAgricole < 15) {
        messageSau = ' avec une faible part de surface agricole';
    } else if (pourcentageSuperficieAgricole < 50) {
        messageSau = ' avec une part minoritaire de surface agricole';
    } else {
        messageSau = ' avec une part majoritaire de surface agricole';
    }

    let messageOtex;
    if (libelleOtexMajoritaire5Postes !== LIBELLE_OTEX_NON_DEFINIE_OU_NON_MAJORITAIRE) {
        messageOtex = ` et une spécialisation agricole dominante '${libelleOtexMajoritaire5Postes}'`;
    } else {
        messageOtex = ` et aucune spécialisation agricole dominante`;
    }

    return rendreInitialeMajuscule(categorieTerritoire.libelleCategorie) + messageDensite + messageSau + messageOtex + '.';
}

export function calculerMessageOtex(territoire: Territoire, libelleOtexMajoritaire5Postes: string): HtmlString {
    let motTerritoire = '';
    if (territoire.categorie === CategorieTerritoire.Commune) {
        motTerritoire = 'la commune';
    } else {
        motTerritoire = 'le territoire';
    }

    const definitionSpecialisation = htmlstring`<c-lien
        href="${PAGES_METHODOLOGIE.presentationGenerale.getUrl({ idElementCibleScroll: PAGE_METHODOLOGIE_HASH_OTEX })}"
        >spécialisation dominante de la production agricole</c-lien
    >`;

    let specialisation;
    if (libelleOtexMajoritaire5Postes !== LIBELLE_OTEX_NON_DEFINIE_OU_NON_MAJORITAIRE) {
        specialisation = htmlstring`La ${definitionSpecialisation} dans ${motTerritoire} est ${libelleOtexMajoritaire5Postes}`;
    } else {
        specialisation = htmlstring`Il n'y a pas de ${definitionSpecialisation} dans ${motTerritoire}`;
    }
    const complementTerritoireSupracommunal =
        ' Celle-ci est déterminée selon la répartition des spécialisations des communes appartenant au territoire (OTEX 5 postes partagée par une majorité de communes) :';
    return htmlstring`${specialisation}.${territoire.categorie !== CategorieTerritoire.Commune ? complementTerritoireSupracommunal : ''}`;
}

export function calculerMessageMetaDescriptionDiagnostic(
    nomTerritoire: string,
    noteTerresAgricoles: Note,
    noteAgriculteursExploitations: Note,
    noteIntrants: Note,
    noteProduction: Note,
    noteTransformationDistribution: Note,
    noteConsommation: Note
): string {
    interface DictMaillonNote {
        idMaillon: string;
        note: Note;
    }
    const maillonsNotes: DictMaillonNote[] = [
        { idMaillon: IDS_MAILLONS.terresAgricoles, note: noteTerresAgricoles },
        { idMaillon: IDS_MAILLONS.agriculteursExploitations, note: noteAgriculteursExploitations },
        { idMaillon: IDS_MAILLONS.intrants, note: noteIntrants },
        { idMaillon: IDS_MAILLONS.production, note: noteProduction },
        { idMaillon: IDS_MAILLONS.transformationDistribution, note: noteTransformationDistribution },
        { idMaillon: IDS_MAILLONS.consommation, note: noteConsommation }
    ];
    interface DictMaillonAvecValeurNumerique {
        idMaillon: string;
        note: number;
    }
    const noteMaillonsAvecValeurNumerique: DictMaillonAvecValeurNumerique[] = maillonsNotes
        .filter((maillon) => Number.isFinite(maillon.note.valeur))
        .map((m) => {
            return { idMaillon: m.idMaillon, note: m.note.valeur };
        }) as DictMaillonAvecValeurNumerique[];
    if (noteMaillonsAvecValeurNumerique.length === 0) {
        return `Évaluation pour ${nomTerritoire} : aucun score disponible`;
    } else {
        return `Évaluation pour ${nomTerritoire} : ${noteMaillonsAvecValeurNumerique
            .map((e) => `Score ${SA.getMaillon(e.idMaillon)?.nom}=${e.note}/10`)
            .join(', ')}`;
    }
}
